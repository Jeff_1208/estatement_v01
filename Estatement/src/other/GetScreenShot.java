package other;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.utils.Common;

public class GetScreenShot {

	String[] sheetNames= {"Login","Card","Payment","Maintenance","GoSave"};
	private ArrayList<String> TestCaseList= new ArrayList<>();

	public static void main(String[] args) {
		//String Folder_path="src/resources";
		String Folder_path="D:\\Project\\Program\\WorkSpaceIDEA\\Automation\\automation-test\\TestReport\\2022-02-28-18-13-49\\screenshots\\out";
		GetScreenShot getScreenShot = new GetScreenShot();
		getScreenShot.create_result_report(Folder_path,"ANDROID");
		System.out.println("Runfinish");

	}
	public void generateExcelReport(String Folder_path,String flag) {
		GetScreenShot getScreenShot = new GetScreenShot();
		getScreenShot.create_result_report(Folder_path,flag.toUpperCase());
	}


	public XSSFWorkbook create_result_report(String folder_path,String flag) {
		String filename = "D:"+"//55Result_"+Common.get_date_string()+".xlsx";
		//创建工作薄对象
		FileOutputStream out;
		XSSFWorkbook workbook = null;
		try {
			workbook=new XSSFWorkbook();//这里也可以设置sheet的Name
			XSSFSheet sheet0 = workbook.createSheet();
			workbook.setSheetName(0,"Result");
			//创建工作表的第一行
			XSSFRow row = sheet0.createRow(0);//设置第一行，从零开始
			XSSFCell c = row.createCell(0);
			c.setCellValue(flag);
			row.createCell(1).setCellValue(Common.get_date_string());
			for(int i =0;i<sheetNames.length;i++) {
				workbook.createSheet();
				workbook.setSheetName(i+1,sheetNames[i]);
			}

			List<String> ScreenShotList = getScreenShotAbsolutePathList(folder_path,flag);
			WritePictureToExcel(ScreenShotList,workbook,flag);

			out = new FileOutputStream(filename);
			workbook.write(out);
			out.flush();
			out.close();

		} catch (Exception e) {
		}
		return workbook;
	}

	public List<String> getScreenShotAbsolutePathList(String Folder_path,String flag) {
		List<String> ScreenShotList = new ArrayList<>();
		File file = new File(Folder_path);
		File[] fs = file.listFiles();
		for(File f:fs) {
			if(f.getName().toUpperCase().contains(flag.toUpperCase())) {
				ScreenShotList.add(f.getAbsolutePath());
			}
		}
		return ScreenShotList;
	}


	public void WritePictureToExcel(List<String> filePaths,XSSFWorkbook workbook,String flag) {
		for(int i =0;i<sheetNames.length;i++) {
			String sheetName =sheetNames[i];
			//System.out.println("sheetName: "+sheetName);
			int count=0;
			for(String s:filePaths) {
				XSSFSheet sheet =workbook.getSheet(sheetName);
				sheet.setColumnWidth(0, 5000);  //设置第一列的宽度
				String file_name = s.substring(s.lastIndexOf("\\")+1);
				if(file_name.toUpperCase().contains(sheetName.toUpperCase())) {
					mergeCellForTestCaseName(sheet,file_name,flag);
					PictureWriteToExcel(s,workbook,sheet,count);
					count+=1;
				}
			}
		}
	}

	public int getTestCaseIndex(String file_name) {
		String[] ss = file_name.split("_");
		return Integer.parseInt(ss[2]);
	}

	public String getTestCaseName(String file_name) {
		String[] ss = file_name.split("_");
		return ss[0]+"_"+ss[1]+"_"+ss[2];
	}
	
    public String getTestCaseName(String file_name,String flag) {
        int index = file_name.toUpperCase().indexOf(flag.toUpperCase())-1;
        return file_name.substring(0,index);
    }

	public XSSFSheet mergeCellForTestCaseName(XSSFSheet sheet,String file_name,String flag){ 
		
        String testCaseName =getTestCaseName(file_name,flag);
        if(TestCaseList.contains(testCaseName)){
            return sheet;
        }
        TestCaseList.add(testCaseName);
        
		// 合并单元格占30行，是0到29行(4个参数，分别为起始行，结束行，起始列，结束列)
		// 行和列都是从0开始计数，且起始结束都会合并
		// 这里是合并excel中的30行为一行
        int index =getTestCaseIndex(file_name);
        int start_row = (index-1)*30;
        int end_row = (index-1)*30+29;
        CellRangeAddress region = new CellRangeAddress(start_row, end_row, 0, 0);
        XSSFCell c=sheet.createRow(start_row).createCell(0);
        c.setCellValue(testCaseName);
        sheet.addMergedRegion(region);
        return sheet;
	}

	public XSSFSheet mergeCell(XSSFSheet sheet,String file_name){ 
		for (int i = 0; i < 15; i++) {
			// 合并单元格占30行，是0到29行(4个参数，分别为起始行，结束行，起始列，结束列)
			// 行和列都是从0开始计数，且起始结束都会合并
			// 这里是合并excel中的30行为一行
			int start_row = i*30;
			int end_row = i*30+29;
			CellRangeAddress region = new CellRangeAddress(start_row, end_row, 0, 0);
			XSSFCell c=sheet.createRow(start_row).createCell(0);
			c.setCellValue("Hello World");
			//System.out.println("start_row: "+start_row+"	end_row: "+end_row);
			sheet.addMergedRegion(region);
		}
		return sheet;
	}

	public static int getRowIndex(String s) {
		s = s.substring(s.lastIndexOf("\\")+1).toUpperCase();
		String flag =getPictureFlag(s);
		int index_end=s.indexOf(flag.toUpperCase());
		String v = s.substring(0,index_end);
		v = v.substring(v.lastIndexOf("_")+1);
		int value = Integer.parseInt(v)-1;
		return value;
	}

	public static int getColumnIndex(String s) {
		s = s.substring(s.lastIndexOf("\\")+1).toUpperCase();
		String flag =getPictureFlag(s);
		int index_end=s.indexOf(flag.toUpperCase());
		String v = s.substring(index_end+flag.length());
		v = v.substring(0,v.indexOf(".PNG"));
		int value = Integer.parseInt(v)-1;
		return value;
	}

	public static String getPictureFlag(String s) {
		if(s.toUpperCase().contains("ANDROID")) {
			return "_ANDROID_";
		}else if(s.toUpperCase().contains("IOS")){
			return "_IOS_";
		}else {
			return "_ANDROID_";
		}
	}

	public  void PictureWriteToExcel(String picture_path,XSSFWorkbook wb,XSSFSheet sheet,int count) {

		int row_index =getRowIndex(picture_path);
		int column_index =getColumnIndex(picture_path);
		//System.out.println("start to write: "+picture_path);
		//System.out.println("row_index: "+row_index+"	column_index:"+column_index);
		FileOutputStream fileOut = null;  
		BufferedImage bufferImg = null;//图片
		try {  
			// 先把读进来的图片放到一个ByteArrayOutputStream中，以便产生ByteArray  
			ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();  
			//将图片读到BufferedImage  
			bufferImg = ImageIO.read(new File(picture_path));  
			// 将图片写入流中  
			ImageIO.write(bufferImg, "png", byteArrayOut);  
			// 创建一个工作薄  
			//XSSFWorkbook wb = new XSSFWorkbook();  
			//创建一个sheet  
			//XSSFSheet sheet = wb.createSheet("out put excel");  
			// 利用HSSFPatriarch将图片写入EXCEL  
			//HSSFPatriarch patriarch = sheet.createDrawingPatriarch();  
			XSSFDrawing patriarch = sheet.createDrawingPatriarch();  
			/**
			 * 该构造函数有8个参数
			 * 前四个参数是控制图片在单元格的位置，分别是图片距离单元格left，top，right，bottom的像素距离
			 * 后四个参数，前两个表示图片左上角所在的cellNum和 rowNum，后两个参数对应的表示图片右下角所在的cellNum和 rowNum，
			 * excel中的cellNum和rowNum的index都是从0开始的
			 *  
			 */  
			//图片一导出到单元格中  
			//			XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0,  
			//					(short) 0, 0, (short) 6, 12);  

			XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0,  
					(short) column_index*5+1, row_index*30, (short) column_index*5+4+1, row_index*30+27);  

			// 插入图片  
			patriarch.createPicture(anchor, wb.addPicture(byteArrayOut  
					.toByteArray(), XSSFWorkbook.PICTURE_TYPE_JPEG));  
			//生成的excel文件地址
			//fileOut = new FileOutputStream("D:\\123.xlsx");  
			// 写入excel文件  
			//wb.write(fileOut);  
		} catch (IOException io) {  
			io.printStackTrace();  
			System.out.println("io erorr : " + io.getMessage());  
		} finally {  
			if (fileOut != null) {  
				try {  
					fileOut.close();  
				} catch (IOException e) {  
					e.printStackTrace();  
				}  
			}  
		}  
	}
}
