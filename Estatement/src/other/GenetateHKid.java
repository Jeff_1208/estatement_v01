package other;

import java.util.Random;

public class GenetateHKid {
    public static String generateRandomHKID() {
        Random random = new Random();
 
        // 前8位为日期码，可以生成从1960年1月1日到2032年12月31日之间的随机日期
        String dateCode = String.format("%d%02d%02d", 
                1960 + random.nextInt(72), // 1960到1932
                random.nextInt(12) + 1,     // 1到12月
                random.nextInt(31) + 1);    // 1到31日
 
        // 接下来的3位为序列码，随机生成001到999之间的数
        String sequenceCode = String.format("%03d", random.nextInt(999) + 1);
 
        // 最后一位为校验码，这部分逻辑比较复杂，需要参考港澳身份证的规则来计算
        String lastCode = calculateChecksum(dateCode + sequenceCode);
 
        return dateCode + sequenceCode + lastCode;
    }
 
    private static String calculateChecksum(String id) {
    	
    	System.out.println("id: "+id);
        int[] weights = {9, 8, 7, 6, 5, 4, 3, 2}; // 对应前8位的权重
        int sum = 0;
        for (int i = 0; i < id.length() - 1; i++) {
        	System.out.println(i);
            sum += (id.charAt(i) - '0') * weights[i];
        }
        sum = sum % 11;
        String lastCode;
        switch (sum) {
            case 10:
                lastCode = "A";
                break;
            case 0:
                lastCode = "0";
                break;
            default:
                lastCode = String.valueOf(sum);
        }
        return lastCode;
    }
 
    public static void main(String[] args) {
        String hkid = generateRandomHKID();
        System.out.println("Random HKID: " + hkid);
    }
}
