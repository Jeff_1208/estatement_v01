package other;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class CompressPicture {



	public static void main(String[] args) throws IOException {

		CompressPicture resize = new CompressPicture();

		//压缩该文件夹下所有的图片，最后一个参数可调节大小，0.2表示文件大小变成原来的0.2倍
		resize.changeByDir("D:/image", 0, 0, 0.2);
		
		// 把文件都都压缩成相同的大小  50000表示50K大小的图片
		//resize.changeByDirToSameSize("D:/image", 0, 0, 50000);

		//针对单个文件进行压缩，压缩比例0.5表示长和宽变成原来的0.5，大小变成原来的4分之1
		//resize.reSizeJpegByproportion("D:/image/Reg_GoSave_001_Android_01.png", "D:/image/2.jpg", 0.5);
		String filepath="src/resources/Reg_GoSave_001_Android_01.png";
		System.out.println("finish");
		System.out.println(getFileSize(filepath));
		File fromF = new File(filepath);
		BufferedImage bi = ImageIO.read(fromF);
		System.out.println("bi.getWidth():"+bi.getWidth());
		System.out.println("bi.getHeight():"+bi.getHeight());

	}
	
    public static long getFileSize(String filename) {
        File file = new File(filename);
        if (!file.exists() || !file.isFile()) {
            System.out.println("文件不存在");
            return -1;
        }
        return file.length();
    }
    
    public double getSizePercent(String filename,long targetFileSize) {
    	double a= (double)targetFileSize/getFileSize(filename);
    	
    	double n=Math.sqrt(a);
    	return a;
    }
    

	public void changeByDir(String dirPath, int width, int height){
		changeByDir(dirPath, width, height, -1);
	}

	public void changeByDir(String dirPath, double proportion){
		changeByDir(dirPath, 0, 0, proportion);
	}

	/**
	 * @param dirPath
	 * @param width
	 * @param height
	 * @param proportion
	 */

	public String changeByDir(String dirPath, int width, int height,double proportion) {
		File dir = new File(dirPath);
		if (!dir.isDirectory()) {
			System.err.println("is not a dir");
			return "";
		}

		File[] fileList = dir.listFiles();
		String outputFileDir = dir.getAbsolutePath() + "/out/";
		File outputFile = new File(outputFileDir);
		if (!outputFile.exists() && !outputFile.isDirectory()) {
			outputFile.mkdir();
		}

		boolean isProportion = false;
		if (proportion != -1) {
			isProportion = true;
		}

		for (int i = 0; i <fileList.length;i++) {
			File file = fileList[i];
			if (file.getName().endsWith(".png")) {
				if (isProportion) {
					reSizeByproportion(file.getAbsolutePath(), outputFileDir+ file.getName(), proportion);
				} else {
					reSizePicture(file.getAbsolutePath(),outputFileDir + file.getName(), width, height);
				}
			}else if(file.getName().endsWith("jpg")){
				if (isProportion) {
					reSizeJpegByproportion(file.getAbsolutePath(),outputFileDir + file.getName(), proportion);
				} else {
					reSizeJepgPicture(file.getAbsolutePath(),outputFileDir + file.getName(), width, height);
				}
			}
		}
		return dir.getAbsolutePath()+"\\out";
	}
	
	/**
	 * @param dirPath
	 * @param width
	 * @param height
	 * @param filesize   把文件都都压缩成相同的大小  50000表示50K大小的图片
	 */
	public void changeByDirToSameSize(String dirPath, int width, int height,long filesize) {
		double proportion=1.0;//缩放比例，这个时初始值，后面需要计算
		
		File dir = new File(dirPath);
		if (!dir.isDirectory()) {
			System.err.println("is not a dir");
			return;
		}

		File[] fileList = dir.listFiles();
		String outputFileDir = dir.getAbsolutePath() + "/out/";
		File outputFile = new File(outputFileDir);
		if (!outputFile.exists() && !outputFile.isDirectory()) {
			outputFile.mkdir();
		}

		boolean isProportion = false;
		if (proportion != -1) {
			isProportion = true;
		}

		for (int i = 0; i <fileList.length;i++) {
			File file = fileList[i];
			if (file.getName().endsWith(".png")) {
				if (isProportion) {
					proportion=getSizePercent(file.getAbsolutePath(), filesize);
					System.out.println("proportion: "+proportion);
					reSizeByproportion(file.getAbsolutePath(), outputFileDir+ file.getName(), proportion);
				} else {
					reSizePicture(file.getAbsolutePath(),outputFileDir + file.getName(), width, height);
				}
			}else if(file.getName().endsWith("jpg")){
				if (isProportion) {
					System.out.println("proportion: "+proportion);
					reSizeJpegByproportion(file.getAbsolutePath(),outputFileDir + file.getName(), proportion);
				} else {
					reSizeJepgPicture(file.getAbsolutePath(),outputFileDir + file.getName(), width, height);
				}
			}
		}
	}
	

	public void reSizeByproportion(String fromFile, String toFile,double proportion) {
		try {
			File fromF = new File(fromFile);
			BufferedImage bi = ImageIO.read(fromF);
			int newWidth = 0, newHeight = 0;
			newWidth = (int) (((double) bi.getWidth()) * proportion);
			newHeight = (int) (((double) bi.getHeight()) * proportion);
			System.out.println(fromFile+"\t"+"newWidth"+newWidth+"\t"+"newHeight"+newHeight);
			reSizePicture(fromFile, toFile, newWidth, newHeight);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reSizeJpegByproportion(String fromFile, String toFile,double proportion) {
		try {
			File fromF = new File(fromFile);
			BufferedImage bi = ImageIO.read(fromF);
			int newWidth = 0, newHeight = 0;
			newWidth = (int) (((double) bi.getWidth()) * proportion);
			newHeight = (int) (((double) bi.getHeight()) * proportion);
			reSizeJepgPicture(fromFile, toFile, newWidth, newHeight);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reSizePicture(String fromFile, String toFile, int newWidth,int newHeight) {
		try {
			File fromF = new File(fromFile);
			BufferedImage bi = ImageIO.read(fromF);
			BufferedImage to = new BufferedImage(newWidth, newHeight,BufferedImage.TYPE_INT_BGR);
			Graphics2D g2d = to.createGraphics();
			to = g2d.getDeviceConfiguration().createCompatibleImage(newWidth,newHeight, Transparency.TRANSLUCENT);
			g2d.dispose();
			g2d = to.createGraphics();
			Image from = bi.getScaledInstance(newWidth, newHeight,BufferedImage.SCALE_AREA_AVERAGING);
			g2d.drawImage(from, 0, 0, null);
			g2d.dispose();
			ImageIO.write(to, "png", new File(toFile));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reSizeJepgPicture(String fromFile, String toFile, int newWidth,int newHeight) {
		try {
			File fromF = new File(fromFile);
			BufferedImage bi = ImageIO.read(fromF);
			BufferedImage to = new BufferedImage(newWidth, newHeight,BufferedImage.TYPE_INT_BGR);
			to.getGraphics().drawImage(bi, 0, 0, newWidth, newHeight, null);
			FileOutputStream out = new FileOutputStream(toFile);
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
			JPEGEncodeParam jep = JPEGCodec.getDefaultJPEGEncodeParam(to);
			jep.setQuality(0.9f, true);
			encoder.encode(to,jep);
			out.close();
			bi.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reName() {
		File filePath = new File("D:/image");
		File[] fileList = filePath.listFiles();
		for (File file : fileList) {
			String fileName = file.getName();
			System.out.println("========" + fileName.replace(".png", ""));
			String newName = fileName.replace(".png", "");
			System.out.println(newName);
			if (newName.length() == 1) {
				newName = "000" + newName;
			} else if (newName.length() == 2) {
				newName = "00" + newName;
			} else if (newName.length() == 3) {
				newName = "0" + newName;
			}
			boolean bool = file.renameTo(new File("D:/image/" + newName+ ".png"));
			System.err.println(bool);
		}
	}
}
