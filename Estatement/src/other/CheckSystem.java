package other;

public class CheckSystem {

	
    public static void main(String[] args) {
        String osName = System.getProperty("os.name");
        System.out.println(osName);  //Windows 10
        if (osName.startsWith("Windows")) {
        	System.out.println("Windows");
        } else {
        	System.out.println("Mac");
        }
        String userName =  System.getProperty("user.home");  //获取用户的路径 C:\Users\jeff.xie
        System.out.println(userName);
       
        String path ="D:\\Project\\Program\\WorkSpaceIDEA\\Automation\\automation-test\\TestReport\\2022-02-28-18-13-49\\screenshots";
        String flag ="android";
        CompressPicture compressPicture = new CompressPicture();
        //String flag =System.getProperty("mobile").toUpperCase();
        double compressPercent=0.2;
        if(flag.contains("IOS")) compressPercent =0.4;
        String compressScreenShotsPath =compressPicture.changeByDir( path,0,0,compressPercent);
        GetScreenShot getScreenShot = new GetScreenShot();
        getScreenShot.create_result_report(compressScreenShotsPath, flag);
    }
    

}
