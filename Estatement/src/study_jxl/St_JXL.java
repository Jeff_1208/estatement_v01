package study_jxl;

import java.io.File;
import java.io.IOException;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class St_JXL {
	public static void main(String args[]) throws IOException, RowsExceededException, WriteException{
		File f = new File("D:\\jxl.xlsx");
		f.createNewFile();
		WritableWorkbook wwb = null;
		wwb = Workbook.createWorkbook(f);   
		//创建Excel工作表 
		WritableSheet ws = wwb.createSheet("通讯录", 0);//创建sheet
		ws.mergeCells(0, 0, 5, 1);//合并单元格(左列，左行，右列，右行)从第1行第1列到第2行第3列
		Label header = new Label(0, 0, "内部通讯录"); 
		ws.addCell(header);//写入头
		Label l = new Label(0, 2, "姓名");//第3行
		ws.addCell(l);
		l = new Label(1, 2, "单位");
		ws.addCell(l);
		l = new Label(2, 2, "手机");
		ws.addCell(l);
		l = new Label(3, 2, "电话");
		ws.addCell(l);
		l = new Label(4, 2, "QQ");
		ws.addCell(l);
		l = new Label(5, 2, "邮箱");
		ws.addCell(l); 
		
		
		l = new Label(0, 3, "Realname");
		ws.addCell(l);
		l = new Label(1, 3, "UserDepart");
		ws.addCell(l);
		l = new Label(2, 3, "Hand_tel");
		ws.addCell(l);
		l = new Label(3, 3, "Telephone");
		ws.addCell(l);
		l = new Label(4, 3, "QQnumber");
		ws.addCell(l);
		l = new Label(5, 3, "E_mail");
		ws.addCell(l);
		wwb.write();
		wwb.close();	
	}

}
