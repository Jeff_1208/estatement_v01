package temp;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;
public class Temp {

	private static String[] month_list= {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};


	public static void main(String[] args) {
//        LocalDate currentDate = LocalDate.now(); // 获取当前日期
//        
//        //14 JUL 2024
//        String monthName = currentDate.getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH); // 获取月份的英文名称
//        String monthName1 = currentDate.getMonth().getDisplayName(TextStyle.SHORT_STANDALONE, Locale.ENGLISH).toUpperCase(); // 获取月份的英文名称
//        System.out.println("Current Month in English: " + monthName);
//        System.out.println("Current Month in English: " + monthName1);
//        
//        Calendar c = Calendar.getInstance();
//        System.out.println(Calendar.MONTH);
//        Date date = new Date();
//       
//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
//		Calendar start = Calendar.getInstance();
//	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_mmm_dd_HH_mm_ss");
//	    String timeString = simpleDateFormat.format(new Date(System.currentTimeMillis()));
//	    System.out.println(timeString);
//	    System.out.println(getLastDate());
//	    System.out.println(getNextDate());
//        Calendar calendar = Calendar.getInstance();
//        int year = calendar.get(Calendar.YEAR);
//        int month = calendar.get(Calendar.MONTH);
//        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
//        System.out.println(daysInMonth);
//        
        
        String[] testStrings = {"123", "123.45", "123.0", "0.45", "123.45.67", "abc", "123d"};
        
        for (String str : testStrings) {
            System.out.println(str + " is a valid double? " + isValidDouble(str));
        }
	}
	
    public static boolean isValidDouble(String str) {
        // 正则表达式，匹配必须包含小数点的double类型数值
        // 匹配由数字开头，后面跟着零个或一个点和至少一个数字的情况
        String pattern = "^\\d+\\.\\d+$";
        return str.matches(pattern);
    }
	
	 public static String getLastDate() {
	        LocalDate date = LocalDate.now().minusDays(1);
	        String monthName1 = date.getMonth().getDisplayName(TextStyle.SHORT_STANDALONE, Locale.ENGLISH).toUpperCase(); // 获取月份的英文名称
	        String v =  date.getDayOfMonth()+" "+monthName1+" "+date.getYear();
	        return v;
	}
	 
	 public static String getNextDate() {
	        LocalDate date = LocalDate.now().plusDays(-1);
	        String monthName1 = date.getMonth().getDisplayName(TextStyle.SHORT_STANDALONE, Locale.ENGLISH).toUpperCase();
	        String v =  date.getDayOfMonth()+" "+monthName1+" "+date.getYear();
	        return v;
	}


	
	public static boolean isPageLine(String line) {
		String s = line.replaceAll("\r", "").replaceAll("\n", "").trim();
		if(!s.startsWith("Page")) return false;
		if(!s.contains("of")) return false;
		
		Pattern p = Pattern.compile("Page .*\\d+.* of .*\\d+.*");
		Matcher m = p.matcher(s);
		if(m.matches()) return false;
		
		return true;
	}
	
	public static boolean is_valued_number(String value) {
		if(value==null) {
			return true;
		}
		if(!(value.contains("."))) {
			return false;
		}
		if(value.contains("-")) {
			int index = value.indexOf("-");
			if (index!=0){
				return false;
			}
		}
		return true;
	}
	
	public static boolean compare_amount_string_to_double(String amount1,String amount2) {
		System.out.println("ss");
		if(!(is_valued_number(amount1)&&is_valued_number(amount2))) {
			return false;
		}
		
		if(amount1==null) return false;
		if(amount2==null) return false;
		boolean re = false;
		double a1 = string_to_double(amount1);
		double a2 = string_to_double(amount2);
		if(a1==a2) {
			re =true;
		}
		return re;
	}
	@SuppressWarnings("null")
	public static double string_to_double(String value) {
		if(value==null) {
			return (Double) null;
		}
		if(value.trim().length()<2) {
			return (Double) 0.0;
		}
		double a=Double.parseDouble(value);
		return a;
	}

	
	public static String doubleToString(double data) {
	
	    NumberFormat numberFormat = NumberFormat.getInstance();
	    numberFormat.setGroupingUsed(false);
	    String s1 = numberFormat.format(data);
	    return numberFormat.format(data);
	}
	
	public static boolean is_available() {
		double data =Math.random();
		if (data >0.1) {
			return true;
		}else {
			return false;
		}
	}

	public static boolean the_line_is_have_a_double_string(String line) {
		line =line.replace("\n", "");
		String[] ss= line.split(" ");
		for (int i = 0; i < ss.length; i++) {
			boolean flag = is_double_string(ss[i]);
			if(flag) return true;
		}
		return false;
	}

	public static boolean is_double_string(String data) {
//		String re= "^(([0])|([1-9]+[0-9]*.{1}[0-9]+)|([0].{1}[1-9]+[0-9]*)|([1-9][0-9]*)|([0][.][0-9]+[1-9]+))$";
		String re= "(-)?[0-9,]+[.]{0,1}[0-9]*[dD]{0,1}";
		Pattern NUMBER_PATTERN = Pattern.compile(re);
		return NUMBER_PATTERN.matcher(data).matches();
	}


	public static boolean check_description(String desc) {
		boolean result = true;
		desc="Send money to mario ??‘:";

		String REGEX ="^[A-Za-z0-9*&?#!\"‘:,'()\'//%+-. ]+$";

		Pattern p = Pattern.compile(REGEX);
		Matcher m = p.matcher(desc); // 获取 matcher 对象
		result= m.matches();
		System.out.println(result);
		return result;

	}

	//銀通櫃員機提款 RATE: CNY 100 : HKD 121.26
	//10 Aug 2021 -CNY 400 -121.26
	public static String get_foreign_currency_Amount_from_line(String line) {
		String value="";
		String flag="CNY";
		int index = 0;
		if(line.contains(flag)) {
			String[] ss = line.split(" ");
			for (int i = 0; i < ss.length; i++) {
				if(ss[i].contains(flag)) {
					index = i+1;
				}
			}
			return ss[index];
		}
		return value;
	}

}
