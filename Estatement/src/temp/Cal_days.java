package temp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import com.entity.Wealth;
import com.utils.Common;

public class Cal_days {
	public static final String[] month_list= {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	public static final String fail_refs_fileName= "/Result_fail_Refs.xlsx";

	public static void main(String[] args) {
/***
		get();
		Calendar cld = Calendar.getInstance();
		System.out.println(cld.YEAR);
		System.out.println(get_month_number("Dec"));
		//SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(System.currentTimeMillis());
		System.out.println(formatter.format(date));
		String s = formatter.format(date);
		s= s.substring(0,s.length()-2);
		s = s+"01";
		System.out.println(s);
		int a =(int)(Math.random()*1000);
		System.out.println("a::::::::::: "+a);
***/			
		Wealth w = new Wealth();
		w.setThisMonth("Jan");
		w.setThisYear("2023");
		
	
		boolean r =compareDate("31 Feb 2023",w);
		System.out.println(r);
	    LocalDateTime now = LocalDateTime.now();
	    int day  = now.getDayOfMonth();
	    System.out.println(day);
	}
	
	public static boolean compareDate(String date1,Wealth w) {
		if(date1.contains("TBC")) return false; //未来日期，不需要计算
		String[] ss = date1.trim().split(" ");
		if(ss.length != 3) return false;
		int year = Integer.parseInt(ss[2].trim());
		int monthIndex = Common.getMonthIndex(ss[1].trim());
		int thisYear = Integer.parseInt(w.getThisYear());
		int thisMonthIndex = Common.getMonthIndex(w.getThisMonth());
		//System.out.println("monthIndex: "+monthIndex);
		//System.out.println("thisMonthIndex: "+thisMonthIndex);
		if(year > thisYear) return false; //settlement 年份大于报告的年份，说明是未来日期，不计算
		if(year == thisYear) {
			if(monthIndex > thisMonthIndex) return false; //年份相同，settlement 月份大于报告的月份，说明是未来日期，不计算
			if(monthIndex <= thisMonthIndex) return true; //当月或当月之前需要计算
		}
		if(year < thisYear) return true; //settlement 年份小于报告的年份，说明是过去日期，计算
		return false;
	}

	

	public static String get_month_number(String M) {

		for(int i=0;i<month_list.length;i++) {
			if(month_list[i].equals(M)) {
				int index = i+1;
				if(index>=10) {
					return index+"";
				}else {
					return "0"+index;
				}
			}
		}
		return "NONE";
	}


	public static void get() {
		DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date star = dft.parse("2021-02-03");//开始时间
			Date endDay=dft.parse("2021-07-02");//结束时间
			Date nextDay=star;
			int i=0;
			while(nextDay.before(endDay)){//当明天不在结束时间之前是终止循环
				Calendar cld = Calendar.getInstance();
				cld.setTime(star);
				cld.add(Calendar.DATE, 1);
				star = cld.getTime();
				//获得下一天日期字符串
				nextDay = star; 
				i++;
			}
			System.out.println("相差天数为："+i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
