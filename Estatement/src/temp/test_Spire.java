package temp;
import com.spire.pdf.*;
import java.awt.*;
import com.spire.pdf.general.find.PdfTextFind;
public class test_Spire {



	public static void main(String[] args) throws Exception {
		String sourcefile="D:\\Project\\e-Statement\\estatement_pdf\\2021_05_10_estatement\\8000024987_est_6bdda4bdddf8a800355a924157b5408e.pdf";
		String targetfile="D:\\Project\\e-Statement\\estatement_pdf\\2021_05_10_estatement\\result.pdf";
		String flag = "FT21098B376L";
		
		//加载示例PDF文档
		PdfDocument pdf = new PdfDocument();
		pdf.loadFromFile(sourcefile);
		PdfTextFind[] result = null;
		//遍历文档每一页
		for (int i = 0; i < pdf.getPages().getCount(); i++) {
			//获取特定页
			PdfPageBase page = pdf.getPages().get(i);
			result = page.findText(flag).getFinds();
			for (PdfTextFind find : result) {

				//高亮显示查找结果
				find.applyHighLight(Color.yellow);
			}
			//保存文档
			pdf.saveToFile(targetfile);
			pdf.close();
		}
	}

}
