package temp;

import static org.apache.commons.lang.StringUtils.replace;
import static org.apache.commons.lang3.StringUtils.trim;
import static org.apache.commons.lang3.StringUtils.upperCase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class HkidNoUtil {

  private static final Pattern HKID_PATTERN = Pattern
      .compile("^([a-zA-Z\\d]{1,2})(\\d{6})\\(?([aA\\d])\\)?$");

  public static String formatHkidNo(String hkidNo) {
    return upperCase(trim(replace(replace(hkidNo, "(", ""), ")", "")));
  }

  public static boolean validate(String hkidNumber) {
    Matcher matcher = HKID_PATTERN.matcher(hkidNumber);
    if (matcher.matches()) {
      return validate(matcher.group(1), matcher.group(2), matcher.group(3));
    }
    return false;
  }

  public static boolean validate(String prefix, String serial, String checkDigit) {
    String prefixU = prefix.toUpperCase();
    long value = 0;
    if (prefixU.length() == 2) {
      value += (prefixU.charAt(0) - 55) * 9 + (prefixU.charAt(1) - 55) * 8;
    } else if (prefixU.length() == 1) {
      value += 36 * 9 + (prefixU.charAt(0) - 55) * 8;
    }
    for (int i = 0; i < 6; i++) {
      value += Integer.parseInt(serial.substring(i, i + 1)) * (7 - i);
    }
    long reminder = value % 11;
    long validCheckDigit = 11 - reminder;
    if ("0".equalsIgnoreCase(checkDigit) && validCheckDigit == 11) {
      return true;
    }
    if ("A".equalsIgnoreCase(checkDigit) && validCheckDigit == 10) {
      return true;
    }
    try {
      if (validCheckDigit == Integer.parseInt(checkDigit)) {
        return true;
      }
    } catch (Exception e) {
      return false;
    }
    return false;
  }

}
