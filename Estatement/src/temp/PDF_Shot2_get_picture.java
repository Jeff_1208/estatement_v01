package temp;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;



public class PDF_Shot2_get_picture {

	public static void main(String[] args) {
		
		 String sourcefile="D:\\Project\\e-Statement\\estatement_pdf\\2021_05_07_estatement\\8000000020_est_b030a0dfa52def751db17b6e5ef9a104.pdf";
		 String targetfile="D:\\Project\\e-Statement\\estatement_pdf\\2021_05_07_estatement\\out55";
		 createScreenShoot(sourcefile,targetfile);
	}
	public static boolean createScreenShoot(String source, String target) {
		File file = new File(source);
		if (!file.exists()) {
			System.err.println("路径[" + source + "]对应的pdf文件不存在!");
			return false;
		}
		try{
			RandomAccessFile raf = new RandomAccessFile(file, "r");
			FileChannel channel = raf.getChannel();
			ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
			PDFFile pdffile = new PDFFile(buf);
			int num = pdffile.getNumPages();
			for(int i = 1; i < num; i++){
				PDFPage page = pdffile.getPage(i);
				// get the width and height for the doc at the default zoom
				//Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(), (int) page.getBBox().getHeight());
				
				Rectangle rect = new Rectangle(0, 410, (int) page.getBBox()
						.getWidth(), 30);
				// generate the image
				Image img = page.getImage(rect.width, rect.height, // width &
						rect, // clip rect
						null, // null for the ImageObserver
						true, // fill background with white
						true // block until drawing is done
						);
				BufferedImage tag = new BufferedImage(rect.width, rect.height,   BufferedImage.TYPE_INT_RGB);
				tag.getGraphics().drawImage(img, 0, 0, rect.width, rect.height,null);
				FileOutputStream out = new FileOutputStream(target+i+".jpg");
				JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
				encoder.encode(tag); // JPEG编码
				out.close();
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return true;
		}

	}
	
	public static boolean createScreenShootbyLocation(String source, String target,int y) {
		File file = new File(source);
		if (!file.exists()) {
			System.err.println("路径[" + source + "]对应的pdf文件不存在!");
			return false;
		}
		try{
			RandomAccessFile raf = new RandomAccessFile(file, "r");
			FileChannel channel = raf.getChannel();
			ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
			PDFFile pdffile = new PDFFile(buf);
			int num = pdffile.getNumPages();
			for(int i = 1; i < num; i++){
				PDFPage page = pdffile.getPage(i);
				// get the width and height for the doc at the default zoom
				//Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(), (int) page.getBBox().getHeight());
				//获得y轴的坐标后减去7，高度设置为30
				Rectangle rect = new Rectangle(0, y-7, (int) page.getBBox().getWidth(), 30);
				// generate the image
				Image img = page.getImage(rect.width, rect.height, // width &
						rect, // clip rect
						null, // null for the ImageObserver
						true, // fill background with white
						true // block until drawing is done
						);
				BufferedImage tag = new BufferedImage(rect.width, rect.height,   BufferedImage.TYPE_INT_RGB);
				tag.getGraphics().drawImage(img, 0, 0, rect.width, rect.height,null);
				FileOutputStream out = new FileOutputStream(target+i+".jpg");
				JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
				encoder.encode(tag); // JPEG编码
				out.close();
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return true;
		}

	}
}
