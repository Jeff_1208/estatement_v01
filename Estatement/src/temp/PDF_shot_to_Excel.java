package temp;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import com.itextpdf.awt.geom.Dimension;
import com.itextpdf.awt.geom.Dimension2D;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

public class PDF_shot_to_Excel {
	static int page = 0;
	public static void main(String[] args) throws Exception {
		PDF_shot_to_Excel p = new PDF_shot_to_Excel();

		String sourcefile="D:\\Project\\e-Statement\\estatement_pdf\\2021_05_10_estatement\\8000024987_est_6bdda4bdddf8a800355a924157b5408e.pdf";
		String targetfile="D:\\Project\\e-Statement\\estatement_pdf\\2021_05_10_estatement\\out77";
		float[] location = p.getKeyWordsByPath(sourcefile, "FT21098B376L");
		int y_location = (int) location[1];

		for(int i=0;i<location.length;i++) {
			System.out.println("value: "+location[i]);
		}

		p.createScreenShootbyLocation(sourcefile, targetfile, y_location);
	}

	/**
	 * @Author AlphaJunS
	 * @Date 18:24 2020/3/7
	 * @Description 用于供外部类调用获取关键字所在PDF文件坐标
	 * @param filepath
	 * @param keyWords
	 * @return float[]
	 */
	public static float[] getKeyWordsByPath(String filepath, String keyWords) {
		float[] coordinate = null;
		try{
			PdfReader pdfReader = new PdfReader(filepath);
			coordinate = getKeyWords(pdfReader, keyWords);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return coordinate;
	}

	/**
	 * @Author AlphaJunS
	 * @Date 18:26 2020/3/7
	 * @Description 获取关键字所在PDF坐标
	 * @param pdfReader
	 * @param keyWords
	 * @return float[]
	 */
	private static float[] getKeyWords(PdfReader pdfReader, String keyWords) {
		float[] coordinate = null;

		try{
			int pageNum = pdfReader.getNumberOfPages();
			PdfReaderContentParser pdfReaderContentParser = new PdfReaderContentParser(pdfReader);
			CustomRenderListener renderListener = new CustomRenderListener();
			renderListener.setKeyWord(keyWords);
			for (page = 1; page <= pageNum; page++) {
				renderListener.setPage(page);
				pdfReaderContentParser.processContent(page, renderListener);
				coordinate = renderListener.getPcoordinate();
				System.out.println("Page: "+ page+" coordinate: "+coordinate);
				if (coordinate != null) break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return coordinate;
	}

	@SuppressWarnings("restriction")
	public boolean createScreenShootbyLocation(String source, String target,int y) {
		File file = new File(source);
		if (!file.exists()) {
			System.err.println("路径[" + source + "]对应的pdf文件不存在!");
			return false;
		}
		try{
			RandomAccessFile raf = new RandomAccessFile(file, "r");
			FileChannel channel = raf.getChannel();
			ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
			PDFFile pdffile = new PDFFile(buf);
			int num = pdffile.getNumPages();
			//for(int i = 1; i < num; i++){
			int p = page;
			PDFPage page = pdffile.getPage(p);


			// get the width and height for the doc at the default zoom
			//Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(), (int) page.getBBox().getHeight());
			//获得y轴的坐标后减去7，高度设置为30
			Rectangle rect = new Rectangle(0, y-7, (int) page.getBBox().getWidth(), 30);
			// generate the image
			//			PDFPage pa= generate_new_page(page, pdffile);
			//			page.addFillAlpha(1.5f);
			Image img = page.getImage(rect.width, rect.height, // width &
					rect, // clip rect
					null, // null for the ImageObserver
					true, // fill background with white
					true // block until drawing is done
					);
			BufferedImage tag = new BufferedImage(rect.width, rect.height,   BufferedImage.TYPE_INT_RGB);
			tag.getGraphics().drawImage(img, 0, 0, rect.width, rect.height,null);
			FileOutputStream out = new FileOutputStream(target+p+".jpg");
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
			JPEGEncodeParam jep = JPEGCodec.getDefaultJPEGEncodeParam(tag);
			
			jep.setQuality(10f, true);    //压缩质量, 1 是最高值
			//encoder.setJPEGEncodeParam(jep);
			encoder.encode(tag, jep);
			//			encoder.encode(tag); // JPEG编码
			out.close();
			//}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return true;
		}

	}

	/***


    public static void zoomImageScale(File imageFile, String newPath, int newWidth) throws IOException {
         if(!imageFile.canRead())
             return;
        BufferedImage bufferedImage = ImageIO.read(imageFile);
        if (null == bufferedImage) 
            return;

        int originalWidth = bufferedImage.getWidth();
        int originalHeight = bufferedImage.getHeight();
        double scale = (double)originalWidth / (double)newWidth;    // 缩放的比例

        int newHeight =  (int)(originalHeight / scale);

        zoomImageUtils(imageFile, newPath, bufferedImage, newWidth, newHeight);
    }


    private static void zoomImageUtils(File imageFile, String newPath, BufferedImage bufferedImage, int width, int height)
            throws IOException{

         String suffix = StringUtils.substringAfterLast(imageFile.getName(), ".");

         // 处理 png 背景变黑的问题
        if(suffix != null && (suffix.trim().toLowerCase().endsWith("png") || suffix.trim().toLowerCase().endsWith("gif"))){
            BufferedImage to= new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); 
            Graphics2D g2d = to.createGraphics(); 
            to = g2d.getDeviceConfiguration().createCompatibleImage(width, height, Transparency.TRANSLUCENT); 
            g2d.dispose(); 

            g2d = to.createGraphics(); 
            Image from = bufferedImage.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING); 
            g2d.drawImage(from, 0, 0, null);
            g2d.dispose(); 

            ImageIO.write(to, suffix, new File(newPath));
        }else{
            // 高质量压缩，其实对清晰度而言没有太多的帮助
//            BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
//            tag.getGraphics().drawImage(bufferedImage, 0, 0, width, height, null);
//
//            FileOutputStream out = new FileOutputStream(newPath);    // 将图片写入 newPath
//            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
//            JPEGEncodeParam jep = JPEGCodec.getDefaultJPEGEncodeParam(tag);
//            jep.setQuality(1f, true);    //压缩质量, 1 是最高值
//            encoder.encode(tag, jep);
//            out.close();

            BufferedImage newImage = new BufferedImage(width, height, bufferedImage.getType());
            Graphics g = newImage.getGraphics();
            g.drawImage(bufferedImage, 0, 0, width, height, null);
            g.dispose();
            ImageIO.write(newImage, suffix, new File(newPath));
        }
    }

	 ***/
	public PDFPage generate_new_page(PDFPage page,PDFFile pdffile) {
		Dimension2D dimension2D = new Dimension();
		PDFPage p = pdffile.getPage(1);

		//设置新文档第一页的页面宽、高为原来的1.2倍

		float scale = 1.2f;
		float width = (float) p.getWidth() * scale;

		float height = (float) p.getHeight() * scale;

		dimension2D.setSize(width, height);

		//设置新文档第一页的页边距为左右50，上下100

		//		PdfPageBase newPage = newDoc.getPages().add(dimension2D, margins);

		//复制原文档的内容到新文档

		//		newPage.getCanvas().drawTemplate(p.createTemplate(), new Point2D.Float());
		p.addFillAlpha(1.5f);
		return p;
	}



	public void fangda() throws Exception {
		String sourcefile="D:\\Project\\e-Statement\\estatement_pdf\\2021_05_10_estatement\\8000024987_est_6bdda4bdddf8a800355a924157b5408e.pdf";
		String targetfile="D:\\Project\\e-Statement\\estatement_pdf\\2021_05_10_estatement\\999.pdf";
		PdfReader reader = new PdfReader(new FileInputStream(sourcefile));
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(targetfile));
		int total = reader.getNumberOfPages() + 1;
		for ( int i=1; i<total; i++) {
			reader.setPageContent(i + 1, reader.getPageContent(i + 1));
		}
		stamper.setFullCompression();
		stamper.close();
	}

}
