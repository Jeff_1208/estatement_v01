package temp;

import java.util.Random;

public class HKIDGenerator {
    public static void main(String[] args) {
        String prefix = generateOneCharacter();
        String serial = generate6Number();
        String r = generateValidate(prefix, serial);
        System.out.println(r);
    }
    
    public static String generate6Number() {
        int min = 100000;
        int max = 999999;
        int a  = (int) (Math.random()*(max-min +1)) + 100000;
        Random random = new Random();
        int randomInteger = random.nextInt(10);
        String r = "Z"+ a + "(" + randomInteger + ")";
        return ""+a;
    }
    public static String generateOneCharacter() {
        Random random = new Random();
        char upperCaseLetter = (char) ('A' + random.nextInt(26)); // 生成一个大写字母 'A'-'Z'
        System.out.println("随机生成的大写字母是: " + upperCaseLetter);
        return upperCaseLetter+"";
    }
    
    public static String generateValidate(String prefix, String serial) {
        String prefixU = prefix.toUpperCase();
        long value = 0;
        if (prefixU.length() == 2) {
          value += (prefixU.charAt(0) - 55) * 9 + (prefixU.charAt(1) - 55) * 8;
        } else if (prefixU.length() == 1) {
          value += 36 * 9 + (prefixU.charAt(0) - 55) * 8;
        }
        for (int i = 0; i < 6; i++) {
          value += Integer.parseInt(serial.substring(i, i + 1)) * (7 - i);
        }
        long reminder = value % 11;
        long validCheckDigit = 11 - reminder;
        String last = "";
        if ( validCheckDigit == 11) {
        	last="0";
        }else if ( validCheckDigit == 10) {
        	last="A";
        }else {
        	last = validCheckDigit+"";
        }
        String result= prefix + serial + "(" + last + ")";
        return result;
    }
    
    public static boolean validate(String prefix, String serial, String checkDigit) {
        String prefixU = prefix.toUpperCase();
        long value = 0;
        if (prefixU.length() == 2) {
          value += (prefixU.charAt(0) - 55) * 9 + (prefixU.charAt(1) - 55) * 8;
        } else if (prefixU.length() == 1) {
          value += 36 * 9 + (prefixU.charAt(0) - 55) * 8;
        }
        for (int i = 0; i < 6; i++) {
          value += Integer.parseInt(serial.substring(i, i + 1)) * (7 - i);
        }
        long reminder = value % 11;
        long validCheckDigit = 11 - reminder;
        if ("0".equalsIgnoreCase(checkDigit) && validCheckDigit == 11) {
          return true;
        }
        if ("A".equalsIgnoreCase(checkDigit) && validCheckDigit == 10) {
          return true;
        }
        try {
          if (validCheckDigit == Integer.parseInt(checkDigit)) {
            return true;
          }
        } catch (Exception e) {
          return false;
        }
        return false;
      }

}
