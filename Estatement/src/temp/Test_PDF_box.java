package temp;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import com.entity.Ref;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfPageBase;
import com.spire.pdf.actions.PdfGoToAction;
import com.spire.pdf.general.*;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

public class Test_PDF_box {

//	public static void main(String[] args) {
//		//加载示例文档
//		PdfDocument doc = new PdfDocument();
//		doc.loadFromFile(sourcefile);
//
//		//居中窗口
//		doc.getViewerPreferences().setCenterWindow(true);
//		//隐藏标题
//		doc.getViewerPreferences().setDisplayTitle(false);
//		//不适合整页至窗口
//		doc.getViewerPreferences().setFitWindow(false);
//		//隐藏菜单栏
//		doc.getViewerPreferences().setHideMenubar(true);
//		//隐藏工具栏
//		doc.getViewerPreferences().setHideToolbar(true);
//		//页面布局设置为单页
//		doc.getViewerPreferences().setPageLayout(PdfPageLayout.Single_Page);
//
//		//保存文档
//		//String output = "output/viewerPreference.pdf";
//		doc.saveToFile(targetfile, FileFormat.PDF);
//	}


	static String sourcefile="D:\\Project\\e-Statement\\estatement_pdf\\2021_05_10_estatement\\8000024987_est_6bdda4bdddf8a800355a924157b5408e.pdf";
	static String targetfile="D:\\Project\\e-Statement\\estatement_pdf\\2021_05_10_estatement\\result55.pdf";
	
	public static void main(String[] args) {

		//加载示例文档
		PdfDocument doc = new PdfDocument();
		doc.loadFromFile(sourcefile);

		//获取第一页
		PdfPageBase page = doc.getPages().get(0);

		//设置PDF页面显示位置
		PdfDestination dest = new PdfDestination(page);
		dest.setMode(PdfDestinationMode.Location);
		dest.setLocation(new Point2D.Float(-40f, -40f));

		//设置缩放比例
		dest.setZoom(5f);

		//设置打开PDF文档时的页面显示缩放比例
		PdfGoToAction gotoAction = new PdfGoToAction(dest);
		doc.setAfterOpenAction(gotoAction);

		//保存文档
//		String output = "output/setZoomFactor.pdf";
		doc.saveToFile(targetfile);
		
		Test_PDF_box t = new Test_PDF_box();
		Ref r =t.getKeyWordsAttribute(targetfile, " FT21113CL3J0");
		t.createScreenShootbyLocation(targetfile, "D:\\result222.jpg",r.getY(),r.getPage());
	
	}
	
	int page=0;
	
	public  Ref getKeyWordsAttribute( String filepath, String keyWords) {
		Ref ref = null;
		try{
			PdfReader pdfReader = new PdfReader(filepath);
			ref = getKeyWords(pdfReader, keyWords);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ref;
	}

	private Ref getKeyWords(PdfReader pdfReader, String keyWords) {
		Ref ref= new Ref();
		float[] coordinate = null;

		try{
			int pageNum = pdfReader.getNumberOfPages();
			PdfReaderContentParser pdfReaderContentParser = new PdfReaderContentParser(pdfReader);
			CustomRenderListener renderListener = new CustomRenderListener();
			renderListener.setKeyWord(keyWords);
			for (page = 1; page <= pageNum; page++) {
				renderListener.setPage(page);
				pdfReaderContentParser.processContent(page, renderListener);
				coordinate = renderListener.getPcoordinate();
				System.out.println("Page: "+ page+" coordinate: "+coordinate);

				if (coordinate != null) {
					ref.setPage(page);
					System.out.println("Page:  "+page);
					ref.setValue(keyWords);
					ref.setY((int)coordinate[1]);
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ref;
	}
	
	public boolean createScreenShootbyLocation(String source, String target,int y,int page_index) {
		File file = new File(source);
		if (!file.exists()) {
			System.err.println("路径[" + source + "]对应的pdf文件不存在!");
			return false;
		}
		try{
			@SuppressWarnings("resource")
			RandomAccessFile raf = new RandomAccessFile(file, "r");
			FileChannel channel = raf.getChannel();
			ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
			PDFFile pdffile = new PDFFile(buf);
			//int num = pdffile.getNumPages();
			PDFPage page = pdffile.getPage(page_index);

			//获得y轴的坐标后减去7，高度设置为30
			Rectangle rect = new Rectangle(0, y-7, (int) page.getBBox().getWidth(), 30);
			// generate the image
			//			PDFPage pa= generate_new_page(page, pdffile);
			//			page.addFillAlpha(1.5f);

			Image img = page.getImage(rect.width, rect.height, // width &
					rect, // clip rect
					null, // null for the ImageObserver
					true, // fill background with white
					true // block until drawing is done
					);
			BufferedImage tag = new BufferedImage(rect.width, rect.height,   BufferedImage.TYPE_INT_RGB);
			tag.getGraphics().drawImage(img, 0, 0, rect.width, rect.height,null);
			FileOutputStream out = new FileOutputStream(target);
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
			JPEGEncodeParam jep = JPEGCodec.getDefaultJPEGEncodeParam(tag);

			jep.setQuality(10f, true);    //压缩质量, 1 是最高值
			//encoder.setJPEGEncodeParam(jep);
			encoder.encode(tag, jep);
			//			encoder.encode(tag); // JPEG编码
			out.close();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return true;
		}

	}

}
