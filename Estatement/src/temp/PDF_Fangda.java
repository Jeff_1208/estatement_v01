package temp;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.icepdf.core.pobjects.Document;
import org.icepdf.core.util.GraphicsRenderingHints;
import org.apache.pdfbox.pdmodel.PDDocument;

/**

 * @author: GPJ

 * @Description: pdf 转单张高清图片

 * @Date Created in 9:27 2018/1/9

 * @Modified By:

 */

public class PDF_Fangda {
	public static void pdf2Pic(String pdfPath, String path){
		Document document = new Document();

		document.setFile(pdfPath);

		//缩放比例

		float scale = 12f;

		//旋转角度

		float rotation = 0f;

		for (int i = 0; i < document.getNumberOfPages(); i++) {
			BufferedImage image = (BufferedImage)document.getPageImage(i, GraphicsRenderingHints.SCREEN, 
					org.icepdf.core.pobjects.Page.BOUNDARY_CROPBOX, rotation, scale);

			RenderedImage rendImage = image;
			
			System.out.println(rendImage.toString());

			try {
				String imgName = i + ".png";

				System.out.println(imgName);

				File file = new File(path + imgName);

				ImageIO.write(rendImage, "png", file);

			} catch (IOException e) {
				e.printStackTrace();

			}

			image.flush();

		}

		document.dispose();

	}

	public static void main(String[] args) {
		String filePath = "D:\\bbb.pdf";
		String sourcefile="D:\\Project\\e-Statement\\estatement_pdf\\2021_05_10_estatement\\8000024987_est_6bdda4bdddf8a800355a924157b5408e.pdf";

		pdf2Pic(sourcefile, "D:\\ccc");

	}

	/***
	
    public  void fangda2(String path) {
        //加载示例文档
        PdfDocument doc = new PdfDocument();
        doc.loadFromFile("Sample.pdf");

        //居中窗口
        doc.getViewerPreferences().setCenterWindow(true);
        //隐藏标题
        doc.getViewerPreferences().setDisplayTitle(false);
        //不适合整页至窗口
        doc.getViewerPreferences().setFitWindow(false);
        //隐藏菜单栏
        doc.getViewerPreferences().setHideMenubar(true);
        //隐藏工具栏
        doc.getViewerPreferences().setHideToolbar(true);
        //页面布局设置为单页
        doc.getViewerPreferences().setPageLayout(PdfPageLayout.Single_Page);

        //保存文档
        String output = "output/viewerPreference.pdf";
        doc.saveToFile(output, FileFormat.PDF);
    }
    ***/
}
