package temp;

import java.io.FileOutputStream;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class TextExcelMerge {

	public static void main(String[] args) {
		TextExcelMerge s = new TextExcelMerge();
		s.ExcelMerge();
		System.out.println("Runfinish");
	}

	public void ExcelMerge() {
		String filename = "D:"+"//MergeExcelCell.xlsx";
		//创建工作薄对象
		FileOutputStream out;
		XSSFWorkbook workbook = null;
		try {
			workbook=new XSSFWorkbook();//这里也可以设置sheet的Name
			XSSFSheet sheet = workbook.createSheet();
			workbook.setSheetName(0,"Result");

			XSSFRow row = sheet.createRow(0);//设置第一行，从零开始
			XSSFCell c = row.createCell(0);
			c.setCellValue("Hello World");

			CellRangeAddress region = new CellRangeAddress(0, 29, 0, 0);
			CellRangeAddress region1 = new CellRangeAddress(0, 29, 1, 1);
			XSSFCell c1 = row.createCell(30);
			c1.setCellValue("Hello World1");
			CellRangeAddress region2 = new CellRangeAddress(30, 59, 0, 0);
			CellRangeAddress region3 = new CellRangeAddress(30, 59, 1, 1);
			sheet.addMergedRegion(region);
			sheet.addMergedRegion(region1);
			sheet.addMergedRegion(region2);
			sheet.addMergedRegion(region3);

			out = new FileOutputStream(filename);
			workbook.write(out);
			out.flush();
			out.close();

		} catch (Exception e) {
		}

	}
}
