package gosave;

import javax.swing.*;
import javax.swing.JOptionPane;
import com.toedter.calendar.JDateChooser;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class GoSaveMain extends JFrame implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//https://www.cnblogs.com/lihello/p/12795948.html 下载使用显示日期的jar包
	// https://toedter.com/jcalendar/ jar包下载地址
	
	//其他参考https://blog.csdn.net/u012426327/article/details/77712882
	
	
	private JLabel date1Lable;
	private JLabel date2Lable;
	private JLabel amountLable;
	private JLabel interestRateLable;
	private JLabel reslutLable;

	
	private JTextField amountField;
	private JTextField interestRateField;
	private JTextField resultField;
	private JDateChooser dateChooser;
	private JDateChooser dateChooser2;
	
	private JButton runButton;
	private JCheckBox simulateBox;
	
    public GoSaveMain() {
        // 创建JDateChooser组件
        dateChooser = new JDateChooser();
        dateChooser2 = new JDateChooser();
        // 设置JDateChooser的宽度
        dateChooser.setPreferredSize(new Dimension(100, dateChooser.getPreferredSize().height));
        dateChooser2.setPreferredSize(new Dimension(100, dateChooser.getPreferredSize().height));
        
        JPanel date_panel = new JPanel();
		date_panel.setLayout(new GridLayout(6,2,1,1)) ;
		
		date1Lable = new JLabel("StartDate");
		date2Lable = new JLabel("EndDate");
		amountLable = new JLabel("Amount");
		reslutLable = new JLabel("Result");
		amountField=new JTextField("") ;
		interestRateLable = new JLabel("Rate");
		interestRateField=new JTextField("") ;
		resultField=new JTextField("") ;
		runButton = new JButton("Calculate") ;
		simulateBox = new JCheckBox("模拟时间8点前");
		date_panel.add(date1Lable);
		date_panel.add(dateChooser);
		date_panel.add(date2Lable);
		date_panel.add(dateChooser2);
		date_panel.add(amountLable);
		date_panel.add(amountField);
		date_panel.add(interestRateLable);
		date_panel.add(interestRateField);
		date_panel.add(simulateBox);
		date_panel.add(runButton);
		date_panel.add(reslutLable);
		date_panel.add(resultField);
		runButton.addActionListener(this);
		
        // 创建一个FlowLayout布局
        this.setLayout(new FlowLayout());
 
        // 将JDateChooser添加到JFrame
        this.add(date_panel);
 
        // 设置JFrame的其他属性
        this.setTitle("计算Gosave利息");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(300, 300);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				int flag = JOptionPane.showConfirmDialog(null, "要退出该程序吗？","友情提示",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				if(flag==JOptionPane.YES_OPTION) {
					System.exit(0);
				}else {
					return;
				}
			}
		});
    }
 
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            new GoSaveMain();
        });
    }
    
    public boolean checkForInput() {
    	boolean flag=true;
    	//https://blog.csdn.net/aerchi/article/details/7744307
    	if(dateChooser.getDate() ==null) {
    		JOptionPane.showMessageDialog(null, "请选择startDate!", "错误提示", JOptionPane.ERROR_MESSAGE);
    		return false;
    	}
    	if(dateChooser2.getDate() ==null) {
    		JOptionPane.showMessageDialog(null, "请选择endDate!", "错误提示", JOptionPane.ERROR_MESSAGE);
    		return false;
    	}
    	if(amountField.getText() ==null||amountField.getText().trim().length()<1) {
    		JOptionPane.showMessageDialog(null, "请输入金额!", "错误提示", JOptionPane.ERROR_MESSAGE);
    		return false;
    	}
    	if(interestRateField.getText() ==null||interestRateField.getText().trim().length()<1) {
    		JOptionPane.showMessageDialog(null, "请输入利率!", "错误提示", JOptionPane.ERROR_MESSAGE);
    		return false;
    	}
    	return flag;
    }


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==runButton){
			if(!checkForInput()) return;
			
			System.out.println("\nStart to Calculate interest!!!");
			String  amount = this.amountField.getText();
			String interestRate = this.interestRateField.getText();
			Date  dateChooser= this.dateChooser.getDate();
			Date  dateChooser2= this.dateChooser2.getDate();
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String startDate = sdf.format(dateChooser);
			String endDate = sdf.format(dateChooser2);
			System.out.println(amount);
			System.out.println(interestRate);
			System.out.println(startDate);
			System.out.println(endDate);
			
			String checkBoxValue = "";
			if(simulateBox.isSelected()) {
				checkBoxValue = "true";
			}else {
				checkBoxValue = "false";
			}
			
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("startDate", startDate);
			map.put("endDate", Common.getRealEndDate(endDate, getMinusDays(checkBoxValue)));
			map.put("amount", amount);
			map.put("interestRate", interestRate);
			map.put("checkBoxValue", checkBoxValue);
			
			
			CalculateUtils calculateUtils = new CalculateUtils(map);
			double value = calculateUtils.getResult();
			this.resultField.setText(""+value);
		}
	}
	
	public int getMinusDays(String checkBoxValue) {
		int minus_days =0;
		if(Common.getHoursThisTime()<8) {
			minus_days =1;
		}
		if(checkBoxValue.contains("true")){
			minus_days =1;
		}
		System.out.println("minus_days: "+minus_days);
		return minus_days;
	}
	

}
