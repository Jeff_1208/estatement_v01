package gosave;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Common {
	
	public static void main(String[] args) {
		
		System.out.println(get_year_days(2024));
		System.out.println(getYearDays(2200));
		System.out.println(getDaysBetweenTwo());
		getRealEndDate("2024-1-1",1);
		getHoursThisTime();
	}
	public static int getYearDays(int year) {
		if(isLeapYear(year)) {
			return 366;
		}
		return 365;
	}

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    return true;  // 能够被400整除，是闰年
                } else {
                    return false;  // 能够被100整除但不能被400整除，不是闰年
                }
            } else {
                return true;  // 能够被4整除但不能被100整除，是闰年
            }
        } else {
            return false;  // 不能够被4整除，不是闰年
        }
    }


	
	public static int get_year_days(int year) {
		if(year%400==0) {
			return 366;
		}else {
			if(year%4==0&&year%100==0) {
				return 365;
			}else if(year%4==0&&year%100!=0) {
				return 366;
			}else {
				return 365;
			}
		}
	}
	public static long getDaysBetweenTwo() {
        // 示例日期
        LocalDate date1 = LocalDate.of(2024, 1, 1);
        LocalDate date2 = LocalDate.of(2024, 4, 15);
 
        // 计算两个日期之间的天数
        long daysBetween = ChronoUnit.DAYS.between(date1, date2);
 
        System.out.println("两个日期之间的天数: " + daysBetween);
        return daysBetween;
	}
	
	public static int stringToInt(String date){
		return Integer.parseInt(date);
	}
	public static double stringToDouble(String date){
		return Double.parseDouble(date);
	}
	
	public static int getDaysBetweenTwoDate(String startDate, String endDate) {
        // 示例日期
		String[] startDateArray = startDate.trim().split("-");
		String[] endDateArray = endDate.trim().split("-");
        LocalDate date1 = LocalDate.of(stringToInt(startDateArray[0]), stringToInt(startDateArray[1]), stringToInt(startDateArray[2]));
        LocalDate date2 = LocalDate.of(stringToInt(endDateArray[0]), stringToInt(endDateArray[1]), stringToInt(endDateArray[2]));
 
        // 计算两个日期之间的天数
        long daysBetween = ChronoUnit.DAYS.between(date1, date2);
        //System.out.println("两个日期之间的天数: " + daysBetween);
        return (int) daysBetween;
	}
	
	public static int getHoursThisTime() {
		Date a = new Date();
		//System.out.println(a.getHours());
		return a.getHours();
	}
	
	public static List<Integer> getIntegerListBetweenTwoNumber(int start, int end) {
		List<Integer> list = new ArrayList<>();
		for(int i = start; i<=end;i++) {
			list.add(i);
		}
		return list;
	}

	public static double get_double_round(double float_value) {
		return (double)(Math.round(float_value*100))/100;
	}

	
	public  static String getRealEndDate(String endDate, int flag) {
		if(flag==0) return endDate;
		String[] endDateArray = endDate.trim().split("-");
        LocalDate date = LocalDate.of(stringToInt(endDateArray[0]), stringToInt(endDateArray[1]), stringToInt(endDateArray[2])).minusDays(1);
		String v =  date.getYear()+"-"+date.getMonthValue()+"-"+date.getDayOfMonth();
		System.out.println("VVV;"+v);
		return v;
	}
}
