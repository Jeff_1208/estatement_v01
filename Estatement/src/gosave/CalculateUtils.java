package gosave;

import java.util.HashMap;
import java.util.List;

public class CalculateUtils {
	
	@SuppressWarnings("unused")
	private HashMap<String, String> paramMap = new HashMap<String, String>();
	
	public CalculateUtils(HashMap<String, String> map) {
		this.paramMap = map;
	}
	
	/***
	 * 
	 * 
	 * 平年计算利息：
	remove time (tenor end date time in HKT - 8 hours) - remove time (tenor 
	start date in HKT)) *deposit amount* interest rate / 365（所以end 
	date时间在8：00前是要减去1天计算天数，gosave是以HKT计算天数，T24是以UTC计算天数,HKT时间晚于UTC8小时）
	
	闰年计算利息：
	remove time (tenor end date time in HKT - 8 hours) - remove time (tenor 
	start date in HKT)) *deposit amount* interest rate / 366（所以end 
	date时间在8：00前是要减去1天计算天数，gosave是以HKT计算天数，T24是以UTC计算天数,HKT时间晚于UTC8小时）
	 * 
	 */
	
	public double getResult() {
		String startDate = this.paramMap.get("startDate");
		String endDate = this.paramMap.get("endDate");
		int startYearsDays = Common.getYearDays(Common.stringToInt(startDate.trim().split("-")[0]));
		int endYearsDays = Common.getYearDays(Common.stringToInt(endDate.trim().split("-")[0]));
		if(startYearsDays == endYearsDays) {
			return getSameYearValue();
		}else {
			return getMoreYearValue();
		}
	}
	

	
	public double getMoreYearValue() {
		System.out.println("跨年计算");
		
		String startDate = this.paramMap.get("startDate");
		String endDate = this.paramMap.get("endDate");
		int startYearsDays = Common.stringToInt(startDate.trim().split("-")[0]);
		int endYearsDays = Common.stringToInt(endDate.trim().split("-")[0]);
		List<Integer> list = Common.getIntegerListBetweenTwoNumber(startYearsDays, endYearsDays);
		System.out.println("list.size():"+list.size());
		System.out.println("startYearsDays"+startYearsDays);
		System.out.println("endYearsDays"+endYearsDays);
		double total_interest = 0.0;
		int size  = list.size();
		for (int i = 0; i < list.size(); i++) {
			int year = list.get(i);
			double r = getInterestForEachYear(i, year, size);
			System.out.println(year+"年利息: "+r);
			total_interest +=r;
		}
		System.out.println("total_interest: "+total_interest);
		double result = Common.get_double_round(total_interest);
		System.out.println("result: "+result);
		return result;
	}
	
	public double getInterestForEachYear(int index, int year,int size) {
		String amount = this.paramMap.get("amount");
		String interestRate = this.paramMap.get("interestRate");
		double v =0.0;
		String endDate = "";
		if(index == (size-1)) {
			System.out.println("最后一年："+year);
			//最后一年,天数加1，上一年的最后一天需要加上1
			endDate = this.paramMap.get("endDate");
			String startDate = year+"-01-01";
			int heldDays = Common.getDaysBetweenTwoDate(startDate, endDate);
			System.out.println("持有时间为:"+heldDays);
			v = (heldDays+1) * Common.stringToDouble(amount) * Common.stringToDouble(interestRate)/Common.getYearDays(year);
		}else if(index ==0) {
			//第一年
			System.out.println("第一年："+year);
			String startDate = this.paramMap.get("startDate");
			endDate = year+"-12-31";
			int heldDays = Common.getDaysBetweenTwoDate(startDate, endDate);
			System.out.println("持有时间为:"+heldDays);
			v = heldDays * Common.stringToDouble(amount) * Common.stringToDouble(interestRate)/Common.getYearDays(year);

		}else {
			System.out.println("中间年："+year);
			v =Common.stringToDouble(amount) * Common.stringToDouble(interestRate);
		}
		v = v /100;
		return v;
	}
	
	public double getSameYearValue() {
		String startDate = this.paramMap.get("startDate");
		String endDate = this.paramMap.get("endDate");
		int startYearsDays = Common.getYearDays(Common.stringToInt(startDate.trim().split("-")[0]));
		int endYearsDays = Common.getYearDays(Common.stringToInt(endDate.trim().split("-")[0]));
		
		String amount = this.paramMap.get("amount");
		String interestRate = this.paramMap.get("interestRate");
		int heldDays = Common.getDaysBetweenTwoDate(startDate, endDate);

		System.out.println("startYearsDays:"+startYearsDays);
		System.out.println("endYearsDays:"+endYearsDays);
		System.out.println("heldDays:"+heldDays);
		double v = heldDays * Common.stringToDouble(amount) * Common.stringToDouble(interestRate)/endYearsDays;
		System.out.println("持有时间为:"+heldDays);
		//System.out.println(v);
		double result = v / 100;
		System.out.println("result: "+result);
		result = Common.get_double_round(result);
		System.out.println("result: "+result);
		return result;
	}
	
}
