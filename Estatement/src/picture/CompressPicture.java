package picture;

import java.io.File;
import java.io.FileOutputStream;
import net.coobird.thumbnailator.Thumbnails;

public class CompressPicture {
	public static void main(String[] args) {
		String filePath="D:\\temp\\sreenshot";
		double compressPer=0.4f;
		
		try {
			changeByDir(filePath,compressPer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static long getFileSize(String filename) {
		File file = new File(filename);
		if (!file.exists() || !file.isFile()) {
			System.out.println("文件不存在");
			return -1;
		}
		return file.length();
	}

	public static void compressPic(String filePath,String descFileDIr,double compressPer) throws Exception{
		String filename ="";
		String osName = System.getProperty("os.name");
		if (osName.startsWith("Windows")) {
			filename =filePath.substring(filePath.lastIndexOf("\\")+1);
		} else {
			filename =filePath.substring(filePath.lastIndexOf("/")+1);
		}
		System.out.println("filename: "+filename);
		Thumbnails.of(new File(filePath))
				.scale(compressPer) //鍥剧墖澶у皬锛堥暱瀹斤級鍘嬬缉 浠�0鎸夌収
				.outputQuality(1f) //鍥剧墖璐ㄩ噺鍘嬬缉姣斾緥 浠�0-1锛岃秺鎺ヨ繎1璐ㄩ噺瓒婂ソ
				.toOutputStream(new FileOutputStream(descFileDIr+filename));
	}


	public static String changeByDir(String dirPath, double compressPercent){
		File dir = new File(dirPath);
		if (!dir.isDirectory()) {
			System.err.println("is not a dir");
			return "";
		}
		File[] fileList = dir.listFiles();
		String outputFileDir = dir.getAbsolutePath() + "/out/";
		File outputFile = new File(outputFileDir);
		if (!outputFile.exists() && !outputFile.isDirectory()) {
			outputFile.mkdir();
		}
		for (int i = 0; i <fileList.length;i++) {
			File file = fileList[i];
			if(!isStringInCaseList(file.getName())){
				continue;
			}
			if (file.getName().endsWith(".png")||file.getName().endsWith(".jpg")) {
				try{
					compressPic(file.getAbsolutePath(),outputFileDir,compressPercent);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return outputFileDir;
	}

	public static boolean isStringInCaseList(String data){
		boolean result = true;
		return result;
	}

}
