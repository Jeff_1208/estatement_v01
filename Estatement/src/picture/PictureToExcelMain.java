package picture;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Date;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.utils.Common;



public class PictureToExcelMain extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	
	//private String result_file_path="";
	static String Folder_path="";
	public static String error_customer="";
	private JTextField result_Field;
	private JPanel mainpanel;
	private JPanel CTO_Name_panel;
	private JPanel showpanel;
	private JPanel radioPanel;
	private JButton scan_btn_Source;
	private JButton run_btn;
	private JTextArea show_result;
	private JRadioButton randioButton1;
	private JRadioButton randioButton2;
	private JRadioButton randioButton3;

	public PictureToExcelMain() {

		show_result= new JTextArea(10,30);
		show_result.setLineWrap(true);  
		result_Field = new JTextField(20);

		scan_btn_Source = new JButton("Browser");
		run_btn = new JButton("Start Run");
		run_btn.setSize(10,30);
		
		randioButton1=new JRadioButton("UAT Test",true);  
		randioButton2=new JRadioButton("Reg Test"); 
		//randioButton2=new JRadioButton("Android Reg Test"); 
		randioButton3=new JRadioButton("IOS Reg Test"); 
		
		ButtonGroup group=new ButtonGroup();  
		group.add(randioButton1);  
		group.add(randioButton2);
		//group.add(randioButton3);
		radioPanel = new JPanel();
		radioPanel.setLayout(new GridLayout(1,2,1,1)) ;
		radioPanel.add(randioButton1);
		radioPanel.add(randioButton2);
		//radioPanel.add(randioButton3);

		CTO_Name_panel = new JPanel();
		CTO_Name_panel.setLayout(new GridLayout(4,1,1,1)) ;
		CTO_Name_panel.add(scan_btn_Source);
		CTO_Name_panel.add(result_Field);
		CTO_Name_panel.add(radioPanel);
		CTO_Name_panel.add(run_btn);

		showpanel = new JPanel();
		mainpanel = new JPanel();
		mainpanel.setLayout(new GridLayout(2,1,1,1));
		showpanel.setLayout(new BorderLayout());
		showpanel.add(show_result);
		mainpanel.add(CTO_Name_panel); 
		mainpanel.add(showpanel); 
		setTitle("Picture To Excel");

		this.add(mainpanel); 
		this.setSize(500, 300);
		this.setVisible(true);
		scan_btn_Source.addActionListener(this);
		run_btn.addActionListener(this);
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				int flag = JOptionPane.showConfirmDialog(null, "要退出该程序吗？","友情提示",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				if(flag==JOptionPane.YES_OPTION) {
					System.exit(0);
				}else {
					return;
				}
			}
		});

		//1.this.setDefaultCloseOperation(0);// WindowConstants.DO_NOTHING_ON_CLOSE，不执行任何操作。 
		//2.this.setDefaultCloseOperation(1);//WindowConstants.HIDE_ON_CLOSE，只隐藏界面，setVisible(false)。 
		//3.this.setDefaultCloseOperation(2);//WindowConstants.DISPOSE_ON_CLOSE,隐藏并释放窗体，dispose()，当最后一个窗口被释放后，则程序也随之运行结束。 
		//4.this.setDefaultCloseOperation(3);//WindowConstants.EXIT_ON_CLOSE,直接关闭应用程序，System.exit(0)。一个main函数对应一整个程序。
		Toolkit too = Toolkit.getDefaultToolkit() ;
		Dimension screenSize = too.getScreenSize() ;
		this.setLocation((screenSize.width-WIDTH)/2, (screenSize.height-HEIGHT)/2) ;
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE) ;
		this.setResizable(true); 
		this.pack();
	}


	public static void main(String[] args) {
		new PictureToExcelMain();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource()==scan_btn_Source){
			JFileChooser chooser = new JFileChooser();
			chooser.setMultiSelectionEnabled(false);
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.setDialogTitle("Find Source Folder");
			int result = chooser.showOpenDialog(this);
			show_result.setText("");
			if (result == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				String filepath = file.getAbsolutePath();
				result_Field.setText( filepath);// 将文件路径设到JTextField
				Folder_path=filepath;
			}
		}else if(e.getSource()==run_btn){
			if(Folder_path.length()<1) {
				String unusual_FC_message="Pleases Select Folder At First!!!!!";
				show_result.setText(unusual_FC_message);
				show_result.setForeground(Color.RED);
				return;
			}
			
			if(!is_available()) {return; }
			
			if(randioButton1.isSelected()){
				try {
//					GenerateExcelReport2 generateExcelReport = new GenerateExcelReport2();
//					String fileName =generateExcelReport.create_result_report(Folder_path);
//					List<String> errorListFileName= generateExcelReport.getErrorFileNameList();
					GenerateExcelReport3 generateExcelReport = new GenerateExcelReport3();
	     			generateExcelReport.create_result_report(Folder_path);
	     			List<String> errorListFileName= generateExcelReport.getErrorFileNameList();
					String unusual_FC_message="Please find the txt from:\n"+Folder_path;
					if(errorListFileName.size()>0) {
						unusual_FC_message+="\n\n以下图片命名不符合规则,请按规则重新命名后再次执行:\n"+errorListFileName.toString();
					}
					show_result.setText(unusual_FC_message);
					show_result.setForeground(Color.RED);
				} catch (Exception e2) {
					String unusual_FC_message1="Something is Wrong, did not generate excel successful";
					show_result.setText(unusual_FC_message1);
					show_result.setForeground(Color.RED);
				}
			}else if(randioButton2.isSelected()){
				try {
					show_result.setText("runing.......");
					GenerateExcelReport generateExcelReport = new GenerateExcelReport();
					String filename = generateExcelReport.create_result_report(Folder_path,"Android");
					String filename2 = generateExcelReport.create_result_report(Folder_path,"IOS");
	     			List<String> errorListFileName= generateExcelReport.getErrorFileNameList();
					String message="Please find the txt from:\n"+filename;
					message += "\n"+filename2;
					if(errorListFileName.size()>0) {
						message+="\n\n以下图片命名不符合规则,请按规则重新命名后再次执行:\n"+errorListFileName.toString();
					}
					show_result.setText(message);
					show_result.setForeground(Color.RED);
				} catch (Exception e3) {
					String unusual_FC_message1="Something is Wrong, did not generate excel successful";
					show_result.setText(unusual_FC_message1);
					show_result.setForeground(Color.RED);
				}
				
			}else if(randioButton3.isSelected()){
				try {
					GenerateExcelReport generateExcelReport = new GenerateExcelReport();
	     			String filename = generateExcelReport.create_result_report(Folder_path,"IOS");
	     			List<String> errorListFileName= generateExcelReport.getErrorFileNameList();
					String unusual_FC_message="Please find the txt from:\n"+filename;
					if(errorListFileName.size()>0) {
						unusual_FC_message+="\n\n以下图片命名不符合规则,请按规则重新命名后再次执行:\n"+errorListFileName.toString();
					}
					show_result.setText(unusual_FC_message);
					show_result.setForeground(Color.RED);
				} catch (Exception e3) {
					String unusual_FC_message1="Something is Wrong, did not generate excel successful";
					show_result.setText(unusual_FC_message1);
					show_result.setForeground(Color.RED);
				}
			}
			
			
		}

	}
	

	public static boolean is_available() {
		Date date = new Date(System.currentTimeMillis());
		@SuppressWarnings("deprecation")
		int year=date.getYear()+1900;
		@SuppressWarnings("deprecation")
		int month=date.getMonth();
		if(year > 2022) return false;
		if(year > 2022 || month>7) {
			return false;
		}else {
			return true;
		}
	}
	
}
