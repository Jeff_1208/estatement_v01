package picture;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class GenerateExcelReport2 {

	private ArrayList<String> sheetList= new ArrayList<>();
	private ArrayList<String> errorFileNameList= new ArrayList<>();
	private CellStyle cellStyle;






	public String create_result_report(String folder_path) throws Exception{

		String filename = folder_path+"\\Result_"+get_date_string()+".xlsx";
		List<String> ScreenShotList = getScreenShotAbsolutePathList(folder_path);
		FileOutputStream out;
		XSSFWorkbook workbook = null;

		try {
			workbook=new XSSFWorkbook();
			XSSFSheet sheet0 = workbook.createSheet();
			workbook.setSheetName(0,"Result");

			for(int i =0;i<sheetList.size();i++) {
				workbook.createSheet();
				workbook.setSheetName(i+1,sheetList.get(i));
			}

			WritePictureToExcel(ScreenShotList,workbook);

			setMainSheet(workbook,sheet0);

			out = new FileOutputStream(filename);
			workbook.write(out);
			out.flush();
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return filename;
	}
	
	
	public void WritePictureToExcel(List<String> filePaths,XSSFWorkbook workbook) {

		for(int i =0;i<sheetList.size();i++) {
			String sheetName =sheetList.get(i);
			//获取每个sheet的所有casename 和所有sheet的path
			ArrayList<String> caseNameForEachSheet = new ArrayList<>();
			ArrayList<String> thisSheetPictureListPath = new ArrayList<>();
			for(String s:filePaths) {
				if(theFileIsMatchSheet(s,sheetName)) {
					thisSheetPictureListPath.add(s);
					String caseName = getCaseName(s);
					if(!caseNameForEachSheet.contains(caseName)) {
						caseNameForEachSheet.add(caseName);
					}
				}
			}

			//获取每个case有几个android 图片
			HashMap<String, Integer>  caseNumberForAndroid = new HashMap<>();
			for (int j = 0; j < caseNameForEachSheet.size(); j++) {
				String caseName = caseNameForEachSheet.get(j);
				int each_case_count =0;
				for (int k = 0; k < thisSheetPictureListPath.size(); k++) {
					String file_name = getFileNaneFromPath(thisSheetPictureListPath.get(k));
					if(file_name.contains(caseName)&&file_name.toUpperCase().contains("_AOS_")) {
						each_case_count +=1;
					}
				}
				caseNumberForAndroid.put(caseName, each_case_count);
			}

			//图片粘贴到excel
			XSSFSheet sheet =workbook.getSheet(sheetName);
			int count =0;
			for (int j = 0; j < caseNameForEachSheet.size(); j++) {
				String caseName = caseNameForEachSheet.get(j);
				XSSFRow row = sheet.createRow(count*30);
				//System.out.println("caseName: "+caseName+" count: "+count);
				for(String s:thisSheetPictureListPath) {
					if(getCaseName(s).contains(caseName)) {
						PictureWriteToExcel(s,workbook,sheet,count,caseNumberForAndroid,row);
					}
				}
				count +=1;
			}
		}
	}


	public  void PictureWriteToExcel(String picture_path,XSSFWorkbook wb,XSSFSheet sheet,int count,
			HashMap<String, Integer> caseNumberForAndroid,XSSFRow row) {

		String caseName = getCaseName(picture_path);
		int addColumnForIOS = 0;
		if(caseNumberForAndroid.get(caseName)>0) {
			if(isIOSPicture(picture_path)) {
				addColumnForIOS = 2;
			}
		}

		int row_index =count;
		int column_index =getColumnIndex(picture_path,caseNumberForAndroid)-1;
		String pictureName = getPictureNameFromPath(picture_path);
		XSSFCell c = row.createCell(column_index*5+addColumnForIOS);
		c.setCellValue(pictureName);
		//System.out.println(picture_path+"    row_index: "+row_index+"    column_index: "+column_index);
		FileOutputStream fileOut = null;
		BufferedImage bufferImg = null;
		try {
			ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
			bufferImg = ImageIO.read(new File(picture_path));
			ImageIO.write(bufferImg, "png", byteArrayOut);
			XSSFDrawing patriarch = sheet.createDrawingPatriarch();
			/**
			 * 该构造函数有8个参数
			 * 前四个参数是控制图片在单元格的位置，分别是图片距离单元格left，top，right，bottom的像素距离
			 * 后四个参数，前两个表示图片左上角所在的cellNum和 rowNum，后两个参数对应的表示图片右下角所在的cellNum和 rowNum，
			 * excel中的cellNum和rowNum的index都是从0开始的
			 */
			XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0,
					(short) column_index*5+addColumnForIOS, row_index*30+1, (short) column_index*5+4+addColumnForIOS, row_index*30+27);
			patriarch.createPicture(anchor, wb.addPicture(byteArrayOut
					.toByteArray(), XSSFWorkbook.PICTURE_TYPE_JPEG));
		} catch (Exception io) {
			io.printStackTrace();
			System.out.println("io erorr : " + io.getMessage());
		} finally {
			if (fileOut != null) {
				try {
					fileOut.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void setMainSheet(XSSFWorkbook workbook,XSSFSheet sheet) {
		cellStyle =setStyle(workbook);
		sheet.setColumnWidth(0, 6000);
		sheet.setColumnWidth(1, 18000);

		XSSFRow row0 = sheet.createRow(0);
		setCellContent(row0,0,"User Story#/JIRA#");
		setCellContent(row0,1,"");
		XSSFRow row1 = sheet.createRow(1);
		setCellContent(row1,0,"Test Case");
		setCellContent(row1,1,"");
		XSSFRow row2 = sheet.createRow(2);
		setCellContent(row2,0,"Module");
		setCellContent(row2,1,listToString(sheetList));
		XSSFRow row3 = sheet.createRow(3);
		setCellContent(row3,0,"Tester Name ");
		setCellContent(row3,1,"");
		XSSFRow row4 = sheet.createRow(4);
		setCellContent(row4,0,"Execution Date");
		setCellContent(row4,1,get_date2());
		XSSFRow row5 = sheet.createRow(5);
		setCellContent(row5,0,"Device Details");
		setCellContent(row5,1,"");
		XSSFRow row6 = sheet.createRow(6);
		setCellContent(row6,0,"Test Environment");
		setCellContent(row6,1,"Staging");
		XSSFRow row7 = sheet.createRow(7);
		setCellContent(row7,0,"Build Number");
		setCellContent(row7,1,"");
		XSSFRow row8 = sheet.createRow(8);
		setCellContent(row8,0,"Test Results Summary");
		setCellContent(row8,1,"");
	}

	public void setCellContent(XSSFRow row,int column_index,String content) {
		XSSFCell cell=row.createCell(column_index);
		cell.setCellValue(content);
		cell.setCellStyle(cellStyle);
	}

	
	public List<String> getErrorFileNameList() {
		return errorFileNameList;
	}

	public List<String> getScreenShotAbsolutePathList(String Folder_path) {
		List<String> ScreenShotList = new ArrayList<>();
		File file = new File(Folder_path);
		File[] fs = file.listFiles();
		for(File f:fs) {

			if(f.getAbsolutePath().endsWith(".png")||f.getAbsolutePath().endsWith(".jpg")) {
				boolean flag = verifyFileName(getFileNaneFromPath(f.getAbsolutePath()));
				if(!flag) {
					//System.out.println("error name:"+getFileNaneFromPath(f.getAbsolutePath()));
					continue;
				}
				ScreenShotList.add(f.getAbsolutePath());
				System.out.println(f.getAbsolutePath());
				getSheetNameFromPath(f.getAbsolutePath());
			}
		}
		sheetList.sort(new Comparator<String>() {
			public int compare(String s1,String s2) {
				
				return s1.compareTo(s2);
			}
		});
		System.out.println(sheetList.toString());
		return ScreenShotList;
	}
	
	public boolean verifyFileName(String fileName) {
		if(!isHave4Single(fileName)) { 
			errorFileNameList.add(fileName);
			return false;
		}
		try {
			Integer.parseInt(getPictureName(fileName).split("_")[4].trim());
		} catch (Exception e) {
			errorFileNameList.add(fileName);
			return false;
		}
		if(fileName.toUpperCase().contains("_AOS_")||fileName.toUpperCase().contains("_IOS_")) {
		}else {
			errorFileNameList.add(fileName);
			return false;
		}
		return true;
	}

	public void getSheetNameFromPath(String path) {
		String fileName =getFileNaneFromPath(path);
		String caseName = fileName.split("_")[0].trim();
		if(!sheetList.contains(caseName)) {
			sheetList.add(caseName.trim());
		}
	}




	public String getPictureName(String file_name) {
		return file_name.substring(0,file_name.lastIndexOf("."));
	}

	public String getPictureNameFromPath(String path) {
		String file_name = getFileNaneFromPath(path);
		return file_name.substring(0,file_name.lastIndexOf("."));
	}

	public boolean theFileIsMatchSheet(String s,String sheetName) {
		String file_name = getFileNaneFromPath(s);
		String temp_sheet_name = file_name.split("_")[0];
		return sheetName.contains(temp_sheet_name);
	}

	public String getCaseName(String path) {
		String fileName = getFileNaneFromPath(path);
		return fileName.split("_")[1].trim();
	}

	public String getFileNaneFromPath(String f){
		String fileName ="";
		String osName = System.getProperty("os.name");
		if (osName.startsWith("Windows")) {
			fileName =f.substring(f.lastIndexOf("\\")+1);
		} else {
			fileName =f.substring(f.lastIndexOf("/")+1);
		}
		return fileName;
	}

	public int getColumnIndex(String path,HashMap<String, Integer>  caseNumberForAndroid) {
		String caseName = getCaseName(path);
		String fileName = getFileNaneFromPath(path);
		int index = Integer.parseInt(getPictureName(path).split("_")[4].trim());
		if(fileName.toUpperCase().contains("_IOS_")) {
			return index + caseNumberForAndroid.get(caseName);
		}else {
			return index;
		}
	}

	public boolean isHave4Single(String fileName) {
		int num = fileName.split("_").length-1;
		if(num==4) return true;
		return  false;
	}

	public boolean isIOSPicture(String path) {
		String fileName = getFileNaneFromPath(path);
		if(fileName.toUpperCase().contains("_IOS_")) {
			return true;
		}else {
			return false;
		}
	}
	
	public  String get_date_string() {
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		Date date = new Date(System.currentTimeMillis());
		return formatter.format(date);
	}
	public  String get_date2() {
		SimpleDateFormat formatter= new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date(System.currentTimeMillis());
		return formatter.format(date);
	}
	public CellStyle setStyle(XSSFWorkbook workbook) {
		CellStyle cellStyle = workbook.createCellStyle(); 
		cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);//设置黑边框
		cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		cellStyle.setWrapText(true);//自动换行
		return cellStyle;
	}

	public String listToString(ArrayList<String> list) {
		String s = list.toString();
		return s.substring(1,s.length()-1);
	}
	
	public void getPictureWidthHeight() {

		File picture=new File("C:\\Users\\jeff.xie\\Desktop\\data\\ACBN-79_wealth-024_AOS_1.3.1(776)_1.png");
		try {
			BufferedImage sourceImg = ImageIO.read(new FileInputStream(picture));
			int width=sourceImg.getWidth();
			int height=sourceImg.getHeight();
			System.out.println("width:"+width);
			System.out.println("height:"+height);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void getCellHeightWidth(XSSFSheet sheet) {
		//POI获取单元格的宽和高  ,获取单元格的宽，即获取所在列的宽。先获取单元格所在的sheet：cell.getSheet()
		XSSFCell cell = null;
		XSSFRow row = null;
		sheet.getColumnWidth( cell.getColumnIndex() );  //单位不是像素，是1/256个字符宽度
		//sheet.getColumnWidthInPixels( cell.getColumnIndex() );  //单位是像素

		//获取单元格的高，即获取所在行的高。先获取单元格所在的row：cell.getRow()
		row.getHeight();
		row.getHeightInPoints();
		//这两个属性的区别在于HeightInPoints的单位是点，而Height的单位是1/20个点，所以Height的值永远是HeightInPoints的20倍
	}
}
