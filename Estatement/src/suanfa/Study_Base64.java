package suanfa;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Base64;

public class Study_Base64 {

	
	public static void main(String[] args) {
		String line1 = MessageFormat.format("(request-target): {0} {1}", "get", "mapi.sta-wlab.com/v1/onboarding-requests/notification/registration");
		System.out.println(line1);
		String message =String.join("\n", line1, "line2", "line3", "line4");
		System.out.print("vvvv"+message);
		System.out.println("KKKKKKKKK");
		Base64_encryption();
	}
	

    private static final Base64.Decoder DECODE_64 = Base64.getDecoder();
    private static final Base64.Encoder ENCODE_64 = Base64.getEncoder();
 
    public static void Base64_encryption() {
        String text = "python";
        try {
            // 编码
            String encodedToStr = ENCODE_64.encodeToString(text.getBytes("UTF-8"));
            System.out.println("encodedToStr = " + encodedToStr);
            
            
            //这种加密方法对应python中的以下加密方式
            //import base64
            //r =str.encode()
            //r1 =base64.b64encode(r)   bytes类型
            //r2 =base64.b64decode(data) data为bytes类型
            //r2.decode()解码成字符串
            
            // 解码
            String byteToText = new String(DECODE_64.decode(encodedToStr), "UTF-8");
            System.out.println("byteToText = " + byteToText);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
