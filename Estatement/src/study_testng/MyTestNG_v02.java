//package study_testng;
//
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//import org.testng.Assert;
//import org.testng.annotations.*;
//
//
////@Listeners({TestReport.class})用于生产测试报告，测试报告路径为当前项目的test-output文件夹中
//@Listeners({TestReport.class})
//public class MyTestNG_v02 {
////	@BeforeSuite > @BeforeTest > @BeforeMethod > @Test > @AfterMethod > @AfterTest > @AfterSuite
//
//
//	// 创建获取参数的方法
//	@DataProvider 
//	public Object[][] getParam() throws Exception{ 
//		Object data[][]={ {"London","Paris"},{"Denver","London"},{"Paris","Denver"} }; 
//		return data; 
//	}
//	//依次从返回值中取值，返回值有几个数据，那么就会有几个测试用例
//	@Test(description="订票",priority=4,dataProvider="getParam") 
//	public void book(String cong , String dao){ 
//		System.out.println("leaves "+cong+" for "+dao);
//	}
//
//	@Test(description="login1",priority = 1)
//	public void  login1() {
//		System.out.println("hello junit1");
//	}
//
//
//	@Test(description="login2",priority = 2)
//	public void  login2() {
//		System.out.println("hello junit2");
//		Assert.assertTrue(true);
//	}
//
//	@Test(description="login3",priority = 3, enabled =false)
//	public void  login3() {
//		System.out.println("hello junit3");
//		Assert.assertEquals("", "");
//
//	}
//
//	@Test(description="login4",priority = 4)
//	public void  login4() {
//		System.out.println("第二个测试套件");
//		Assert.assertTrue(true);
//	}
//	
//	@BeforeClass
//	public void  start() {
//		System.out.println("hello start");
//	}
//
//	@AfterClass
//	public void  end() {
//		System.out.println("hello end");
//	}
//
//}
