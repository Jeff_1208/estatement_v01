//package study_testng;
//
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileReader;
//
//import org.testng.Assert;
//import org.testng.annotations.*;
//
//
////eclise 需要安装testng插件
////Eclipse在线安装testNG步骤：Eclipse顶部菜单栏中点击Help，然后选择eclipse Marketplace,搜索testng，然后默认安装
//
//
////@Listeners({TestReport.class})用于生产测试报告，测试报告路径为当前项目的test-output文件夹中
//@Listeners({TestReport.class})
//public class MyTestNG_v01 {
////	@BeforeSuite > @BeforeTest > @BeforeMethod > @Test > @AfterMethod > @AfterTest > @AfterSuite
//
//	@DataProvider 
//	public Object[][] getParam1() throws Exception{ 
//		File file=new File("C:\\...\\flights.txt"); //打开文件，计算文件行数 
//		FileReader bytes=new FileReader(file); 
//		BufferedReader chars=new BufferedReader(bytes); 
//		String row=null; 
//		int lineCount=0; 
//		while((row=chars.readLine())!=null)
//			lineCount++; 
//		chars.close();
//		//重新打开文件，读取每行 
//		bytes=new FileReader(file); 
//		chars=new BufferedReader(bytes); 
//		Object[][] data=new Object[lineCount][]; 
//		int i=0;
//
//		while((row=chars.readLine())!=null){
//			String cols[]=row.split("\t"); 
//			data[i++]=cols; 
//		}
//
//		//测试查看每行 
//		/*
//		 * for(Object x[]:data){ 
//		 * for(Object y:x) System.out.print(y+" "); 
//		 * System.out.println(); }
//		 */ 
//		chars.close(); 
//		return data; 
//	}
//
//	// 创建获取参数的方法
//	@DataProvider 
//	public Object[][] getParam() throws Exception{ 
//		Object data[][]={ {"London","Paris"},{"Denver","London"},{"Paris","Denver"} }; 
//		return data; 
//	}
//	//依次从返回值中取值，返回值有几个数据，那么就会有几个测试用例
//	@Test(description="订票",priority=4,dataProvider="getParam") 
//	public void book(String cong , String dao){ 
//		System.out.println("leaves "+cong+" for "+dao);
//	}
//
//	@Test(description="login1",priority = 1)
//	public void  login1() {
//		System.out.println("hello junit1");
//	}
//
//
//	@Test(description="login2",priority = 2)
//	public void login2() {
//		System.out.println("hello junit2");
//		Assert.assertTrue(true);
//	}
//
//	@Test(description="login3",priority = 3, enabled =false)
//	public void  login3() {
//		System.out.println("hello junit3");
//		Assert.assertEquals("", "");
//
//	}
//	
////	@Parameters({"param"})  //该参数在xml文件中定义
////	@Test(description="login5",priority = 5)
////	public void login5(String param) {
////		System.out.println("param: "+param);
////
////	}
//
//	@BeforeClass
//	public void  start() {
//		System.out.println("hello start");
//	}
//
//	@AfterClass
//	public void  end() {
//		System.out.println("hello end");
//	}
//	
//}
//
//***/
