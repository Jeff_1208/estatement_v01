package webui;

import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.KeyFrameIntervalKey;
import static org.monte.media.FormatKeys.MIME_AVI;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.FormatKeys.MimeTypeKey;
import static org.monte.media.VideoFormatKeys.CompressorNameKey;
import static org.monte.media.VideoFormatKeys.DepthKey;
import static org.monte.media.VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE;
import static org.monte.media.VideoFormatKeys.QualityKey;

import java.awt.AWTException;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.monte.media.Format;
import org.monte.media.FormatKeys.MediaType;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class webdriverdemo {
    private static ScreenRecorder screenRecorder;
    //ScreenRecorder 官方地址，可以下载源码
    //http://www.randelshofer.ch/monte/index.html
    
    //ScreenRecorde封装代码如下链接所示    
    //https://www.cnblogs.com/conquerorren/p/11848782.html

    public static void main(String[] args) throws IOException, AWTException {
        GraphicsConfiguration gconfig = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        screenRecorder = new ScreenRecorder(gconfig, new Format(MediaTypeKey,
                MediaType.FILE, MimeTypeKey, MIME_AVI), new Format(
                MediaTypeKey, MediaType.VIDEO, EncodingKey,
                ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE, CompressorNameKey,
                ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE, DepthKey, (int) 24,
                FrameRateKey, Rational.valueOf(15), QualityKey, 1.0f,
                KeyFrameIntervalKey, (int) (15 * 60)), new Format(MediaTypeKey,
                MediaType.VIDEO, EncodingKey, "black", FrameRateKey,
                Rational.valueOf(30)), null);

        WebDriver driver = new ChromeDriver();
        

        // 开始捕获视频
        screenRecorder.start();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.navigate().to("https://www.baidu.com/");

        driver.manage().window().maximize();

        for (int i = 0; i < 3; i++) {
            driver.findElement(By.id("kw")).sendKeys("selenium", Keys.ENTER);
            driver.navigate().forward();
            driver.navigate().back();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        FileUtils.copyFile(screenshot, new File("D:/screenshotsscreenshots1.jpg"));
        System.out.println(screenRecorder.getStartTime());
        
        String vedioPath = "";
        //默认录制的视频保存在“C:users<<UserName>>Videos”文件夹，如下图所示。
        //获取视频所在位置，这里只有一个视频
        List<File> ll= screenRecorder.getCreatedMovieFiles();
        for (int i = 0; i < ll.size(); i++) {
        	System.out.println(ll.get(i).getAbsolutePath());
        	vedioPath = ll.get(i).getAbsolutePath();
		}
        
        // 停止捕获视频
        screenRecorder.stop();
        //复制视频到固定位置
        FileUtils.copyFile(new File(vedioPath), new File("D:/111.mp4"));
        System.out.println("Finish!!!!!");
    }
    
    public static void copyFile(File srcFile, File destFile, boolean preserveFileDate) throws IOException {
        if (srcFile.isDirectory()) {
            throw new IOException("Source '" + srcFile + "' exists but is a directory");
        } else if (srcFile.getCanonicalPath().equals(destFile.getCanonicalPath())) {
            throw new IOException("Source '" + srcFile + "' and destination '" + destFile + "' are the same");
        } else {
            File parentFile = destFile.getParentFile();
            if (parentFile != null && !parentFile.mkdirs() && !parentFile.isDirectory()) {
                throw new IOException("Destination '" + parentFile + "' directory cannot be created");
            } else if (destFile.exists() && !destFile.canWrite()) {
                throw new IOException("Destination '" + destFile + "' exists but is read-only");
            } else {
                doCopyFile(srcFile, destFile, preserveFileDate);
            }
        }
    }
    private static void doCopyFile(File srcFile, File destFile, boolean preserveFileDate) throws IOException {
        if (destFile.exists() && destFile.isDirectory()) {
            throw new IOException("Destination '" + destFile + "' exists but is a directory");
        } else {
            Path srcPath = srcFile.toPath();
            Path destPath = destFile.toPath();
            long newLastModifed = preserveFileDate ? srcFile.lastModified() : destFile.lastModified();
            Files.copy(srcPath, destPath, StandardCopyOption.REPLACE_EXISTING);
            checkEqualSizes(srcFile, destFile, Files.size(srcPath), Files.size(destPath));
            checkEqualSizes(srcFile, destFile, srcFile.length(), destFile.length());
            destFile.setLastModified(newLastModifed);
        }
    }
    
    private static void checkEqualSizes(File srcFile, File destFile, long srcLen, long dstLen) throws IOException {
        if (srcLen != dstLen) {
            throw new IOException("Failed to copy full contents from '" + srcFile + "' to '" + destFile + "' Expected length: " + srcLen + " Actual: " + dstLen);
        }
    }
}
