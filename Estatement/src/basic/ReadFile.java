package basic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


public class ReadFile {

	public static void main(String[] args) {
		String path = "D:\\Spec_REF.txt";
		readFile(path);
	}
	
	
	public static void readFile(String path) {
		File f = new File(path);
		FileReader fileReader;
		try {
			fileReader = new FileReader(f);
			@SuppressWarnings("resource")
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			int lineCount=0;
			String line="";
			while((line=bufferedReader.readLine())!=null) {
				lineCount++;
				System.out.println(line);
			}
			fileReader.close();
			System.out.println(lineCount);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
