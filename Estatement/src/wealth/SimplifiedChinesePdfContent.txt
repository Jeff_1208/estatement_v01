wealth.getCustomerID(): 8000030400-1
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
sta00076 wang 大文
RM RM4,13/F
sampleRisk82
728 Kings Road
Central
Hong Kong
账户摘要 Account Summary
账户详情 Account Details
 期初余额 期末余额 期末余额 (港币等值)
 Value on 01 Mar 2024 Value on 31 Mar 2024 Value on 31 Mar 2024 (HKD equivalent)
智动投资顾问 Digital Wealth Advisory
建立投資習慣2.9 (1) USD 1,119.51 USD 1,131.50 HKD 8,855.40
向財務自由出發4.18 USD 1,357.20 USD 1,387.50 HKD 10,858.92
Build My Investing Habit4.24 USD 283.49 USD 289.24 HKD 2,263.66
Achieve Financial Freedom4.21 USD 1,782.75 USD 1,811.33 HKD 14,175.92
Reach My Target2.10 USD 2,157.83 USD 2,195.54 HKD 17,182.84
建立投資習慣 USD 774.92 USD 790.05 HKD 6,183.13
Reach My Target4.18 (1) USD 394.82 USD 402.09 HKD 3,146.86
Build My Investing Habit4.14 USD 2,667.12 USD 2,685.37 HKD 21,016.38
達成我的目標太苦了 USD 1,194.05 USD 1,213.40 HKD 9,496.37
Achieve Financial Freedom4.18 USD 38,710.74 USD 38,850.10 HKD 304,050.60
Reach My Target4.23 USD 397.09 USD 404.43 HKD 3,165.17
達成我的目標2.4 USD 2,115.20 USD 2,149.21 HKD 16,820.25
Build My Investing Habit4.23 USD 88.34 USD 89.78 HKD 702.64
Build My Investing Habit4.18 (1) USD 3,744.73 USD 3,813.69 HKD 29,846.89
Build My Investing Habit2.10 USD 885.31 USD 900.81 HKD 7,049.96
達成我的目標2.9 USD 138.55 USD 141.10 HKD 1,104.28
建立投資習慣4.18 USD 318.44 USD 325.24 HKD 2,545.41
Reach My Target4.14 USD 2,692.37 USD 2,742.04 HKD 21,459.89
Reach My Target4.18 USD 286.41 USD 294.25 HKD 2,302.87
Page（页） 1 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
自选基金 Featured Funds
美元 USD USD 7,155.38 USD 7,201.47 HKD 56,360.50
账户总额 Account Total HKD 538,587.94
基金持仓明细 Total Individual Fund Positions
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值)
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 (HKD equivalent)
安联总回报亚洲股票基金 (美元) (累积) 22.2240 22.2240 38.4858 USD 855.31 HKD 6,693.87
Allianz Total Return Asian Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 254.8770 254.8770 12.3466 USD 3,146.86 HKD 24,628.11
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
安联环球成长基金 (美元) (累积) 485.6220 485.6220 11.8459 USD 5,752.63 HKD 45,021.52
Allianz Global Equity Growth (USD)
(Accumulation)
PIMCO亚洲策略收益基金 (美元) (累积) 206.7490 206.7490 9.1500 USD 1,891.75 HKD 14,805.31
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
安联环球机遇债券基金 (美元) (累积) 157.7570 157.7570 9.7622 USD 1,540.06 HKD 12,052.89
Allianz Global Opportunistic Bond (USD)
(Accumulation)
景顺亚洲机遇股票基金 (美元) (累积) 9.2870 9.2870 130.1800 USD 1,208.98 HKD 9,461.78
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
骏利亨德森美国短期债券基金 (美元) (累积) 77.7220 77.7220 18.0900 USD 1,405.99 HKD 11,003.63
Janus Henderson US Short-Term Bond Fund 
(USD)(Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 209.5040 209.5040 33.0100 USD 6,915.73 HKD 54,124.23
Blackrock Global Dynamic Equity (USD)
(Accumulation)
贝莱德亚洲老虎债券基金(美元)(每月派息) 14.4100 14.4100 8.2800 USD 119.31 HKD 933.75
BGF Asian Tiger Bond A6 USD
安联收益及增长基金(美元)(每月派息) 20.5530 20.5530 8.3301 USD 171.21 HKD 1,339.93
Allianz Income and Growth (USD)(Mth 
Distribution)
骏利亨德森平衡基金(美元)(每月派息) 30.1030 30.1030 13.0900 USD 394.05 HKD 3,083.93
Janus Henderson
安联环球机遇债券基金(美元)(每月派息) 173.1270 173.1270 8.4789 USD 1,467.93 HKD 11,488.39
Allianz Global Opportunistic Bond (USD
安联环球人工智能股票基金(美元)(累积) 7.4640 7.4640 26.8720 USD 200.57 HKD 1,569.71
Allianz Global Artificial Intelligence (USD)
(Accumulation)
Page（页） 2 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值)
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 (HKD equivalent)
聯博-短期債券基金BT股美元 11.4440 11.4440 10.7915 USD 123.50 HKD 966.54
Allianz HKD Income (USD)(Accumulation)
贝莱德动力高息基金(美元)(每月派息) 11.1600 11.1600 7.9200 USD 88.39 HKD 691.76
BGF Dynamic High Income A6 USD
景順永續性環球非投資等級債券基金A-固定 11.1200 11.1200 9.9400 USD 110.53 HKD 865.04
月配息股 美元
Invesco Global High Income Fund (USD)(Mth 
Distribu
駿利亨德森資產管理基金 - 駿利亨德森環球 0.4610 0.4610 54.6300 USD 25.18 HKD 197.06
生命科技基金 A2 美元
Janus Henderson Global Life Sciences Fund 
(USD)(Ac
PIMCO GIS Glb Rl Ret E USD Inc 中文 278.7890 278.7890 13.1300 USD 3,660.50 HKD 28,647.99
PIMCO GIS Glb Rl Ret E USD Inc
景順環球高評級企業債券基金A(美元)每年派 19.6790 19.6790 11.3810 USD 223.97 HKD 1,752.85
息股份
Invesco Global Inv Grd CorpBd A USD AD
安联亚洲多元入息基金(美元)(每月派息) 68.1020 68.1020 5.6648 USD 385.78 HKD 3,019.21
Allianz Asian Multi Inc Plus AM USD
PIMCO环球高孳息债券基金 (美元) (累积) 11.0240 11.0240 25.3800 USD 279.79 HKD 2,189.71
PIMCO Global High Yield Bond Fund (USD)
(Accumulation)
贝莱德美元货币基金 (美元) (累积) 228.4100 228.4100 170.0893 USD 38,850.10 HKD 304,050.60
Blackrock US Dollar Reserve Fund (USD)
(Accumulation)
本部分提供投资账户（包括智动投资顾问与自选基金）的相关详情。各项服务的交易记录与持仓详情，请查阅相关部分。
This section provides you details of your investment account covering Digital Wealth Advisory and Featured Funds Services. For transaction details and positions of each service, please refer to 
relevant sections.
交易记录与持仓详情 - 智动投资顾问
Detailed Transaction History and Positions – Digital Wealth Advisory 
投资持仓 Investment Holdings
Achieve Financial Freedom4.18  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
贝莱德美元货币基金 (美元) (累积) 228.4100 228.4100 170.0893 USD 38,850.10 HKD 304,050.60
Blackrock US Dollar Reserve Fund (USD)
(Accumulation)
Page（页） 3 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
Achieve Financial Freedom4.21  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
PIMCO亚洲策略收益基金 (美元) (累积) 21.6720 21.6720 9.1500 USD 198.30 HKD 1,551.95
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
安联环球机遇债券基金 (美元) (累积) 9.4760 9.4760 9.7622 USD 92.51 HKD 724.01
Allianz Global Opportunistic Bond (USD)
(Accumulation)
骏利亨德森美国短期债券基金 (美元) (累积) 5.2130 5.2130 18.0900 USD 94.30 HKD 738.02
Janus Henderson US Short-Term Bond Fund 
(USD)(Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 17.1300 17.1300 33.0100 USD 565.46 HKD 4,425.43
Blackrock Global Dynamic Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 24.8650 24.8650 12.3466 USD 307.00 HKD 2,402.66
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
安联环球成长基金 (美元) (累积) 46.7470 46.7470 11.8459 USD 553.76 HKD 4,333.86
Allianz Global Equity Growth (USD)
(Accumulation)
Build My Investing Habit2.10  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
PIMCO亚洲策略收益基金 (美元) (累积) 10.7850 10.7850 9.1500 USD 98.68 HKD 772.29
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
景顺亚洲机遇股票基金 (美元) (累积) 0.6050 0.6050 130.1800 USD 78.76 HKD 616.40
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 8.6800 8.6800 33.0100 USD 286.53 HKD 2,242.46
Blackrock Global Dynamic Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 12.0370 12.0370 12.3466 USD 148.62 HKD 1,163.14
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
安联环球成长基金 (美元) (累积) 24.3310 24.3310 11.8459 USD 288.22 HKD 2,255.68
Allianz Global Equity Growth (USD)
(Accumulation)
Page（页） 4 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
Build My Investing Habit4.14  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
安联环球机遇债券基金 (美元) (累积) 80.5910 80.5910 9.7622 USD 786.75 HKD 6,157.30
Allianz Global Opportunistic Bond (USD)
(Accumulation)
PIMCO环球高孳息债券基金 (美元) (累积) 11.0240 11.0240 25.3800 USD 279.79 HKD 2,189.71
PIMCO Global High Yield Bond Fund (USD)
(Accumulation)
骏利亨德森美国短期债券基金 (美元) (累积) 44.4340 44.4340 18.0900 USD 803.81 HKD 6,290.82
Janus Henderson US Short-Term Bond Fund 
(USD)(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 39.8690 39.8690 12.3466 USD 492.25 HKD 3,852.47
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
PIMCO亚洲策略收益基金 (美元) (累积) 35.2750 35.2750 9.1500 USD 322.77 HKD 2,526.08
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
Build My Investing Habit4.18 (1)  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
安联环球成长基金 (美元) (累积) 100.0370 100.0370 11.8459 USD 1,185.03 HKD 9,274.34
Allianz Global Equity Growth (USD)
(Accumulation)
PIMCO亚洲策略收益基金 (美元) (累积) 31.4470 31.4470 9.1500 USD 287.74 HKD 2,251.93
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
景顺亚洲机遇股票基金 (美元) (累积) 2.7140 2.7140 130.1800 USD 353.31 HKD 2,765.09
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 36.3500 36.3500 33.0100 USD 1,199.91 HKD 9,390.80
Blackrock Global Dynamic Equity (USD)
(Accumulation)
安联总回报亚洲股票基金 (美元) (累积) 9.0480 9.0480 38.4858 USD 348.22 HKD 2,725.26
Allianz Total Return Asian Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 35.5950 35.5950 12.3466 USD 439.48 HKD 3,439.48
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
Page（页） 5 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
Build My Investing Habit4.23  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
PIMCO亚洲策略收益基金 (美元) (累积) 1.0340 1.0340 9.1500 USD 9.46 HKD 74.04
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
安联环球机遇债券基金 (美元) (累积) 0.4470 0.4470 9.7622 USD 4.36 HKD 34.12
Allianz Global Opportunistic Bond (USD)
(Accumulation)
骏利亨德森美国短期债券基金 (美元) (累积) 0.2470 0.2470 18.0900 USD 4.47 HKD 34.98
Janus Henderson US Short-Term Bond Fund 
(USD)(Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 0.8700 0.8700 33.0100 USD 28.72 HKD 224.77
Blackrock Global Dynamic Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 1.1880 1.1880 12.3466 USD 14.67 HKD 114.81
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
安联环球成长基金 (美元) (累积) 2.3720 2.3720 11.8459 USD 28.10 HKD 219.92
Allianz Global Equity Growth (USD)
(Accumulation)
Build My Investing Habit4.24  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
景顺亚洲机遇股票基金 (美元) (累积) 0.4140 0.4140 130.1800 USD 53.89 HKD 421.76
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 2.7900 2.7900 33.0100 USD 92.10 HKD 720.80
Blackrock Global Dynamic Equity (USD)
(Accumulation)
安联总回报亚洲股票基金 (美元) (累积) 1.3800 1.3800 38.4858 USD 53.11 HKD 415.65
Allianz Total Return Asian Equity (USD)
(Accumulation)
安联环球成长基金 (美元) (累积) 7.6090 7.6090 11.8459 USD 90.14 HKD 705.46
Allianz Global Equity Growth (USD)
(Accumulation)
Reach My Target2.10  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
PIMCO亚洲策略收益基金 (美元) (累积) 26.8220 26.8220 9.1500 USD 245.42 HKD 1,920.72
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
Page（页） 6 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
Reach My Target2.10  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
景顺亚洲机遇股票基金 (美元) (累积) 1.5050 1.5050 130.1800 USD 195.92 HKD 1,533.32
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 21.0000 21.0000 33.0100 USD 693.21 HKD 5,425.23
Blackrock Global Dynamic Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 29.9840 29.9840 12.3466 USD 370.20 HKD 2,897.28
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
安联环球成长基金 (美元) (累积) 58.3150 58.3150 11.8459 USD 690.79 HKD 5,406.30
Allianz Global Equity Growth (USD)
(Accumulation)
Reach My Target4.14  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
安联环球成长基金 (美元) (累积) 73.2690 73.2690 11.8459 USD 867.94 HKD 6,792.72
Allianz Global Equity Growth (USD)
(Accumulation)
PIMCO亚洲策略收益基金 (美元) (累积) 22.1280 22.1280 9.1500 USD 202.47 HKD 1,584.58
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
景顺亚洲机遇股票基金 (美元) (累积) 1.8930 1.8930 130.1800 USD 246.43 HKD 1,928.62
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 26.5000 26.5000 33.0100 USD 874.77 HKD 6,846.17
Blackrock Global Dynamic Equity (USD)
(Accumulation)
安联总回报亚洲股票基金 (美元) (累积) 6.2700 6.2700 38.4858 USD 241.31 HKD 1,888.55
Allianz Total Return Asian Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 25.0370 25.0370 12.3466 USD 309.12 HKD 2,419.25
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
Reach My Target4.18  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
贝莱德可持续环球动力股票基金 (美元) (累积) 8.9140 8.9140 33.0100 USD 294.25 HKD 2,302.87
Blackrock Global Dynamic Equity (USD)
(Accumulation)
Page（页） 7 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
Reach My Target4.18 (1)  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
安联环球成长基金 (美元) (累积) 10.7120 10.7120 11.8459 USD 126.89 HKD 993.07
Allianz Global Equity Growth (USD)
(Accumulation)
PIMCO亚洲策略收益基金 (美元) (累积) 3.2460 3.2460 9.1500 USD 29.70 HKD 232.44
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
景顺亚洲机遇股票基金 (美元) (累积) 0.2820 0.2820 130.1800 USD 36.71 HKD 287.30
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 3.8500 3.8500 33.0100 USD 127.09 HKD 994.64
Blackrock Global Dynamic Equity (USD)
(Accumulation)
安联总回报亚洲股票基金 (美元) (累积) 0.9430 0.9430 38.4858 USD 36.29 HKD 284.01
Allianz Total Return Asian Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 3.6780 3.6780 12.3466 USD 45.41 HKD 355.39
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
Reach My Target4.23  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
安联环球成长基金 (美元) (累积) 10.7140 10.7140 11.8459 USD 126.92 HKD 993.31
Allianz Global Equity Growth (USD)
(Accumulation)
PIMCO亚洲策略收益基金 (美元) (累积) 3.2450 3.2450 9.1500 USD 29.69 HKD 232.36
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
景顺亚洲机遇股票基金 (美元) (累积) 0.2820 0.2820 130.1800 USD 36.71 HKD 287.30
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 3.9200 3.9200 33.0100 USD 129.40 HKD 1,012.72
Blackrock Global Dynamic Equity (USD)
(Accumulation)
安联总回报亚洲股票基金 (美元) (累积) 0.9430 0.9430 38.4858 USD 36.29 HKD 284.01
Allianz Total Return Asian Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 3.6790 3.6790 12.3466 USD 45.42 HKD 355.47
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
Page（页） 8 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
向財務自由出發4.18  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
PIMCO亚洲策略收益基金 (美元) (累积) 8.7440 8.7440 9.1500 USD 80.01 HKD 626.18
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
景顺亚洲机遇股票基金 (美元) (累积) 0.5010 0.5010 130.1800 USD 65.22 HKD 510.43
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 27.4300 27.4300 33.0100 USD 905.46 HKD 7,086.36
Blackrock Global Dynamic Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 9.8990 9.8990 12.3466 USD 122.22 HKD 956.52
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
安联环球成长基金 (美元) (累积) 18.1150 18.1150 11.8459 USD 214.59 HKD 1,679.43
Allianz Global Equity Growth (USD)
(Accumulation)
建立投資習慣  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
安联环球成长基金 (美元) (累积) 20.7090 20.7090 11.8459 USD 245.32 HKD 1,919.94
Allianz Global Equity Growth (USD)
(Accumulation)
景顺亚洲机遇股票基金 (美元) (累积) 0.8440 0.8440 130.1800 USD 109.87 HKD 859.87
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 7.5900 7.5900 33.0100 USD 250.55 HKD 1,960.87
Blackrock Global Dynamic Equity (USD)
(Accumulation)
安联总回报亚洲股票基金 (美元) (累积) 2.8150 2.8150 38.4858 USD 108.34 HKD 847.90
Allianz Total Return Asian Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 6.1530 6.1530 12.3466 USD 75.97 HKD 594.56
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
建立投資習慣2.9 (1)  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
景顺亚洲灵活债券基金 (美元) (累积) 9.1490 9.1490 12.3466 USD 112.96 HKD 884.05
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
Page（页） 9 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
建立投資習慣2.9 (1)  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
安联环球成长基金 (美元) (累积) 15.2030 15.2030 11.8459 USD 180.09 HKD 1,409.43
Allianz Global Equity Growth (USD)
(Accumulation)
安联环球机遇债券基金 (美元) (累积) 33.1840 33.1840 9.7622 USD 323.95 HKD 2,535.31
Allianz Global Opportunistic Bond (USD)
(Accumulation)
骏利亨德森美国短期债券基金 (美元) (累积) 18.2950 18.2950 18.0900 USD 330.96 HKD 2,590.18
Janus Henderson US Short-Term Bond Fund 
(USD)(Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 5.5600 5.5600 33.0100 USD 183.54 HKD 1,436.43
Blackrock Global Dynamic Equity (USD)
(Accumulation)
建立投資習慣4.18  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
景顺亚洲灵活债券基金 (美元) (累积) 2.0070 2.0070 12.3466 USD 24.78 HKD 193.93
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
PIMCO亚洲策略收益基金 (美元) (累积) 1.7800 1.7800 9.1500 USD 16.29 HKD 127.49
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
安联总回报亚洲股票基金 (美元) (累积) 0.5080 0.5080 38.4858 USD 19.55 HKD 153.00
Allianz Total Return Asian Equity (USD)
(Accumulation)
安联环球成长基金 (美元) (累积) 5.7600 5.7600 11.8459 USD 68.23 HKD 533.99
Allianz Global Equity Growth (USD)
(Accumulation)
景顺亚洲机遇股票基金 (美元) (累积) 0.1520 0.1520 130.1800 USD 19.79 HKD 154.88
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 5.3500 5.3500 33.0100 USD 176.60 HKD 1,382.12
Blackrock Global Dynamic Equity (USD)
(Accumulation)
達成我的目標2.4  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
PIMCO亚洲策略收益基金 (美元) (累积) 25.4300 25.4300 9.1500 USD 232.68 HKD 1,821.01
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
Page（页） 10 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
達成我的目標2.4  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
安联环球机遇债券基金 (美元) (累积) 11.2110 11.2110 9.7622 USD 109.44 HKD 856.50
Allianz Global Opportunistic Bond (USD)
(Accumulation)
骏利亨德森美国短期债券基金 (美元) (累积) 6.1820 6.1820 18.0900 USD 111.83 HKD 875.21
Janus Henderson US Short-Term Bond Fund 
(USD)(Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 20.4900 20.4900 33.0100 USD 676.37 HKD 5,293.44
Blackrock Global Dynamic Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 28.9700 28.9700 12.3466 USD 357.68 HKD 2,799.29
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
安联环球成长基金 (美元) (累积) 55.8180 55.8180 11.8459 USD 661.21 HKD 5,174.79
Allianz Global Equity Growth (USD)
(Accumulation)
達成我的目標2.9  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
安联环球成长基金 (美元) (累积) 3.8360 3.8360 11.8459 USD 45.44 HKD 355.62
Allianz Global Equity Growth (USD)
(Accumulation)
PIMCO亚洲策略收益基金 (美元) (累积) 1.1440 1.1440 9.1500 USD 10.47 HKD 81.94
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
景顺亚洲机遇股票基金 (美元) (累积) 0.0950 0.0950 130.1800 USD 12.37 HKD 96.81
Invesco Asia Opportunities Equity Fund 
(USD) (Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 1.3600 1.3600 33.0100 USD 44.89 HKD 351.32
Blackrock Global Dynamic Equity (USD)
(Accumulation)
安联总回报亚洲股票基金 (美元) (累积) 0.3170 0.3170 38.4858 USD 12.20 HKD 95.48
Allianz Total Return Asian Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 1.2740 1.2740 12.3466 USD 15.73 HKD 123.11
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
Page（页） 11 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
達成我的目標太苦了  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值) 
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
PIMCO亚洲策略收益基金 (美元) (累积) 13.9970 13.9970 9.1500 USD 128.07 HKD 1,002.31
PIMCO Asia Strategic Interest Bond Fund 
(USD)(Accumulation)
安联环球机遇债券基金 (美元) (累积) 6.0600 6.0600 9.7622 USD 59.16 HKD 463.00
Allianz Global Opportunistic Bond (USD)
(Accumulation)
骏利亨德森美国短期债券基金 (美元) (累积) 3.3510 3.3510 18.0900 USD 60.62 HKD 474.43
Janus Henderson US Short-Term Bond Fund 
(USD)(Accumulation)
贝莱德可持续环球动力股票基金 (美元) (累积) 11.7200 11.7200 33.0100 USD 386.88 HKD 3,027.82
Blackrock Global Dynamic Equity (USD)
(Accumulation)
景顺亚洲灵活债券基金 (美元) (累积) 16.0940 16.0940 12.3466 USD 198.71 HKD 1,555.15
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
安联环球成长基金 (美元) (累积) 32.0750 32.0750 11.8459 USD 379.96 HKD 2,973.66
Allianz Global Equity Growth (USD)
(Accumulation)
投资交易记录 Investment Transactions
交易日期 交收日期 交易种类 交易参考号码 交易详情 交易金额 认购费
Transaction Settlement Transaction Transaction ID Transaction Description Transaction Amount Subscription
Date Date Type fee
待确认之交易明细信息会显示为“TBC” (即“待确认”)。我们将在下期的结单中为你提供已确认之交易明细。你亦可参阅该交易的相关成交单据以了解更多详情。
Pending transaction details are displayed as  "TBC" (i.e. to be confirmed). We will provide you the confirmed transaction details in the next statement.  You may also refer 
to the relevant contract note for such transaction for more details.
单一投资组合的交易中，所包含的个别基金交易，将会与适用于该投资组合基金交易的唯一交易参考号码及认购费（如有）于同一表单中表示。以上显示之认购费包含在
投资组合的“买入”交易所收取的认购费用。详情请参阅相关银行月结单与相关交易之成交单据。
All underlying transactions in a single portfolio transaction will be listed out in the same table with single Transaction ID and Subscription Fee (if any) applicable to the 
portfolio transaction.  Subscription Fee consists of all subscription fees for all underlying  "buy"  transactions in the portfolio transaction. For details, please refer to the 
relevant Banking Statement and the contract note of the relevant transactions.
交易记录与持仓详情 - 自选基金
Detailed Transaction History and Positions – Featured Funds Services 
投资持仓 Investment Holdings
Page（页） 12 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
美元 USD  
基金名称 期初单位 期末单位 期末单位淨值 期末参考市值 期末参考市值(港币等值)
Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value on 31 Mar 2024 (HKD 
01 Mar 2024 31 Mar 2024 31 Mar 2024 31 Mar 2024 equivalent)
贝莱德亚洲老虎债券基金(美元)(每月派息) 14.4100 14.4100 8.2800 USD 119.31 HKD 933.75
BGF Asian Tiger Bond A6 USD
安联收益及增长基金(美元)(每月派息) 20.5530 20.5530 8.3301 USD 171.21 HKD 1,339.93
Allianz Income and Growth (USD)(Mth 
Distribution)
骏利亨德森平衡基金(美元)(每月派息) 30.1030 30.1030 13.0900 USD 394.05 HKD 3,083.93
Janus Henderson
安联环球机遇债券基金(美元)(每月派息) 173.1270 173.1270 8.4789 USD 1,467.93 HKD 11,488.39
Allianz Global Opportunistic Bond (USD
安联环球人工智能股票基金(美元)(累积) 7.4640 7.4640 26.8720 USD 200.57 HKD 1,569.71
Allianz Global Artificial Intelligence (USD)
(Accumulation)
聯博-短期債券基金BT股美元 11.4440 11.4440 10.7915 USD 123.50 HKD 966.54
Allianz HKD Income (USD)(Accumulation)
贝莱德动力高息基金(美元)(每月派息) 11.1600 11.1600 7.9200 USD 88.39 HKD 691.76
BGF Dynamic High Income A6 USD
景順永續性環球非投資等級債券基金A-固定 11.1200 11.1200 9.9400 USD 110.53 HKD 865.04
月配息股 美元
Invesco Global High Income Fund (USD)(Mth 
Distribu
景顺亚洲灵活债券基金 (美元) (累积) 5.3990 5.3990 12.3466 USD 66.66 HKD 521.70
Invesco Asian Flexible Bond Fund (USD)
(Accumulation)
安联环球机遇债券基金 (美元) (累积) 16.7880 16.7880 9.7622 USD 163.89 HKD 1,282.64
Allianz Global Opportunistic Bond (USD)
(Accumulation)
駿利亨德森資產管理基金 - 駿利亨德森環球 0.4610 0.4610 54.6300 USD 25.18 HKD 197.06
生命科技基金 A2 美元
Janus Henderson Global Life Sciences Fund 
(USD)(Ac
PIMCO GIS Glb Rl Ret E USD Inc 中文 278.7890 278.7890 13.1300 USD 3,660.50 HKD 28,647.99
PIMCO GIS Glb Rl Ret E USD Inc
景順環球高評級企業債券基金A(美元)每年派 19.6790 19.6790 11.3810 USD 223.97 HKD 1,752.85
息股份
Invesco Global Inv Grd CorpBd A USD AD
安联亚洲多元入息基金(美元)(每月派息) 68.1020 68.1020 5.6648 USD 385.78 HKD 3,019.21
Allianz Asian Multi Inc Plus AM USD
投资交易记录 Investment Transactions
Page（页） 13 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
交易日期 交收日期 交易种类 交易参考号码 交易详情 交易金额 认购费
Transaction Settlement Transaction Transaction ID Transaction Description Transaction Amount Subscription
Date Date Type fee
待确认之交易明细信息会显示为“TBC” (即“待确认”)。我们将在下期的结单中为你提供已确认之交易明细。你亦可参阅该交易的相关成交单据以了解更多详情。
Pending transaction details are displayed as "TBC" (i.e. to be confirmed). We will provide you the confirmed transaction details in the next statement.  You may also refer to 
the relevant contract note for such transaction for more details.
现金派息分派记录 Cash Dividend Summary
基金名称 派息日期 派息币种 派息金额
Fund Name Dividend Paid Date Dividend Currency Dividend Amount
Allianz Global Artificial Intelligence (USD)(Accumulation) 01 Mar 2024 USD 0.66
安联环球人工智能股票基金(美元)(累积)
BGF Asian Tiger Bond A6 USD 05 Mar 2024 USD 0.01
贝莱德亚洲老虎债券基金(美元)(每月派息)
BGF Asian Tiger Bond A6 USD 20 Mar 2024 USD 0.29
贝莱德亚洲老虎债券基金(美元)(每月派息)
Allianz Asian Multi Inc Plus AM USD 25 Mar 2024 USD 4.09
安联亚洲多元入息基金(美元)(每月派息)
Allianz Asian Multi Inc Plus AM USD 26 Mar 2024 USD 4.09
安联亚洲多元入息基金(美元)(每月派息)
本部分提供你在自选基金下所持有的派息基金在本结单期内的现金派息分派纪录。以上之派息日期为该派息金额存入予你的核心账户/外币账户（视乎派息货币而定）之
日期。
This part provides you the cash dividend summary of your distributing fund holdings under the Featured Funds Services within the statement period. The dividend paid 
date shown above is the date that the dividend amount is credited into your Core Account/Foreign Currency Account (depending on the dividend currency).
备注 Remarks
? 本结单所载之基金单位资产净值及巿值之价格仅供参考。
The NAV per unit and the Market Value of the fund shown in this statement are for reference only.
? 本结单所载之港元等值以以下参考外币汇率计算并仅供参考:
外币兑换率截至2024年03月31日 -
美元/港元: 7.82625000
The HKD equivalent values shown in this statement are calculated based the reference exchange rate as follows and are for reference only:
Fx Rate as of 31 Mar 2024 -
USD/HKD: 7.82625000
Page（页） 14 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
投资账户月结单 Investment Account Monthly Statement
列印日期 Date of Issue: 02 Apr 2024
投资账户号码 Investment Account Number: 8000030400-1
结单期 Statement Period: 01 Mar 2024 - 31 Mar 2024
重要提示 Important Notes
? 客户 （“你”）可在登入汇立银行app 后按“我的账户”>“关于我们”>“一般服务收费”了解以浏览汇立银行有限公司（“我们”）的收费。
Customers ("You" or "Your") can browse WeLab Bank Limited ("We" or "Our") latest fees and charges at "My account" > "About us" > "General service charges" on our 
WeLab Bank app.
? 如你认为此结单有任何错误、遗漏、差异、不当情况及/或未经授权的交易，请于列印日期起计90天内通知我们。如我们在上述时限内未收到任何你的相关通知，此结
单所有纪录均对你具最终约束力，而你不可以以任何理由对此结单上的任何纪录提出争议，惟我们仍有绝对权力对任何错误之纪录作出修正。
If you find any error, omission, discrepancy, irregularity and/or any unauthorised transaction in this statement, you should notify us within 90 days from the issue date 
of this statement. If we have not received any relevant notice from you within the specified time limit, all the records in this statement shall be conclusive and binding 
on you and you shall not be allowed to dispute any record of this statement for any reason. However, we shall have absolute discretion to amend any incorrect recor.
? 你可查阅及下载长达7年的电子月结单，亦可打印或下载你的电子月结单以记录所需信息。如你下载电子月结单到任何电脑或电子设备，请你确保该电脑及电子设备保
安的安全，并防止其他人士查阅任何机密资料，包括电子月结单。
You may view and download eStatement(s) of up to 7 years in our app, and you can print or download a copy of your eStatement(s) for record purposes. If you download 
an eStatement to any computer or electronic device, please ensure that the device is secured to prevent others from accessing any confidential information, including 
the eStatement.
? 我们提醒你请注意冒充我们职员的欺诈来电、语音讯息、电邮、短讯或其他伪冒方式，请不要向可疑第三者提供任何敏感个人资料。如你怀疑曾向可疑人士披露个人
资料，请致电我们的24小时客户服务热线 +852 3898 6988 核实来电者身份及立即向警方报案。
We remind you to be alert to fraudulent phone calls, voice messages, emails, SMS or communications in other formats imitating our staff. Please be reminded to 
protect your personal information and not provide any sensitive information to suspicious callers. If you have disclosed your personal details, please contact our 24-
hour Customer Service hotline at +852 3898 6988 to verify the caller's identity, and report to the Police immediately.
? 本投资结单只列出于本结单期内于你的投资账户中的基金持仓、基金交易及/或你持有的基金之公司行动的详情，而与核心账户及外币账户相关的的交易详情（例如存
入或扣除与基金交易相关的款项及基金交易涉及的货币转换）将于我们的银行月结单列出，请将此结单与该结单一并参阅。
This investment statement will only show the details of the fund holdings, fund transactions and/or corporation actions relating to your fund holdings in your 
investment account(s) during the statement period, transaction details relating to your Core Account and FX Account (e.g. deposit or debit of amounts relating to fund 
transactions and foreign exchange transactions involved in fund transactions) will be shown in our Banking Statement. Please read this statement in conjunction with 
the Banking Statement.
? 本投资结单的投资交易纪录只列出“交易日期”，为你发出交易指示之日期。而有关执行有关基金交易之日期（“基金交易日期”）及一般情况下有关基金交易的结算日
（“交收日期”）之详情，请参阅相关交易之成交单据。
The "Investment Transactions" section of this investment statement will only show the "Transaction Date", which is your order instruction placement date. While for the 
execution date of the relevant fund transaction ("Fund Dealing Date") and the day of the relevant fund transaction settled under normal situations ("Settlement Date"), 
please refer to the Contract Notes of relevant transactions.
? （只适用于自选基金交易）基金平台费将按月收取并由你的核心账户中扣除。每月之基金平台费为你每日平台费的每月总额，而每日平台费则根据你每日所有于自选
基金服务中除货币市场基金外已结算基金之持仓价值（港元或以每日参考汇率计算的等值港元）乘以基金平台月费率再除以该月历日数计算。当月衍生的基金平台费
（如有）将显示在相应的投资账户月结单上。有关现行基金平台月费率之详情，请参阅“一般服务收费”。有关基金资产类别，请参阅WeLab Bank app内的基金详情页
面。
(Applicable to Featured Funds transactions only) The fund platform fee shall be charged monthly and will be debited from your Core Account . The fund platform fee of 
a month is an aggregate amount of the daily fee within the month. Daily fee is calculated based on the daily market value of all your settled fund holdings in HKD (or HKD 
equivalent based on daily reference rate) under Featured Funds Services, excluding all money market fund holdings, multiplied by the fund platform fee monthly rate 
and divided by the number of calendar days of the month. Fund platform fee accrued in a month (if any) will be shown on the corresponding Investment Account 
Monthly Statement. For details of the prevailing fund platform fee monthly rate, please refer to the "General Services Charges". For fund asset class information, please 
refer to the fund details page in WeLab Bank app.
? 如你对账单有任何问题，你可在登入WeLab Bank app，进入“我的账户”后按“协助”，致电客户服务热线 +852 3898 6988 或发送电邮至wecare@welab.bank 联络我们。
If you have any queries concerning your contract note, you can contact us by login the WeLab Bank app and tap "My account" then tap "Support". You can also call our 
Customer Service hotline at +852 3898 6988 or email us at wecare@welab.bank.
Page（页） 15 of 15
23/F, K11 Atelier King's Road, 728 King's Road, Quarry Bay, Hong Kong    |    Tel: +852 3898 6988    |    Email: wecare@welab.bank
香港，鲗鱼涌，英皇道 728号，K11 Atelier 23楼   |    客服热线: +852 3898 6988    |    电邮 : wecare@welab.bank
