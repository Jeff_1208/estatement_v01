package wealth;

import java.util.ArrayList;
import java.util.HashMap;
import com.entity.IndividualFundPositions;
import com.entity.InvestmentTransactions;
import com.entity.Wealth;
import com.itextpdf.text.log.SysoCounter;
import com.utils.Common;
import com.utils.Global;

public class InvestmentTransactionsUtils_4 {
	
	//第三部分，交易记录，包含交易了多少个单位，TBC等相关信息
	public static void main(String[] args) {
		
		String line ="";
		line="30 Jun 2022 08 Jul 2022 基金認購 SE0000000259 PIMCO亞洲策略收益基金 (美元) (累積) USD 24.39 HKD 36";
		//String[] ss=setStringToStringArray(line);
		String[] ss=line.split(" ");
		
		for (int i = 0; i < ss.length; i++) {
			System.out.println(ss[i].trim());
		}
		System.out.println(ss.length);
		int amount_flag=0;
		if(line.contains("USD")&line.contains("HKD")) {
			amount_flag=1;
		}else if(line.contains("USD")&(!line.contains("HKD"))){
			amount_flag=2;
		}else if(!line.contains("USD")&(line.contains("HKD"))){
			amount_flag=3;
		}
		System.out.println(amount_flag);
	}
	
	String[] TypeList= {Global.SUBSCRIPTION_REGULAR,Global.SUBSCRIPTION_REGULAR_SIMPLIFIED_CHINESE,
			Global.REDEMPTION_ONE_OFF, Global.REDEMPTION_ONE_OFF_SIMPLIFIED_CHINESE};
	private ArrayList<String> InvestmentTransactionsFailList=new ArrayList<>();

	public Wealth getInvestmentTransactionsList(Wealth wealth) {
		ArrayList<InvestmentTransactions> investmentTransactionsList= new ArrayList<>();
		String content = wealth.getContent();
		String[] content_list = content.split("\n");
		boolean individualFundPositionsFlag=false;
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsInvestmentTransaction(line)) {
				//System.out.println(line);
				individualFundPositionsFlag =true;
			}
			if(Common.isContainsFeaturedFundsServices(line)||Common.isContainsCashDividendSummary(line)
					||Common.isContainsRemarks(line)||Common.isContainsRemarks2(line)||Common.isContainsImportantNotes(line)) {
				break;
			}
			if(!individualFundPositionsFlag) {continue;}
			if(Common.isWealthTitleString(line)) continue;
			if(Common.the_line_is_have_a_double_string(line)&&Common.isValidLine(line)){
				if(Common.isContainsNoOfUnit(line)||Common.isContainsNavPerUnit(line)) continue;
				if(!line.contains("基金")) continue;  //20230704
				//System.out.println(line);
				investmentTransactionsList.add(setTransactionHistoryAndPositions(content_list,i));
			}
		}
		wealth.setInvestmentTransactionsList(investmentTransactionsList);
		wealth=checkInvestmentTransactions(wealth);
		wealth=CompareInvestmentTransactions(wealth);
		wealth=checkSumOfFundSettledUnit(wealth);
		
		return wealth;
	}
	
	
	public InvestmentTransactions setTransactionHistoryAndPositions(String[] content_list,int index) {
		InvestmentTransactions investmentTransactions = new InvestmentTransactions();
		
		String line  = content_list[index].trim();
		int amount_flag=getUSD_HKD_Flag(line);
		String[] ss=setStringToStringArray(line);
		int len = ss.length;
		int typeIndex=0;
		if(Common.is_one_of_shuzu_item_in_line(line, TypeList)) {
			if(line.contains(Global.SUBSCRIPTION_REGULAR)) {
				typeIndex=Common.getIndexFromArray(ss,Global.SUBSCRIPTION_REGULAR);
			}else if(line.contains(Global.SUBSCRIPTION_REGULAR_SIMPLIFIED_CHINESE)) {
				typeIndex=Common.getIndexFromArray(ss,Global.SUBSCRIPTION_REGULAR_SIMPLIFIED_CHINESE);
			}else if(line.contains(Global.REDEMPTION_ONE_OFF)) {
				typeIndex=Common.getIndexFromArray(ss,Global.REDEMPTION_ONE_OFF);
			}else if(line.contains(Global.REDEMPTION_ONE_OFF_SIMPLIFIED_CHINESE)) {
				typeIndex=Common.getIndexFromArray(ss,Global.REDEMPTION_ONE_OFF_SIMPLIFIED_CHINESE);
			}
		}
		
		investmentTransactions.setTransactionDate(ss[0].trim()+" "+ss[1].trim()+" "+ss[2].trim());
		if(typeIndex==6) {
			investmentTransactions.setSettlementDate(ss[3].trim()+" "+ss[4].trim()+" "+ss[5].trim());
		}else {
			String temp="";
			for (int i = 3; i < typeIndex; i++) {
				temp +=ss[i]+" ";
			}
			investmentTransactions.setSettlementDate(temp.trim());
		}
		investmentTransactions.setTransactionType(ss[typeIndex].trim());
		investmentTransactions.setTransactionID(ss[typeIndex+1].trim());
		
		getTransactionDescriptionFromLine(line,amount_flag,len,typeIndex,ss);
		//investmentTransactions.setTransactionDescription(ss[typeIndex+2].trim()+" "+ss[typeIndex+3].trim()+" "+ss[typeIndex+4].trim());
		investmentTransactions.setTransactionDescription(getTransactionDescriptionFromLine(line,amount_flag,len,typeIndex,ss));
		
		if(amount_flag==1||amount_flag==3) {
			investmentTransactions.setTransactionAmount(ss[len-3].trim());
		}else {
			investmentTransactions.setTransactionAmount(ss[len-2].trim());
		}
		investmentTransactions.setSubscription(ss[len-1].trim());

		for (int i = index+1; i < content_list.length; i++) {
			String s = content_list[i];
			if(Common.isContainsNoOfUnit(s)) {
				investmentTransactions.setNoOfUnit(s.split(":")[1].trim());
			}
			if(Common.isContainsNavPerUnit(s)) {
				investmentTransactions.setNavOfUnit(s.split(":")[1].trim());
				break;
			}
		}
		//20231204
		if(Common.isDividend(line)) {
			investmentTransactions.setDividend(true);
		}
		//System.out.println("tostring: "+investmentTransactions.toString());
		return investmentTransactions;
	}
	

	public Wealth checkInvestmentTransactions(Wealth wealth) {
		ArrayList<InvestmentTransactions> investmentTransactionsList= wealth.getInvestmentTransactionsList();
		ArrayList<String> checkSettlementDateList= new ArrayList<>();
		ArrayList<String> checkTransactionTypeList= new ArrayList<>();
		ArrayList<String> checkTransactionIdList= new ArrayList<>();
		
		for (int i = 0; i < investmentTransactionsList.size(); i++) {
			InvestmentTransactions a = investmentTransactionsList.get(i);
			//System.out.println("getTransactionType: "+a.getTransactionType());
			
			//check transactionID
			//ID pattern:SE + 10 digits unique txn no., e.g. SE0000066484
			String transactionID = a.getTransactionID().trim();
			if(transactionID.startsWith("S")) {
				if(Common.isTenDigits(transactionID.substring(2))) {
				}else {
					a.setCheckTransactionID(false);
					checkTransactionIdList.add(a.getTransactionDescription());
				}
			}else {
				if(transactionID.length()>0&&transactionID.equals("-")) {
					
				}else {
					a.setCheckTransactionID(false);
					checkTransactionIdList.add(a.getTransactionDescription());
				}
			}
			if(a.getTransactionID().length()<1) {
				a.setCheckTransactionID(false);
				checkTransactionIdList.add(a.getTransactionDescription());
			}
			
			//check SettlementDate
			if(a.getSettlementDate().contains("TBC")) {
				if((!a.getNoOfUnit().contains("TBC"))||(!a.getNavOfUnit().contains("TBC"))&&(!a.getTransactionAmount().contains("TBC"))){
					a.setCheckTBC(false);
					checkSettlementDateList.add(a.getTransactionDescription());
				}
			}else {
				if((a.getNoOfUnit().contains("TBC"))||(a.getNavOfUnit().contains("TBC"))&&(a.getTransactionAmount().contains("TBC"))){
					if(Common.moveSpecialStringToDouble(a.getTransactionAmount())<Global.NO_OF_LIMIT_AMOUNT) {
						//金额太小，导致NoOfUnit为TBC
					}else {
						a.setCheckTBC(false);
						checkSettlementDateList.add(a.getTransactionDescription());
					}
				}
			}
			//check TransactionType
			if(!Common.is_one_of_shuzu_item_in_line(a.getTransactionType().trim(), TypeList)) {
				a.setCheckType(false);
				System.out.println("Fail item: "+a.toString());
				checkTransactionTypeList.add(a.getTransactionDescription());
			}

		}
		wealth.setCheckSettlementDateList(checkSettlementDateList);
		wealth.setCheckTransactionIdList(checkTransactionIdList);
		wealth.setCheckTransactionTypeList(checkTransactionTypeList);
		return wealth;
	}
	
	public Wealth CompareInvestmentTransactions(Wealth wealth) {
		//计算每个基金交易了多少个单位
		ArrayList<InvestmentTransactions> investmentTransactions= wealth.getInvestmentTransactionsList();
		HashMap<String, String> investmentTransactionsMap = new HashMap<>();
		//System.out.println("investmentTransactions.size(): "+investmentTransactions.size());
		//获取持有基金的名称List
		ArrayList<String> IndividualFundNameList= new ArrayList<>();
		for (int i = 0; i < investmentTransactions.size(); i++) {
			String fundName = investmentTransactions.get(i).getTransactionDescription();
			String fundName0 =fundName.split(" ")[0].trim();
			if(!IndividualFundNameList.contains(fundName0)) {
				IndividualFundNameList.add(fundName0);
			}
		}
		//根据每个名称，去遍历investmentTransactions中的交易记录，并计算出总交易值
		for (int i = 0; i < IndividualFundNameList.size(); i++) {
			String fundName = IndividualFundNameList.get(i);
			double unit = 0.0;
			for (int j = 0; j < investmentTransactions.size(); j++) {
				InvestmentTransactions a = investmentTransactions.get(j);
				String fundName1 = investmentTransactions.get(j).getTransactionDescription();
				String type =investmentTransactions.get(j).getTransactionType();
				if(a.isDividend()) continue; //20231204
				if(fundName1.contains(fundName)) {
					//System.out.println(fundName);
					if(a.getSettlementDate().contains(wealth.getThisMonth())&&a.getSettlementDate().trim().startsWith("01")) {
						continue;
						//1日为交收日(SettlementDate)会算入初期,1日为交易日(transactionDate)不会算入初期
					}

					//如果SettlementDate 不是这个月，不需要不计算
					//if(!checkSettlementDateIncludeThisMonth(wealth,investmentTransactions.get(j))) continue;
					//如果SettlementDate 是上个月或之前的日期是要计算的
					if(!checkSettlementDateToConfirmWhetherNeedCalulate(investmentTransactions.get(j).getSettlementDate(),wealth)) continue;
					
					String NoOfUnit = investmentTransactions.get(j).getNoOfUnit();
					//交易單位No. of Unit: TBC  USD 0.01  金额太小，交易单位就是TBC
					if(NoOfUnit.contains("TBC")) {
						String amountString = investmentTransactions.get(j).getTransactionAmount();
						double amount = Common.moveSpecialStringToDouble(amountString);
						System.out.println("amount: "+amount);
						if(amount < Global.NO_OF_LIMIT_AMOUNT) continue;
					}
					
					if(Common.isContainsSubscriptionRegular(type)) {
						unit = unit+Common.moveSpecialStringToDouble(NoOfUnit);
					}else {
						unit = unit-Common.moveSpecialStringToDouble(NoOfUnit);
					}
				}
			}
			if(unit!=0.0) {
				//System.out.println(fundName+" 交易单位累计: "+unit+"实际交易：");
				investmentTransactionsMap.put(fundName, Common.double_to_String_by_round_count(unit));
			}
		}
		wealth.setInvestmentTransactionsMap(investmentTransactionsMap);
		return wealth;
	}
	
	
	public boolean checkSettlementDateIncludeThisMonth(Wealth wealth,InvestmentTransactions investmentTransactions) {
		return investmentTransactions.getSettlementDate().contains(wealth.getThisMonth());
	}
	
	public boolean checkSettlementDateToConfirmWhetherNeedCalulate(String settlementDate,Wealth w) {
		if(settlementDate.contains("TBC")) return false; //未来日期，不需要计算
		String[] ss = settlementDate.trim().split(" ");
		if(ss.length != 3) return false;
		int year = Integer.parseInt(ss[2].trim());
		int monthIndex = Common.getMonthIndex(ss[1].trim());
		int thisYear = Integer.parseInt(w.getThisYear());
		int thisMonthIndex = Common.getMonthIndex(w.getThisMonth());
		//System.out.println("monthIndex: "+monthIndex);
		//System.out.println("thisMonthIndex: "+thisMonthIndex);
		if(year > thisYear) return false; //settlement 年份大于报告的年份，说明是未来日期，不计算
		if(year == thisYear) {
			if(monthIndex > thisMonthIndex) return false; //年份相同，settlement 月份大于报告的月份，说明是未来日期，不计算
			if(monthIndex <= thisMonthIndex) return true; //当月或当月之前需要计算
		}
		if(year < thisYear) return true; //settlement 年份小于报告的年份，说明是过去日期，计算
		return false;
	}
	
	//-check if the sum of fund settled unit = ending unit – beginning unit (circled at point 2)
	public Wealth checkSumOfFundSettledUnit(Wealth wealth) {
		ArrayList<IndividualFundPositions> individualFundPositionsList= wealth.getIndividualFundPositionsList();
		HashMap<String, String> investmentTransactionsMap = wealth.getInvestmentTransactionsMap();
		
		for (String key : investmentTransactionsMap.keySet()) {
			String NoOfUnit = investmentTransactionsMap.get(key);
			for (int j = 0; j < individualFundPositionsList.size(); j++) {
				if(individualFundPositionsList.get(j).getFundName().contains(key)) {
					if(individualFundPositionsList.get(j).isDividend()) continue;  //如果这条记录是每月派息，那么不用这条对比，继续寻找下一条对比
					String startUnit = individualFundPositionsList.get(j).getStartUnitNumber();
					String endUnit = individualFundPositionsList.get(j).getEndUnitNumber();
					//System.out.println("name: "+individualFundPositionsList.get(j).getFundName());
					//System.out.println("startUnit:"+startUnit);
					//System.out.println("endUnit:"+endUnit);
					String NoOfUnitCalc=Common.double_to_String_by_round_count(Common.moveSpecialStringToDouble(endUnit)-Common.moveSpecialStringToDouble(startUnit));
					//System.out.println("startUnit: "+startUnit);
					//System.out.println("endUnit: "+endUnit);
					//System.out.println("NoOfUnit: "+NoOfUnit);
					//System.out.println("NoOfUnitCalc: "+NoOfUnitCalc);
					if(!Common.isApproximatelyEqual(Common.moveSpecialStringToDouble(NoOfUnit), Common.moveSpecialStringToDouble(NoOfUnitCalc))) {
						//System.out.println(key+" 交易单位累计: "+NoOfUnit+"月初月底差额为："+Common.moveSpecialStringToDouble(NoOfUnitCalc));
						InvestmentTransactionsFailList.add(key+" 交易单位累计: "+NoOfUnit+",月初月底差额为："+Common.moveSpecialStringToDouble(NoOfUnitCalc));
					}
					break;
				}
			}
		}
		wealth.setInvestmentTransactionsFailList(InvestmentTransactionsFailList);
		return wealth;
	}
	
	public int getUSD_HKD_Flag(String line) {
		int amount_flag=0;
		if(line.contains("USD")&line.contains("HKD")) {
			amount_flag=1;
		}else if(line.contains("USD")&(!line.contains("HKD"))){
			amount_flag=2;
		}else if(!line.contains("USD")&(line.contains("HKD"))){
			amount_flag=3;
		}
		return amount_flag;
	}
	
	public String getTransactionDescriptionFromLine(String line,int amount_flag,int len,int typeIndex,String[] ss) {
		int lastIndexOfTransactionDescription=0;
		if(amount_flag==0) {
			lastIndexOfTransactionDescription = len-2-1;
		}else if(amount_flag==2||amount_flag==3) {
			lastIndexOfTransactionDescription=len-3-1;
		}else{
			lastIndexOfTransactionDescription=len-4-1;
		}
		String temp="";
		for (int i = typeIndex+2; i <= lastIndexOfTransactionDescription; i++) {
			temp +=ss[i]+" ";
		}
		return temp.trim();
	}

	public static String[] setStringToStringArray(String line) {
		String[] s1 = line.split(" ");
		ArrayList<String> list = new ArrayList<>();
		for (int i = 0; i < s1.length; i++) {
			if(s1[i].trim().length()>=1) {
				list.add(s1[i].trim());
			}
		}
		String[] ss= new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			ss[i]=list.get(i);
		}
		return ss;
	}
}
