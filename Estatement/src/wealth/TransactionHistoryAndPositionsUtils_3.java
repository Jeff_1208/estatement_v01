package wealth;

import java.util.ArrayList;
import com.entity.TransactionHistoryAndPositions;
import com.entity.Wealth;
import com.utils.Common;

public class TransactionHistoryAndPositionsUtils_3 {

	//第二部分，持有基金的转账记录，同一个持有的基金可能有多比转账记录
	//但是对象的属性值是一样的
	//TransactionHistoryAndPositions总金额等于title总金额减去Feature Funds金额
	private ArrayList<String> TransactionHistoryAndPositionsFailList=new ArrayList<>();
	
	public Wealth getTransactionHistoryAndPositionsList(Wealth wealth) {
		ArrayList<TransactionHistoryAndPositions> transactionHistoryAndPositionsList= new ArrayList<>();
		String content = wealth.getContent();
		String[] content_list = content.split("\n");
		boolean transactionHistoryAndPositionsFlag=false;
		ArrayList<String> InvestmentNameList= wealth.getInvestmentNameList();
		boolean isAlreadyStartFeatureFunds=false;
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsTransactionHistoryAndPositions(line)) {
				if(isAlreadyStartFeatureFunds) break;
				transactionHistoryAndPositionsFlag =true;
			}
			
			if(Common.isContainsInvestmentTransaction(line)||Common.isContainsFeaturedFundsServices(line)) {
				break;
			}
			if(Common.isContainsFeaturedFundsServices(line)) {
				isAlreadyStartFeatureFunds=true;
			}
			if(Common.isWealthTitleString(line)) continue;
			if(!transactionHistoryAndPositionsFlag) {continue;}
			if(Common.the_line_is_have_a_double_string(line)&&Common.isValidLine(line)){
				if(!Common.is_one_of_list_item_in_line(line, InvestmentNameList)) {
					//System.out.println(line);
					//System.out.println("isAlreadyStartFeatureFunds: "+isAlreadyStartFeatureFunds);
					
					transactionHistoryAndPositionsList.add(setTransactionHistoryAndPositions(line));
				}else {
					System.out.println("line index: " + i + " 打印异常数据： "+line);
				}
			}
		}
		
		wealth.setTransactionHistoryAndPositionsList(transactionHistoryAndPositionsList);
		
		wealth=checkTransactionHistoryAndPositions(wealth);
		return wealth;
	}
	
	
	public TransactionHistoryAndPositions setTransactionHistoryAndPositions(String line) {
		//景順亞洲機遇股票基金 (美元)(累積)
		//貝萊德環球動力股票基金 (美元) (累積)
		ArrayList<Integer> list = Common.getEmptyListFromString(line);
		TransactionHistoryAndPositions transactionHistoryAndPositions = new TransactionHistoryAndPositions();
//		transactionHistoryAndPositions.setFundName(line.substring(0, list.get(2)).trim());
//		transactionHistoryAndPositions.setStartUnitNumber(line.substring(list.get(2), list.get(3)).trim());
//		transactionHistoryAndPositions.setEndUnitNumber(line.substring(list.get(3), list.get(4)).trim());
//		transactionHistoryAndPositions.setNavPerUnit(line.substring(list.get(4), list.get(5)).trim());
//		transactionHistoryAndPositions.setMarketValueDollar(line.substring(list.get(6), list.get(7)).trim());
//		transactionHistoryAndPositions.setMarketValueHKD(line.substring(list.get(8)).trim());
		transactionHistoryAndPositions.setDollarFund(Common.checkIsDollarFund(line));
		transactionHistoryAndPositions.setFundName(line.substring(0, list.get(list.size()-7)).trim());
		transactionHistoryAndPositions.setStartUnitNumber(line.substring(list.get(list.size()-7), list.get(list.size()-6)).trim());
		transactionHistoryAndPositions.setEndUnitNumber(line.substring(list.get(list.size()-6), list.get(list.size()-5)).trim());
		transactionHistoryAndPositions.setNavPerUnit(line.substring(list.get(list.size()-5), list.get(list.size()-4)).trim());
		transactionHistoryAndPositions.setMarketValueDollar(line.substring(list.get(list.size()-3), list.get(list.size()-2)).trim());
		transactionHistoryAndPositions.setMarketValueHKD(line.substring(list.get(list.size()-1)).trim());
		//System.out.println(transactionHistoryAndPositions.toString());
		//System.out.println(line.substring(list.get(list.size()-3), list.get(list.size()-2)).trim() +"\t" +line.substring(list.get(list.size()-1)).trim());
		return transactionHistoryAndPositions;
	}
	public Wealth checkTransactionHistoryAndPositions(Wealth wealth) {
		//TransactionHistoryAndPositions 不涉及Feature Funds数据
		
		ArrayList<TransactionHistoryAndPositions> transactionHistoryAndPositions= wealth.getTransactionHistoryAndPositionsList();
		double totalDollarCalc=0.0;
		double totalHKDCalc=0.0;
		
		for (int i = 0; i < transactionHistoryAndPositions.size(); i++) {
			TransactionHistoryAndPositions a = transactionHistoryAndPositions.get(i);
			if(!a.isDollarFund()) continue; //不是美元金额，先不进行计算，后面单独重新计算
			totalDollarCalc+=Common.moveSpecialStringToDouble(a.getMarketValueDollar());
			totalHKDCalc+=Common.moveSpecialStringToDouble(a.getMarketValueHKD());
			
			//检测 美元*汇率 结果是不是等于港币金额
			double r=Common.getMultiplicationResult(Common.string_move_special(a.getMarketValueDollar()),wealth.getFxRate()+"");
			if(!Common.isApproximatelyEqual(r, Common.moveSpecialStringToDouble(a.getMarketValueHKD()))) {
				//System.out.println(r+"\t"+a.getMarketValueHKD());
				wealth.setTransactionHistoryAndPositionsEachItemPass(false);
				Common.addFailList(TransactionHistoryAndPositionsFailList, a.getMarketValueHKD());
			}
			
			//检测 月底单位数量*单位金额 结果是不是等于美元金额
			double marketValueCalc = Common.getMultiplicationResult(a.getEndUnitNumber(), a.getNavPerUnit());
			if(!Common.isApproximatelyEqual(marketValueCalc, Common.moveSpecialStringToDouble(a.getMarketValueDollar()))) {
				wealth.setTransactionHistoryAndPositionsEachItemOfNAVPass(false);
				Common.addFailList(TransactionHistoryAndPositionsFailList, a.getMarketValueHKD());
			}else {
				//System.out.println(a.getMarketValueDollar()+"\t"+marketValueCalc);
			}
		}
		
		double totalHKDFeatureFundsCalc=0.0;  //20230921
		for (int i = 0; i < transactionHistoryAndPositions.size(); i++) {
			TransactionHistoryAndPositions a = transactionHistoryAndPositions.get(i);
			if(!a.isDollarFund()) {
				totalHKDFeatureFundsCalc+=Common.moveSpecialStringToDouble(a.getMarketValueHKD());
			}
		}
		//System.out.println("totalHKDFeatureFundsCalc: "+totalHKDFeatureFundsCalc);

		wealth.setTransactionHistoryAndPositionsTotalDollarCalc(Common.double_to_String(totalDollarCalc));
		wealth.setTransactionHistoryAndPositionsHKDCalc(Common.double_to_String(totalHKDCalc));
		
//		System.out.println("totalDollarCalc: "+totalDollarCalc);
//		System.out.println("wealth.getAccountTotalDollarCapture(): "+wealth.getAccountTotalDollarCapture());
//		System.out.println("wealth.getFeaturedFundsEndValueDollar(): "+wealth.getFeaturedFundsEndValueDollar());
//		System.out.println("dollar: "+(Common.moveSpecialStringToDouble(wealth.getAccountTotalDollarCapture())-Common.string_to_double(wealth.getFeaturedFundsEndValueDollar())));
//		System.out.println("totalHKDCalc: "+totalHKDCalc);
//		System.out.println("wealth.getAccountTotalHKDCapture(): "+wealth.getAccountTotalHKDCapture());
//		System.out.println("wealth.getFeaturedFundsEndValueHKD(): "+wealth.getFeaturedFundsEndValueHKD());
//		System.out.println("hkd: "+(Common.moveSpecialStringToDouble(wealth.getAccountTotalHKDCapture())-Common.string_to_double(wealth.getFeaturedFundsEndValueHKD())));
	
		//TransactionHistoryAndPositions 不涉及Feature Funds数据
		//TransactionHistoryAndPositions 不涉及Feature Funds数据
		//TransactionHistoryAndPositions 不涉及Feature Funds数据
		
		//第一个参数是TransactionHistory计算出来的总金额totalDollarCalc，第二个参数是title抓取的美金总金额 减去 feature funds中的美金金额
		if(Common.isApproximatelyEqual(totalDollarCalc, Common.moveSpecialStringToDouble(wealth.getAccountTotalDollarCapture())-Common.string_to_double(wealth.getFeaturedFundsEndValueDollar()))) {
			wealth.setTransactionHistoryAndPositionsTotalDollarPass(true);
		}
		//减去 feature funds中的金额
		//20230921 减去 feature funds 港币基金中的金额
		double except_result_hkd = Common.moveSpecialStringToDouble(wealth.getAccountTotalHKDCapture()) 
				- Common.string_to_double(wealth.getFeaturedFundsEndValueHKD()) 
				- Common.moveSpecialStringToDouble(wealth.getHKDFeaturedFundsEndValue());
		if(Common.isApproximatelyEqual(totalHKDCalc, except_result_hkd)) {
			wealth.setTransactionHistoryAndPositionsTotalHKDPass(true);
		}	
		
		if(Common.isApproximatelyEqual(Common.moveSpecialStringToDouble(wealth.getAccountTotalDollarCapture()) , except_result_hkd)) {
			wealth.setTransactionHistoryAndPositionsTotalHKDPass(true);
		}	
		
		/**
		// title 没有显示自选基金港币，那么就用自选基金 美元 * 汇率 和计算的美元金额对比
		double  except_result_dollar =Common.moveSpecialStringToDouble(wealth.getFeaturedFundsEndValueDollar()) * wealth.getFxRate();
		if(Common.isApproximatelyEqual(except_result_dollar , totalDollarCalc)) {
			wealth.setTransactionHistoryAndPositionsTotalHKDPass(true);
		}	
		
		System.out.println("except_result_dollar: "+except_result_dollar);
		System.out.println("except_result_hkd: "+except_result_hkd);
		System.out.println("totalDollarCalc: "+totalDollarCalc);
		System.out.println("totalHKDCalc: "+totalHKDCalc);
		System.out.println("wealth.getFeaturedFundsEndValueHKD(): "+wealth.getFeaturedFundsEndValueHKD());
		System.out.println("wealth.getAccountTotalHKDCapture(): "+wealth.getAccountTotalHKDCapture());
		System.out.println("wealth.getHKDFeaturedFundsEndValue(): "+wealth.getHKDFeaturedFundsEndValue());
		**/
		double HDKCalc = Common.getMultiplicationResult(totalDollarCalc+"",wealth.getFxRate()+"");
		//减去 feature funds中的金额
		//20230921 减去 feature funds 港币基金中的金额
		if(Common.isApproximatelyEqual(HDKCalc, except_result_hkd)) {
			wealth.setTransactionHistoryAndPositionsDollarEqualHKDPass(true);
		}
//		System.out.println("kkkkkkk");
//		System.out.println("totalDollarCalc: "+totalDollarCalc);
//		System.out.println("totalHKDCalc: "+totalHKDCalc);
//		System.out.println("HDKCalc："+HDKCalc);
//		System.out.println("except_result_hkd："+except_result_hkd);
//		System.out.println("totalHKDFeatureFundsCalc："+totalHKDFeatureFundsCalc);
//		System.out.println("TransactionHistoryAndPositions Check Finish!!!!!!!!!");
//		System.out.println();

		
//		System.out.println(wealth.isTransactionHistoryAndPositionsDollarEqualHKDPass());
//		System.out.println(wealth.isTransactionHistoryAndPositionsTotalHKDPass());
//		System.out.println(wealth.isTransactionHistoryAndPositionsTotalDollarPass());
//		System.out.println(wealth.isTransactionHistoryAndPositionsEachItemPass());
//		System.out.println(wealth.isTransactionHistoryAndPositionsEachItemOfNAVPass());
//		System.out.println(wealth.getTransactionHistoryAndPositionsHKDCalc());
//		System.out.println(wealth.getTransactionHistoryAndPositionsTotalDollarCalc());
		
		wealth.setTransactionHistoryAndPositionsFailList(TransactionHistoryAndPositionsFailList);
		
		return wealth;
	}
	
}
