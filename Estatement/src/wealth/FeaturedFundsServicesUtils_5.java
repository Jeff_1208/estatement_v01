package wealth;

import java.util.ArrayList;
import java.util.HashMap;
import com.entity.IndividualFundPositions;
import com.entity.InvestmentTransactions;
import com.entity.TransactionHistoryAndPositions;
import com.entity.Wealth;
import com.utils.Common;
import com.utils.Global;

public class FeaturedFundsServicesUtils_5 {
	
	// 自选基金的第一部分持仓和第二部分的交易记录都合并在一起了，需要怎么处理？？
	ArrayList<String> FeaturedFundsServicesFailList=new ArrayList<>();
	
	public Wealth getFeaturedFundsServicesInventmentHoldingsList(Wealth wealth) {
		//类似于检查第二部分
		ArrayList<TransactionHistoryAndPositions> FeaturedFundsServicesInventmentHoldingsList= new ArrayList<>();
		String content = wealth.getContent();
		String[] content_list = content.split("\n");
		boolean featuredFundsServicesFlag=false;
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsFeaturedFundsServices(line)) {
				featuredFundsServicesFlag =true;
			}
			if(!featuredFundsServicesFlag) {continue;}
			if(Common.isContainsCashDividendSummary(line)||Common.isContainsRemarks(line)||Common.isContainsRemarks2(line)) {
				break;
			}
			if(Common.isContainsInvestmentTransaction(line)) {
				break;
			}
			if(Common.isWealthTitleString(line)) continue;
			if(Common.the_line_is_have_a_double_string(line)&&Common.isValidLine(line)){
				//System.out.println(line);
				FeaturedFundsServicesInventmentHoldingsList.add(setTransactionHistoryAndPositions(line));
			}
		}
		wealth.setFeaturedFundsServicesInventmentHoldings(FeaturedFundsServicesInventmentHoldingsList);
		wealth=checkFeaturedFundsServicesInventmentHoldings(wealth);
		return wealth;
	}
	
	public TransactionHistoryAndPositions setTransactionHistoryAndPositions(String line) {
		//景順亞洲機遇股票基金 (美元)(累積)
		//貝萊德環球動力股票基金 (美元) (累積)
		//基金名稱 單位數量於 單位數量於 單位資產淨值於 參考市值於 參考市值於 (港幣等值) 
		//駿利亨德森美國短期債券基金 (美元) (累積) 0.0000 0.4490 17.4300 USD 7.83 HKD 61.43
		ArrayList<Integer> list = Common.getEmptyListFromString(line);
		TransactionHistoryAndPositions transactionHistoryAndPositions = new TransactionHistoryAndPositions();
//		transactionHistoryAndPositions.setFundName(line.substring(0, list.get(2)).trim());
//		transactionHistoryAndPositions.setStartUnitNumber(line.substring(list.get(2), list.get(3)).trim());
//		transactionHistoryAndPositions.setEndUnitNumber(line.substring(list.get(3), list.get(4)).trim());
//		transactionHistoryAndPositions.setNavPerUnit(line.substring(list.get(4), list.get(5)).trim());
//		transactionHistoryAndPositions.setMarketValueDollar(line.substring(list.get(6), list.get(7)).trim());
//		transactionHistoryAndPositions.setMarketValueHKD(line.substring(list.get(8)).trim());
		
		transactionHistoryAndPositions.setDollarFund(Common.checkIsDollarFund(line));
		transactionHistoryAndPositions.setFundName(line.substring(0, list.get(list.size()-7)).trim());
		transactionHistoryAndPositions.setStartUnitNumber(line.substring(list.get(list.size()-7), list.get(list.size()-6)).trim());
		transactionHistoryAndPositions.setEndUnitNumber(line.substring(list.get(list.size()-6), list.get(list.size()-5)).trim());
		transactionHistoryAndPositions.setNavPerUnit(line.substring(list.get(list.size()-5), list.get(list.size()-4)).trim());
		transactionHistoryAndPositions.setMarketValueDollar(line.substring(list.get(list.size()-3), list.get(list.size()-2)).trim());
		transactionHistoryAndPositions.setMarketValueHKD(line.substring(list.get(list.size()-1)).trim());
		//System.out.println(transactionHistoryAndPositions.toString());
		return transactionHistoryAndPositions;
	}
	
	public Wealth checkFeaturedFundsServicesInventmentHoldings(Wealth wealth) {
		ArrayList<TransactionHistoryAndPositions> FeaturedFundsServicesInventmentHoldingsList= wealth.getFeaturedFundsServicesInventmentHoldings();
		double totalDollarCalc=0.0;
		double totalHKDCalc=0.0;
		for (int i = 0; i < FeaturedFundsServicesInventmentHoldingsList.size(); i++) {
			TransactionHistoryAndPositions a = FeaturedFundsServicesInventmentHoldingsList.get(i);
			if(!a.isDollarFund()) {
				System.out.println("do not need check for USD: "+a.getMarketValueHKD());
				continue;  //非美元不需要检查
			}
			
			totalDollarCalc+=Common.moveSpecialStringToDouble(a.getMarketValueDollar());
			totalHKDCalc+=Common.moveSpecialStringToDouble(a.getMarketValueHKD());
			//检测 月底单位数量*单位金额 结果是不是等于美元金额
			double r=Common.getMultiplicationResult(Common.string_move_special(a.getMarketValueDollar()),wealth.getFxRate()+"");
			if(!Common.isApproximatelyEqual(r, Common.moveSpecialStringToDouble(a.getMarketValueHKD()))) {
				//System.out.println(r+"\t"+a.getMarketValueHKD());
				wealth.setTransactionHistoryAndPositionsEachItemPass(false);
				Common.addFailList(FeaturedFundsServicesFailList, a.getMarketValueHKD());
			}
			//检测 美元*汇率 结果是不是等于港币金额
			double marketValueCalc = Common.getMultiplicationResult(a.getEndUnitNumber(), a.getNavPerUnit());
			if(!Common.isApproximatelyEqual(marketValueCalc, Common.moveSpecialStringToDouble(a.getMarketValueDollar()))) {
				wealth.setTransactionHistoryAndPositionsEachItemOfNAVPass(false);
				Common.addFailList(FeaturedFundsServicesFailList, a.getMarketValueHKD());
			}else {
				//System.out.println(a.getMarketValueDollar()+"\t"+marketValueCalc);
			}
		}
		
		//20230912 由于港币的feature funds有港币场景，因为没有累加单独港币的场景，所以对比不需要增加单独港币金额
		double totalHKDFeatureFundsCalc=0.0;  //20230921
		for (int i = 0; i < FeaturedFundsServicesInventmentHoldingsList.size(); i++) {
			TransactionHistoryAndPositions a = FeaturedFundsServicesInventmentHoldingsList.get(i);
			if(!a.isDollarFund()) {
				totalHKDFeatureFundsCalc+=Common.moveSpecialStringToDouble(a.getMarketValueHKD());
			}
		}
		//20230912对比港币基金总和是不是和首页抓取金额一致
		if(Common.isApproximatelyEqual(totalHKDFeatureFundsCalc, Common.moveSpecialStringToDouble(wealth.getHKDFeaturedFundsEndValue()))) {
			wealth.setHKDFeaturedFundsTotalHKDPass(true);
		}
		
		//计算美元金额是否与抓取的金额相同
		if(Common.isApproximatelyEqual(totalDollarCalc, Common.moveSpecialStringToDouble(wealth.getFeaturedFundsEndValueDollar()))) {
			wealth.setFeaturedFundsServicesInventmentHoldingsTotalDollarPass(true);
		}
		//Account Total (Wealth Balance) ~= Sum (MV of all funds HKD equivalent) 总金额与每一项港币金额计算总和对比
		if(Common.isApproximatelyEqual(totalHKDCalc, Common.moveSpecialStringToDouble(wealth.getFeaturedFundsEndValueHKD()))) {
			wealth.setFeaturedFundsServicesInventmentHoldingsTotalHKDPass(true);
		}	
		//Market Value (HKD Equivalent) = MV per fund x FX rate (currency/HKD) 美元乘以汇率是否等于港币
		double HDKCalc = Common.getMultiplicationResult(totalDollarCalc+"",wealth.getFxRate()+"");
		if(Common.isApproximatelyEqual(HDKCalc, Common.moveSpecialStringToDouble(wealth.getFeaturedFundsEndValueHKD()))) {
			wealth.setFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass(true);
		}
		
//		System.out.println("totalHKDFeatureFundsCalc: "+totalHKDFeatureFundsCalc);
//		System.out.println("HDKCalc: "+HDKCalc);
//		System.out.println("wealth.getFxRate()"+wealth.getFxRate());
//		System.out.println("totalDollarCalc:"+totalDollarCalc);
//		System.out.println("totalHKDCalc:"+totalHKDCalc);
//		System.out.println("wealth.getFeaturedFundsEndValueDollar():"+wealth.getFeaturedFundsEndValueDollar());
//		System.out.println("wealth.getFeaturedFundsEndValueHKD():"+wealth.getFeaturedFundsEndValueHKD());
//		System.out.println(""+wealth.isFeaturedFundsServicesInventmentHoldingsTotalDollarPass());
//		System.out.println(""+wealth.isFeaturedFundsServicesInventmentHoldingsTotalHKDPass());
//		System.out.println(""+wealth.isFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass());
//		System.out.println(""+wealth.isFeaturedFundsServicesInventmentHoldingsEachItemOfNAVPass());
//		System.out.println(""+wealth.isFeaturedFundsServicesInventmentHoldingsEachItemPass());
//		System.out.println("wealth.isHKDFeaturedFundsTotalHKDPass(): "+wealth.isHKDFeaturedFundsTotalHKDPass());
//		System.out.println("check FeaturedFundsServicesInventmentHoldings finish!!!");
		return wealth;
	}
	
	public IndividualFundPositions setIndividualFundPositions(String line) {
		ArrayList<Integer> list = Common.getEmptyListFromString(line);
		int size = list.size();
		IndividualFundPositions individualFundPositions = new IndividualFundPositions();
		individualFundPositions.setFundName(line.substring(0, list.get(size-7)).trim());
		individualFundPositions.setStartUnitNumber(line.substring(list.get(size-7), list.get(size-6)).trim());
		individualFundPositions.setEndUnitNumber(line.substring(list.get(size-6), list.get(size-5)).trim());
		individualFundPositions.setNavPerUnit(line.substring(list.get(size-5), list.get(size-4)).trim());
		individualFundPositions.setMarketValueDollar(line.substring(list.get(size-3), list.get(size-2)).trim());
		individualFundPositions.setMarketValueHKD(line.substring(list.get(size-1)).trim());
		//System.out.println(individualFundPositions.toString());
		return individualFundPositions;
	}
	
	
	public Wealth getFeaturedFundsServicesInventmentTransactionsList(Wealth wealth) {
		//类似于检查第三部分
		ArrayList<InvestmentTransactions> FeaturedFundsServicesInventmentTransactionsList= new ArrayList<>();
		String content = wealth.getContent();
		String[] content_list = content.split("\n");
		boolean featuredFundsServicesFlag=false;
		boolean featuredFundsServicesTransactionsFlag=false;
		
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsFeaturedFundsServices(line)) {
				featuredFundsServicesFlag =true;
			}
			if(Common.isContainsInvestmentTransaction(line)&&featuredFundsServicesFlag) {
				featuredFundsServicesTransactionsFlag = true;
			}
			if(Common.isWealthTitleString(line)) continue;
			if(!featuredFundsServicesTransactionsFlag) {continue;}
			if(Common.isContainsCashDividendSummary(line)||Common.isContainsRemarks(line)||Common.isContainsRemarks2(line)||Common.isContainsFundPlatformFeeSummary(line)) {
				break;
			}

			if(Common.the_line_is_have_a_double_string(line)&&Common.isValidLine(line)){
				if(Common.isContainsNoOfUnit(line)||Common.isContainsNavPerUnit(line)) continue;
				//System.out.println(line);
				FeaturedFundsServicesInventmentTransactionsList.add(setTransactionHistoryAndPositions(content_list, i));
			}
		}
		wealth.setFeaturedFundsServicesInventmentTransactions(FeaturedFundsServicesInventmentTransactionsList);
		wealth = checkFeaturedFundsServicesInventmentTransactions(wealth);
		
		//由于持仓部分与交易记录部分合并了，无法抓取持仓部分，月底减去月初的金额计算不用检查，
		wealth=CompareFeaturedFundsServicesInvestmentTransactions(wealth); //统计总共有多少个基金，每个基金交易的单位
		wealth=checkSumOfFundSettledUnit(wealth);  //需要检查，上个方法中的单位金额，以上面的月底月初差额做对比
		wealth.setFeaturedFundsServicesFailList(FeaturedFundsServicesFailList);
		return wealth;
	}
	
	String[] TypeList= {Global.SUBSCRIPTION_REGULAR,Global.SUBSCRIPTION_REGULAR_SIMPLIFIED_CHINESE,
			Global.REDEMPTION_ONE_OFF, Global.REDEMPTION_ONE_OFF_SIMPLIFIED_CHINESE};
	public InvestmentTransactions setTransactionHistoryAndPositions(String[] content_list,int index) {
		InvestmentTransactions investmentTransactions = new InvestmentTransactions();
		
		String line  = content_list[index].trim();
		if(line.contains("(港元)")) {
			investmentTransactions.setIs_HDK_transation(true);
		}
		int amount_flag=getUSD_HKD_Flag(line);
		String[] ss=setStringToStringArray(line);
		int len = ss.length;
		int typeIndex=0;
		if(Common.is_one_of_shuzu_item_in_line(line, TypeList)) {
			if(line.contains(Global.SUBSCRIPTION_REGULAR)) {
				typeIndex=Common.getIndexFromArray(ss,Global.SUBSCRIPTION_REGULAR);
			}else if(line.contains(Global.SUBSCRIPTION_REGULAR_SIMPLIFIED_CHINESE)) {
				typeIndex=Common.getIndexFromArray(ss,Global.SUBSCRIPTION_REGULAR_SIMPLIFIED_CHINESE);
			}else if(line.contains(Global.REDEMPTION_ONE_OFF)) {
				typeIndex=Common.getIndexFromArray(ss,Global.REDEMPTION_ONE_OFF);
			}else if(line.contains(Global.REDEMPTION_ONE_OFF_SIMPLIFIED_CHINESE)) {
				typeIndex=Common.getIndexFromArray(ss,Global.REDEMPTION_ONE_OFF_SIMPLIFIED_CHINESE);
			}
			
		}
		
		investmentTransactions.setTransactionDate(ss[0].trim()+" "+ss[1].trim()+" "+ss[2].trim());
		if(typeIndex==6) {
			investmentTransactions.setSettlementDate(ss[3].trim()+" "+ss[4].trim()+" "+ss[5].trim());
		}else {
			String temp="";
			for (int i = 3; i < typeIndex; i++) {
				temp +=ss[i]+" ";
			}
			investmentTransactions.setSettlementDate(temp.trim());
		}
		investmentTransactions.setTransactionType(ss[typeIndex].trim());
		investmentTransactions.setTransactionID(ss[typeIndex+1].trim());
		
		getTransactionDescriptionFromLine(line,amount_flag,len,typeIndex,ss);
		//investmentTransactions.setTransactionDescription(ss[typeIndex+2].trim()+" "+ss[typeIndex+3].trim()+" "+ss[typeIndex+4].trim());
		investmentTransactions.setTransactionDescription(getTransactionDescriptionFromLine(line,amount_flag,len,typeIndex,ss));
		
		if(amount_flag==1||amount_flag==3) {
			investmentTransactions.setTransactionAmount(ss[len-3].trim());
		}else {
			investmentTransactions.setTransactionAmount(ss[len-2].trim());
		}
		investmentTransactions.setSubscription(ss[len-1].trim());

		for (int i = index+1; i < content_list.length; i++) {
			String s = content_list[i];
			if(Common.isContainsNoOfUnit(s)) {
				investmentTransactions.setNoOfUnit(s.split(":")[1].trim());
			}
			if(Common.isContainsNavPerUnit(s)) {
				investmentTransactions.setNavOfUnit(s.split(":")[1].trim());
				break;
			}
		}
		//20231204
		if(Common.isDividend(line)) {
			investmentTransactions.setDividend(true);
		}
		//System.out.println("tostring: "+investmentTransactions.toString());
		return investmentTransactions;
	}

	public Wealth checkFeaturedFundsServicesInventmentTransactions(Wealth wealth) {
		ArrayList<InvestmentTransactions> investmentTransactionsList= wealth.getInvestmentTransactionsList();
		ArrayList<String> checkSettlementDateList= new ArrayList<>();
		ArrayList<String> checkTransactionTypeList= new ArrayList<>();
		ArrayList<String> checkTransactionIdList= new ArrayList<>();
		//检查结算日期是否为“TBC”，那么 1) 单位数量，2) NAV 和 3) 交易金额必须为“TBC”
		//检查结算日期是否为“TBC”，然后 1) 单位数量，2) NAV 和 3) txn 金额不得为“TBC”
		for (int i = 0; i < investmentTransactionsList.size(); i++) {
			InvestmentTransactions a = investmentTransactionsList.get(i);
			if(a.getSettlementDate().contains("TBC")) {
				if((!a.getNoOfUnit().contains("TBC"))||(!a.getNavOfUnit().contains("TBC"))&&(!a.getTransactionAmount().contains("TBC"))){
					a.setCheckTBC(false);
					checkSettlementDateList.add(a.getTransactionDescription());
				}
			}else {
				if((a.getNoOfUnit().contains("TBC"))||(a.getNavOfUnit().contains("TBC"))&&(a.getTransactionAmount().contains("TBC"))){
					if(Common.moveSpecialStringToDouble(a.getTransactionAmount())<Global.NO_OF_LIMIT_AMOUNT) {
						//金额太小，导致NoOfUnit为TBC
					}else {
						a.setCheckTBC(false);
						checkSettlementDateList.add(a.getTransactionDescription());
					}
				}
			}

			//交易类型不能为空且为以下任意一种
			if(!Common.is_one_of_shuzu_item_in_line(a.getTransactionType().trim(), TypeList)) {
				a.setCheckType(false);
				checkTransactionTypeList.add(a.getTransactionDescription());
			}

			//check transactionID
			if(a.getTransactionID().length()<1) {
				a.setCheckTransactionID(false);
				checkTransactionIdList.add(a.getTransactionDescription());
			}
			//ID pattern:SE + 10 digits unique txn no., e.g. SE0000066484
			String transactionID = a.getTransactionID().trim();
			if(transactionID.startsWith("S")) {
				if(Common.isTenDigits(transactionID.substring(2))) {
				}else {
					a.setCheckTransactionID(false);
					checkTransactionIdList.add(a.getTransactionDescription());
				}
			}else {
				if(transactionID.length()>0&&transactionID.equals("-")) {
					
				}else {
					a.setCheckTransactionID(false);
					checkTransactionIdList.add(a.getTransactionDescription());
				}
			}
		}
		if(checkSettlementDateList.size()<=0&&checkTransactionIdList.size()<=0&&checkTransactionTypeList.size()<=0) {
			wealth.setFeaturedFundsServicesInventmentTransactionsEachItemPass(true);
		}
		FeaturedFundsServicesFailList.addAll(checkSettlementDateList);
		FeaturedFundsServicesFailList.addAll(checkTransactionIdList);
		FeaturedFundsServicesFailList.addAll(checkTransactionTypeList);
//		System.out.println(checkSettlementDateList.toString());
//		System.out.println("checkTransactionIdList.toString():"+checkTransactionIdList.toString());
//		System.out.println(checkTransactionTypeList.toString());
//		System.out.println(FeaturedFundsServicesFailList.toString());
		
		return wealth;
	}
	
	public String getFundNameFromTransactionDescription(String fundName) {
		if(fundName.trim().contains(" ")) {
			return fundName.substring(0,fundName.indexOf(" ")).trim();
		}
		return fundName.trim();
	}
	public Wealth CompareFeaturedFundsServicesInvestmentTransactions(Wealth wealth) {
		//计算每个基金交易了多少个单位
		ArrayList<InvestmentTransactions> FeaturedFundsServicesInventmentTransactions= wealth.getFeaturedFundsServicesInventmentTransactions();
		HashMap<String, String> FeaturedFundsServicesTransactionsMap = new HashMap<>();
		System.out.println("FeaturedFundsServicesInventmentTransactions.size(): "+FeaturedFundsServicesInventmentTransactions.size());
		//获取持有基金的名称List
		ArrayList<String> FeaturedFundsServicesInventmentHoldingsNameList= new ArrayList<>();
		for (int i = 0; i < FeaturedFundsServicesInventmentTransactions.size(); i++) {
			String fundName = FeaturedFundsServicesInventmentTransactions.get(i).getTransactionDescription(); //TransactionDescription获取基金名称
			//String fundName0 =fundName.split(" ")[0].trim();
			//String fundName0 =fundName.substring(0,fundName.indexOf("(")).trim(); //20231204 update
			String fundName0 =getFundNameFromTransactionDescription(fundName); //20231204 update
			if(!FeaturedFundsServicesInventmentHoldingsNameList.contains(fundName0)) {
				FeaturedFundsServicesInventmentHoldingsNameList.add(fundName0);
			}
		}
		//根据每个名称，去遍历investmentTransactions中的交易记录，并计算出总交易值
		for (int i = 0; i < FeaturedFundsServicesInventmentHoldingsNameList.size(); i++) {
			String fundName = FeaturedFundsServicesInventmentHoldingsNameList.get(i);
			double unit = 0.0;
			for (int j = 0; j < FeaturedFundsServicesInventmentTransactions.size(); j++) {
				InvestmentTransactions a = FeaturedFundsServicesInventmentTransactions.get(j);
				String fundName1 = FeaturedFundsServicesInventmentTransactions.get(j).getTransactionDescription();
				String type =FeaturedFundsServicesInventmentTransactions.get(j).getTransactionType();
				if(a.isDividend()) continue; //20231204
				if(a.isIs_HDK_transation()) {
					System.out.println(a.getTransactionDescription());
					continue;
				}

				if(fundName1.contains(fundName)) {
					if(a.getSettlementDate().contains(wealth.getThisMonth())&&a.getSettlementDate().trim().startsWith("01")) {
						continue;
						//1日为交收日(SettlementDate)会算入初期,1日为交易日(transactionDate)不会算入初期
					}

					//如果SettlementDate 不是这个月，不需要不计算
					//if(!checkSettlementDateIncludeThisMonth(wealth,investmentTransactions.get(j))) continue;
					//如果SettlementDate 是上个月或之前的日期是要计算的
					if(!checkSettlementDateToConfirmWhetherNeedCalulate(FeaturedFundsServicesInventmentTransactions.get(j).getSettlementDate(),wealth)) continue;
					
					String NoOfUnit = FeaturedFundsServicesInventmentTransactions.get(j).getNoOfUnit();
					//交易單位No. of Unit: TBC  USD 0.01  金额太小，交易单位就是TBC
					if(NoOfUnit.contains("TBC")) {
						String amountString = FeaturedFundsServicesInventmentTransactions.get(j).getTransactionAmount();
						double amount = Common.moveSpecialStringToDouble(amountString);
						System.out.println("amount: "+amount);
						if(amount < Global.NO_OF_LIMIT_AMOUNT) continue;
					}
					if(Common.isContainsSubscriptionRegular(type)) {
						unit = unit+Common.moveSpecialStringToDouble(NoOfUnit);
					}else {
						unit = unit-Common.moveSpecialStringToDouble(NoOfUnit);
					}
				}
			}
			if(unit!=0.0) {
				//System.out.println(fundName+" 交易单位累计: "+unit);
				FeaturedFundsServicesTransactionsMap.put(fundName, Common.double_to_String_by_round_count(unit));
				
			}
		}
		wealth.setFeatureFundsServiceInvestmentTransactionsMap(FeaturedFundsServicesTransactionsMap);
		return wealth;
	}
	
	
	
	public boolean checkSettlementDateIncludeThisMonth(Wealth wealth,InvestmentTransactions investmentTransactions) {
		return investmentTransactions.getSettlementDate().contains(wealth.getThisMonth());
	}
	
	public boolean checkSettlementDateToConfirmWhetherNeedCalulate(String settlementDate,Wealth w) {
		if(settlementDate.contains("TBC")) return false; //未来日期，不需要计算
		String[] ss = settlementDate.trim().split(" ");
		if(ss.length != 3) return false;
		int year = Integer.parseInt(ss[2].trim());
		int monthIndex = Common.getMonthIndex(ss[1].trim());
		int thisYear = Integer.parseInt(w.getThisYear());
		int thisMonthIndex = Common.getMonthIndex(w.getThisMonth());
		//System.out.println("monthIndex: "+monthIndex);
		//System.out.println("thisMonthIndex: "+thisMonthIndex);
		if(year > thisYear) return false; //settlement 年份大于报告的年份，说明是未来日期，不计算
		if(year == thisYear) {
			if(monthIndex > thisMonthIndex) return false; //年份相同，settlement 月份大于报告的月份，说明是未来日期，不计算
			if(monthIndex <= thisMonthIndex) return true; //当月或当月之前需要计算
		}
		if(year < thisYear) return true; //settlement 年份小于报告的年份，说明是过去日期，计算
		return false;
	}
	
	//-check if the sum of fund settled unit = ending unit – beginning unit (circled at point 2)
	public Wealth checkSumOfFundSettledUnit(Wealth wealth) {
		ArrayList<TransactionHistoryAndPositions> FeaturedFundsServicesInventmentHoldingsList= wealth.getFeaturedFundsServicesInventmentHoldings();
		HashMap<String, String> FeaturedFundsServicesTransactionsMap = wealth.getFeatureFundsServiceInvestmentTransactionsMap();
		
		for (String key : FeaturedFundsServicesTransactionsMap.keySet()) {
			String NoOfUnit = FeaturedFundsServicesTransactionsMap.get(key);
			for (int j = 0; j < FeaturedFundsServicesInventmentHoldingsList.size(); j++) {
				if(FeaturedFundsServicesInventmentHoldingsList.get(j).getFundName().contains(key)) {
//					if(FeaturedFundsServicesInventmentHoldingsList.get(j).)
					String startUnit = FeaturedFundsServicesInventmentHoldingsList.get(j).getStartUnitNumber();
					String endUnit = FeaturedFundsServicesInventmentHoldingsList.get(j).getEndUnitNumber();
					String NoOfUnitCalc=Common.double_to_String_by_round_count(Common.moveSpecialStringToDouble(endUnit)-Common.moveSpecialStringToDouble(startUnit));
					//System.out.println("startUnit: "+startUnit);
					//System.out.println("endUnit: "+endUnit);
					//System.out.println("NoOfUnit: "+NoOfUnit);
					//System.out.println("NoOfUnitCalc: "+NoOfUnitCalc);
					if(!Common.isApproximatelyEqual(Common.moveSpecialStringToDouble(NoOfUnit), Common.moveSpecialStringToDouble(NoOfUnitCalc))) {
						System.out.println("FeaturedFundsServicesUtils_5.java "+key+" 交易单位累计: "+NoOfUnit+"月初月底差额为："+Common.moveSpecialStringToDouble(NoOfUnitCalc));
						FeaturedFundsServicesFailList.add(key+" 交易单位累计: "+NoOfUnit+",月初月底差额为："+Common.moveSpecialStringToDouble(NoOfUnitCalc));
					}
					//System.out.println(key+" 交易单位累计: "+NoOfUnit+"月初月底差额为："+Common.moveSpecialStringToDouble(NoOfUnitCalc));
					break;
				}
			}
		}
		
		return wealth;
	}
	
	public int getUSD_HKD_Flag(String line) {
		int amount_flag=0;
		if(line.contains("USD")&line.contains("HKD")) {
			amount_flag=1;
		}else if(line.contains("USD")&(!line.contains("HKD"))){
			amount_flag=2;
		}else if(!line.contains("USD")&(line.contains("HKD"))){
			amount_flag=3;
		}
		return amount_flag;
	}
	
	public String getTransactionDescriptionFromLine(String line,int amount_flag,int len,int typeIndex,String[] ss) {
		int lastIndexOfTransactionDescription=0;
		if(amount_flag==0) {
			lastIndexOfTransactionDescription = len-2-1;
		}else if(amount_flag==2||amount_flag==3) {
			lastIndexOfTransactionDescription=len-3-1;
		}else{
			lastIndexOfTransactionDescription=len-4-1;
		}
		String temp="";
		for (int i = typeIndex+2; i <= lastIndexOfTransactionDescription; i++) {
			temp +=ss[i]+" ";
		}
		return temp.trim();
	}

	public static String[] setStringToStringArray(String line) {
		String[] s1 = line.split(" ");
		ArrayList<String> list = new ArrayList<>();
		for (int i = 0; i < s1.length; i++) {
			if(s1[i].trim().length()>=1) {
				list.add(s1[i].trim());
			}
		}
		String[] ss= new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			ss[i]=list.get(i);
		}
		return ss;
	}
	
}
