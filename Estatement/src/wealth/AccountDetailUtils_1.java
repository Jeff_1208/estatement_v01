package wealth;

import java.util.ArrayList;
import com.entity.AccountDetails;
import com.entity.Wealth;
import com.utils.Common;
import com.utils.Global;

public class AccountDetailUtils_1 {
	private ArrayList<String> AccountDetailsFailList=new ArrayList<>();
	
	public Wealth getAccountDetailsList(Wealth wealth) {
		ArrayList<AccountDetails> accountDetailsList= new ArrayList<>();
		ArrayList<String> InvestmentNameList= new ArrayList<>();
		String content = wealth.getContent();
		getFxRate(content,wealth);
		String[] content_list = content.split("\n");
		boolean wealth_flag=false;
		boolean fund_flag=false;
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsEmailAddress(line))  continue;
			if(Common.isContainsAccountDetails(line)) {
				wealth_flag =true;
			}
			if(Common.isContainsFeaturedFunds(line)) {
				fund_flag =true;
			}
			if(Common.isContainsIndividualFundPositions2(line)) {
				System.out.println(line);
				System.out.println("KKKKKKKKKKKKKK");
				break;
			}
			if(Common.isContainsStatementPeriod(line)) {
				String date = "";
				if(line.contains(Global.STATEMENT_PERIOD)) {
					date = line.substring(Global.STATEMENT_PERIOD.length()+1);
				}
				if(line.contains(Global.STATEMENT_PERIOD_SIMPLIFIED_CHINESE)) {
					date = line.substring(Global.STATEMENT_PERIOD_SIMPLIFIED_CHINESE.length()+1);
				}
				wealth.setThisMonth(date.split(" ")[1].trim());
				wealth.setThisYear(date.split(" ")[2].trim());
			}
			/***

			//获取自选基金金额
			if(Common.isContainsFeaturedFunds(line)) {
				String featureFundsLine  = content_list[i+1];
				//System.out.println(featureFundsLine);
				//美元 USD USD 103.61 USD 112.31 HKD 881.62
				int index_usd2=featureFundsLine.lastIndexOf("USD");
				int index_hdk=featureFundsLine.lastIndexOf("HKD");
				wealth.setFeaturedFundsEndValueDollar(Common.string_move_special(featureFundsLine.substring(index_usd2+3,index_hdk).trim()));
				wealth.setFeaturedFundsEndValueHKD(Common.string_move_special(featureFundsLine.substring(index_hdk+3).trim()));
				String sub_line = featureFundsLine.substring(0,index_usd2);  //fundname 里面含有USD,所以需要从后往前寻找USD
				int last_index_usd=sub_line.lastIndexOf("USD");
				wealth.setFeaturedFundsStartValueDollar(Common.string_move_special(featureFundsLine.substring(last_index_usd+3,index_usd2).trim()));
//				System.out.println("Feature Funds start Dollar: "+wealth.getFeaturedFundsStartValueDollar());
//				System.out.println("Feature Funds end Dollar: "+wealth.getFeaturedFundsEndValueDollar());
//				System.out.println("Feature Funds HKD: "+wealth.getFeaturedFundsEndValueHKD());
			}
			***/

			if(line.startsWith("美元")&&fund_flag&&Common.the_line_is_have_a_double_string(line)&&Common.isValidLine(line)) {
				//美元 USD USD 103.61 USD 112.31 HKD 881.62
				System.out.println("美元： "+line);
				int index_usd2=line.lastIndexOf("USD");
				int index_hdk=line.lastIndexOf("HKD");
				wealth.setFeaturedFundsEndValueDollar(Common.string_move_special(line.substring(index_usd2+3,index_hdk).trim()));
				wealth.setFeaturedFundsEndValueHKD(Common.string_move_special(line.substring(index_hdk+3).trim()));
				String sub_line = line.substring(0,index_usd2);  //fundname 里面含有USD,所以需要从后往前寻找USD
				int last_index_usd=sub_line.lastIndexOf("USD");
				wealth.setFeaturedFundsStartValueDollar(Common.string_move_special(line.substring(last_index_usd+3,index_usd2).trim()));
				accountDetailsList.add(setAccountDetail(line));//自选基金也要添加进行计算
			}
			if(line.startsWith("港元")&&fund_flag&&Common.the_line_is_have_a_double_string(line)&&Common.isValidLine(line)) {
				//港元 HKD HKD 0.00 HKD 99.73 HKD 99.73
				
				String[] ss = line.split(" ");
				wealth.setHKDFeaturedFundsStartValue(ss[3].trim());
				wealth.setHKDFeaturedFundsEndValue(ss[7].trim());
				if(!ss[5].trim().equals(ss[7].trim())) {
					wealth.setHKDFeaturedFundsTotalHKDPass(false);
				}
				
				AccountDetails accountDetails = new AccountDetails();
				accountDetails.setDollarFund(false);
				accountDetails.setGoalName("港元");
				accountDetails.setStartValueDollar("0.0");
				accountDetails.setEndValueDollar("0.0");
				accountDetails.setEndValueHKD(ss[7].trim());
				System.out.println("港元： "+line+"  Amount: "+ss[7].trim());
				accountDetailsList.add(accountDetails);//自选基金也要添加进行计算
			}

			if(Common.isContainsAccountTotal(line)) {
				System.out.println(line);

				if(Common.the_line_is_have_a_double_string(line)&&Common.isValidLine(line)){
					if(line.contains("USD")) {
						int index_usd=line.lastIndexOf("USD");
						int index_hdk=line.lastIndexOf("HKD");
						wealth.setAccountTotalDollarCapture(Common.string_move_special(line.substring(index_usd+3,index_hdk).trim()));
					}
					if(line.contains("HKD")) {
						int index_hdk=line.lastIndexOf("HKD");
						wealth.setAccountTotalHKDCapture(Common.string_move_special(line.substring(index_hdk+3).trim()));
					}
					
				}else {
					// 金额与 賬戶總額 Account Total 不在同一行， 特殊场景
					String temp_line = content_list[i-1];
					if(temp_line.contains("USD")) {
						int index_usd=temp_line.lastIndexOf("USD");
						int index_hdk=temp_line.lastIndexOf("HKD");
						wealth.setAccountTotalDollarCapture(Common.string_move_special(temp_line.substring(index_usd+3,index_hdk).trim()));
					}
					if(temp_line.contains("HKD")) {
						int index_hdk=temp_line.lastIndexOf("HKD");
						wealth.setAccountTotalHKDCapture(Common.string_move_special(temp_line.substring(index_hdk+3).trim()));
					}
					System.out.println("account total format error");
					wealth.setAccountTotalFormat(false);
				}
				break;
			}
			
			if(!wealth_flag) {continue;}
			
			if(fund_flag) {continue;}  //因为在上面有添加自选基金到accountDetailsList，所以自选基金不需要在进行下面的代码
			if(Common.the_line_is_have_a_double_string(line)&&Common.isValidLine(line)){
				//自选基金的所有选项也要添加到这个当中
				//System.out.println(line);
				accountDetailsList.add(setAccountDetail(line));
				InvestmentNameList.add(getInvestmentName(line));
			}
		}
		//System.out.println("wealth.getAccountTotalHKDCapture(): "+wealth.getAccountTotalHKDCapture());
		
//		if(wealth.getAccountTotalDollarCapture()==null) {
//			System.out.println(wealth.getCustomerID() + " 首页美元金额为空");
//			wealth.setAccountTotalDollarCapture("0.00");
//			wealth.setTotalUSDShow(false);
//		}
		if(wealth.getAccountTotalHKDCapture()==null) {
			System.out.println(wealth.getCustomerID() + " 首页港币金额为空");
			wealth.setAccountTotalHKDCapture("0.00");
			wealth.setTotalHKDShow(false);
		}
		wealth.setAccountDetailsList(accountDetailsList);
		wealth.setInvestmentNameList(InvestmentNameList);
		//System.out.println(InvestmentNameList.toString());
		wealth=checkAccountDetail(wealth);
		return wealth;
	}
	
	public static String getInvestmentName(String line) {
		int index_usd = line.indexOf("USD");
		return line.substring(0, index_usd).trim();
	}
	
	
	public AccountDetails setAccountDetail(String line) {
		AccountDetails accountDetails = new AccountDetails();
		int index_usd1=line.indexOf("USD");
		int index_usd2=line.lastIndexOf("USD");
		int index_hdk=line.lastIndexOf("HKD");
		accountDetails.setGoalName(line.substring(0,index_usd1).trim());
		accountDetails.setStartValueDollar(Common.string_move_special(line.substring(index_usd1+3,index_usd2).trim()));
		accountDetails.setEndValueDollar(Common.string_move_special(line.substring(index_usd2+3,index_hdk).trim()));
		accountDetails.setEndValueHKD(Common.string_move_special(line.substring(index_hdk+3).trim()));
		return accountDetails;
	}
	
	public Wealth getFxRate(String content,Wealth wealth) {
		String[] content_list = content.split("\n");
		boolean wealth_flag=false;
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsRemarks(line)) {
				wealth_flag =true;
			}
			if(!wealth_flag) {continue;}
			if(line.contains("USD/HKD:")) {
				String s = line.substring(8).trim();
				wealth.setFxRate(Common.string_to_double(s));
				break;
			}
		}
		return wealth;
	}
	
	
	public Wealth checkAccountDetail(Wealth wealth) {
		ArrayList<AccountDetails> accountDetailsList= wealth.getAccountDetailsList();
		double totalDollarCalc=0.0;
		double totalHKDCalc=0.0;
		for (int i = 0; i < accountDetailsList.size(); i++) {
			AccountDetails a = accountDetailsList.get(i);
			//System.out.println("totalDollarCalc: "+totalDollarCalc);
			//System.out.println("totalHKDCalc: "+totalHKDCalc);
			totalDollarCalc+=Common.moveSpecialStringToDouble(a.getEndValueDollar());
			totalHKDCalc+=Common.moveSpecialStringToDouble(a.getEndValueHKD());
			//System.out.println("totalDollarCalc: "+totalDollarCalc);
			//System.out.println("totalHKDCalc: "+totalHKDCalc);
			
			//对比美元和港币汇率换算
			double r=Common.getMultiplicationResult(Common.string_move_special(a.getEndValueDollar()),wealth.getFxRate()+"");
			if(!Common.isApproximatelyEqual(r, Common.moveSpecialStringToDouble(a.getEndValueHKD()))) {
				if(!a.isDollarFund()) continue; //不是美元基金，不需要验证汇率
				//System.out.println(r+"\t"+a.getEndValueHKD());
				wealth.setAccountDetailsEachItemPass(false);
				Common.addFailList(AccountDetailsFailList, a.getEndValueHKD());
			}
		}
//		if(Common.isApproximatelyEqual(totalDollarCalc, Common.moveSpecialStringToDouble(wealth.getAccountTotalDollarCapture()))) {
//			wealth.setAccountTotalDollarCalc(Common.double_to_String_by_round_count(totalDollarCalc));
//			wealth.setAccountDetailsTotalDollar(true);
//		}
		//20230104 首页 去除了setAccountTotalDollarCapture，所以把setAccountTotalDollarCalc和setAccountTotalDollarCalc设置相同的值
		if(Common.isApproximatelyEqual(totalDollarCalc, totalDollarCalc)) {
			wealth.setAccountTotalDollarCalc(Common.double_to_String_by_round_count(totalDollarCalc));
			wealth.setAccountDetailsTotalDollar(true);
			wealth.setAccountTotalDollarCapture(Common.double_to_String_by_round_count(totalDollarCalc));
		}
		
		wealth.setAccountTotalHKDCalc(Common.double_to_String_by_round_count(totalHKDCalc));
		if(Common.isApproximatelyEqual(totalHKDCalc, Common.moveSpecialStringToDouble(wealth.getAccountTotalHKDCapture()))) {
			wealth.setAccountDetailsTotalHKD(true);
		}	
		
		//单独的港币是美元的，所以：  账户港币总额 = HDKCalc + 单独的港币基金金额 
		double HDKCalc = Common.getMultiplicationResult(wealth.getAccountTotalDollarCapture(),wealth.getFxRate()+"");
		if(Common.isApproximatelyEqual(HDKCalc  +Common.moveSpecialStringToDouble(wealth.getHKDFeaturedFundsEndValue()) ,
				Common.moveSpecialStringToDouble(wealth.getAccountTotalHKDCapture()))) {
			wealth.setAccountDetailsDollarEqualHKD(true);
		}else {
			AccountDetailsFailList.add(wealth.getAccountTotalHKDCapture());
		}
		
//		System.out.println("totalDollarCalc : "+totalDollarCalc);
//		System.out.println("totalHKDCalc : "+totalHKDCalc);
//		System.out.println(" wealth.getAccountTotalHKDCapture(): "+wealth.getAccountTotalHKDCapture());
//		System.out.println(" HDKCalc: "+HDKCalc);
		
//		System.out.println(wealth.isAccountDetailsDollarEqualHKD());
//		System.out.println(wealth.isAccountDetailsEachItemPass());
//		System.out.println(wealth.isAccountDetailsTotalDollar());
//		System.out.println(wealth.isAccountDetailsTotalHKD());
//		System.out.println(wealth.getAccountTotalDollarCalc());
//		System.out.println(wealth.getAccountTotalDollarCapture());
//		System.out.println(wealth.getAccountTotalHKDCalc());
//		System.out.println(wealth.getAccountTotalHKDCapture());
		wealth.setAccountDetailsFailList(AccountDetailsFailList);
		return wealth;
	}
}
