package wealth;

import java.util.ArrayList;
import com.entity.CashDividend;
import com.entity.Wealth;
import com.utils.Common;

public class CashDividendSummaryUtils_6 {

	public Wealth getCashDividendList(Wealth wealth) {
		ArrayList<CashDividend> CashDividendList= new ArrayList<>();
		String content = wealth.getContent();
		String[] content_list = content.split("\n");
		boolean CashDividendFlag=false;
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsCashDividendSummary(line)) {
				CashDividendFlag =true;
			}
			if(!CashDividendFlag) {continue;}
			if(Common.isContainsFundPlatformFeeSummary(line)) {
				break;
			}
			if(Common.isWealthTitleString(line)) continue;
			if(Common.isContainsRemarks(line)||Common.isContainsRemarks2(line)) {
				break;
			}
			if(Common.the_line_is_have_a_double_string(line)&&Common.isValidLine(line)){
				//System.out.println(line);
				CashDividendList.add(setCashDividend(line));
			}
		}
		wealth.setCashDividendList(CashDividendList);
		wealth = checkCashDividend(wealth);
		return wealth;
	}
	
	public Wealth checkCashDividend(Wealth wealth) {
		ArrayList<CashDividend> CashDividendList= wealth.getCashDividendList();
		double dividend =0.0;
		for (int i = 0; i < CashDividendList.size(); i++) {
			CashDividend a = CashDividendList.get(i);
			dividend+=Common.moveSpecialStringToDouble(a.getDividendAmount());
		}
		wealth.setTotalCashDividend(Common.double_to_String_by_round_count(dividend));
		return wealth;
	}
	
	public CashDividend setCashDividend(String line) {
		//Allianz Global Artfcl Intlgc AT USD 15 Mar 2023 USD 32.47
		ArrayList<Integer> list = Common.getEmptyListFromString(line);
		//System.out.println(list.toString()+"\tlist.size(): "+list.size());
		int size = list.size();
		CashDividend cashDividend = new CashDividend();
		cashDividend.setFundName(line.substring(0, list.get(size-5)).trim());
		cashDividend.setDividendPaidDATE(line.substring(list.get(size-5), list.get(size-2)).trim());
		cashDividend.setDividendCurrency(line.substring(list.get(size-2), list.get(size-1)).trim());
		cashDividend.setDividendAmount(line.substring(list.get(size-1)).trim());
		//System.out.println(cashDividend.toString());
		return cashDividend;
	}

}
