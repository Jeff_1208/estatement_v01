package wealth;

import java.io.File;
import java.util.ArrayList;
import com.entity.Wealth;
import com.main.Estatement_Main;
import com.pdf.PDF;

public class WealthMainUtils {
	
	private String result_file_path="";
	public String  get_result_file_path() {
		return result_file_path;
	}
	
	public static void main(String[] args) {
		WealthMainUtils m = new WealthMainUtils();
		Wealth wealth = new Wealth();
		String folder_path = "";
		String path="";

		
		folder_path = "C:\\Users\\jeff.xie\\Desktop\\w";
		folder_path = "D:/Project/NewData/Wealth/pdf/20230426";
		folder_path = "D:/Project/NewData/Wealth/pdf/20230504";
		folder_path = "D:/Project/NewData/Wealth/pdf/20230904";
//		folder_path = "D:/Project/NewData/Wealth/pdf/20230921";
		folder_path = "D:/Project/NewData/Wealth/pdf/20240426";
		//folder_path = "D:/Project/NewData/Wealth/pdf/20240403";
		path = "D:/Project/NewData/Wealth/pdf/20220809/MonthlyStatement_8000000246-1_20220803.pdf";
		path = "C:\\Users\\jeff.xie\\Desktop\\w\\MonthlyStatement_8000191648-1_20230104.pdf";
		path = "D:/Project/NewData/Wealth/pdf/20220809/MonthlyStatement_8000000246-1_20220803.pdf";
		path = "D:/Project/NewData/Wealth/pdf/20230426/MonthlyStatement_8000046899-1_20230321.pdf";
		path = "D:/Project/NewData/Wealth/pdf/20230426/MonthlyStatement_8000000324-1_20230503.pdf";
		path = "D:/Project/NewData/Wealth/pdf/20230904/MonthlyStatement_8000168557-1_20230904.pdf";
		m.run_all_wealth(folder_path);
//		m.wealthTest(path,wealth);
	}


	@SuppressWarnings("static-access")
	public  ArrayList<Wealth> run_all_wealth(String path) {
		long start_time =System.currentTimeMillis();
		ArrayList<String> file_List = new ArrayList<>();
		ArrayList<Wealth> wealthList = new ArrayList<>();
		File file = new File(path);
		File[] fs = file.listFiles();
		for(File f:fs) {
			String file_name = f.getName();
			if(file_name.endsWith(".pdf")) {
				file_List.add(f.getAbsolutePath().trim());
				Wealth wealth = new Wealth();
				String id = file_name.split("_")[1].trim();
				wealth.setCustomerID(id);
				System.out.println("wealth.getCustomerID(): "+wealth.getCustomerID());
				Estatement_Main.error_customer =wealth.getCustomerID();
				wealth = wealthTest(f.getAbsolutePath().trim(), wealth);
				wealthList.add(wealth);
			}
		}
		
		WealthReport wealthReport = new WealthReport();
		String filepath=wealthReport.Print_result_to_Excel(wealthList, path);
		result_file_path =filepath;
		long end_time =System.currentTimeMillis();
		long cost_time =end_time-start_time;
		System.out.println("cost time: "+cost_time);
		return wealthList;
	}
	
	public Wealth wealthTest(String filepath,Wealth wealth) {
		PDF p = new PDF();
		String content = p.Get_PDF_Content(filepath);
		//System.out.println(content);
		wealth.setContent(content);
		AccountDetailUtils_1 wealthUtils = new AccountDetailUtils_1();
		wealth=wealthUtils.getAccountDetailsList(wealth);
		
		IndividualFundPositionsUtils_2 individualFundPositionsUtils = new IndividualFundPositionsUtils_2();
		wealth=individualFundPositionsUtils.getIndividualFundPositionsList(wealth);
		TransactionHistoryAndPositionsUtils_3 transactionHistoryAndPositionsUtils = new TransactionHistoryAndPositionsUtils_3();
		wealth=transactionHistoryAndPositionsUtils.getTransactionHistoryAndPositionsList(wealth);
		InvestmentTransactionsUtils_4 investmentTransactionsUtils = new InvestmentTransactionsUtils_4();
		wealth=investmentTransactionsUtils.getInvestmentTransactionsList(wealth);
		FeaturedFundsServicesUtils_5 featuredFundsServicesUtils = new FeaturedFundsServicesUtils_5();
		featuredFundsServicesUtils.getFeaturedFundsServicesInventmentHoldingsList(wealth);
		featuredFundsServicesUtils.getFeaturedFundsServicesInventmentTransactionsList(wealth);
		CashDividendSummaryUtils_6 cashDividendSummaryUtils = new CashDividendSummaryUtils_6();
		wealth=cashDividendSummaryUtils.getCashDividendList(wealth);
		return wealth;
	}

}
