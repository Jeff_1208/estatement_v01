package wealth;

import java.util.ArrayList;
import com.entity.IndividualFundPositions;
import com.entity.Wealth;
import com.utils.Common;

public class IndividualFundPositionsUtils_2 {
	
	//第一部分持有的基金，持有基金名称是不会重复的
	//IndividualFundPositions总金额要和title总金额相等，包含Feature Funds金额
	private ArrayList<String> IndividualFundPositionsFailList=new ArrayList<>();
	
	public Wealth getIndividualFundPositionsList(Wealth wealth) {
		ArrayList<IndividualFundPositions> individualFundPositionsList= new ArrayList<>();
		String content = wealth.getContent();
		String[] content_list = content.split("\n");
		ArrayList<String> InvestmentNameList= wealth.getInvestmentNameList();
		boolean individualFundPositionsFlag=false;
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsIndividualFundPositions2(line)) {
				individualFundPositionsFlag =true;
			}
			if(!individualFundPositionsFlag) {continue;}
			if(Common.isContainsTransactionHistoryAndPositions(line)||Common.isContainsFeaturedFundsServices(line)) {
				break;
			}
			if(Common.isContainsInvestmentTransaction(line)||Common.isContainsRemarks(line)) {
				break;
			}
			if(Common.isWealthTitleString(line)) continue;
			if(Common.the_line_is_have_a_double_string(line)&&Common.isValidLine(line)){
				if(!Common.is_one_of_list_item_in_line(line, InvestmentNameList)) {
					//System.out.println(line);
					//ArrayList<Integer> list = Common.getEmptyListFromString(line);
					//int size = list.size();
					//String v =line.substring(list.get(size-1)).trim();
					//String v =line.substring(list.get(size-3), list.get(size-2)).trim();
					//System.out.println(Common.moveSpecialStringToDouble(v));
					individualFundPositionsList.add(setIndividualFundPositions(line));
				}else {
					System.out.println(i);
					System.out.println(line);
				}
			}
		}
		
		wealth.setIndividualFundPositionsList(individualFundPositionsList);
		wealth=checkIndividualFundPositions(wealth);
		return wealth;
	}
	
	public IndividualFundPositions setIndividualFundPositions(String line) {
		ArrayList<Integer> list = Common.getEmptyListFromString(line);
		//System.out.println(list.toString()+"\tlist.size(): "+list.size());
		int size = list.size();
		IndividualFundPositions individualFundPositions = new IndividualFundPositions();
		if(Common.isDividend(line)) {
			individualFundPositions.setDividend(true);
		}
//		individualFundPositions.setFundName(line.substring(0, list.get(2)).trim());
//		individualFundPositions.setStartUnitNumber(line.substring(list.get(2), list.get(3)).trim());
//		individualFundPositions.setEndUnitNumber(line.substring(list.get(3), list.get(4)).trim());
//		individualFundPositions.setNavPerUnit(line.substring(list.get(4), list.get(5)).trim());
//		individualFundPositions.setMarketValueDollar(line.substring(list.get(6), list.get(7)).trim());
//		individualFundPositions.setMarketValueHKD(line.substring(list.get(8)).trim());
		
		individualFundPositions.setDollarFund(Common.checkIsDollarFund(line)); //20230921
		individualFundPositions.setFundName(line.substring(0, list.get(size-7)).trim());
		individualFundPositions.setStartUnitNumber(line.substring(list.get(size-7), list.get(size-6)).trim());
		individualFundPositions.setEndUnitNumber(line.substring(list.get(size-6), list.get(size-5)).trim());
		individualFundPositions.setNavPerUnit(line.substring(list.get(size-5), list.get(size-4)).trim());
		individualFundPositions.setMarketValueDollar(line.substring(list.get(size-3), list.get(size-2)).trim());
		individualFundPositions.setMarketValueHKD(line.substring(list.get(size-1)).trim());
		return individualFundPositions;
	}
	
	public Wealth checkIndividualFundPositions(Wealth wealth) {
		ArrayList<IndividualFundPositions> individualFundPositionsList= wealth.getIndividualFundPositionsList();
		double totalDollarCalc=0.0;
		double totalHKDCalc=0.0;
		
		for (int i = 0; i < individualFundPositionsList.size(); i++) {
			IndividualFundPositions a = individualFundPositionsList.get(i);
			if(!a.isDollarFund()) continue; //不是美元金额，先不进行计算，后面单独重新计算
			totalDollarCalc+=Common.moveSpecialStringToDouble(a.getMarketValueDollar());
			totalHKDCalc+=Common.moveSpecialStringToDouble(a.getMarketValueHKD()); //不管是不是港币基金，都要对港币进行累加
			
			//检测 美元*汇率 结果是不是等于港币金额
			double r=Common.getMultiplicationResult(Common.string_move_special(a.getMarketValueDollar()),wealth.getFxRate()+"");
			if(!Common.isApproximatelyEqual(r, Common.moveSpecialStringToDouble(a.getMarketValueHKD()))) {
				//System.out.println("AAAAAAAA:"+r+"\t"+a.getMarketValueHKD());
				wealth.setIndividualFundPositionsEachItemPass(false);
				Common.addFailList(IndividualFundPositionsFailList, a.getMarketValueHKD());
			}
			//检测 月底单位数量*单位金额 结果是不是等于美元金额
			//Market Value = No. of Unit in month end x NAV per unit in month end (per fund currency)
			double marketValueCalc = Common.getMultiplicationResult(a.getEndUnitNumber(), a.getNavPerUnit());
			if(!Common.isApproximatelyEqual(marketValueCalc, Common.moveSpecialStringToDouble(a.getMarketValueDollar()))) {
				//System.out.println("BBBBBBBB:"+marketValueCalc+"\t"+a.getMarketValueHKD());
				wealth.setIndividualFundPositionsEachItemOfNAVPass(false);
				Common.addFailList(IndividualFundPositionsFailList, a.getMarketValueHKD());
			}else {
				//System.out.println(a.getMarketValueDollar()+"\t"+marketValueCalc);
			}
		}
		
		double totalHKDFeatureFundsCalc=0.0;  //20230921
		for (int i = 0; i < individualFundPositionsList.size(); i++) {
			IndividualFundPositions a = individualFundPositionsList.get(i);
			if(!a.isDollarFund()) {
				totalHKDFeatureFundsCalc+=Common.moveSpecialStringToDouble(a.getMarketValueHKD());
			}
		}
		//System.out.println("totalDollarCalc"+totalDollarCalc);
		wealth.setIndividualFundPositionsTotalDollarCalc(Common.double_to_String(totalDollarCalc));
		//20230912 由于港币的feature funds有港币场景，所以需要增加单独的港币基金
		wealth.setIndividualFundPositionsHKDCalc(Common.double_to_String(totalHKDCalc+totalHKDFeatureFundsCalc));
		
		//计算美元金额是否与抓取的金额相同
		if(Common.isApproximatelyEqual(totalDollarCalc, Common.moveSpecialStringToDouble(wealth.getAccountTotalDollarCapture()))) {
			wealth.setIndividualFundPositionsTotalDollarPass(true);
		}
		//Account Total (Wealth Balance) ~= Sum (MV of all funds HKD equivalent) 总金额与每一项港币金额计算总和对比
		//20230912 由于港币的feature funds有港币场景，所以需要增加单独的港币基金
		if(Common.isApproximatelyEqual(totalHKDCalc+totalHKDFeatureFundsCalc, Common.moveSpecialStringToDouble(wealth.getAccountTotalHKDCapture()))) {
			wealth.setIndividualFundPositionsTotalHKDPass(true);
		}	
		
		//Market Value (HKD Equivalent) = MV per fund x FX rate (currency/HKD) 美元乘以汇率是否等于港币
		double HDKCalc = Common.getMultiplicationResult(totalDollarCalc+"",wealth.getFxRate()+"");
		//20230912 由于港币的feature funds有港币场景，所以需要增加单独的港币基金
		if(Common.isApproximatelyEqual(HDKCalc+totalHKDFeatureFundsCalc, Common.moveSpecialStringToDouble(wealth.getAccountTotalHKDCapture()))) {
			wealth.setIndividualFundPositionsDollarEqualHKDPass(true);
		}
		
		
//		System.out.println(wealth.isIndividualFundPositionsDollarEqualHKDPass());
//		System.out.println(wealth.isIndividualFundPositionsTotalHKDPass());
//		System.out.println(wealth.isIndividualFundPositionsTotalDollarPass());
//		System.out.println(wealth.isIndividualFundPositionsEachItemPass());
//		System.out.println(wealth.isIndividualFundPositionsEachItemOfNAVPass());
//		System.out.println(wealth.getIndividualFundPositionsHKDCalc());
//		System.out.println(wealth.getIndividualFundPositionsTotalDollarCalc());
		
		wealth.setIndividualFundPositionsFailList(IndividualFundPositionsFailList);
		
		return wealth;
	}
	
	
}
