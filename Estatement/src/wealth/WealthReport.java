package wealth;

import java.io.FileOutputStream;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.entity.Wealth;
import com.utils.Common;

public class WealthReport {
	
	public static void main(String[] args) {
		WealthReport wealthReport = new WealthReport();
		wealthReport.Print_result_to_Excel(new ArrayList<Wealth>(), "D:");
	}
	String[] title_list= {
			"Customer_ID",
			"Account Detail Total Dollar Capture","Account Detail Total Dollar Calc",
			"Account Detail Total HKD Capture", "Account Detail Total HKD Calc",
			"Account Detail Dollar Equal HKD","Account Detail Fail Ref",
			"Individual Fund Positions Dollar Calc","Individual Fund Positions HKD Calc",
			"Individual Fund Positions Dollar Equal HKD","Individual Fund Positions Fail Ref",
			"Transaction History And Positions Dollar Calc","Transaction History And Positions HKD Calc",
			"Transaction History And Positions Dollar Equal HKD","Transaction History And Positions Fail Ref",
			"Settlement Date Fail List","Transaction Id Fail List","Transaction Type Fail List","Investment Transactions Fail Ref",
			"(Dollar)Feature Funds Dollar Amount","(Dollar)Feature Funds HKD Amount","(HKD)Feature Funds Amount",
			"Feature Funds Holding result","Feature Funds Transaction result","Feature Funds Fail List",
			"Cash Dividend",
			"FX Rate","Comment","Test Result"};
	public String Print_result_to_Excel(ArrayList<Wealth> Wealth_List,String folder_path) {
		String filename = folder_path+"/Wealth_result_"+Common.get_time_string()+".xlsx";
		//创建工作薄对象
		FileOutputStream out;
		try {
			XSSFWorkbook workbook=new XSSFWorkbook();//这里也可以设置sheet的Name
			//创建工作表对象
			XSSFSheet sheet = workbook.createSheet();

			//int col1width = sheet.getColumnWidth(0);
			//System.out.println("col1width:  "+col1width);
			//sheet.setColumnWidth(0, 2*col1width);


			for(int i=0;i<title_list.length;i++) {
				sheet.setColumnWidth(i, 4500);
			}
			//创建工作表的行
			XSSFRow row = sheet.createRow(0);//设置第一行，从零开始
			row.setHeightInPoints(50);  //设置单元格高度
			for(int i=0;i<title_list.length;i++) {
				XSSFCellStyle yellow_style = SetCellStyle(workbook, 2);
				XSSFCell c = row.createCell(i);
				c.setCellStyle(yellow_style);
				c.setCellValue(title_list[i]);
				
			}
			for(int i=0;i<Wealth_List.size();i++) {
				Wealth wealth = Wealth_List.get(i);
				XSSFRow r= sheet.createRow(i+1);
				int cell_column=0;
				StringBuffer stringBuffer = new StringBuffer();
				for(int j=0;j<title_list.length;j++) {

					write_cell(r,cell_column,wealth,workbook,stringBuffer);
					cell_column+=1;
				}
				//System.out.println(e.getFile_name()+":    "+ stringBuffer.toString());
			}

			int row_number  = sheet.getLastRowNum();
			XSSFCellStyle yellow_style_3 = SetCellStyle(workbook, 3);
			XSSFCell cell=sheet.createRow(row_number+2).createCell(0);
			cell.setCellValue("部分Column解释如下：");
			cell.setCellStyle(yellow_style_3);
			
			int row_num =row_number+3;
			sheet.createRow(row_num).createCell(0).setCellValue("Column G: Account Detail部分数据美元乘以汇率不等于港币，根据港币金额查找对应行的数据");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("Column K: Individual Fund Positions部分数据美元乘以汇率不等于港币，或单位数量乘以单位净值不等于美元金额，根据港币金额查找对应行的数据");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("Column O: Transaction History And Positions部分数据美元乘以汇率不等于港币，或单位数量乘以单位净值不等于美元金额，根据港币金额查找对应行的数据");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("Column P: 结算日期有异常，根据港币金额查找对应行的数据");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("Column Q: 交易ID有异常，根据港币金额查找对应行的数据");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("Column R: 交易类型有异常，根据港币金额查找对应行的数据");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("Column S: 交易单位计算有异常，根据港币金额查找对应行的数据");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("column B 金额=column H 金额=（column L 金额 + column T 金额）");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("column C 金额=column I 金额=（column M 金额 + column U 金额）");

			workbook.setSheetName(0,"Result");
			out = new FileOutputStream(filename);
			workbook.write(out);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filename;
	}
	
	public String getAttributeContent(ArrayList<String> data) {
		if(data==null) {
			return "";
		}else {
			return data.toString();
		}
	}
	
	public void write_cell(XSSFRow r,int index,Wealth wealth,XSSFWorkbook workbook,StringBuffer stringBuffer) {
		XSSFCellStyle green_style = SetCellStyle(workbook, 1);
		XSSFCellStyle red_style = SetCellStyle(workbook, 0);
		XSSFCellStyle yellow_style_3 = SetCellStyle(workbook, 3);
		XSSFCell c =r.createCell(index);

		String cell_content ="";
		if(index==0) cell_content= wealth.getCustomerID();
		if(index==1) cell_content= wealth.getAccountTotalDollarCapture();
		if(index==2) cell_content= wealth.getAccountTotalDollarCalc();
		if(index==3) cell_content= wealth.getAccountTotalHKDCapture();
		if(index==4) cell_content= wealth.getAccountTotalHKDCalc();
		if(index==5) cell_content= wealth.isAccountDetailsDollarEqualHKD()+"";
		if(index==6) cell_content= getAttributeContent(wealth.getAccountDetailsFailList());
		if(index==7) cell_content= wealth.getIndividualFundPositionsTotalDollarCalc();
		if(index==8) cell_content= wealth.getIndividualFundPositionsHKDCalc();
		if(index==9) cell_content= wealth.isIndividualFundPositionsDollarEqualHKDPass()+"";
		if(index==10) cell_content=wealth.getIndividualFundPositionsFailList().toString();
		if(index==11) cell_content= wealth.getTransactionHistoryAndPositionsTotalDollarCalc();
		if(index==12) cell_content= wealth.getTransactionHistoryAndPositionsHKDCalc();
		if(index==13) cell_content= wealth.isTransactionHistoryAndPositionsDollarEqualHKDPass()+"";
		if(index==14) cell_content= wealth.getTransactionHistoryAndPositionsFailList().toString();
		if(index==15) cell_content= wealth.getCheckSettlementDateList().toString();
		if(index==16) cell_content= wealth.getCheckTransactionIdList().toString();
		if(index==17) cell_content= wealth.getCheckTransactionTypeList().toString();
		if(index==18) cell_content= wealth.getInvestmentTransactionsFailList().toString();
	
		if(index==0) {c.setCellStyle(yellow_style_3);}
		if(index==1) {
			if(wealth.isTotalUSDShow()) {c.setCellStyle(yellow_style_3);}else {
				stringBuffer.append("美元总金额显示异常;\n");
				c.setCellStyle(red_style);
			}
		}
		if(index==3) {
			if(wealth.isTotalHKDShow()) {
				c.setCellStyle(yellow_style_3);
			}else {
				stringBuffer.append("港币总金额显示异常;\n");
				c.setCellStyle(red_style);
			}
			if(!wealth.isAccountTotalFormat()) {
				stringBuffer.append("港币总金额格式异常;\n");
				c.setCellStyle(red_style);
			}
		}
		if(index==2) stringBuffer=setStyleByBooleanValue(wealth.isAccountDetailsTotalDollar(),c,green_style,red_style,stringBuffer,"Account Details Dollar 总金额异常;\n");
		if(index==4) stringBuffer=setStyleByBooleanValue(wealth.isAccountDetailsTotalHKD(),c,green_style,red_style,stringBuffer,"Account Details HKD 总金额异常;\n");
		if(index==7) stringBuffer=setStyleByBooleanValue(wealth.isIndividualFundPositionsTotalDollarPass(),c,green_style,red_style,stringBuffer,"Individual Fund Positions Dollar 总金额异常;\n");
		if(index==8) stringBuffer=setStyleByBooleanValue(wealth.isIndividualFundPositionsTotalHKDPass(),c,green_style,red_style,stringBuffer,"Individual Fund Positions HKD 总金额异常;\n");
		if(index==11) stringBuffer=setStyleByBooleanValue(wealth.isTransactionHistoryAndPositionsTotalDollarPass(),c,green_style,red_style,stringBuffer,"Transaction History And Positions Dollar 总金额异常;\n");
		if(index==12) stringBuffer=setStyleByBooleanValue(wealth.isTransactionHistoryAndPositionsTotalHKDPass(),c,green_style,red_style,stringBuffer,"Transaction History And Positions HKD 总金额异常;\n");
		if(index==5) stringBuffer=setStyleByBooleanValue(wealth.isAccountDetailsDollarEqualHKD(),c,green_style,red_style,stringBuffer,"Account Details 美金总额乘以汇率不等于港币总额;\n");
		if(index==9) stringBuffer=setStyleByBooleanValue(wealth.isIndividualFundPositionsDollarEqualHKDPass(),c,green_style,red_style,stringBuffer,"Individual Fund Positions HKD 美金总额乘以汇率不等于港币总额;\n");
		if(index==13) stringBuffer=setStyleByBooleanValue(wealth.isTransactionHistoryAndPositionsDollarEqualHKDPass(),c,green_style,red_style,stringBuffer,"Transaction History And Positions HKD  美金总额乘以汇率不等于港币总额;\n");

		if(index==6) stringBuffer=setStyleBySizeValue(wealth.getAccountDetailsFailList().size(),c,green_style,red_style,stringBuffer,"Account Details有美元余额乘以汇率不等于港币;\n");
		if(index==10) stringBuffer=setStyleBySizeValue(wealth.getIndividualFundPositionsFailList().size(),c,green_style,red_style,stringBuffer,"Individual Fund Positions HKD 有美元余额乘以汇率不等于港币;\n");
		if(index==14) stringBuffer=setStyleBySizeValue(wealth.getTransactionHistoryAndPositionsFailList().size(),c,green_style,red_style,stringBuffer,"Transaction History And Positions HKD有美元余额乘以汇率不等于港币;\n");
		if(index==15) stringBuffer=setStyleBySizeValue(wealth.getCheckSettlementDateList().size(),c,green_style,red_style,stringBuffer,"结算日期有异常;\n");
		if(index==16) stringBuffer=setStyleBySizeValue(wealth.getCheckTransactionIdList().size(),c,green_style,red_style,stringBuffer,"交易ID有异常;\n");
		if(index==17) stringBuffer=setStyleBySizeValue(wealth.getCheckTransactionTypeList().size(),c,green_style,red_style,stringBuffer,"交易类型有异常;\n");
		if(index==18) stringBuffer=setStyleBySizeValue(wealth.getInvestmentTransactionsFailList().size(),c,green_style,red_style,stringBuffer," 交易单位计算有异常;\n");
		if(index==18) {
			System.out.println(wealth.getCustomerID()+" wealth.getInvestmentTransactionsFailList().toString() : "+wealth.getInvestmentTransactionsFailList().size());
		}
		//FeatureFundsServices 增加两列，分别是Dollar和HKD的金额
		if(index==19) {
			cell_content = wealth.getFeaturedFundsEndValueDollar();
			c.setCellStyle(green_style);
		}
		if(index==20) {
			cell_content = wealth.getFeaturedFundsEndValueHKD();
			c.setCellStyle(green_style);
		}
		//20230921 FeatureFundsServices 增加1列，单独港币基金场景
		if(index==21) {
			cell_content = wealth.getHKDFeaturedFundsEndValue();
			if(wealth.isHKDFeaturedFundsTotalHKDPass()) {
				c.setCellStyle(green_style);
			}else {
				c.setCellStyle(red_style);
			}
		}
		//FeatureFundsServices需要增加3列，InventmentHoldings，InventmentTransations需要输出Dollar/HKD的金额是否正常，还有fail ref
		if(index==22) {
			boolean f1 = wealth.isFeaturedFundsServicesInventmentHoldingsTotalDollarPass();
			boolean f2 = wealth.isFeaturedFundsServicesInventmentHoldingsTotalHKDPass();
			boolean f3 = wealth.isFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass();
			if(f1&&f2&&f3) {
				cell_content = "true";
				c.setCellStyle(green_style);
			}else {
				cell_content = "false";
				stringBuffer.append("Featured Funds Services Inventment Holdings 金额计算异常;\n ");
				c.setCellStyle(red_style);
			}
		}
		if(index==23) {
			if(wealth.isFeaturedFundsServicesInventmentTransactionsEachItemPass()) {
				cell_content = "true";
				c.setCellStyle(green_style);
			}else {
				cell_content = "false";
				c.setCellStyle(red_style);
				stringBuffer.append("Featured Funds Services Inventment Transactions 检查异常;\n ");
			}

		}
		if(index==24) cell_content = wealth.getFeaturedFundsServicesFailList().toString();
		if(index==24) stringBuffer=setStyleBySizeValue(wealth.getFeaturedFundsServicesFailList().size(),c,green_style,red_style,stringBuffer," Featured Funds Services Fail Ref;\n");
		
		
		if(index==25) {
			cell_content = wealth.getTotalCashDividend();
			c.setCellStyle(green_style);
		}
		if(index==26) {
			cell_content = wealth.getFxRate()+"";
			if(wealth.getFxRate()>1.0 && wealth.getFxRate()<15.0) {
				c.setCellStyle(green_style);
			}else {
				c.setCellStyle(red_style);
			}
		}
		if(index==27) {
			if(!Common.checkForComment()) {
				stringBuffer.setLength(0);
			}
			cell_content= stringBuffer.toString();
			if(cell_content.length()>1) {
				int index_enter = cell_content.lastIndexOf("\n");
				cell_content = cell_content.substring(0,index_enter);
				c.setCellStyle(red_style);
			}else {
				c.setCellStyle(green_style);
			}
		}
		if(index==28) {
			//last update
			//stringBuffer.setLength(0);
			if(!Common.checkForComment()) {
				stringBuffer.setLength(0);
			}
			cell_content= stringBuffer.toString();
			if(cell_content.length()>1) {
				cell_content = "Fail";
				c.setCellStyle(red_style);
			}else {
				c.setCellStyle(green_style);
				cell_content = "Pass";
			}
		}
		
		c.setCellValue(cell_content);
	}
	
	public StringBuffer setStyleBySizeValue(int size,XSSFCell c,XSSFCellStyle green_style,XSSFCellStyle red_style,StringBuffer stringBuffer,String Commet) {
		if(size<=0) {
			c.setCellStyle(green_style);
		}else {
			stringBuffer.append(Commet);
			c.setCellStyle(red_style);
		}
		return stringBuffer;
	}
	public StringBuffer setStyleByBooleanValue(boolean value,XSSFCell c,XSSFCellStyle green_style,XSSFCellStyle red_style,StringBuffer stringBuffer,String Commet) {
		if(value) {
			c.setCellStyle(green_style);
		}else {
			stringBuffer.append(Commet);
			c.setCellStyle(red_style);
		}
		return stringBuffer;
	}
	
	public String setStyleByBooleanValue1(boolean value,XSSFCell c,XSSFCellStyle green_style,XSSFCellStyle red_style,StringBuffer stringBuffer,String Commet,String cellContent) {
		if(value) {
			c.setCellStyle(green_style);
		}else {
			stringBuffer.append(Commet);
			c.setCellStyle(red_style);
		}
		return cellContent;
	}


	public static XSSFCellStyle SetCellStyle(XSSFWorkbook Workbook,int flag) {
		XSSFCellStyle style=Workbook.createCellStyle();
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); 
		if(flag ==0) {
			//last update
			if(Common.checkForComment()) {
				style.setFillForegroundColor(HSSFColor.RED.index);
			}else {
				style.setFillForegroundColor(HSSFColor.GREEN.index);
			}
		}
		if(flag ==1) {
			style.setFillForegroundColor(HSSFColor.GREEN.index);
		}
		if(flag ==2) {
			XSSFFont font = Workbook.createFont();
			font.setFontName("黑体");
			font.setFontHeightInPoints((short) 12);//设置字体大小
			style.setFont(font);//选择需要用到的字体格式
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setVerticalAlignment(VerticalAlignment.TOP);
			style.setAlignment(HorizontalAlignment.JUSTIFY);
			
		}
		if(flag ==3) {
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			//style.setAlignment(HSSFCellStyle.ALIGN_LEFT); 
		}
		if(flag ==4) {
			style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		}
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		return style;
	}

}
