package useful;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

public class MainFrame extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
	private javax.swing.JButton button;
	private javax.swing.JTextField fieldText;
	private javax.swing.JLabel jLabel;
	private javax.swing.JLabel jTextField;
	private javax.swing.JLabel labelText;

	public MainFrame() {
		initComponents();
	}

	private void initComponents() {
		jLabel = new JLabel();
		labelText = new JLabel();
		jTextField = new JLabel();
		fieldText = new JTextField();
		button = new JButton();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jLabel.setText("JLabel:");
		labelText.setBorder(BorderFactory.createEtchedBorder());
		jTextField.setText("JTextField: ");
		button.setText("click");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				buttonActionPerformed(evt);
			}
		});

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);

		layout.setHorizontalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addGap(10, 10, 10)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
								.addComponent(button)
								.addGroup(layout.createSequentialGroup()
										.addComponent(jLabel)
										.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(labelText, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE))
								.addGroup(layout.createSequentialGroup()
										.addComponent(jTextField)
										.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(fieldText, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(17, Short.MAX_VALUE))
				);

		layout.setVerticalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addGap(20, 20, 20)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jLabel)
								.addComponent(labelText, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
								.addComponent(jTextField)
								.addComponent(fieldText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(button)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		pack();

	}

	private void buttonActionPerformed(ActionEvent evt) {
		changeText("Button clicked");
		try{
			Thread.sleep(1000);
		} catch(InterruptedException ex) {
			ex.printStackTrace();
		}
		changeText("Start to change text...");
		try{
			Thread.sleep(1000);
		} catch(InterruptedException ex) {
			ex.printStackTrace();
		}

		for (int i = 0; i < 3; i++) {
			changeText((i+1)+"");
			try{
				Thread.sleep(1000);
			} catch(InterruptedException ex) {
				ex.printStackTrace();
			}
		}
		//changeText("action end");
	}

	private void changeText(String text) {
		labelText.setText(text);
		fieldText.setText(text);
	}

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MainFrame().setVisible(true);
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
