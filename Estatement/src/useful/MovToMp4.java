package useful;

import java.io.File;
import java.io.IOException;
import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncodingAttributes;
import it.sauronsoftware.jave.VideoAttributes;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
//import ws.schild.jave.Encoder;
//import ws.schild.jave.MultimediaObject;
//import ws.schild.jave.encode.AudioAttributes;
//import ws.schild.jave.encode.EncodingAttributes;
//import ws.schild.jave.encode.VideoAttributes;

public class MovToMp4 {

	public static void main(String[] args) {
		movToMp4_2();
	}


	//https://codeantenna.com/a/QFoYfQUJXV
	
//	<dependency>
//	   <groupId>it.sauronsoftware.jave</groupId>
//	   <artifactId>jave</artifactId>
//	   <version>1.0.2</version>
//	</dependency>
	public  void movToMp4_1() {
		File source = new File("D:/Handle_the_loan_in_sake2_with_maker_approved_2023-11-08_17-34-20.mov");
		File target = new File("D:/Web1.mp4");
		try {

			AudioAttributes audio = new AudioAttributes();
			audio.setCodec("libmp3lame");
			audio.setBitRate(new Integer(56000));
			audio.setChannels(new Integer(1));
			audio.setSamplingRate(new Integer(22050));
			VideoAttributes video = new VideoAttributes();
			video.setCodec("mpeg4");
			video.setBitRate(new Integer(800000));
			video.setFrameRate(new Integer(15));
			EncodingAttributes attrs = new EncodingAttributes();
			attrs.setFormat("mp4");
			attrs.setAudioAttributes(audio);
			attrs.setVideoAttributes(video);
			Encoder encoder = new Encoder();
			encoder.encode(source, target, attrs);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Finish");
	}

	//ffmpeg转换视频	
	//https://www.cnblogs.com/lccsdncnblogs/p/17723211.html
	

//	<dependency>
//		<groupId>net.bramp.ffmpeg</groupId>
//		<artifactId>ffmpeg</artifactId>
//		<version>0.7.0</version>
//	</dependency>

	public static void movToMp4() {
		System.err.println("---------开始执行----------------");

		String path1 = "D:/Handle_the_loan_in_sake2_with_maker_approved_2023-11-08_17-34-20.mov";
		String path2 = "D:/Web2.mp4";

		//将下载后并解压的 "ffmpeg.exe,ffprobe.exe" 执行文件路径填写进去
		FFmpeg ffmpeg;
		try {
			ffmpeg = new FFmpeg("D:/Project/Program/ffmpeg-master-latest-win64-gpl-shared/bin/ffmpeg.exe");
			FFprobe ffprobe = new FFprobe("D:/Project/Program/ffmpeg-master-latest-win64-gpl-shared/bin/ffprobe.exe");
			FFmpegBuilder builder = new FFmpegBuilder()
					// 源视频文件
					.setInput(path1)
					// 目标视频文件
					.addOutput(path2)
					.done();

			FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);
			executor.createJob(builder).run();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.err.println("---------执行完毕----------------");
	}
//	
//	<!-- mov 转换 mp4 -->
//	<dependency>
//	    <groupId>ws.schild</groupId>
//	    <artifactId>jave-all-deps</artifactId>
//	    <version>3.0.1</version>
//	</dependency>
	
	
	//https://www.codenong.com/cs111037890/
	public static void movToMp4_2(){
	File source = new File("D:/Handle_the_loan_in_sake2_with_maker_approved_2023-11-08_17-34-20.mov");
	File target = new File("D:/Web3.mp4");
    try {

    	ws.schild.jave.encode.AudioAttributes audio = new ws.schild.jave.encode.AudioAttributes();
        audio.setCodec("libmp3lame");
        audio.setBitRate(new Integer(800000));//设置比特率
        audio.setChannels(new Integer(1));//设置音频通道数
        audio.setSamplingRate(new Integer(44100));//设置采样率
        ws.schild.jave.encode.VideoAttributes video = new ws.schild.jave.encode.VideoAttributes();
        video.setCodec("mpeg4");
//        video.setCodec("libx264");
        video.setBitRate(new Integer(3200000));
        video.setFrameRate(new Integer(15));
        ws.schild.jave.encode.EncodingAttributes attrs = new ws.schild.jave.encode.EncodingAttributes();
        attrs.setOutputFormat("mp4");
        attrs.setAudioAttributes(audio);
        attrs.setVideoAttributes(video);
        ws.schild.jave.Encoder encoder = new ws.schild.jave.Encoder();
        encoder.encode(new ws.schild.jave.MultimediaObject(source), target, attrs);
    } catch (Exception e) {
        e.printStackTrace();
    }
}
}
