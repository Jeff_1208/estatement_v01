package useful;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class TimerExample {

	public static void main(String[] args) {
		
		Timer timer = new Timer();
		
		TimerTask task = new TimerTask() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Calendar instance = Calendar.getInstance();
				int year3  = instance.get(Calendar.YEAR);
				int month3 = instance.get(Calendar.MONTH);
				int date3  = instance.get(Calendar.DATE);
				int hour3  = instance.get(Calendar.HOUR_OF_DAY);
				int minute3 = instance.get(Calendar.MINUTE);
				int second3 = instance.get(Calendar.SECOND);
				System.out.println("example 3: " + year3 + "." + month3 + "." + date3 + " " + hour3 + ":" + minute3 + ":" + second3);//2023.2.25 17:27:20
			}
		};
		
		long delay = 0; //延迟时间  单位：毫秒
		//long period = 30 * 24 * 60 * 60 * 1000;//间隔时间一个月(毫秒数)
		long period = 5 * 1000;//间隔时间一个月(毫秒数)
		timer.schedule(task, delay, period); //启动定时任务
		
	}
}
