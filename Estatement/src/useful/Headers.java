package useful;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
//需要导入jar包fastjson.jar

public class Headers {
	private static Map<String, String> GLOBAL_VARIABLES = new HashMap<>();
	
	public static void main(String[] args) throws Exception{
        
		Headers headers = new Headers();
//        String payload = args[0];
//        String url = args[1];
//        String host = args[2];
//        String method = args[3].toUpperCase();
//        int offset = Integer.parseInt(args[4]);
//        String keyId = args[5];  //需要提前写入GLOBAL_VARIABLES
//        String signkey = args[6]; //需要提前写入GLOBAL_VARIABLES
//      headers.generateHeaderSignature(payload, url, host, method, offset);
		String keyId = "46I1McJXxJh9rh6sIKhsRbKZEsN65QVWsP4K1364CebtqCNzcnO6I1";
      	String signkey = "IqprUXaWKGuyQUvH+7oLuFcQbB5uCI1TRz0wdpNvyCoX23cgyV/JOeihyyIx6OpkxJf4oWr5xvF0YmQvfswMRQ==";
		GLOBAL_VARIABLES.put("keyId", keyId);
		GLOBAL_VARIABLES.put("signkey", signkey);
        headers.generateHeaderSignature("", "/v1/accounts", "https://mapi.sta-wlab.com", "POST", 1);

	}

    
    // 以下方法是为headers生成两个参数Digest,Signature
    public Map<String, String> generateHeaderSignature(String payload, String url, String host, String myMethod, int offset)  throws Exception{
    	// payload 是从body json文件读取出来的的字符串
    	//url 是接口路径，不包含host
    	// host
    	//请求方法，大写
    	// offset 
        String digest = generateDigest(payload);
        Map<String, Object> headerHash = new HashMap<>();
        int timestamp = offset + Math.round(System.currentTimeMillis() / 1000);
        headerHash = generateHeaderHash(timestamp, myMethod, url, digest, host.substring(8));
        Map<String, Object> configMap = new HashMap<>();
        configMap = generateConfig(GLOBAL_VARIABLES.get("keyId"), timestamp, GLOBAL_VARIABLES.get("signkey"));
        computeHttpSignature(configMap, headerHash);
        System.out.println("Digest: " + GLOBAL_VARIABLES.get("Digest"));
        System.out.println("Signature: " + GLOBAL_VARIABLES.get("Signature"));
        return GLOBAL_VARIABLES;
    }
    
    public String generateDigest(String payload) {
        try {
            MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
            byte[] shaByteArr = mDigest.digest(payload.getBytes(Charset.forName("UTF-8")));
            GLOBAL_VARIABLES.put("Digest", "SHA-256=" + Base64.getEncoder().encodeToString(shaByteArr));
            return "SHA-256=" + Base64.getEncoder().encodeToString(shaByteArr);
        } catch (Exception ex) {
            System.out.println("error");
        }
        return "";
    }
    
    public Map<String, Object> generateHeaderHash(int timestamp, String method, String targetUrl, String computerDigest, String host) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("(created)", timestamp);
        map.put("digest", computerDigest);
        map.put("(request-target)", method + ' ' + targetUrl);
        map.put("host", host);
        return map;
    }
    
    public Map<String, Object> generateConfig(String keyId, int timestamp, String secretKey) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("algorithm", "hs2019");
        map.put("KeyId", keyId);
        map.put("timestamp", timestamp);
        map.put("secretKey", secretKey);
        ArrayList<String> list = new ArrayList<String>();
        list.add("(request-target)");
        list.add("(created)");
        list.add("host");
        list.add("digest");
        map.put("headers", list);
        return map;
    }
    
    public String computeHttpSignature(Map<String, Object> config, Map<String, Object> headerHash) throws Exception {
        String signingBase = "";
        ArrayList<String> list = new ArrayList<String>();
        list = (ArrayList<String>) config.get("headers");
        for (String header : list) {
            if (signingBase != "") {
                signingBase += "\n";
            }
            signingBase += header.toLowerCase() + ": " + headerHash.get(header);
        }
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec((config.get("secretKey").toString()).getBytes(), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        String hash = Base64.getEncoder().encodeToString(sha256_HMAC.doFinal(signingBase.getBytes()));
        String result = "keyId=" + config.get("KeyId").toString() + ",algorithm=" + config.get("algorithm").toString() +
                ",created=" + config.get("timestamp").toString() + ",headers=" + String.join(" ", (List) config.get("headers")) +
                ",signature=" + hash;
        GLOBAL_VARIABLES.put("Signature", result);
        return result;
//        try {
//        	System.out.println("kkkkkk");
//            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
//            SecretKeySpec secret_key = new SecretKeySpec((config.get("secretKey").toString()).getBytes(), "HmacSHA256");
//            sha256_HMAC.init(secret_key);
//            String hash = Base64.getEncoder().encodeToString(sha256_HMAC.doFinal(signingBase.getBytes()));
//            String result = "keyId=" + config.get("KeyId").toString() + ",algorithm=" + config.get("algorithm").toString() +
//                    ",created=" + config.get("timestamp").toString() + ",headers=" + String.join(" ", (List) config.get("headers")) +
//                    ",signature=" + hash;
//            GLOBAL_VARIABLES.put("Signature", result);
//            return result;
//        } catch (Exception ex) {
//            System.out.println("error");
//        }
//        return "";
    }
    

}

