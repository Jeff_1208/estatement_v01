package useful;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class CopyFile {

	
	public static void main(String[] args) {
		
		try {
			Files.copy(new File("D:/logs.txt").toPath(), new File("D:/logs1.txt").toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("finish!");
	}
}

