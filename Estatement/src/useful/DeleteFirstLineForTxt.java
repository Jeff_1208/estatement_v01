package useful;

import java.io.RandomAccessFile;
import java.util.ArrayList;

public class DeleteFirstLineForTxt {
	
	public static void main(String[] args) throws Exception{
		String path ="D://1//data.txt";
		deleteFirstLineForTxt(path);
		System.out.println("OK");
	}
	
	
	public static String deleteFirstLineForTxt(String path) throws Exception {
		long start = System.currentTimeMillis();

	    RandomAccessFile raf = new RandomAccessFile(path, "rw");          
	     //Initial write position                                             
	    long writePosition = raf.getFilePointer();                            
	    String username = raf.readLine().replace("\n", "").replace("\r", "").trim();                                                       
	    // Shift the next lines upwards.                                      
	    long readPosition = raf.getFilePointer();                             

	    byte[] buff = new byte[1024];                                         
	    int n;                                                                
	    while (-1 != (n = raf.read(buff))) {                                  
	        raf.seek(writePosition);                                          
	        raf.write(buff, 0, n);                                            
	        readPosition += n;                                                
	        writePosition += n;                                               
	        raf.seek(readPosition);                                           
	    }                                                                     
	    raf.setLength(writePosition);                                         
	    raf.close(); 
	    System.out.println("username: " + username);
	    
	    long end = System.currentTimeMillis();
	    long cost = end - start;
	    System.out.println("cost: " + cost);
	    return username;
	    
	}
	
	
}
