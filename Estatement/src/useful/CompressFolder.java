package useful;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CompressFolder {

	 
    public static void main(String[] args) {
    	//compressFolder();
    	
//        String folderPath = "D:\\Project\\Program\\IDEAWorkspace\\myauto\\automation-test\\TestReport\\2023-09-08-11-06-16"; 
//        compressFolder2(folderPath);
        
        String zipFilePath = "D:\\result.zip";
        deleteFile(zipFilePath);
    }
    
    public static void compressFolder2(String path) {
//        int index = path.lastIndexOf("\\");
//        String parentPath =path.substring(0,index)+"\\";
//        String filename = path.substring(index+1);
//        System.out.println(parentPath);
//        System.out.println(filename);
//        String output = parentPath+filename+".zip";
        String output = path + ".zip";
        try {
            zipFolder(path, output);
            System.out.println("Zip file created successfully.");
        } catch (IOException e) {
            System.out.println("Error creating zip file: " + e.getMessage());
        }
        
    }
    
    public static void deleteFile(String filePath){
    	
        try {
        	Path path = Paths.get(filePath);
			Files.delete(path);
			System.out.println("delete file successfully.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    public static void compressFolder() {
        // 将要被压缩的文件夹路径  
        String folderPath = "D:\\Project\\Program\\IDEAWorkspace\\myauto\\automation-test\\TestReport\\2023-09-08-11-06-16"; 
        
        // 压缩后生成的压缩文件路径及文件名
        String zipFilePath = "D:\\result.zip";
        
        try {
            zipFolder(folderPath, zipFilePath);
            System.out.println("Zip file created successfully.");
        } catch (IOException e) {
            System.out.println("Error creating zip file: " + e.getMessage());
        }
    }
    
 
    /**
     * 打包压缩文件夹
     *
     * @param folderPath 文件夹路径
     * @param zipFilePath 压缩后的文件路径
     * @throws IOException IO异常
     */
    public static void zipFolder(String folderPath, String zipFilePath) throws IOException {
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        try {
            fos = new FileOutputStream(zipFilePath);
            zos = new ZipOutputStream(fos);
            
            // 递归遍历整个文件夹并添加到压缩包
            addFolderToZip("", new File(folderPath), zos);
        } finally {
            if (zos != null) {
                zos.close();
            }
            if (fos != null) {
                fos.close();
            }
        }
    }
 
    /**
     * 将文件夹及其中的文件递归添加到压缩流中
     *
     * @param parentPath 父级路径
     * @param folder 文件夹
     * @param zos Zip输出流
     * @throws FileNotFoundException 文件未找到异常
     * @throws IOException IO异常
     */
    private static void addFolderToZip(String parentPath, File folder, ZipOutputStream zos) throws FileNotFoundException, IOException {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                // 递归添加子文件夹中的文件
                addFolderToZip(parentPath + folder.getName() + "/", file, zos);
            } else {
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    
                    // 新建Zip条目并将输入流加入到Zip包中
                    ZipEntry zipEntry = new ZipEntry(parentPath + folder.getName() + "/" + file.getName());
                    zos.putNextEntry(zipEntry);
                    
                    byte[] bytes = new byte[1024];
                    int length;
                    while ((length = fis.read(bytes)) >= 0) {
                        zos.write(bytes, 0, length);
                    }
                } finally {
                    if (fis != null) {
                        fis.close();
                    }
                }
            }
        }
    }


}
