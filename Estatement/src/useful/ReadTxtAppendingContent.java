package useful;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Calendar;


public class ReadTxtAppendingContent {
    public static void main(String[] args) {
        updateForHkidInProperties();
        //get();
        generateAccount();
    }
    
    public static void get() {
        String inputFile = "D:/1/createAccountData.txt";
        String outputFile = "D:/1/new.txt";
 
        try (FileReader fr = new FileReader(inputFile);
             FileWriter fw = new FileWriter(outputFile)) {
            char[] buffer = new char[1024];
            int read;
            while ((read = fr.read(buffer)) != -1) {
                fw.write(buffer, 0, read);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void generateAccount() {
        Calendar calendar = Calendar.getInstance();
        
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1; // 月份从0开始，加1表示实际月份
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        
        year = year % 100;
        String yearStr = year+"";
        String monthStr = String.format("%02d", month);
        String dayStr = String.format("%02d", day);
        String hourStr = String.format("%02d", hour);
        String minuteStr = String.format("%02d", minute);
        String secondStr = String.format("%02d", second);
        System.out.println("年：" + year);
        System.out.println("月：" + month);
        System.out.println("日：" + day);
        System.out.println("时：" + hour);
        System.out.println("分：" + minute);
        System.out.println("秒：" + second);
        String result = yearStr + monthStr + dayStr + hourStr + minuteStr + secondStr;
        System.out.println(result);
    }

    public static void updateForHkidInProperties(){
        try {

            String path = "D:/1/createAccountData.txt";
            String path1 = "D:/1/new1.txt";
            
            File f = new File(path);
            FileReader bytes = new FileReader(f);
            BufferedReader chars = new BufferedReader(bytes); //字节类型转换成字符形式
            String row ="";
            StringBuffer stringBuffer = new StringBuffer();
            while((row=chars.readLine())!=null) {
                stringBuffer.append(row+"\n");
            }
            stringBuffer.append("\n\n");
            FileWriter file = new FileWriter(path1,true);//true表示追加数据
            file.write(stringBuffer.toString());
            file.close();
        } catch (Exception e) {
        }
    }
}




