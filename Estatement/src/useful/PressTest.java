package useful;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class PressTest {
	
	// Java实现压力测试
    static class StressTask implements Callable<Boolean> {
        public Boolean call() throws Exception {
            // 这里放置你的业务逻辑
            // 例如：
            // Thread.sleep(1000); // 模拟耗时操作
        	Thread.sleep(getRandomNumber());
            return true;
        }
    }
 
    public static void main(String[] args) throws Exception {
//    	int random = getRandomNumber();
//    	System.out.println(random);
//    	Thread.sleep(random);
        LocalDateTime localDateTime = LocalDateTime.now();
//        String b = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        System.out.println(localDateTime);
//    	System.out.println(Calendar.getInstance());
    	StressTest();
    	System.out.println(Calendar.getInstance());
        LocalDateTime localDateTime1 = LocalDateTime.now();
//      String b = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        System.out.println(localDateTime1);
    	System.out.println("Finish!!!!!");
    }
    
    public static int getRandomNumber() {
        // 创建一个Random对象
        Random random = new Random();
        
        // 生成一个0到1之间的随机小数
        double randomDouble = random.nextDouble();
        return (int) (randomDouble *1000);
    }
    
    public static void getRandomBetween(int min, int max) {
    	 Random random = new Random();
        int randomInt = random.nextInt(max - min + 1) + min;
        System.out.println("随机整数：" + randomInt);
    }
    
    public static void StressTest() throws Exception {
        // 创建固定大小的线程池
        ExecutorService executor = Executors.newFixedThreadPool(10); // 假设我们要10个并发线程
 
        // 提交100个并发任务
        int tasks = 100;
        Future<Boolean>[] futures = new Future[tasks];
        for (int i = 0; i < tasks; i++) {
            futures[i] = executor.submit(new StressTask());
        }
 
        // 等待所有任务完成
        for (Future<Boolean> future : futures) {
            future.get(); // 这将阻塞直到任务完成
        }
 
        // 关闭线程池
        executor.shutdown();
    }
}
