package useful;

import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import javax.crypto.spec.SecretKeySpec;

import com.alibaba.fastjson.JSONObject;
//需要导入jar包fastjson.jar

public class jiami {
	private  Map<String, String> GLOBAL_VARIABLES = new HashMap<>();
	
	public static void main(String[] args) throws Exception{
		jiami j = new jiami();
		int offset = Integer.parseInt(args[1]);
		//String v = j.getSecretValue(publicKey,1, "qatest054","Aa123456");
		String v = j.getSecretValue(args[0],offset, args[2],args[3]);
		System.out.println(v);
	}

    public String getSecretValue(String publicKey, int offset,String account, String pwd) throws Exception {
    	
        String publicKeyPEM = publicKey
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PUBLIC KEY-----", "");

        byte[] pbks = Base64.getMimeDecoder().decode(publicKeyPEM);
        X509EncodedKeySpec encodedKeySpec = new X509EncodedKeySpec(pbks);
        PublicKey newPbk = KeyFactory.getInstance("RSA").generatePublic(encodedKeySpec);
        OAEPParameterSpec oaepParameterSpec = new OAEPParameterSpec("SHA-256", "MGF1", new MGF1ParameterSpec("SHA-256"), PSource.PSpecified.DEFAULT);
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPPadding");
        cipher.init(Cipher.ENCRYPT_MODE, newPbk, oaepParameterSpec);
        JSONObject jsonObject = new JSONObject();
        jsonObject = createSignInPayload(offset, account, pwd);
        byte[] bytes = cipher.doFinal(jsonObject.toString().getBytes());
        return Base64.getEncoder().encodeToString(bytes);
    }
    
    public JSONObject createSignInPayload(int offset, String account, String pwd) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", account);
        jsonObject.put("authType", "PASSWORD");
        jsonObject.put("authValue", pwd);
        int timestamp = Math.round(System.currentTimeMillis() / 1000);
        jsonObject.put("timestamp", offset + timestamp);
        jsonObject.put("salt", UUID.randomUUID().toString());
        return jsonObject;
    }
    
    // 以下方法是为headers生成两个参数Digest,Signature
    public Map<String, String> generateHeaderSignature(String payload, String url, String host, String myMethod, int offset) {
        String digest = generateDigest(payload);
        Map<String, Object> headerHash = new HashMap<>();
        int timestamp = offset + Math.round(System.currentTimeMillis() / 1000);
        headerHash = generateHeaderHash(timestamp, myMethod, url, digest, host.substring(8));
        Map<String, Object> configMap = new HashMap<>();
        configMap = generateConfig(GLOBAL_VARIABLES.get("keyId"), timestamp, GLOBAL_VARIABLES.get("signkey"));
        computeHttpSignature(configMap, headerHash);
        System.out.println("Digest: " + GLOBAL_VARIABLES.get("Digest"));
        System.out.println("Signature" + GLOBAL_VARIABLES.get("Signature"));
        return GLOBAL_VARIABLES;
    }
    
    public String generateDigest(String payload) {
        try {
            MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
            byte[] shaByteArr = mDigest.digest(payload.getBytes(Charset.forName("UTF-8")));
            GLOBAL_VARIABLES.put("Digest", "SHA-256=" + Base64.getEncoder().encodeToString(shaByteArr));
            return "SHA-256=" + Base64.getEncoder().encodeToString(shaByteArr);
        } catch (Exception ex) {
            System.out.println("error");
        }
        return "";
    }
    
    public Map<String, Object> generateHeaderHash(int timestamp, String method, String targetUrl, String computerDigest, String host) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("(created)", timestamp);
        map.put("digest", computerDigest);
        map.put("(request-target)", method + ' ' + targetUrl);
        map.put("host", host);
        return map;
    }
    
    public Map<String, Object> generateConfig(String keyId, int timestamp, String secretKey) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("algorithm", "hs2019");
        map.put("KeyId", keyId);
        map.put("timestamp", timestamp);
        map.put("secretKey", secretKey);
        ArrayList<String> list = new ArrayList<String>();
        list.add("(request-target)");
        list.add("(created)");
        list.add("host");
        list.add("digest");
        map.put("headers", list);
        return map;
    }
    
    public String computeHttpSignature(Map<String, Object> config, Map<String, Object> headerHash) {
        String signingBase = "";
        ArrayList<String> list = new ArrayList<String>();
        list = (ArrayList<String>) config.get("headers");
        for (String header : list) {
            if (signingBase != "") {
                signingBase += "\n";
            }
            signingBase += header.toLowerCase() + ": " + headerHash.get(header);
        }
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec((config.get("secretKey").toString()).getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            String hash = Base64.getEncoder().encodeToString(sha256_HMAC.doFinal(signingBase.getBytes()));
            String result = "keyId=" + config.get("KeyId").toString() + ",algorithm=" + config.get("algorithm").toString() +
                    ",created=" + config.get("timestamp").toString() + ",headers=" + String.join(" ", (List) config.get("headers")) +
                    ",signature=" + hash;
            GLOBAL_VARIABLES.put("Signature", result);
            return result;
        } catch (Exception ex) {
            System.out.println("error");
        }
        return "";
    }
    

}
