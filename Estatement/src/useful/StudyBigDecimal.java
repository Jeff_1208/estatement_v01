package useful;

import java.math.BigDecimal;
import java.math.MathContext;

public class StudyBigDecimal {
	
	public static void main(String[] args) {
		String s1 = "41.30";
		String s2 = "-120.140";
		StudyBigDecimal studyBigDecimal = new StudyBigDecimal();
		String r = studyBigDecimal.add(s1, s2);
		System.out.println(r);
		studyBigDecimal.other(s1, s2);
	}

	public String add(String s1,String s2) {
		BigDecimal b1 = new BigDecimal(String.valueOf(s1));
		BigDecimal b2 = new BigDecimal(String.valueOf(s2));
		BigDecimal b3 = b1.add(b2); //加法运算
		System.out.println(b3);
		return b3.toPlainString();
	}
	
	public void other(String s1,String s2) {
		BigDecimal b1 = new BigDecimal(String.valueOf(s1));
		BigDecimal b2 = new BigDecimal(String.valueOf(s2));
		BigDecimal b3 = b1.subtract(b2);  //减法运算
		BigDecimal b4 = b1.multiply(b2);  //乘法运算
		BigDecimal b5 = b1.divide(b2);    //除法运算，会报错
		BigDecimal b6 = b1.divide(b2,MathContext.DECIMAL32);    //除法运算
		System.out.println(b3);
		System.out.println(b4);
		System.out.println(b5);
		System.out.println(b6);
	}
	
	
    public static void debug(String[] args) {
        BigDecimal number = new BigDecimal("1E11");
        System.out.println(number.toEngineeringString());
        System.out.println(number.toPlainString());   //很大的数值也不会变成科学计数法
        System.out.println(number.toString());
    }
}
