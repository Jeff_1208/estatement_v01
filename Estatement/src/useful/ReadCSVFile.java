package useful;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ReadCSVFile {




	public static void main(String[] args) throws Exception {
		readFile("D:\\Project\\Automation\\ZyphrcScale\\suites.csv");
	}
	
	public static void readFile(String  absPath) throws Exception {
		// 读取文件内容到Stream流中，按行读取
		List<String> lines = Files.readAllLines(Paths.get(absPath));

		for (int j = 0; j < lines.size(); j++) {
			String line = lines.get(j);
			System.out.println(line);
			String[] caseResult=line.split(",");
			System.out.println(caseResult[0]);
			System.out.println(caseResult[caseResult.length-1]);
		}
	}


	public static void readFeatureFiles() throws Exception {
		String path="D:\\Project\\Program";  //文件夹路径
		List<String> caseNames =new ArrayList<String>();
		File f = new File(path);
		File[] files = f.listFiles();

		for (int i = 0; i < files.length; i++) {
			System.out.println(files[i].getName());
			String absPath = files[i].getAbsolutePath();

			// 读取文件内容到Stream流中，按行读取
			List<String> lines = Files.readAllLines(Paths.get(absPath));

			for (int j = 0; j < lines.size(); j++) {
				String line = lines.get(j);
				if(line.contains("Scenario")) {
					System.out.println(line);
					String caseName = line.substring(line.indexOf(":")+1).trim();
					caseName = caseName.split(" ")[0].trim();
					caseNames.add(caseName);
				}
			}
		}
		for (int i = 0; i < caseNames.size(); i++) {
			System.out.println(caseNames.get(i));
		}
	}



}
