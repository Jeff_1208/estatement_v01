package useful;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.Random;

public class GenerateRandomNumber {
	
	public static void main(String[] args) {
		System.out.println(getNumber1());
		System.out.println(getNumber2());
		System.out.println(getNumber3());
		System.out.println(getNumber4());
		System.out.println(getNumber5());
		Random r = new Random();
		int a =r.nextInt(8)+1;
		System.out.println(a);
	}

	public static String getNumber1() {
		Random r = new Random();
		int v = r.nextInt(500)+100;
		String value  = String.valueOf(v);
		return value;
	}
	
	public static String getNumber2() {
		Random r = new Random();
		Float f = r.nextFloat();
		return f+"";
	}
	
	public static String getNumber3() {
		Random r = new Random();
		Double d = r.nextDouble();
		return d+"";
	}
	
	public static String getNumber4() {
		int max =100;
		int randomNum = (int)(Math.random()*max + 0.5); //加0.5 是为了防止是0，四舍五入变成1
		return randomNum + "";
	}
	
	public static String getNumber5() {
		SecureRandom s  = new SecureRandom();
		int r = s.nextInt();//生成一个安全的随机数
		return r+"";
		
	//Math.random()是生成随机数的最简单方法，它返回一个介于0.0到1.0之间的伪随机数（即从0（包括0）往上，但是不包括1（排除1））。
	}
	public static String getNumber6() {
		LocalDateTime now = LocalDateTime.now();
//		long r = now.toInstant().toEpochMilli();
		return "";
	}
}


