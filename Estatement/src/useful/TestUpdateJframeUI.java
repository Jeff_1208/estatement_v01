package useful;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.utils.Common;
import com.utils.Main_utils;

import wealth.WealthMainUtils;


public class TestUpdateJframeUI extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	
	private String result_file_path="";
	static String Folder_path="";
	public static String error_customer="";
	private JTextField result_Field;
	private JPanel mainpanel;
	private JPanel CTO_Name_panel;
	private JPanel showpanel;
	private JPanel radioButtonpanel;
	private JButton scan_btn_Source;
	private JButton run_btn;
	private JTextArea show_result;
	
	private JRadioButton eStatementRandioButton;
	private JRadioButton wealthRandioButton;

	public TestUpdateJframeUI() {
		eStatementRandioButton= new JRadioButton("eStatement",true);
		wealthRandioButton= new JRadioButton("Wealth");
		radioButtonpanel = new JPanel();
		radioButtonpanel.setLayout(new GridLayout(1,2,1,1));
		radioButtonpanel.add(eStatementRandioButton);
		radioButtonpanel.add(wealthRandioButton);
		ButtonGroup group=new ButtonGroup();  
		group.add(eStatementRandioButton);
		group.add(wealthRandioButton);

		show_result= new JTextArea(10,30);
		show_result.setLineWrap(true);  
		result_Field = new JTextField(20);

		scan_btn_Source = new JButton("Find Folder");
		run_btn = new JButton("Start Run");
		run_btn.setSize(10,30);

		CTO_Name_panel = new JPanel();
		CTO_Name_panel.setLayout(new GridLayout(4,1,1,1)) ;
		CTO_Name_panel.add(scan_btn_Source);
		CTO_Name_panel.add(result_Field);
		CTO_Name_panel.add(radioButtonpanel);
		CTO_Name_panel.add(run_btn);

		showpanel = new JPanel();
		mainpanel = new JPanel();
		mainpanel.setLayout(new GridLayout(2,1,1,1));
		showpanel.setLayout(new BorderLayout());
		showpanel.add(show_result);
		mainpanel.add(CTO_Name_panel); 
		mainpanel.add(showpanel); 
		setTitle("eStatement&&Wealth Tool");

		this.add(mainpanel); 
		this.setSize(500, 300);
		this.setVisible(true);
		scan_btn_Source.addActionListener(this);
		run_btn.addActionListener(this);
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				int flag = JOptionPane.showConfirmDialog(null, "要退出该程序吗？","友情提示",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				if(flag==JOptionPane.YES_OPTION) {
					System.exit(0);
				}else {
					return;
				}
			}
		});

		Toolkit too = Toolkit.getDefaultToolkit() ;
		Dimension screenSize = too.getScreenSize() ;
		this.setLocation((screenSize.width-WIDTH)/2, (screenSize.height-HEIGHT)/2) ;
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setResizable(true); 
		this.setVisible(true);
		this.pack();
	}


	public static void main(String[] args) {
		new TestUpdateJframeUI();

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource()==scan_btn_Source){
			JFileChooser chooser = new JFileChooser();
			chooser.setMultiSelectionEnabled(false);
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.setDialogTitle("Find Source Folder");
			int result = chooser.showOpenDialog(this);
			show_result.setText("");
			if (result == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				String filepath = file.getAbsolutePath();
				result_Field.setText( filepath);// 将文件路径设到JTextField
				Folder_path=filepath;
			}
		}else if(e.getSource()==run_btn){
			
			if(Folder_path.length()<1) {
				String unusual_FC_message="Pleases Select Folder At First!!!!!";
				show_result.setText(unusual_FC_message);
				//show_result.setBackground(Color.red);
				show_result.setForeground(Color.RED);
				return;
			}
			if(!Common.is_available()) {return; }
			String text="Checking......\nChecking......\nChecking......\n";
			show_result.setText(text);
			show_result.setForeground(Color.RED);
			show_result.updateUI();
			this.repaint();
			run_btn.disable();
			
			if(eStatementRandioButton.isSelected()) {
				Main_utils main_utils = new Main_utils();
				try {
					System.out.println("");
					String unusual_FC_message="Please find the eStatements  test Result from:\n"+result_file_path;
					show_result.setText(unusual_FC_message);
					show_result.setForeground(Color.RED);
				} catch (Exception e2) {
					String unusual_FC_message="Something is Wrong, the checking is not Finished:\n"+error_customer+" "+e2;
					show_result.setText(unusual_FC_message);
					show_result.setForeground(Color.RED);
				}
			}else if(wealthRandioButton.isSelected()) {
				try {
					String unusual_FC_message="Please find the Wealth test Result from:\n"+result_file_path;
					show_result.setText(unusual_FC_message);
					show_result.setForeground(Color.RED);
				} catch (Exception e2) {
					String unusual_FC_message="Something is Wrong, the checking is not Finished:\n"+error_customer+" "+e2;
					show_result.setText(unusual_FC_message);
					show_result.setForeground(Color.RED);
				}
			}
			run_btn.enable();

		}


	}

}
