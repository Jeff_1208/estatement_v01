package AppUI;


import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;


//selenium-server-standalone-3.141.59.jar
//java-client-7.3.0.jar

public class App {
	DesiredCapabilities cap;

	public static void main(String[] args) {
		AndroidDriver driver;
		 
        DesiredCapabilities des = new DesiredCapabilities();
 
  //    des.setCapability("automationName", "Appium");//Selendroid //自动化的模式选择
 //     des.setCapability("app", "C:\\software\\CalcTest.apk");//配置待测试的apk的路径
//      des.setCapability("browserName", "chrome");  //h5
        des.setCapability("platformName", "Android");//平台名称
        //adb shell getprop ro.build.version.release
        des.setCapability("platformVersion", "13");//手机操作系统版本
//        des.setCapability("udid", "192.168.229.101:5555");//连接的物理设备的唯一设备标识
        des.setCapability("deviceName", "emulator-5554");//使用的手机类型或模拟器类型  UDID
        des.setCapability("appPackage", "com.android.settings");//App安装后的包名,注意与原来的CalcTest.apk不一样
        des.setCapability("appActivity", "com.android.settings.Settings");//app测试人员常常要获取activity，进行相关测试,后续会讲到
        des.setCapability("unicodeKeyboard", true);//支持中文输入
        des.setCapability("resetKeyboard", true);//支持中文输入
        des.setCapability("newCommandTimeout", "10");//没有新命令时的超时时间设置
        des.setCapability("nosign", true);//跳过检查和对应用进行 debug 签名的步骤
        try {
			driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), des);
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);//设置超时等待时间,默认250ms
	        
	        //*[contains(@name,"")]
	        //android.widget.TextView[contains(@text,"Network")]
	        
	        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,\"Network\")]")).click();//定位'1'
	        System.out.println("11");
//	        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Network')]")).click();//定位'1'
	 
//	        driver.findElement(By.id("com.android.calculator2:id/plus")).click();//定位'+'
//	 
//	        driver.findElement(By.id("com.android.calculator2:id/digit6")).click();//定位'6'
//	 
//	        driver.findElement(By.id("com.android.calculator2:id/equal")).click();//定位'='
	        Thread.sleep(5000);
	        // 关闭Appium会话
	        driver.quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//虚拟机默认地址
 
        
        System.out.println("Pass");

 
		
	}

	
	
}
