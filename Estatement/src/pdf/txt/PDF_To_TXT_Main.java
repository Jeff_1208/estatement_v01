package pdf.txt;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class PDF_To_TXT_Main extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	
	private String result_file_path="";
	static String Folder_path="";
	public static String error_customer="";
	private JTextField result_Field;
	private JPanel mainpanel;
	private JPanel CTO_Name_panel;
	private JPanel showpanel;
	private JButton scan_btn_Source;
	private JButton run_btn;
	private JTextArea show_result;

	public PDF_To_TXT_Main() {

		show_result= new JTextArea(10,30);
		show_result.setLineWrap(true);  
		result_Field = new JTextField(20);

		scan_btn_Source = new JButton("Find PDF File Folder");
		run_btn = new JButton("Start Run");
		run_btn.setSize(10,30);

		CTO_Name_panel = new JPanel();
		CTO_Name_panel.setLayout(new GridLayout(3,1,1,1)) ;
		CTO_Name_panel.add(scan_btn_Source);
		CTO_Name_panel.add(result_Field);
		CTO_Name_panel.add(run_btn);

		showpanel = new JPanel();
		mainpanel = new JPanel();
		mainpanel.setLayout(new GridLayout(2,1,1,1));
		showpanel.setLayout(new BorderLayout());
		showpanel.add(show_result);
		mainpanel.add(CTO_Name_panel); 
		mainpanel.add(showpanel); 
		setTitle("PDF To Txt");

		this.add(mainpanel); 
		this.setSize(500, 300);
		this.setVisible(true);
		scan_btn_Source.addActionListener(this);
		run_btn.addActionListener(this);
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				int flag = JOptionPane.showConfirmDialog(null, "要退出该程序吗？","友情提示",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				if(flag==JOptionPane.YES_OPTION) {
					System.exit(0);
				}else {
					return;
				}
			}
		});

		Toolkit too = Toolkit.getDefaultToolkit() ;
		Dimension screenSize = too.getScreenSize() ;
		this.setLocation((screenSize.width-WIDTH)/2, (screenSize.height-HEIGHT)/2) ;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		this.setResizable(true); 
		this.pack();
	}


	public static void main(String[] args) {
		new PDF_To_TXT_Main();

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource()==scan_btn_Source){
			JFileChooser chooser = new JFileChooser();
			chooser.setMultiSelectionEnabled(false);
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.setDialogTitle("Find Source Folder");
			int result = chooser.showOpenDialog(this);
			show_result.setText("");
			if (result == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				String filepath = file.getAbsolutePath();
				result_Field.setText( filepath);// 将文件路径设到JTextField
				Folder_path=filepath;
			}
		}else if(e.getSource()==run_btn){
			if(Folder_path.length()<1) {
				String unusual_FC_message="Pleases Select Folder At First!!!!!";
				show_result.setText(unusual_FC_message);
				//show_result.setBackground(Color.red);
				show_result.setForeground(Color.RED);
				return;
			}

			try {
				
				PDF pdf = new PDF();
				
				
				File file = new File(Folder_path);
				File[] fs = file.listFiles();
				for(File f:fs) {
					String file_name = f.getName();
					if(file_name.endsWith(".pdf")) {
						pdf.save_result_to_txt_with_path(Folder_path+"/"+file_name);
					}
				}
//				result_file_path = Folder_path.replace(".pdf", ".txt");
				String unusual_FC_message="Please find the txt from:\n"+Folder_path;
				show_result.setText(unusual_FC_message);
				show_result.setForeground(Color.RED);
			} catch (Exception e2) {
				String unusual_FC_message="Something is Wrong, did not generate txt successful";
				show_result.setText(unusual_FC_message);
				show_result.setForeground(Color.RED);
			}

		}


	}
	
	

}
