package com.utils;

import java.io.File;
import java.util.ArrayList;

import org.jpedal.jbig2.segment.symboldictionary.SymbolDictionarySegment;

import com.entity.Estatement;
import com.entity.Ref;
import com.itextpdf.text.log.SysoCounter;
import com.main.Estatement_Main;
import com.pdf.PDF;

public class Main_utils {

	private String result_file_path="";
	private String fail_ref_path= "";
	private ArrayList<Ref> fail_refs=new ArrayList<>();
	public String  get_result_file_path() {
		return result_file_path;
	}

	public static  ArrayList<String> Spec_REF_list;
	public static  ArrayList<String> Spec_FPS_account;
	public static void main(String[] args) {
		Main_utils m = new Main_utils();
		Estatement e = new Estatement();
		String folder_path = "";
		String path="";

		folder_path = "C:\\Users\\jeff.xie\\Desktop\\ERROR";
		folder_path = "C:\\Users\\jeff.xie\\Desktop\\error2";
		folder_path = "C:\\Users\\jeff.xie\\Desktop\\estatement";
		folder_path = "D:\\Project\\Automation\\estatement\\20240418";
		folder_path = "D:\\Project\\Automation\\estatement\\20230803";
		folder_path = "D:\\Project\\Automation\\estatement\\20240903";
		folder_path = "D:\\PC\\Automation\\estatement工具更新集合\\增加USD\\20240903";
		folder_path = "D:\\PC\\Automation\\estatement\\20250206";
		path = "C:/Users/jeff.xie/Desktop/error2/8000000305_est_76c7626ac19a2dee5fc8d250b745b8b3.pdf";
		path = "C:\\Users\\jeff.xie\\Desktop\\estatement\\8000022729_est_d56f85373d22b3f6cf4ba1b4b7a85377.pdf";
		path = "D:\\Project\\Automation\\estatement\\usd\\8000051205_est_27bff9b18f2e272b2ef9274a64c7cd53.pdf";
		Spec_REF_list= Common.read_list_from_txt("/src/com/utils/Spec_REF.txt");
		//Spec_REF_list= Common.read_list_from_local_txt();
		Spec_FPS_account= Common.read_list_from_txt("/src/com/utils/Spec_FPS_account.txt");
		m.run_all_estatement(folder_path);
//		m.estatement_test(path,e,Spec_REF_list);
	}


	@SuppressWarnings("static-access")
	public  ArrayList<Estatement> run_all_estatement(String path) {
		fail_ref_path= path+Common.fail_refs_fileName;
		long start_time =System.currentTimeMillis();
		//Spec_REF_list= Common.read_list_from_txt("/src/com/utils/Spec_REF.txt");
		Spec_REF_list= Common.read_list_from_local_txt();
		Spec_FPS_account= Common.read_list_from_txt("/src/com/utils/Spec_FPS_account.txt");
		ArrayList<String> file_List = new ArrayList<>();
		ArrayList<Estatement> Estatement_List = new ArrayList<>();
		File file = new File(path);
		File[] fs = file.listFiles();
		for(File f:fs) {
			String file_name = f.getName();
			//if(file_name.endsWith(".pdf")&&f.getName().contains("800")) {
			if(file_name.endsWith(".pdf")) {
				file_List.add(f.getAbsolutePath().trim());
				Estatement e = new Estatement();
				//String customer_id = file_name.substring(0,10);
				if(f.getName().contains("800")) {
					String customer_id = get_customer_id(file_name);
					e.setCustomer_id(customer_id);
				}else {
					//e.setCustomer_id(f.getName().trim());
					e.setCustomer_id(f.getName().split("_")[0].trim());
				}
				System.out.println(e.getCustomer_id());
				e.setFile_name(file_name);
				e.setAbsolute_path(path);
				e.setAbsolute_filename(path+"/"+file_name);
				Estatement_Main.error_customer =e.getCustomer_id();
				e = estatement_test(f.getAbsolutePath().trim(),e,Spec_REF_list);
				Estatement_List.add(e);
			}
		}

		get_results(Estatement_List,path);    //检查所有错误

		Result_Utils result_Utils = new Result_Utils(Spec_FPS_account);
		String filepath = result_Utils.Print_result_to_Excel(Estatement_List, path,fail_refs);

		Generate_Picture_Report();//调用Python程序截图

		result_file_path =filepath;
		long end_time =System.currentTimeMillis();
		long cost_time =end_time-start_time;
		System.out.println("cost time: "+cost_time);
		return Estatement_List;
	}

	private void Generate_Picture_Report() {
		try {
			//Java_to_Python.run_python_2(fail_ref_path);
			Java_to_Python.run_python_with_abs_path(fail_ref_path);
		} catch (Exception e) {
		}
	}


	public String get_customer_id(String filename){
		if(filename.contains("800")) {
			int index = filename.indexOf("800");
			return filename.substring(index,index+10);
		}else {
			return filename.substring(0,10);
		}
	}

	public Estatement estatement_test(String filepath,Estatement estatement,ArrayList<String> Spec_REF_list) {
		PDF p = new PDF();
		String content = p.Get_PDF_Content(filepath);
		//System.out.println(content);
		estatement = get_first_infomations(content,estatement);
		estatement = check_title_for_every_page(estatement,content);
		Core_Account_Utils core = new Core_Account_Utils(Spec_REF_list);
		estatement=core.get_core_account_transactions(content,estatement);
		
		CoreAccountUsdUtils coreAccountUsdUtils = new CoreAccountUsdUtils();
		estatement = coreAccountUsdUtils.getCoreAccountUsdTransactions(content,estatement);
		
//		GoSave_Utils goSave_Utils = new GoSave_Utils();
//		estatement=goSave_Utils.get_go_save_exsit(content, estatement);
//		estatement=goSave_Utils.get_go_save_transactions_history(content, estatement);
		
		GoSave2_Utils goSave2_Utils = new GoSave2_Utils();
		estatement=goSave2_Utils.get_go_save_exsit(content, estatement);
		estatement=goSave2_Utils.get_go_save_transactions_history(content, estatement);
		
		
		GoSave2UsdUtils goSave2UsdUtils = new GoSave2UsdUtils();
		estatement =goSave2UsdUtils.get_go_save_usd_exsit(content, estatement);
		estatement =goSave2UsdUtils.getGoSave2UsdTransactionsHistory(content, estatement);
		
		

		Loans_Utils loans_Utils = new Loans_Utils();
		estatement=loans_Utils.get_loans_transactions(content, estatement);
		Foreign_Currency_Utils foreign_Currency_Utils = new Foreign_Currency_Utils();
		estatement=foreign_Currency_Utils.get_foreign_currency_transactions(content, estatement);
		estatement=get_total_ammount(estatement);
		estatement=get_all_fail_ref(estatement);
		if(estatement.getAll_fail_Ref_list().size()>0){
			Ref file_ref_object= new Ref();
			file_ref_object.setCustomer_ID(estatement.getCustomer_id());
			file_ref_object.setFail_Ref_list(estatement.getAll_fail_Ref_list());
			file_ref_object.setFile_name(estatement.getFile_name());
			file_ref_object.setPath(estatement.getAbsolute_path());
			fail_refs.add(file_ref_object);
		}
		//System.out.println(estatement.toResult());
		return estatement;
	}

	public Estatement  get_all_fail_ref(Estatement e) {

		ArrayList<String> all_fail_ref_list = new ArrayList<>();

		if(e.getLoan_fail_trans_Ref_for_record()==null ||e.getLoan_fail_trans_Ref_for_record().size()==0) {
		}else {
			all_fail_ref_list.addAll(e.getLoan_fail_trans_Ref_for_record());
		}
		if(e.getFail_trans_Ref_list_for_desc()==null ||e.getFail_trans_Ref_list_for_desc().size()==0) {
		}else {
			all_fail_ref_list.addAll(e.getFail_trans_Ref_list_for_desc());
		}
		//		if(e.getGoSave_fail_trans_Ref_from_coreaccount_list()==null ||e.getGoSave_fail_trans_Ref_from_coreaccount_list().size()==0) {
		//		}else {
		//			all_fail_ref_list.addAll(e.getGoSave_fail_trans_Ref_from_coreaccount_list());
		//		}
		if(e.getFail_start_Ref_list()==null ||e.getFail_start_Ref_list().size()==0) {
		}else {
			all_fail_ref_list.addAll(e.getFail_start_Ref_list());
		}
		if(e.getWrong_single_amount_of_ref_list()==null ||e.getWrong_single_amount_of_ref_list().size()==0) {
		}else {
			all_fail_ref_list.addAll(e.getWrong_single_amount_of_ref_list());
		}
		if(e.getFail_trans_Ref_list_for_Invalid_trans()==null ||e.getFail_trans_Ref_list_for_Invalid_trans().size()==0) {
		}else {
			ArrayList<String> l = e.getFail_trans_Ref_list_for_Invalid_trans();
			if(!Spec_FPS_account.contains(e.getCustomer_id())) {
				if(l.size()>10) {
					all_fail_ref_list.addAll(l.subList(0, 10));
				}else{
					all_fail_ref_list.addAll(e.getFail_trans_Ref_list_for_Invalid_trans());
				}
			}
		}
		
		if(e.getGoSave_fail_trans_Ref_from_coreaccount_list()==null ||e.getGoSave_fail_trans_Ref_from_coreaccount_list().size()==0) {
			
		}else {
			all_fail_ref_list.addAll(e.getGoSave_fail_trans_Ref_from_coreaccount_list());
		}
		e.setAll_fail_Ref_list(all_fail_ref_list);
		return e;
	}


	public String get_results(ArrayList<Estatement> Estatement_List,String path) {
		StringBuilder stringBuilder = new StringBuilder();
		for(int i =0;i<Estatement_List.size();i++) {
			Estatement e = Estatement_List.get(i);
			//stringBuilder.append(Estatement_List.get(i).toResult());
			if("-".equals(e.getLast_month_amount())) {
				stringBuilder.append(e.getCustomer_id()+": Previous statement result : False"+"\n");
			}
			if(!e.isCore_account_hkd_result()) {
				stringBuilder.append(e.getCustomer_id()+": Core Account result: False"+"\n");
			}
			if(!e.isGo_save_result()) {
				stringBuilder.append(e.getCustomer_id()+": GoSave result: False"+"\n");
			}
			if(!e.isLoans_result()) {
				stringBuilder.append(e.getCustomer_id()+": Loans result : False"+"\n");
			}
			if(!e.isIs_total_amount_equal()) {
				stringBuilder.append(e.getCustomer_id()+": total amount result: False"+"\n");
			}
		}
		stringBuilder.append("\n\n");
		stringBuilder=print_customer_account_ID(Estatement_List,stringBuilder);
		String filepath=Common.save_result_to_txt(stringBuilder.toString(), path);
		return filepath;
	}

	public StringBuilder print_customer_account_ID(ArrayList<Estatement> Estatement_List,StringBuilder stringBuilder) {
		for(int i =0;i<Estatement_List.size();i++) {
			stringBuilder.append(Estatement_List.get(i).getCustomer_id()+"\t"+Estatement_List.get(i).getAccount_id()+"\n");
		}
		return stringBuilder;
	}

	/****
	public void print_summary_result(ArrayList<Estatement> Estatement_List) {
		for(int i =0;i<Estatement_List.size();i++) {
			Estatement e = Estatement_List.get(i);
			if(e.isCore_account_result()&&e.isGo_save_result()&&e.isIs_total_amount_equal()&&e.isLoans_result()) {

			}else {
				//System.out.println(e.getCustomer_id());
			}
			if(e.getLast_month_amount().equals("-")) {
				System.out.println(e.getCustomer_id()+": 承上结余为：-");
			}
			if(!e.isCore_account_result()) {
				System.out.println(e.getCustomer_id()+": 核心账户结果为：False");
			}
			if(!e.isGo_save_result()) {
				System.out.println(e.getCustomer_id()+": GoSave结果为：False");
			}
			if(!e.isLoans_result()) {
				System.out.println(e.getCustomer_id()+": Loans结果为：False");
			}
			if(!e.isIs_total_amount_equal()) {
				System.out.println(e.getCustomer_id()+": 结存总额结果为：False");
			}
		}
	}
	 ****/


	public ArrayList<Estatement> run_test(String folder_path) {
		ArrayList<Estatement> estatement_List= run_all_estatement(folder_path);
		return estatement_List;
	}


	public  Estatement  check_title_for_every_page(Estatement e,String content) {
		boolean page_title_check=true;
		//Page 1 of 7
		String[] page_line_need_shuzu = {"Page","of",e.getPage()};

		String[] content_list = content.split("\n");
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.is_all_of_shuzu_item_in_line(line, page_line_need_shuzu)) {

				String  p =Common.get_value_string_from_line(line, -1);
				String  p2 =Common.get_value_string_from_line(line, -3).trim();
				if(p2.contains(e.getPage())) {
					break;
				}

				if(p.contains(e.getPage())) {
					//	你的銀行月結單Your Bank Statement (1 Feb 2021 - 28 Feb 2021)
					//	列印日期 Date of issue : 8 Mar 2021
					//	賬戶號碼 Account Number : 1000000184
					//	銀行代碼 Bank Code / 分行編號 Branch Code : 390 / 750

					try {
						if(!(Common.isContainsYourBankStatementString(content_list[i+1])&&content_list[i+1].contains(e.getWhich_month()))){
							page_title_check=false;
							e.setIs_page_title_right(page_title_check);
							break;
						}
					} catch (ArrayIndexOutOfBoundsException e2) {
						// TODO: handle exception
						page_title_check=false;
						e.setIs_page_title_right(page_title_check);
						break;
					}

					if(!(Common.isContainsDateOfIssueString(content_list[i+2]))){
						page_title_check=false;
						e.setIs_page_title_right(page_title_check);
						break;
					}
					//if(!(content_list[i+3].contains("賬戶號碼 Account Number")&&content_list[i+1].contains(e.getAccount_id()))){
					if(!(Common.isContainsAccountNumberString(content_list[i+3]))){
						page_title_check=false;
						e.setIs_page_title_right(page_title_check);
						break;
					}
					String[] temp_4= {Global.BANK_CODE_STRING,Global.BRANCH_CODE_STRING,"390","750"};
					String[] temp_4_SIMPLIFIED_CHINESE= {Global.BANK_CODE_STRING_SIMPLIFIED_CHINESE,Global.BRANCH_CODE_STRING_SIMPLIFIED_CHINESE,"390","750"};
					if(!(Common.is_all_of_shuzu_item_in_line(content_list[i+4], temp_4))){
						if(!(Common.is_all_of_shuzu_item_in_line(content_list[i+4], temp_4_SIMPLIFIED_CHINESE))){
							page_title_check=false;
							e.setIs_page_title_right(page_title_check);
							break;
						}
					}
				}
			}
		}
		e.setIs_page_title_right(page_title_check);
		return e;
	}

	
	//逻辑重写
	public  Estatement  get_total_ammount(Estatement estatement) {
		
		//double gosave_amount =Common.string_to_double(estatement.getGosave_total_calculate());
		//double gosave_amount2 =Common.string_to_double(estatement.getGosave2_total_calculate());
		double gosave_amount2 =Common.string_to_double(estatement.getGosave2_total_capture_title());

		double total_coreaccout_amount =Common.string_to_double(estatement.getCoreaccount_total_capture_title());
		double usdAccoutAmount =0.0;
		if(estatement.getUsdAccountAmount()!=null) {
			usdAccoutAmount =Common.string_to_double(estatement.getUsdAccountAmount());
		}
		double investmentAccountAmount =0.0;
		if(estatement.getInvestmentAccountAmount()!=null) {
			investmentAccountAmount =Common.string_to_double(estatement.getInvestmentAccountAmount());
		}
		double ForeignCurrencyAccountAmount =0.0;
		if(estatement.getForeignCurrencyAccountAmount()!=null) {
			ForeignCurrencyAccountAmount =Common.string_to_double(estatement.getForeignCurrencyAccountAmount());
		}
		//double total = total_coreaccout_amount+usdAccoutAmount+investmentAccountAmount+ForeignCurrencyAccountAmount+gosave_amount2;
		double total = total_coreaccout_amount + gosave_amount2 + investmentAccountAmount;
		estatement.setTotal_Amount_calculate(Common.double_to_String(Common.get_double_round(total)));

		if(estatement.getTotal_Amount_capture_title()==null||estatement.getTotal_Amount_capture_title().contains("-")) {
			if(estatement.getTotal_Amount_capture_title()==null) {
				estatement.setTotal_Amount_capture_title("0.0");
				estatement.setIs_account_close(true);
			}
			if(estatement.isIs_account_close()){
				estatement.setIs_total_amount_equal(true);
			}else{
				if(estatement.getTotal_Amount_capture_title().contains("-")&&(Common.is_amount_zero(estatement.getTotal_Amount_calculate()))) {
					estatement.setIs_total_amount_equal(true);
				}else {
					estatement.setIs_total_amount_equal(false);
				}
			}
		}else if(Common.compare_amount_string_to_double(estatement.getTotal_Amount_calculate(), estatement.getTotal_Amount_capture_title())) {
			//需要四舍五入就用 compareAmountStringByApproximatelyEqual  不用四舍五入就用compare_amount_string_to_double
			
			//System.out.println("estatement.getCustomer_id():"+estatement.getCustomer_id());
			//System.out.println("estatement.getTotal_Amount_calculate():"+estatement.getTotal_Amount_calculate());
			//System.out.println("estatement.getTotal_Amount_capture_title():"+estatement.getTotal_Amount_capture_title());
			estatement.setIs_total_amount_equal(true);
		}else{
			estatement.setIs_total_amount_equal(false);
		}
		
		return estatement;
	}

	public  Estatement  get_first_infomations(String content,Estatement estatement) {
		//取消賬戶 Account Closed
		if(Common.isContainsAccountClosedString(content)&&content.contains("Account Closed")) {
			estatement.setIs_account_close(true);
		}

		boolean is_have_gosave=false;
		boolean is_have_gosave2=false;
		String[] content_list = content.split("\n");
		
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsFxRateAsOf(line)) {
				String v = content_list[i+1].replace("\r", "").replace("\n", "").trim();
				int index = v.indexOf(":");
				String fxRate = v.substring(index+1).trim();
				System.out.println("fxRate: "+fxRate);
				estatement.setFxRate(fxRate);
				break;
			}
		}
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			String[] first_info_want_find_shuzu_1 = {"(",")","202"};
			if(Common.isContainsTransactionHistoryStartString(line)) {
				break;
			}
			//System.out.println(line);
			//Common.isContainsAccountNumberFullString(line)
			//帐户号码 Account Number: 1020643307
			if(Common.isContainsAccountNumberFullString(line)) {
				String account = Common.get_string_value_from_string_with_different_start(line,
						Global.ACCOUNT_NUMBER_FULL_STRING,Global.ACCOUNT_NUMBER_FULL_STRING_SIMPLIFIED_CHINESE);
				estatement.setAccount_id(account);
			}
			//			核心賬戶 Core Account 310.82
			//核心賬戶 Core Account 18,657,187.59
			//港元 HKD 3,506,970.09
			//美元 USD 15,150,217.50
			if(Common.isContainsCoreAccountString(line)) {
				//System.out.println(line);
				String ammount_string = Common.get_string_value_from_string_with_start(line, "Core Account");
				if(ammount_string.trim().startsWith("-")) {
					estatement.setCoreaccount_total_capture_title("0.0");
				}else {
					estatement.setCoreaccount_total_capture_title(Common.string_move_special(ammount_string));
				}
				
				String hkd_line = content_list[i+1];
				if(Common.isContainsHkDollarString(hkd_line)) {
					String v = Common.get_string_value_from_string_with_start(hkd_line, "HKD");
					estatement.setCoreAccountHkdTitle(Common.string_move_special(v));
				}else {
					estatement.setCoreAccountHkdTitle("0.0");
				}
				
				String usd_line = content_list[i+2];
				if(Common.isContainsUsDollarString(usd_line)) {
					String v = Common.get_string_value_from_string_with_start(usd_line, "USD");
					estatement.setCoreAccountUsdTitle(Common.string_move_special(v));
				}else {
					estatement.setCoreAccountUsdTitle("0.0");
				}
				//System.out.println("estatement.setCoreaccount_total_capture_title: "+estatement.getCoreaccount_total_capture_title());
				//System.out.println("estatement.getCoreAccountHkdTitle: "+estatement.getCoreAccountHkdTitle());
				//System.out.println("estatement.getCoreAccountUsdTitle: "+estatement.getCoreAccountUsdTitle());
			}
			
			//定期存款 Time Desposit - GoSave 2.0 1,255,882.28
			//港元 HKD 312,000.00
			//美元 USD 943,882.28
			if(Common.isContainsGosave20TitleString(line)) {
				is_have_gosave2=true;
				String ammount_string = Common.get_string_value_from_string_with_different_start(line, 
						Global.GOSAVE20_TITLE_STRING,Global.GOSAVE20_TITLE_STRING_SIMPLIFIED_CHINESE);
				estatement.setGosave2_total_capture_title(Common.string_move_special(ammount_string));
				
				String hkd_line = content_list[i+1];
				if(Common.isContainsHkDollarString(hkd_line)) {
					String v = Common.get_string_value_from_string_with_start(hkd_line, "HKD");
					estatement.setGosave2_hkd_capture_title(Common.string_move_special(v));
				}else {
					estatement.setCoreAccountHkdTitle("0.0");
				}
				
				String usd_line = content_list[i+2];
				if(Common.isContainsUsDollarString(usd_line)) {
					String v = Common.get_string_value_from_string_with_start(usd_line, "USD");
					estatement.setGosave2_usd_capture_title(Common.string_move_special(v));
				}else {
					estatement.setCoreAccountUsdTitle("0.0");
				}
				//System.out.println("estatement.getGosave2_total_capture_title: "+estatement.getGosave2_total_capture_title());
				//System.out.println("estatement.getGosave2_hkd_capture_title: "+estatement.getGosave2_hkd_capture_title());
				//System.out.println("estatement.getGosave2_usd_capture_title: "+estatement.getGosave2_usd_capture_title());
			}
			//			結存總額 Net Position 6,810.82
			if(Common.isContainsTotalAmountString(line)) {
				//System.out.println(line);
				String ammount_string = Common.get_string_value_from_string_with_start(line, "Net Position");
				//System.out.println(Common.string_move_special(ammount_string));
				estatement.setTotal_Amount_capture_title(Common.string_move_special(ammount_string));
			}
			//			WeLab 私人貸款 WeLab Personal Loan 0.00	   更改为 GoFlexi
			if(Common.isContainsPersonalLoanString(line)||Common.isContainsLoansString(line)) {
				if(Common.isContainsPersonalLoanString(line)) {
					String ammount_string = Common.get_string_value_from_string_with_start(line, "Personal Loan");
					estatement.setLaon_total_capture_title(Common.string_move_special(ammount_string));
				}else if(Common.isContainsLoansString(line)){
					String ammount_string = Common.get_string_value_from_string_with_different_start(line,
							Global.LOANS_STRING,Global.LOANS_STRING_SIMPLIFIED_CHINESE);
					estatement.setLaon_total_capture_title(Common.string_move_special(ammount_string));	
				}
			}
			
			

			//	投資賬戶 Investment Account 64,872.64
			if(Common.isContainsInvestmentAccountString(line)) {
				String ammount_string = Common.get_string_value_from_string_with_different_start(line,
						Global.INVESTMENT_ACCOUNT_STRING, Global.INVESTMENT_ACCOUNT_STRING_SIMPLIFIED_CHINESE);
				estatement.setInvestmentAccountAmount(Common.string_move_special(ammount_string));
			}
			
			//USD 版本删除外币账户
			/*
			//外幣賬戶 Foreign Currency Account
			if(Common.isContainsFireignCurrencyAccount(line)) {
				String ammount_string = Common.get_string_value_from_string_with_different_start(line,
						Global.FIREIGN_CURRENCY_ACCOUNT, Global.FIREIGN_CURRENCY_ACCOUNT_SIMPLIFIED_CHINESE);
				estatement.setForeignCurrencyAccountAmount(Common.string_move_special(ammount_string));
			}
			//	美元 USD 3,579.50
			if(Common.isContainsUsdString(line)) {
				String ammount_string = Common.get_string_value_from_string_with_different_start(line,
						Global.USD_STRING, Global.USD_STRING_SIMPLIFIED_CHINESE);
				estatement.setUsdAccountAmount(Common.string_move_special(ammount_string));
			}
						//			GoSave 6,500.00  2024年开始没有GOSAVE 1.0
			if(Common.isContainsGosave20TitleString(line)&&(!Common.isContainsGosaveTitleString(line))) {
				is_have_gosave=true;
				String ammount_string = Common.get_string_value_from_string_with_start(line, "GoSave");
				estatement.setGosave_total_capture_title(Common.string_move_special(ammount_string));
			}
			*/
			
			if(Common.is_all_of_shuzu_item_in_line(line,first_info_want_find_shuzu_1)&&
					Common.isContainsMonthlyStatementString(line) && Common.is_one_of_shuzu_item_in_line(line,Common.month_list)){

				//你的銀行月結單Your Bank Statement (1 Jan 2021 - 31 Jan 2021)
				String v = Common.get_string_value_from_string_with_start_and_end(line, "(", ")");
				//System.out.println(v);
				estatement.setWhich_month(v);
				int YEAR = Integer.parseInt(Common.get_value_string_from_line(v, -1));
				String Month = Common.get_value_string_from_line(v, -2);
				if("Jan".equals(Month)) {
					estatement.setLast_months_YEAR(YEAR-1);
				}else {
					estatement.setLast_months_YEAR(YEAR);
				}
				estatement.setLast_Month(Common.get_last_month(Month));
				estatement.setNext_Month(Common.get_next_month(Month));
				estatement.setYEAR(YEAR);
				estatement.setMonth(Month);
			}
		}

		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(line.contains("Page 1 of")) {
				String p =Common.get_string_value_from_string_with_start(line, "Page 1 of");
				estatement.setPage(p.trim());
				break;
			}
		}
		if(!is_have_gosave) {
			estatement.setGosave_total_capture_title("0.00");
		}
		if(!is_have_gosave2) {
			estatement.setGosave2_total_capture_title("0.00");
		}
		estatement.setIs_have_gosave_trans(is_have_gosave);
		estatement.setIs_have_gosave2_trans(is_have_gosave2);
		return estatement;
	}

}
