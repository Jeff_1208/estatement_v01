package com.utils;

import java.util.ArrayList;
import com.entity.Estatement;
import com.entity.Transactions;


public class Core_Account_Utils{


	//String[] trans_type_shuzu= {"收款","付款","Debit Card 消費","現金回贈","存款利息","GoSave 轉至核心賬戶","存入GoSave","銀通櫃員機","其他","Cirrus/海外櫃員機","Debit Arrangement","每月供款"};
//	String[] trans_type_shuzu= {Global.SEND_MONEY_STRING,
//			Global.RECEIVE_MONEY_STRING,
//			Global.DEBIT_CARD_SPENDING_STRING,
//			Global.CASH_REBATE_STRING,
//			Global.DEPOSIT_INTEREST_STRING,
//			Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING,
//			Global.DEPOSIT_TO_GOSAVE_STRING,
//			Global.JETCO_ATM_STRING,
//			Global.OTHER_STRING,
//			Global.OVERSEAS_ATM_STRING,
//			Global.DEBIT_ARRANGEMENT_STRING,
//			Global.MONTHLY_REPAYMENT_STRING
//			};
	//String[] checK_FT_START_shuzu= {"收款","付款","Debit Card 消費","現金回贈","銀通櫃員機","海外櫃員機","存入款項"};
	String[] checK_FT_START_shuzu= {
			Global.SEND_MONEY_STRING,Global.SEND_MONEY_STRING_SIMPLIFIED_CHINESE,
			Global.RECEIVE_MONEY_STRING,Global.RECEIVE_MONEY_STRING_SIMPLIFIED_CHINESE,
			Global.DEBIT_CARD_SPENDING_STRING, Global.DEBIT_CARD_SPENDING_STRING_SIMPLIFIED_CHINESE,
			Global.CASH_REBATE_STRING,Global.CASH_REBATE_STRING_SIMPLIFIED_CHINESE,
			Global.JETCO_ATM_STRING,Global.JETCO_ATM_STRING_SIMPLIFIED_CHINESE,
			Global.OVERSEAS_ATM_STRING,Global.OVERSEAS_ATM_STRING_SIMPLIFIED_CHINESE,
			Global.ADD_MONEY_STRING, Global.ADD_MONEY_STRING_SIMPLIFIED_CHINESE};
	//String[] checK_AAAC_START_shuzu= {"存入GoSave","GoSave 轉至核心賬戶"};
	String[] checK_AAAC_START_shuzu= {Global.DEPOSIT_TO_GOSAVE_STRING,Global.DEPOSIT_TO_GOSAVE_STRING_SIMPLIFIED_CHINESE,
			Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING, Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE};

	private ArrayList<String> Spec_REF_list;
	public Core_Account_Utils(ArrayList<String> Spec_REF_list) {
		this.Spec_REF_list= Spec_REF_list;
	}
	public Core_Account_Utils() {
	}
	//{"收款","付款","Debit Card 消費","現金回贈","存款利息","GoSave 轉至核心賬戶","存入GoSave","銀通櫃員機提款","其他","Cirrus/海外櫃員機提款","Debit Arrangement"};
	public String get_type(String line,String[] content_list,int index){

		if(Common.isContainsGosaveTimeDepositString(line)&&(!Common.isContainsCoreAccountString(line))&&
				(!Common.isContainsDepositToGosaveString(line))) {
			line  = content_list[index-1];
		}
		//System.out.println("print type line: "+line);
		String type="";
		if(Common.isContainsReceiveMoneyString(line)){
			type = Global.RECEIVE_MONEY_STRING;
		}else if(Common.isContainsSendMoneyString(line)) {
			type = Global.SEND_MONEY_STRING;
		}else if(Common.isContainsDebitCardSpendingString(line)) {
			type = Global.DEBIT_CARD_SPENDING_STRING;
		}else if(Common.isContainsCashRebateString(line)) {
			type = Global.CASH_REBATE_STRING;
		}else if(Common.isContainsAddMoneyString(line)) {
			type = Global.ADD_MONEY_STRING;
		}else if(Common.isContainsDepositInterestString(line)) {
			type = Global.DEPOSIT_INTEREST_STRING;
		}else if(Common.isContainsGosaveTransferredToCoreAccountString(line)) {
			//System.out.println(line);
			type = Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING;
		}else if(Common.isContainsDepositToGosaveString(line)) {
			type = Global.DEPOSIT_TO_GOSAVE_STRING;
		}else if(Common.isContainsJetcoAtmWithdrawalString(line)) {
			type = Global.JETCO_ATM_WITHDRAWAL_STRING;
		}else if(Common.isContainsJetcoAtmRefundString(line)) {
			type = Global.JETCO_ATM_REFUND_STRING;
		}else if(Common.isContainsOtherString(line)) {
			type = Global.OTHER_STRING;
		}else if(Common.isContainsOverseasAtmWithdrawalString(line)) {
			type = Global.OVERSEAS_ATM_WITHDRAWAL_STRING;
		}else if(Common.isContainsOverseasAtmRefundString(line)) {
			type = Global.OVERSEAS_ATM_REFUND_STRING;
		}else if(Common.isContainsDebitArrangementString(line)) {
			type = Global.DEBIT_ARRANGEMENT_STRING;
		}else if(Common.isContainsLoanDrawdownString(line)) {
			type = Global.LOAN_DRAWDOWN_STRING;
		}else if(Common.isContainsMonthlyRepaymentString(line)) {
			type = Global.MONTHLY_REPAYMENT_STRING;
		}else{
			type = "unexcept";
		}
		return type;
	}


	public Transactions check_type_for_transaction(String type ,String[] content_list,int index,Transactions tran,Estatement e) {
		boolean result = false;
		//String[] need_check_type_shuzu = {"存款利息","存入款項","付款","收款","Debit Card 消費","銀通櫃員機提款","銀通櫃員機退款","Cirrus/海外櫃員機提款","Cirrus/海外櫃員機退款","存入GoSave","GoSave 轉至核心賬戶"};

		if(Common.isContainsDepositInterestString(type)) {
			result = is_have_flag_string(content_list,index,"Deposit interest");
		}else if(Common.isContainsAddMoneyString(type)) {
			result = is_have_flag_string(content_list,index,"Add money");
		}else if(Common.isContainsSendMoneyString(type)) {
			result = is_have_flag_string(content_list,index,"Send money");
		}else if(Common.isContainsReceiveMoneyString(type)) {
			boolean result1 = is_have_flag_string(content_list,index,"Receive money");
			boolean result2 = is_have_flag_string(content_list,index,"reversal");
			result = result2||result1;
		}else if(Common.isContainsDebitCardString(type)) {
			String[] debit_shuzu ={"Debit Card refund","Debit Card spending"};
			result = is_have_flag_string_shuzu(content_list,index,debit_shuzu);
		}else if(Common.isContainsJetcoAtmWithdrawalString(type)) {
			//String[] JETCO_shuzu ={"JETCO ATM refund","JETCO ATM withdrawal"};
			//result = is_have_flag_string_shuzu(content_list[index],JETCO_shuzu);
			result = is_have_flag_string(content_list,index,"JETCO ATM withdrawal");
		}else if(Common.isContainsJetcoAtmRefundString(type)) {
			result = is_have_flag_string(content_list,index,"JETCO ATM refund");
		}else if(Common.isContainsOverseasAtmString(type)) {
			//String[] Overseas_shuzu ={"Cirrus/ Overseas ATM withdrawal","Cirrus/ Overseas ATM refund"};
			String[] Overseas_shuzu ={"Cirrus","overseas","ATM"};
			result = is_have_all_flag_inline(content_list,index,Overseas_shuzu);
		}else if(Common.isContainsDepositToGosaveString(type)) {
			result = is_have_flag_string(content_list,index,"Deposit to GoSave");
		}else if(Common.isContainsGosaveTransferredToCoreAccountString(type)) {
			result = is_have_flag_string(content_list,index,"GoSave transferred to");
		}else {
			result=true;
		}

		tran.setIs_right_type(result);
		return tran;
	}
	public boolean  is_have_flag_string(String[] content_list,int index,String flag) {
		if(content_list[index].contains(flag)) {
			return true;
		}else if(content_list[index-1].contains(flag)) {
			return true;
		}else if(content_list[index-2].contains(flag)) {
			return true;
		}else if(content_list[index+1].contains(flag)) {
			return true;
		}else{
			return false;
		}
	}
	public boolean  is_have_flag_string_shuzu(String[] content_list,int index,String[] flag) {
		if(Common.is_one_of_shuzu_item_in_line(content_list[index], flag)) {
			return true;
		}else if(Common.is_one_of_shuzu_item_in_line(content_list[index-1], flag)) {
			return true;
		}else if(Common.is_one_of_shuzu_item_in_line(content_list[index-2], flag)) {
			return true;
		}else if(Common.is_one_of_shuzu_item_in_line(content_list[index+1], flag)) {
			return true;
		}else{
			return false;
		}
	}
	public boolean  is_have_all_flag_inline(String[] content_list,int index,String[] flag) {
		if(Common.is_all_of_shuzu_item_in_line(content_list[index], flag)) {
			return true;
		}else if(Common.is_all_of_shuzu_item_in_line(content_list[index-1], flag)) {
			return true;
		}else if(Common.is_all_of_shuzu_item_in_line(content_list[index-2], flag)) {
			return true;
		}else if(Common.is_all_of_shuzu_item_in_line(content_list[index+1], flag)) {
			return true;
		}else{
			return false;
		}
	}

	public ArrayList<Transactions> get_Transactions(String[] content_list, int index,ArrayList<Transactions> trans_list,Estatement e,int trans_id) {
		Transactions tran = new Transactions();
		tran.setCore_account_trans_number(trans_id);
		tran.setAmount_row_content(content_list[index]);
		String amount =Common.get_value_string_from_line(content_list[index], -1);
		tran.setAmount(amount);
		tran.setDay_string(Common.get_days_String_value_from_line(content_list[index]));
		tran.setDay(Common.get_days_value_from_line(content_list[index]));
		tran.setMonth(Common.get_month_value_from_line(content_list[index]));
		tran.setYear(Common.get_year_value_from_line(content_list[index]));
		String type =get_type(content_list[index-1],content_list,index-1);
		//System.out.println(content_list[index] + "type: "+type);
		//检查  type是否正确
		tran=check_type_for_transaction(type,content_list,index,tran,e);
		
		if(Common.isContainsSendMoneyString(type)) {
			if(amount.contains("-")) {
				tran.setIs_recieve_money_have_right_fuhao(false);
			}else {
				tran.setIs_recieve_money_have_right_fuhao(true);
			}
		}
		if(Common.isContainsReceiveMoneyString(type)) {
			if(amount.contains("-")) {
				tran.setIs_send_money_have_right_fuhao(true);
			}else {
				tran.setIs_send_money_have_right_fuhao(false);
			}
		}
		
		if(Common.isContainsGosaveTitleString(type)) {
			tran.setIs_goSave_trans(true);
		}
		if(Common.isContainsGosaveTransferredToCoreAccountString(type)) {
			tran.setIs_JECTO_transaction(true);
			if(content_list[index].contains("CNY")) {
				boolean result=Common.check_foreign_currency_Amount_for_trans(content_list,index);
				tran.setCheck_foreign_currency_Amount(result);
			}
		}
		//tran =Common.get_Date_String_from_line(content_list[index], e,tran);
		tran.setDate(tran.getDay_string()+" "+tran.getMonth()+" "+tran.getYear());
		tran.setType(type);

		String ref = Common.get_Ref_for_spec_trans(content_list, index, "f:");
		if(ref=="NONE") {
			ref = Common.get_Ref_for_spec_trans(content_list, index, ":");
		}
		tran.setRef(ref);
		// 检查记录暂时跳过Jeff
		tran = check_trans_desc(content_list, index, e, tran, type);// 检查transation的   description

		trans_list.add(tran);
		return trans_list;
	}

	public ArrayList<Transactions> get_Transactions_spec(String[] content_list, int index,ArrayList<Transactions> trans_list,Estatement e,int trans_id) {
		//		Receive money Ref: FT21152TJW06
		//		付款 Send money to FUTU S********* I************ (**** K**** L**************
		//		1 Jun 2021 A**
		//		Send money Ref: FT211523QL9L -50,000.00
		Transactions tran = new Transactions();
		tran.setCore_account_trans_number(trans_id);
		tran.setAmount_row_content(content_list[index]);
		String amount =Common.get_value_string_from_line(content_list[index], -1);
		tran.setAmount(amount);
		tran.setDay_string(Common.get_days_String_value_from_line(content_list[index]));
		tran.setDay(Common.get_days_value_from_line(content_list[index-1]));
		tran.setMonth(Common.get_month_value_from_line(content_list[index-1]));
		tran.setYear(Common.get_year_value_from_line(content_list[index-1]));
		String type =get_type(content_list[index-2],content_list,index-2);
		tran.setType(type);
		//tran=check_type_for_transaction(type,content_list,index,tran,e);//检查  type是否正确
		System.out.println(content_list[index] + "type: "+type);
		if(type.contains("GoSave")) {
			tran.setIs_goSave_trans(true);
		}
		if(Common.isContainsGosaveTransferredToCoreAccountString(type)) {
			tran.setIs_JECTO_transaction(true);
			if(content_list[index].contains("CNY")) {
				boolean result=Common.check_foreign_currency_Amount_for_trans(content_list,index);
				tran.setCheck_foreign_currency_Amount(result);
			}
		}
		//tran =Common.get_Date_String_from_line(content_list[index], e,tran);
		tran.setDate(tran.getDay_string()+" "+tran.getMonth()+" "+tran.getYear());
		tran.setRef(Common.get_Ref_for_spec_trans(content_list, index,"f:"));
		//tran = check_trans_desc(content_list, index, e, tran, type);// 检查transation的   description
		trans_list.add(tran);
		return trans_list;
	}

	public boolean compare_ref_for_ATM_revesal(Transactions tran, ArrayList<Transactions> trans_list,Estatement e) {
		boolean r =false;
		for(int i =0;i<trans_list.size();i++) {
			Transactions t = trans_list.get(i);
			if(!Common.isContainsDebitCardSpendingString(tran.getType())) continue;
			if(tran.getCore_account_trans_number() ==t.getCore_account_trans_number()) continue;
			if(compare_invalid_trans_amount(tran.getAmount(), t.getAmount())) {
				if(tran.getRef().contains(t.getRef())) {
					r=true;
				}else {  
				}
			}
		}
		return r;
	}

	public  Estatement check_single_for_amount(ArrayList<Transactions> trans_list,Estatement e) {
		//A "-" sign should be shown to indicate for a debit value.
		//String[] checK_single_should_be_found_shuzu= {Global.SEND_MONEY_STRING,Global.DEBIT_CARD_SPENDING_STRING,Global.DEPOSIT_TO_GOSAVE_STRING,Global.OVERSEAS_ATM_WITHDRAWAL_STRING};
		//String[] checK_single_should_not_be_found_shuzu= {Global.RECEIVE_MONEY_STRING};
		//GoSave 轉至核心賑戶 （在Gosave的转账记录查询）
		ArrayList<String> fail_single_list = new ArrayList<>();
		/***
		for(int i =0;i<trans_list.size();i++) {
			Transactions tran = trans_list.get(i);
			if(tran.isIs_invalid_trans()) {
				continue;
			}
			if(Common.is_one_of_shuzu_item_in_line(tran.getType(), checK_single_should_be_found_shuzu)){
				if(!(tran.getAmount().contains("-"))) {
					if(!(tran.getDescription().contains("Send money by FPS"))){  //Invalid trans description是Send money by FPS，付款的金额符号可能为 正，即不包含“-”
						//System.out.println(tran.getDate()+": "+tran.getAmount());
						if(!(tran.getDescription().contains("reversal"))){  //退款的时候金额可以不包含“-”
							fail_single_list.add(tran.getRef());
						}
					}
				}
			}

			if(Common.is_one_of_shuzu_item_in_line(tran.getType(), checK_single_should_not_be_found_shuzu)){
				if(tran.getAmount().contains("-")) {
					fail_single_list.add(tran.getRef());
				}
			}
			if(Common.isContainsGosaveTransferredToCoreAccountString(tran.getType())) {//针对ATM机取款又退回的情况
				if((!tran.getAmount().contains("-"))&&(!compare_ref_for_ATM_revesal(tran,trans_list,e))) {
					fail_single_list.add(tran.getRef());
				}
			}
		}
		 ***/  
		//remove to check singlel for amount, due to invalid transaction update logic, 
		//the description update from "Send Money by FPS" to "Send Money to User name", 
		e.setWrong_single_amount_of_ref_list(fail_single_list);
		return e;
	}

	public  Estatement check_Ref_Start_flag(ArrayList<Transactions> trans_list,Estatement e) {
		//'"GoSave Time Deposit" with correct reference no. should be shown as transaction description. 
		//Prefix of the reference no. should be "AAAC"
		ArrayList<String> fail_ref_start_list = new ArrayList<>();
		for(int i =0;i<trans_list.size();i++) {
			Transactions tran = trans_list.get(i);
			if(Common.is_one_of_shuzu_item_in_line(tran.getType(), checK_FT_START_shuzu)){
				if(tran.getRef()==null) {
					//System.out.println(e.getCustomer_id()+tran.toString()+content_list[index]+content_list[index+1]);
				}else if(!(tran.getRef().startsWith("FT"))) {
					//System.out.println(e.getCustomer_id()+tran.toString());
					if(!tran.getRef().contains("money")) {
						fail_ref_start_list.add(tran.getRef());
					}
				}
			}
			if(Common.is_one_of_shuzu_item_in_line(tran.getType(), checK_AAAC_START_shuzu)){
				if(!(tran.getRef().startsWith("AAAC"))) {
					if(!tran.getRef().contains("money")) {
						fail_ref_start_list.add(tran.getRef());
					}
				}
			}
		}
		e.setFail_start_Ref_list(fail_ref_start_list);
		return e;
	}

	private Transactions check_trans_desc(String[] content_list, int index, Estatement e, Transactions tran, String type) {
		boolean desc_check_result=true;
		//if(content_list[index].contains(e.getNext_Month())) { //判断是否有下个月的记录
		//	desc_check_result=false;
		//}
		String desc =Common.get_string_value_from_string_with_start(content_list[index-1], type);
		tran.setDescription(desc);

		String[] shuzu_temp= {Global.DEBIT_ARRANGEMENT_STRING,Global.DEBIT_ARRANGEMENT_STRING_SIMPLIFIED_CHINESE,
				Global.DEBIT_CARD_SPENDING_STRING,Global.DEBIT_CARD_SPENDING_STRING_SIMPLIFIED_CHINESE};
		if(desc.length()<3) {
			if(!Common.is_one_of_shuzu_item_in_line(tran.getType(), shuzu_temp)) {
				desc_check_result=false;
			}
		}

		//  Reversal Transaction   Invalid transation 返回金额 的description
		//Send money by FPS //Receive Money by FPS
		if(Common.compareUpperStr(desc, "SEND MONEY BY FPS")||Common.compareUpperStr(desc, "RECEIVE MONEY BY FPS")) {
			tran.setIs_invalid_trans(true);
		}

		//Check 銀通櫃員機提款外币取款
		if(Common.isContainsGosaveTransferredToCoreAccountString(type)) {
			if(desc==null||desc.length()<5) {
				desc_check_result=true;
			}
			if(desc.contains("CNY")) {
				desc_check_result=false;
				if(tran.getAmount_row_content().contains("CNY")) {
					desc_check_result=true;
				}
			}
		}

		//Check miss ")"
		/***
		if(desc.contains("(")) {
			//System.out.println(desc);
			desc_check_result=false;
			if(desc.contains(")")) {
				desc_check_result=true;
			}
		}
		 ***/

		/***
		if(desc.contains("Receive money")||desc.contains("Send money")) {
			if(!desc.contains("*")) { //检查名字是否缩写
				desc_check_result=false;
			}
		}
		//'- Unexpected Jan ATM cash withdrawal is shown on the eStatement
		if((whichmonth.contains(tran.getMonth()))){
			if(desc.contains("ATM")) {
				desc_check_result=false;
			}
		}
		 ***/

		String[] ss1 = {"related","Receive money","eversa","FPS"};
		if(Common.isContainsReceiveMoneyString(tran.getType())){
			if(Common.is_one_of_shuzu_item_in_line(tran.getDescription(), ss1)) {
			}else{
				desc_check_result=false;
			}
		}
		if(Common.isContainsDepositInterestString(tran.getType())){
			if(!(tran.getDescription().contains("Deposit Interest")||tran.getDescription().contains("Deposit interest"))) {
				desc_check_result=false;
			}
		}

		String[] xianjinhuizeng_wanna_shuzu= {"WeLab Debit Card rebate","18 District Campaign","18 Districts Campaign","Cash for all","Referral Campaign Reward","Campaign",
				"rebate","Interest Rate Boost","Bring-a-friend","Account Opening Reward","Test code","Bakery","Rebate","Spring Seasonal","Debit Card reversal"};
//		tran.getType().contains(Global.CASH_REBATE_STRING)
		if(Common.isContainsCashRebateString(tran.getType())){
			if(!Common.is_one_of_shuzu_item_in_line(tran.getDescription(), xianjinhuizeng_wanna_shuzu)) {
				desc_check_result=false;
			}
		}
		if(Common.isContainsAddMoneyString(tran.getType())){
			System.out.println(e.getFile_name()+": "+tran.getRef() +" "+content_list[index-1]+" "+" "+content_list[index]+tran.getDescription());
		}

		//type.contains(Global.DEBIT_CARD_STRING)
		if(Common.isContainsDebitCardString(type)){
			String tran_date = Common.get_date_string(tran.getYear(), tran.getMonth(), tran.getDay_string());
			//String date_today = Common.get_today_date_string();
			//String settlement_date = Common.get_settlement_date_string();
			String settlement_date_1 = Common.get_settlement_date_string(e.getYEAR(),e.getMonth());
			int diff_days = Common.get_days_diffs(tran_date, settlement_date_1,e,tran);
			if(diff_days>=90) {
				if(!tran.getDescription().contains("#")) {
					desc_check_result=false;
				}
			}else {
				if(tran.getDescription().contains("#")) {
					desc_check_result=false;
				}
			}
		}
		if(desc_check_result) {
			//if(!Common.check_description(desc)) desc_check_result=false;
		}
		//System.out.println("tran.getType(): "+tran.getType());
		//System.out.println("tran.getType(): "+type);
		//System.out.println("desc_check_result: "+desc_check_result);
		tran.setCheck_desc(desc_check_result);
		return tran;
	}


	static String[] coreaccount_want_find_shuzu_for_long_before = {"20","."};
	static String[] donot_want_find_shuzu = {"Statement","Financial","APPLE.COM","TAOBAO.COM", "SURFSHARK.COM", 
			Global.FOREIGN_CURRENCY_EXCHANGE_STRING,Global.FOREIGN_CURRENCY_EXCHANGE_STRING_SIMPLIFIED_CHINESE,
			"USD/ HKD"};
	static String[] add_want_find_shuzu = {"Receive money","Send money","Ref","rebate","Add money","JETCO ATM","Overseas ATM",
			"GoSave transferred to","Debit Card","Deposit","Other Transaction","Account Opening Reward","WeLab Bank rebate",
			"Bill/merchant payment"};
	public  Estatement get_core_account_transactions(String content,Estatement estatement) {
		boolean printflag = false;
		boolean core_account_flag =false;
		double coreaccount_total_calculate=0;

		String[] coreaccount_want_find_shuzu_1 = {estatement.getMonth(),String.valueOf(estatement.getYEAR()),"."};
		String[] coreaccount_want_find_shuzu_2 = {estatement.getLast_Month(),String.valueOf(estatement.getLast_months_YEAR()),"."};
		String[] coreaccount_want_find_shuzu_3 = {estatement.getNext_Month(),"20",".",String.valueOf(estatement.getYEAR())};
		ArrayList<Transactions> trans_list = new ArrayList<>();
		int days = Common.get_this_month_days(estatement.getYEAR(), estatement.getMonth());
		String[] content_list = content.split("\n");
		boolean is_have_intetest=false;
		int trans_id=0;
		String accountId = estatement.getAccount_id().trim();
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];

			if(Common.isContainsCurrentlyHeldString(line)) {break;}
			if(Common.isContainsCoreAccountUsd(line, accountId)) {break;}
			if(Common.isContainsTransactionHistoryStartString(line)) {core_account_flag =true;}
			//1 Jan 2021 承上結餘 Balance From Previous Statement -
			if(Common.isContainsBalanceFromPreviousStatementString(line)) {
				String v = Common.get_string_value_from_string_with_start(line, "Previous Statement");
				estatement.setLast_month_amount(Common.string_move_special(v));
				if(!(Common.string_move_special(v).equals("-"))) {
					coreaccount_total_calculate += Common.string_to_double(Common.string_move_special(v));
				}
				core_account_flag =true;
			}
			if(Common.isContainsClosingBalanceString(line)) {
				String v = Common.get_string_value_from_string_with_start(line, "Closing Balance");
				estatement.setCoreaccount_total_hkd_capture(Common.string_move_special(v));
				break;
			}
			if(Common.isContainsForeignCurrencySavingAccountString(line)||Common.isContainsPendingTransactionsStirng(line)) {break;}

			//Deposit interest 存款利息
			if(Common.isContainsDepositInterestString(line)) {
				is_have_intetest = true;
				String next_line =content_list[i+1];
				if(next_line.contains(""+days)&&next_line.contains(estatement.getMonth())&&next_line.contains(".")) {
					estatement.setInterest_capture(Common.get_double_value(next_line, -1));
				}
			}

			if(!core_account_flag) {continue;}
			if(Common.isContainsDebitCardSpendingString(line)) { continue;}//这一行没有金额，所有不抓取
			//27 Jan 2021 取消賬戶 Account Closed
			if(Common.isContainsAccountClosedString(line)) {
				estatement.setIs_account_close(true);
				continue;
			}
			
			if(Common.isContainsTitleContent(line)) {continue;}

			//Send money by FPS  Reversal Transaction  属于Invaild transaction  
			//26 Jan 2021 Debit Arrangement -0.01
			//26 Jan 2021 0.01
			if(Common.is_all_of_shuzu_item_in_line(line,coreaccount_want_find_shuzu_1) && Common.is_all_of_shuzu_item_not_in_line(line,donot_want_find_shuzu)&&Common.the_line_is_have_a_double_string(line)){
				//System.out.println(line);
				//检查当月的记录
				trans_id+=1;
				if(printflag) {System.out.println(Common.get_double_value(line, -1));}
				trans_list=get_Transactions(content_list, i,trans_list,estatement,trans_id);
				coreaccount_total_calculate +=Common.get_double_value(line, -1);
			}else if(Common.is_all_of_shuzu_item_in_line(line,coreaccount_want_find_shuzu_2) && Common.is_all_of_shuzu_item_not_in_line(line,donot_want_find_shuzu)&&Common.the_line_is_have_a_double_string(line)){
				//System.out.println(line);
				//System.out.println(Common.get_double_value(line, -1));
				//检查上个月的记录
				trans_id+=1;
				if(printflag) {System.out.println(Common.get_double_value(line, -1));}
				trans_list=get_Transactions(content_list, i,trans_list,estatement,trans_id);
				coreaccount_total_calculate +=Common.get_double_value(line, -1);
			}else if(Common.is_all_of_shuzu_item_in_line(line,coreaccount_want_find_shuzu_3) && Common.is_all_of_shuzu_item_not_in_line(line,donot_want_find_shuzu)&&Common.the_line_is_have_a_double_string(line)){
				//检查下个月的记录
				if(printflag) {System.out.println(Common.get_double_value(line, -1));}
				trans_id+=1;
				trans_list=get_Transactions(content_list, i,trans_list,estatement,trans_id);
				estatement.setIs_have_next_month_tran(false);
				coreaccount_total_calculate +=Common.get_double_value(line, -1);
			}else  if(Common.is_all_of_shuzu_item_in_line(line,coreaccount_want_find_shuzu_for_long_before) && Common.is_all_of_shuzu_item_not_in_line(line,donot_want_find_shuzu)&&Common.is_one_of_shuzu_item_in_line(line, Common.month_list)&&Common.the_line_is_have_a_double_string(line)){
				//检查很久之前的记录
				trans_id+=1;
				
				trans_list=get_Transactions(content_list, i,trans_list,estatement,trans_id);
				coreaccount_total_calculate +=Common.get_double_value(line, -1);
			}else if(Common.is_one_of_shuzu_item_in_line(line,add_want_find_shuzu) && Common.is_all_of_shuzu_item_not_in_line(line,donot_want_find_shuzu)&&Common.the_line_is_have_a_double_string(line)&&line.contains(".")){
				//			}else if(Common.is_one_of_shuzu_item_in_line(line,add_want_find_shuzu)&&line.contains(".") && Common.is_all_of_shuzu_item_not_in_line(line,donot_want_find_shuzu)){
				String amout=Common.get_value_string_from_line(line, -1);
				if(Common.is_valued_number(amout)) {
					//特殊的场景
					//Receive money Ref: FT2W10A5I6 MH5IN92ENRVA 20,000.00
					//Send money Ref: FT21033WQJ6T -1,000.00
					//Receive money ReIfN: FGT2105602TZV 0.10
					//Debit Card spending Ref: FT21159X9QBG 168.00
					//Send money CONSULTANT LTD -4,000.00
					//Cash rebate Cash Rebate) 50.00
					//Debit Card 退款 Debit Card reversal SURFSHARK.COM GBR Online 4816
					
					//Bill/merchant payment rent -5,077.00
					if(printflag) {System.out.println(line);}
					if(!line.contains("Ref:")) {
						System.out.println(line);
						if(printflag) {System.out.println(Common.get_double_value(line, -1));}
						coreaccount_total_calculate +=Common.get_double_value(line, -1);
						trans_id+=1;
						trans_list=get_Transactions_spec(content_list, i,trans_list,estatement,trans_id);
					}

				}
			}
		}

		if(!core_account_flag) {
			estatement.setLast_month_amount("-");
		}

		//添加核心账户的所有转账记录
		estatement.setTransations_history(trans_list);

		estatement =check_interest(trans_list, estatement);
		//System.out.println(estatement.getCustomer_id()+"\t estatement.getInterest() "+estatement.getInterest()+"\t"+estatement.getInterest_capture());
		double interest_add=Common.compare_interest(estatement);
		//if(interest_add>=0.1) {
		//System.out.println(estatement.getCustomer_id()+"\t 利息异常,金额差为： "+interest_add);
		//System.out.println(estatement.getCustomer_id()+"\t 利息异常,金额差为： "+interest_add +"\t"+"计算:"+estatement.getInterest()+"\t抓取:"+estatement.getInterest_capture());
		//estatement.setInterest_diff(interest_add);
		//estatement.setIs_check_interest(false);
		//}
		if((!is_have_intetest)&&estatement.getInterest()>=0.02) {
			//System.out.println(estatement.getCustomer_id()+"\t interest error, need show interest: "+estatement.getInterest());
			estatement.setInterest_diff(interest_add);
			//estatement.setIs_check_interest(false);   //2021112逻辑更改: 利息在下个月显示，所以不检查
		}

		//检测Ref的开头是否正确
		estatement = check_Ref_Start_flag(trans_list, estatement);

		//检测invalid_transations  
		//必须放在Set_fail_Ref之前，因为后面需要用到这一块的数据
		estatement = check_invalid_transations(trans_list,estatement);
		//检查desc是否正确
		estatement=Set_fail_Ref(estatement, trans_list);
		//System.out.println("estatement.getFail_trans_Ref_list_for_desc().size():"+estatement.getFail_trans_Ref_list_for_desc().size()+estatement.getFail_trans_Ref_list_for_desc().toString());

		//付款,Debit Card 消費,存入GoSave, 銀通櫃員機提款 , 海外櫃員機提款   的金额需要带有“-”    Send money to 
		//收款的金额不能带有“-”    Receive money from
		estatement=check_single_for_amount(trans_list,estatement);

		//计算金额保留两位小数
		coreaccount_total_calculate = Common.get_double_round(coreaccount_total_calculate);
		estatement.setCoreaccount_total_hkd_calculate(Common.doubleToString(coreaccount_total_calculate));
		System.out.println("coreaccount_total_calculate： "+coreaccount_total_calculate);
		System.out.println("estatement.getCoreaccount_total_hkd_capture()： "+estatement.getCoreaccount_total_hkd_capture());

		//对比计算结果是否相等
		if(estatement.isIs_account_close()) {
			estatement.setCore_account_hkd_result(true);	
		}else if(estatement.getCoreaccount_total_hkd_capture()==null) {
			estatement.setCore_account_hkd_result(false);	
		}else if(Common.compare_amount_string_to_double(estatement.getCoreaccount_total_hkd_calculate(), estatement.getCoreaccount_total_hkd_capture())) {
				estatement.setCore_account_hkd_result(true);
		}else if(estatement.getCoreaccount_total_hkd_capture().contains("-")) {
			if(Common.string_to_double(estatement.getCoreaccount_total_hkd_calculate()) ==0.0) {
				estatement.setCore_account_hkd_result(true);
			}else {
				estatement.setCore_account_hkd_result(false);	
			}
		}else{
			estatement.setCore_account_hkd_result(false);	
		}
		return estatement;
	}

	public Estatement check_invalid_transations(ArrayList<Transactions> trans_list,Estatement estatement) {
		ArrayList<String> fail_invalid_ref_list = new ArrayList<>();
		for(int i =0;i<trans_list.size();i++) {
			if(!trans_list.get(i).isIs_invalid_trans()) {
				continue;
			}
			if(!compare_invalid_trans(trans_list.get(i),trans_list,estatement)) {
				if(!Spec_REF_list.contains(trans_list.get(i).getRef().trim())){
					fail_invalid_ref_list.add(trans_list.get(i).getRef());
				}
			}
		}
		//System.out.println(estatement.getCustomer_id()+": "+fail_invalid_ref_list.toString());
		estatement.setFail_trans_Ref_list_for_Invalid_trans(fail_invalid_ref_list);
		return estatement;
	}
	public boolean compare_invalid_trans(Transactions tran, ArrayList<Transactions> trans_list,Estatement e) {
		boolean r =false;
		for(int i =0;i<trans_list.size();i++) {
			Transactions t = trans_list.get(i);
			if(!t.isIs_invalid_trans()) continue;
			if(compare_invalid_trans_amount(tran.getAmount(), t.getAmount())) {
				if(tran.getRef().contains(t.getRef())) {
					//System.out.println(e.getCustomer_id()+": "+tran.getRef());
					r=true;
				}else {  //针对手动操作invalid trans
				}
			}
		}
		return r;
	}

	public boolean compare_invalid_trans_amount(String amount1, String amount2) {
		boolean r =false;
		if(amount1.contains("-")&&amount2.contains("-")) {
			return false;
		}else if((!amount1.contains("-"))&&amount2.contains("-")) {
			r= Common.compare_absolute_amount(amount1, amount2);
		}else if(amount1.contains("-")&&(!amount2.contains("-"))) {
			r= Common.compare_absolute_amount(amount1, amount2);
		}else if((!amount1.contains("-"))&&(!amount2.contains("-"))) {
			return false;
		}else{
			return false;
		}
		return r;
	}


	private Estatement Set_fail_Ref(Estatement estatement,
			ArrayList<Transactions> trans_list) {
		ArrayList<String> gosave_amount_list = new ArrayList<>();
		ArrayList<String> fail_desc_Ref_list = new ArrayList<>();
		for(int i =0;i<trans_list.size();i++) {
			Transactions tran = trans_list.get(i);
			if(trans_list.get(i).isIs_goSave_trans()) {
				gosave_amount_list.add(trans_list.get(i).getAmount());
			}
			if(Common.isContainsSendMoneyString(tran.getType())) {
				if((tran.getAmount().contains("-"))) {
					if(tran.getDescription()==null) continue;
					if((tran.getDescription().contains("Send money to"))){   //Send money to **  名字全部被注释
						String name  = Common.get_string_value_from_string_with_start(tran.getDescription(), "Send money to");
						name = Common.string_move_special(name);
						name = name.replace("*", "");
						if(name.length()<1){  
							//System.out.println(estatement.getCustomer_id()+": "+  tran.getDescription()+": "+  tran.getRef());
							System.out.println("A");
							fail_desc_Ref_list.add(tran.getRef());
							//System.out.println("KKKK"+tran.getRef());
						}
					}
				}
			}
			//String[] spec_ref_remove= {"付款","Debit","收款","GoSave","存款利息","NONE"};
			if(!(trans_list.get(i).isCheck_desc())) {  //如果desc的结果的为假，则添加对应的ref到fail list中
				String REF = trans_list.get(i).getRef();
				if(!(REF.contains("NONE"))) {
					//if(!Common.is_one_of_shuzu_item_in_line(REF, spec_ref_remove)) {
					fail_desc_Ref_list.add(REF);
					System.out.println("B");
				}
			}
			if(!trans_list.get(i).isCheck_foreign_currency_Amount()) {
				fail_desc_Ref_list.add(trans_list.get(i).getRef());
			}
		}
		//添加invalid trans的错误desc
		//fail_Ref_list.addAll(estatement.getFail_trans_Ref_list_for_Invalid_trans());  
		//System.out.println(fail_Ref_list.toString());
		estatement.setFail_trans_Ref_list_for_desc(fail_desc_Ref_list);
		estatement.setGoSave_Amount_list_in_coreaccount_trans_history(gosave_amount_list);
		//System.out.println(gosave_amount_list.toString());
		return estatement;
	}

	public  Estatement check_interest(ArrayList<Transactions> trans_list,Estatement e) {
		int days = Common.get_this_month_days(e.getYEAR(), e.getMonth());
		int y = Common.get_year_days(e.getYEAR());
		double last_amount =0.0;
		String chengshangyue=e.getLast_month_amount();
		if(chengshangyue ==null) {
			e.setLast_month_amount("-");
			chengshangyue="-";
		}
		if(chengshangyue.equals("-")) {
			last_amount = last_amount+get_last_month_records_amount(trans_list, e);	
		}else {
			last_amount = Common.string_to_double(e.getLast_month_amount())+get_last_month_records_amount(trans_list, e);
			//last_amount = Common.string_to_double(e.getLast_month_amount());
		}

		ArrayList<Double> each_day_amount_list = new ArrayList<>();
		ArrayList<Double> current_held_each_day_amount_list = new ArrayList<>();
		for (int i = 1; i <=days; i++) {
			each_day_amount_list.add(get_amount_each_day(trans_list,i,e));
		}
		for (int i = 1; i <=days; i++) {
			double amount =last_amount;
			for (int j = 0; j < each_day_amount_list.size(); j++) {
				if(j>(i-1)) break;
				amount += each_day_amount_list.get(j);
			}
			current_held_each_day_amount_list.add(amount);
		}
		double total1=0.0;
		for (int i = 0; i < current_held_each_day_amount_list.size(); i++) {
			total1 += current_held_each_day_amount_list.get(i);
		}
		double total_interest = total1*0.001/y;


		/***
		double a1 = get_amount_each_day(trans_list,1,e);
		double a2 = get_amount_each_day(trans_list,2,e);
		double a3 = get_amount_each_day(trans_list,3,e);
		double a4 = get_amount_each_day(trans_list,4,e);
		double a5 = get_amount_each_day(trans_list,5,e);
		double a6 = get_amount_each_day(trans_list,6,e);
		double a7 = get_amount_each_day(trans_list,7,e);
		double a8 = get_amount_each_day(trans_list,8,e);
		double a9 = get_amount_each_day(trans_list,9,e);
		double a10 = get_amount_each_day(trans_list,10,e);
		double a11 = get_amount_each_day(trans_list,11,e);
		double a12 = get_amount_each_day(trans_list,12,e);
		double a13 = get_amount_each_day(trans_list,13,e);
		double a14 = get_amount_each_day(trans_list,14,e);
		double a15 = get_amount_each_day(trans_list,15,e);
		double a16 = get_amount_each_day(trans_list,16,e);
		double a17 = get_amount_each_day(trans_list,17,e);
		double a18 = get_amount_each_day(trans_list,18,e);
		double a19 = get_amount_each_day(trans_list,19,e);
		double a20 = get_amount_each_day(trans_list,20,e);
		double a21 = get_amount_each_day(trans_list,21,e);
		double a22 = get_amount_each_day(trans_list,22,e);
		double a23 = get_amount_each_day(trans_list,23,e);
		double a24 = get_amount_each_day(trans_list,24,e);
		double a25 = get_amount_each_day(trans_list,25,e);
		double a26 = get_amount_each_day(trans_list,26,e);
		double a27 = get_amount_each_day(trans_list,27,e);
		double a28 = get_amount_each_day(trans_list,28,e);
		double a29=0.0;
		double a30=0.0;
		double a31=0.0;
		if(days>=29)a29 = get_amount_each_day(trans_list,29,e);
		if(days>=30)a30 = get_amount_each_day(trans_list,30,e);
		if(days>=31)a31 = get_amount_each_day(trans_list,31,e);

		double day1 = last_amount+a1;
		double day2 = last_amount+a1+a2;
		double day3 = last_amount+a1+a2+a3;
		double day4 = last_amount+a1+a2+a3+a4;
		double day5 = last_amount+a1+a2+a3+a4+a5;
		double day6 = last_amount+a1+a2+a3+a4+a5+a6;
		double day7 = last_amount+a1+a2+a3+a4+a5+a6+a7;
		double day8 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8;
		double day9 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9;
		double day10 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10;
		double day11 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11;
		double day12 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12;
		double day13 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13;
		double day14 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14;
		double day15 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15;
		double day16 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16;
		double day17 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17;
		double day18 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18;
		double day19 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19;
		double day20 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20;
		double day21 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21;
		double day22 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21+a22;
		double day23 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21+a22+a23;
		double day24 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21+a22+a23+a24;
		double day25 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21+a22+a23+a24+a25;
		double day26 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21+a22+a23+a24+a25+a26;
		double day27 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21+a22+a23+a24+a25+a26+a27;
		double day28 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21+a22+a23+a24+a25+a26+a27+a28;
		double day29 =0.0;
		double day30 =0.0;
		double day31 =0.0;
		if(days>=29)  day29 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21+a22+a23+a24+a25+a26+a27+a28+a29;
		if(days>=30)  day30 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21+a22+a23+a24+a25+a26+a27+a28+a29+a30;
		if(days>=31)  day31 = last_amount+a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18+a19+a20+a21+a22+a23+a24+a25+a26+a27+a28+a29+a30+a31;

		double total_interest=0.0;
		double total=0.0;
		if(days==28) total = day1+day2+day3+day4+day5+day6+day7+day8+day9+day10+day11+day12+day13+day14+day15+day16+day17+day18+day19+day20+day21+day22+day23+day24+day25+day26+day27+day28;
		if(days==29) total = day1+day2+day3+day4+day5+day6+day7+day8+day9+day10+day11+day12+day13+day14+day15+day16+day17+day18+day19+day20+day21+day22+day23+day24+day25+day26+day27+day28+day29;
		if(days==30) total = day1+day2+day3+day4+day5+day6+day7+day8+day9+day10+day11+day12+day13+day14+day15+day16+day17+day18+day19+day20+day21+day22+day23+day24+day25+day26+day27+day28+day29+day30;
		if(days==31) total = day1+day2+day3+day4+day5+day6+day7+day8+day9+day10+day11+day12+day13+day14+day15+day16+day17+day18+day19+day20+day21+day22+day23+day24+day25+day26+day27+day28+day29+day30+day31;
		total_interest = total*0.001/y;
		 ***/

		/***
		System.out.println("last_amount : "+Common.get_double_round(last_amount));
		System.out.println("day1 : \t"+Common.get_double_round(day1));
		System.out.println("day2 : \t"+Common.get_double_round(day2));
		System.out.println("day3 : \t"+Common.get_double_round(day3));
		System.out.println("day4 : \t"+Common.get_double_round(day4));
		System.out.println("day5 : \t"+Common.get_double_round(day5));
		System.out.println("day6 : \t"+Common.get_double_round(day6));
		System.out.println("day7 : \t"+Common.get_double_round(day7));
		System.out.println("day8 : \t"+Common.get_double_round(day8));
		System.out.println("day9 : \t"+Common.get_double_round(day9));
		System.out.println("day10 : \t"+Common.get_double_round(day10));
		System.out.println("day11 : \t"+Common.get_double_round(day11));
		System.out.println("day12 : \t"+Common.get_double_round(day12));
		System.out.println("day13 : \t"+Common.get_double_round(day13));
		System.out.println("day14 : \t"+Common.get_double_round(day14));
		System.out.println("day15 : \t"+Common.get_double_round(day15));
		System.out.println("day16 : \t"+Common.get_double_round(day16));
		System.out.println("day17 : \t"+Common.get_double_round(day17));
		System.out.println("day18 : \t"+Common.get_double_round(day18));
		System.out.println("day19 : \t"+Common.get_double_round(day19));
		System.out.println("day20 : \t"+Common.get_double_round(day20));
		System.out.println("day21 : \t"+Common.get_double_round(day21));
		System.out.println("day22 : \t"+Common.get_double_round(day22));
		System.out.println("day23 : \t"+Common.get_double_round(day23));
		System.out.println("day24 : \t"+Common.get_double_round(day24));
		System.out.println("day25 : \t"+Common.get_double_round(day25));
		System.out.println("day26 : \t"+Common.get_double_round(day26));
		System.out.println("day27 : \t"+Common.get_double_round(day27));
		System.out.println("day28 : \t"+Common.get_double_round(day28));
		if(days>=29)  System.out.println("day29 : \t"+Common.get_double_round(day29));
		if(days>=30)  System.out.println("day30 : \t"+Common.get_double_round(day30));
		if(days>=31)  System.out.println("day31 : \t"+Common.get_double_round(day31));
		 ***/

		/***
		double amount1 = interest(day1,y);
		double amount2 = interest(day2,y);
		double amount3 = interest(day3,y);
		double amount4 = interest(day4,y);
		double amount5 = interest(day5,y);
		double amount6 = interest(day6,y);
		double amount7 = interest(day7,y);
		double amount8 = interest(day8,y);
		double amount9 = interest(day9,y);
		double amount10 = interest(day10,y);
		double amount11 = interest(day11,y);
		double amount12 = interest(day12,y);
		double amount13 = interest(day13,y);
		double amount14 = interest(day14,y);
		double amount15 = interest(day15,y);
		double amount16 = interest(day16,y);
		double amount17 = interest(day17,y);
		double amount18 = interest(day18,y);
		double amount19 = interest(day19,y);
		double amount20 = interest(day20,y);
		double amount21 = interest(day21,y);
		double amount22 = interest(day22,y);
		double amount23 = interest(day23,y);
		double amount24 = interest(day24,y);
		double amount25 = interest(day25,y);
		double amount26 = interest(day26,y);
		double amount27 = interest(day27,y);
		double amount28 = interest(day28,y);
		double amount29 =0.0;
		double amount30 =0.0;
		double amount31 =0.0;
		if(days>=29)   amount29 = interest(day29,y);
		if(days>=30)   amount30 = interest(day30,y);
		if(days>=31)   amount31 = interest(day31,y);

		if(days==28) total_interest = amount1+amount2+amount3+amount4+amount5+amount6+amount7+amount8+amount9+amount10+amount11+amount12+amount13+amount14+amount15+amount16+amount17+amount18+amount19+amount20+amount21+amount22+amount23+amount24+amount25+amount26+amount27+amount28;
		if(days==29) total_interest = amount1+amount2+amount3+amount4+amount5+amount6+amount7+amount8+amount9+amount10+amount11+amount12+amount13+amount14+amount15+amount16+amount17+amount18+amount19+amount20+amount21+amount22+amount23+amount24+amount25+amount26+amount27+amount28+amount29;
		if(days==30) total_interest = amount1+amount2+amount3+amount4+amount5+amount6+amount7+amount8+amount9+amount10+amount11+amount12+amount13+amount14+amount15+amount16+amount17+amount18+amount19+amount20+amount21+amount22+amount23+amount24+amount25+amount26+amount27+amount28+amount29+amount30;
		if(days==31) total_interest = amount1+amount2+amount3+amount4+amount5+amount6+amount7+amount8+amount9+amount10+amount11+amount12+amount13+amount14+amount15+amount16+amount17+amount18+amount19+amount20+amount21+amount22+amount23+amount24+amount25+amount26+amount27+amount28+amount29+amount30+amount31;
		 ***/


		if(total_interest>=0.01) {
			//return Common.get_double_round(total_interest);
			e.setInterest(Common.get_double_round(total_interest));
		}else {
			e.setInterest(0.0);
		}
		return e;
	}

	public double interest(double a,int year_days) {
		//'Amount = Sum of (daily account balance x (0.1%/total days of the year)) within the reporting month
		return Common.get_double_round(a*0.001/year_days);
	}

	public double get_amount_each_day(ArrayList<Transactions> trans_list,int d,Estatement e) {
		double amount =0.0;
		for(int i =0;i<trans_list.size();i++) {
			Transactions tran   = trans_list.get(i);
			int day =tran.getDay();
			String month =tran.getMonth();
			if(d==day&&e.getMonth().equals(month)) {
				amount += Common.string_to_double(tran.getAmount());
			}else {
				continue;
			}
		}
		//System.out.println("The day"+d+":\t"+Common.get_double_round(amount));
		return Common.get_double_round(amount);
	}

	public double get_last_month_records_amount(ArrayList<Transactions> trans_list,Estatement e) {
		String last_Month =e.getLast_Month();
		double amount =0.0;
		for(int i =0;i<trans_list.size();i++) {
			Transactions tran   = trans_list.get(i);
			if(tran.getMonth().equals(last_Month)) {
				amount += Common.string_to_double(tran.getAmount());
			}
		}
		//System.out.println("last amount: "+amount);
		return amount;
	}

}
