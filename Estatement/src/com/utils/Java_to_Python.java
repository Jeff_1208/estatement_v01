package com.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.io.IOException;
import java.io.InputStream;

public class Java_to_Python {
	public static void main(String[] args) {
		//String path = "D:/Project/e-Statement/estatement_pdf/2021_06_08_estatement/Result_fail_Refs.xlsx";
		//		//run_python_2(path);
		run_opencv("D:/Search.png", "D:/Setting.png",50,50);
		String data="D:/Project/e-Statement/estatement_pdf/2021_07_27_estatement/Result_fail_Refs.xlsx";
		run_exe(data);
		//run_cmd(data);
	}

//	(652, 1358)
//	(280, 1400)
	
	public static HashMap<String, Integer> run_opencv(String picturePath,String PagePicturePath,int xPercent,int yPercent) {
		HashMap<String, Integer> location = new HashMap<>();
		try {
			//x,y = get_center_location('D:/Battery.png', 'D:/Setting.png',0,0)
			//String cmds = String.format("python D:\\Project\\Program\\PythonWorkspace\\myProject\\python_project\\apptest\\myopencv\\other_case\\get_location_by_opencv.py %s %s %d %d", picturePath,PagePicturePath,xPercent,yPercent);
			//String cmds = String.format("python D:\\Project\\Program\\PythonWorkspace\\MyOpencv\\welab\\get_location_by_opencv.py %s %s %d %d", picturePath,PagePicturePath,xPercent,yPercent);
			String cmds = String.format("python D:\\get_location_by_opencv.py %s %s %d %d", picturePath,PagePicturePath,xPercent,yPercent);

			System.out.println("Executing python script for picture location.");
			Process pcs = Runtime.getRuntime().exec(cmds);
			pcs.waitFor();
			Thread.sleep(1000);
			
			// 定义Python脚本的返回值
			String result = null;
			// 获取CMD的返回流
			BufferedInputStream in = new BufferedInputStream(pcs.getInputStream());// 字符流转换字节流
			BufferedReader br = new BufferedReader(new InputStreamReader(in));// 这里也可以输出文本日志
			String lineStr = null;
			while ((lineStr = br.readLine()) != null) {
				result = lineStr;//Python 代码中print的数据就是返回值
				//xLocation: 147
				//yLocation: 212
				if(lineStr.contains("xLocation")) {
					int x =  Integer.parseInt(lineStr.split(":")[1].trim());
					location.put("x", x);
				}
				if(lineStr.contains("yLocation")) {
					int x =  Integer.parseInt(lineStr.split(":")[1].trim());
					location.put("y", x);
				}
			}
			// 关闭输入流
			br.close();
			in.close();
			System.out.println(result);
			System.out.println(location.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return location;
	}
	public static void run_python_2(String path) {
		try {

			// 定义传入Python脚本的命令行参数，将参数放入字符串数组里
			//String cmds = String.format("python C:/Users/jeff.xie/PycharmProjects/MyProject/study/test_for_java.py %s", path);
			//String cmds = String.format("python C:\\Users\\jeff.xie\\PycharmProjects\\MyProject\\welab\\estatement\\shot_screen_for_estatement02.py %s", path);

			String project_path =System.getProperty("user.dir");
			//String cmds = String.format("python D:\\Project\\Program\\workspace\\Estatement\\src\\com\\utils\\shot_screen_for_estatement02.py %s", path);
			String cmds = String.format("python "+project_path+"\\src\\com\\utils\\shot_screen_for_estatement02.py %s", path);
			
			// 定义传入Python脚本的命令行
			//String cmds = String.format("python C:/Users/jeff.xie/PycharmProjects/MyProject/study/test_for_java.py");

			// 执行CMD命令
			System.out.println("Executing python script file now.");
			Process pcs = Runtime.getRuntime().exec(cmds);
			pcs.waitFor();
			//Thread.sleep(1000);

			// 定义Python脚本的返回值
			String result = null;
			// 获取CMD的返回流
			BufferedInputStream in = new BufferedInputStream(pcs.getInputStream());// 字符流转换字节流
			BufferedReader br = new BufferedReader(new InputStreamReader(in));// 这里也可以输出文本日志
			String lineStr = null;
			while ((lineStr = br.readLine()) != null) {
				result = lineStr;//Python 代码中print的数据就是返回值
			}
			// 关闭输入流
			br.close();
			in.close();
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void run_python_with_abs_path(String path) {
		try {
			String python_execute_path="";
	        String osName = System.getProperty("os.name");
	        if (osName.startsWith("Windows")) {
	        	python_execute_path="D:/estatement/shot_screen_for_estatement02.exe";
	        } else {
	        	python_execute_path="/opt/estatement/shot_screen_for_estatement02.exe";
	        }
	        String[] cmds = {python_execute_path, path};
			// 执行CMD命令
			System.out.println("Executing python script file now.");
			Process pcs = Runtime.getRuntime().exec(cmds);
			pcs.waitFor();

			// 定义Python脚本的返回值
			String result = null;
			// 获取CMD的返回流
			BufferedInputStream in = new BufferedInputStream(pcs.getInputStream());// 字符流转换字节流
			BufferedReader br = new BufferedReader(new InputStreamReader(in));// 这里也可以输出文本日志
			String lineStr = null;
			while ((lineStr = br.readLine()) != null) {
				result = lineStr;//Python 代码中print的数据就是返回值
			}
			// 关闭输入流
			br.close();
			in.close();
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void run_python_1() {

		String data="Python";
		try {
			System.out.println("start");
			//String[] args1=new String[]{"D://Program Files//PP//Python//python.exe","C://Users//jeff.xie//PycharmProjects//MyProject//study//test_for_java.py",
			//		"css"};

			String cmds = String.format("python C:/Users/jeff.xie/PycharmProjects/MyProject/study/test_for_java.py %s", data);
			Process pr=Runtime.getRuntime().exec(cmds);


			BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			//BufferedReader in = new BufferedReader(new InputStreamReader(new BufferedInputStream(pr.getInputStream())));
			String line;
			System.out.println("in.readLine():"+in.readLine());
			while ((line = in.readLine()) != null) {
				System.out.println("KKKKKK: "+line); //Python 代码中print的数据就是返回值
				//System.out.println(line);
			}

			in.close();
			pr.waitFor();
			System.out.println("end");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void run_exe(String path) {
		//代码执行成功
		try {
			System.out.println("start");

			String[] cmds= {"D:\\a2\\shot_screen_for_estatement02.exe", path};
			Process pr=Runtime.getRuntime().exec(cmds);
			BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String line;
			//System.out.println("in.readLine():"+in.readLine());
			while ((line = in.readLine()) != null) {
				System.out.println(line);//Python 代码中print的数据就是返回值
			}
			pr.waitFor();
			System.out.println("end");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static String consumeInputStream(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is,"GBK"));
		String s;
		StringBuilder sb = new StringBuilder();
		while ((s = br.readLine()) != null) {
			System.out.println(s);
			sb.append(s);
		}
		return sb.toString();
	}


	public static void run_cmd(String path) {
		//代码执行成功
		try {
			LocalDateTime now = LocalDateTime.now();
			String path1 = DateTimeFormatter.ofPattern("yyy-MM-dd-HH-mm-ss").format(now);
			System.out.println(path1);
			//String cmds = String.format("/d");
			String[] cmds = {"cmd", "D:/Project/Program/WorkSpaceIDEA/Maven03/TestAllure"};
			//String[] cmds = {"cmd", "/d","cd  D:\\Project\\Program\\WorkSpaceIDEA\\Maven03\\TestAllure","allure generate report/ -o report/html --clean"};
			Runtime rt =Runtime.getRuntime();
			rt.exec(cmds);
			rt.exec("allure generate report/ -o report/html --clean");
			// 标准输入流（必须写在 waitFor 之前）
			//			String inStr = consumeInputStream(process.getInputStream());
			// 标准错误流（必须写在 waitFor 之前）
			//			String errStr = consumeInputStream(process.getErrorStream()); //若有错误信息则输出
			//			System.out.println(inStr);
			//			System.out.println(inStr);
			//			BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
			//			String line;
			//			//System.out.println("in.readLine():"+in.readLine());
			//			while ((line = in.readLine()) != null) {
			//				System.out.println(line);//Python 代码中print的数据就是返回值
			//			}

			//process.waitFor();

			/***
			String project_path =System.getProperty("user.dir");

			//	        cmds = String.format("cd %s",project_path);
			//	        rt.exec(cmds);
			cmds = String.format("allure generate target/allure-results/ -o report/html --clean");
			Process pcs = Runtime.getRuntime().exec(cmds);
			//	        rt.exec(cmds);

			String[] cmds= {"D:\\a2\\shot_screen_for_estatement02.exe", path};
			Process pr=Runtime.getRuntime().exec(cmds);
			BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String line;
			//System.out.println("in.readLine():"+in.readLine());
			while ((line = in.readLine()) != null) {
				System.out.println(line);//Python 代码中print的数据就是返回值
			}
			pr.waitFor();

			 ***/
			System.out.println("end");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
/**
Java调用Python程序


开发项目中需要在Java程序调用Python代码实现相应功能，通过查阅资料已实现，故记录之。

调用方式
直接通过Runtime进行调用
通过Jython调用
实践过程
1、直接通过Runtime进行调用
简单粗暴的做法，需要在本地安装Python，安装过程可以参考安装Python，粒度更加粗糙，效率较高。
test_argv.py文件放于D盘：

python代码如下
import sys
a = sys.argv[1]
b = sys.argv[2]
print("%s * %s = %s"%(a,b,int(a)*int(b)))   #print的数据就是返回值



通过Jython调用
Jython（原JPython），是一个用Java语言写的Python解释器。不需要安装Python软件。它不仅提供了Python的库，同时也提供了所有的Java类，
这就使得其有一个巨大的资源库。Jython可以直接调用Python程序中的指定函数或者对象方法，粒度更加精细。但遗憾的是，Jython运行速度并不理想。
导入jar包：


Java 代码
import org.python.util.PythonInterpreter;

public class TestJython {
	public static void main(String[] args) {
		PythonInterpreter interpreter = new PythonInterpreter();
		interpreter.exec("import sys ");
		interpreter.exec("a = input('Enter a:')");
		interpreter.exec("b = input('Enter b:')");
		interpreter.exec("print('%s * %s = %s' %(a, b, a*b))");
	}
}

 **/