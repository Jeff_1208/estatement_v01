package com.utils;

import java.util.ArrayList;
import java.util.HashMap;
import com.entity.Estatement;
import com.entity.GoSave;
import com.entity.Transactions;

public class GoSave2UsdUtils {

	static GoSave2_Utils goSave2_Utils = new GoSave2_Utils();
	public  Estatement get_go_save_usd_exsit(String content,Estatement estatement) {
		boolean goSaveUsdFlag =false;
		boolean isHaveGoSave2Usd =false;
		double go_save_usd_total_calculate=0;
		HashMap<String, String> current_usd_gosave_exist= new HashMap<>();
		String[] want_find_shuzu = {"p.a.","%"};
		
		
		ArrayList<GoSave> gosave2UsdCurrentHeldList = new ArrayList<>();
		int gosave2_usd_size = 0;
		String[] content_list = content.split("\n");
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsGosave2Usd(line)) {
				goSaveUsdFlag =true;
				continue;
			}
			if(goSaveUsdFlag&&Common.isContainsTransactionHistoryStartString(line)) {break;}
			if(goSaveUsdFlag&&Common.isContainsGosave2TransactionHistoryStartString(line)) {break;}
			if(goSaveUsdFlag&&Common.isContainsImportantNotes(line)) {break;}
			if(Common.isContainsLoansString(line)) {break;}
			if(!goSaveUsdFlag) {continue;}
			if(Common.isContainsTitleContent(line)) {continue;}

			if(Common.is_all_of_shuzu_item_in_line(line,want_find_shuzu) ){
				//System.out.println(line);
				isHaveGoSave2Usd=true;
				double amount = Common.get_double_value(line, -1);
				String gosave_id = Common.get_value_string_from_line(line, -4);
				//System.out.println("gosave_id: "+gosave_id);
				go_save_usd_total_calculate +=Common.get_double_value(line, -1);
				current_usd_gosave_exist.put(gosave_id, Common.double_to_String(amount));
				gosave2_usd_size+=1;
				GoSave gosave = GoSave2_Utils.setExistGosaveList(content_list, i, line, gosave2_usd_size);
				gosave2UsdCurrentHeldList.add(gosave);
			}
		}
		
		estatement.setGosave2_usd_size(gosave2_usd_size);

		if(isHaveGoSave2Usd) {
			estatement.setIs_have_gosave2_usd_record(isHaveGoSave2Usd);
			ArrayList<String> failReferenceNumberList =GoSave2_Utils.checkExistGosaveHeldIsDuplicate(gosave2UsdCurrentHeldList); 
			if(failReferenceNumberList.size()>1) {
				ArrayList<String> refs = estatement.getGoSave2_Exist_Held_Duplicate_Reference_Number();
				for (int i = 0; i < failReferenceNumberList.size(); i++) {
					refs.add(failReferenceNumberList.get(i));
				}
				estatement.setGoSave2_Exist_Held_Duplicate_Reference_Number(refs);
			}
		}else {
			estatement.setIs_have_gosave2_usd_record(false);
			estatement.setGosave2_usd_total_calculate("0");
			estatement.setGo_save2_usd_result(true);
			return estatement;
		}
		HashMap<String, String> map=GoSave2_Utils.sort_map(current_usd_gosave_exist);
		estatement.setCurrent_usd_gosave_exist(map);

		go_save_usd_total_calculate = Common.get_double_round(go_save_usd_total_calculate);
		estatement.setGosave2_usd_total_calculate(Common.doubleToString(go_save_usd_total_calculate));
		
		System.out.println("estatement.getGosave2_usd_total_calculate(): "+estatement.getGosave2_usd_total_calculate());
		System.out.println("estatement.getGosave2_usd_capture_title(): "+estatement.getGosave2_usd_capture_title());
		
		
		//美元乘以汇率和首页对比
		double usdToHKDGoSave =Common.string_to_double(estatement.getGosave2_usd_total_calculate()) * Common.string_to_double(estatement.getFxRate());
		//System.out.println("usdToHKDGoSave: "+usdToHKDGoSave);
		//System.out.println("estatement.getGosave2_usd_capture_title(): "+estatement.getGosave2_usd_capture_title());
		
		String usdToHKDGoSaveString =  Common.double_to_String_by_round_count(usdToHKDGoSave,2);
		if(Common.compare_amount_string_to_double(estatement.getGosave2_usd_capture_title(), usdToHKDGoSaveString)) {
			//System.out.println("Gosave对比美元转换成港币正确！");
		}else {
			System.out.println("Gosave对比美元转换成港币错误！");
		}


		/**
		//print_current_gosave(map);//输出现有GoSave金额
		if(Common.compare_amount_string_to_double(estatement.getGosave2_usd_total_calculate(), estatement.getGosave2_usd_capture_title())){
			estatement.setGo_save2_usd_result(true);
		}else {
			System.out.println("GoSave 2.0美元 金额异常");
			estatement.setGo_save2_usd_result(false);
		}
		**/
		// title显示的是美元对应的港元值，计算金额为美元，所以不做对比
		estatement.setGo_save2_usd_result(true);
		return estatement;
	}
	

	public  Estatement getGoSave2UsdTransactionsHistory(String content,Estatement estatement) {
		boolean go_save_usd_trans_flag =false;
		boolean go_save_usd_start_flag =false;
		boolean is_have_go_save2_usd_transactions =false;
		ArrayList<GoSave>  gosave2_usd_trans_list = new ArrayList<>();
 		int count=0;
		String[] content_list = content.split("\n");
		
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsGosave2Usd(line)) {
				go_save_usd_start_flag =true;
				continue;
			}
			if(Common.isContainsTransactionHistoryStartString(line)&&go_save_usd_start_flag) {go_save_usd_trans_flag=true;}
			if(Common.isContainsLoansString(line)||Common.isContainsLoansChineseString(line)||Common.isContainsImportantNotes(line)) {break;}
			if(Common.isContainsGosave20TitleString(line)) {continue;}
			if(Common.isContainsTitleContent(line)) {continue;}
			if(!go_save_usd_start_flag) {continue;}
			if(!go_save_usd_trans_flag) {continue;}

			
			//25 Oct 2022 Principal transferred to core -21.00
			//25 Oct 2022 22.00
			//20,000.00 21 Jul 2021 20 Jul 2023 AA21202ZKT06 -8,752.29
			//18 Mar 2024 Principal transferred to core -6.00   REF 在下一行
			if(Common.is_one_of_shuzu_item_in_line(line, Global.month_list)&&Common.the_line_is_have_a_double_string(line)){
				//System.out.println(line);
				count+=1;
				is_have_go_save2_usd_transactions=true;
				gosave2_usd_trans_list=GoSave2_Utils.set_one_gosave_trans(gosave2_usd_trans_list,line,-1,-3,estatement,content_list,i,count);
			}
		}
		
		//gosave_trans_list=generate_combine_gosave_trans(gosave_trans_list);//组合记录

		if(is_have_go_save2_usd_transactions) {
			estatement.setGoSave2_usd_trans_history(gosave2_usd_trans_list);
			estatement.setIs_have_gosave2_usd_trans(is_have_go_save2_usd_transactions);
			//System.out.println(estatement.getCustomer_id()+" estatement.getGoSave_trans_history().size():  "+estatement.getGoSave_trans_history().size());
		}else {
			estatement.setGoSave_fail_trans_Ref_for_amount_single(new ArrayList<>());
			estatement.setIs_have_gosave2_usd_trans(false);
			return estatement;
		}

		//检查核心账户中关于Gosave的记录，是否和Gosave的转账记录匹配
		//estatement=Set_Gosave_fail_amout_list(estatement, gosave2_trans_list);
		estatement=compareUsdTransactionForCoreAccountAndGosave(estatement);

		//检查gosave交易记录是否有利息重复,有
		estatement=check_usd_gosave_duplicate_interest(estatement, gosave2_usd_trans_list);

		//检查Gosave转账记录金额符号是否正确
		estatement=set_GoSave_fail_usd_trans_Ref_for_amount_single(estatement, gosave2_usd_trans_list);

		//System.out.println("fail:"+estatement.getGoSave_fail_trans_Ref_from_coreaccount_list());
		return estatement;
	}

	public static Estatement compareUsdTransactionForCoreAccountAndGosave(Estatement estatement) {
		ArrayList<Transactions> usd_transaction_core_account = estatement.getUsd_transations_history();
		ArrayList<GoSave> gosave_usd_trans = estatement.getGoSave2_usd_trans_history();
//		System.out.println("estatement.getUsd_transations_history().size(): "+usd_transaction_core_account.size());
//		System.out.println("gosave_usd_trans.size(): "+gosave_usd_trans.size());
//		for (int i = 0; i < usd_transaction_core_account.size(); i++) {
//			if(usd_transaction_core_account.get(i).isIs_goSave_trans()) {
//				System.out.println("KKKKKK");
//			}
//		}
		if( gosave_usd_trans.size()==0) {
			return estatement;
		}
		ArrayList<String> result =Common.CompareTransactionAndGosaveList(usd_transaction_core_account, gosave_usd_trans);
		ArrayList<String> result1 =Common.CompareTransactionAndGosaveListAccordingGosave(usd_transaction_core_account, gosave_usd_trans);
		result.addAll(result1);
		if(result.size()>0) {
			//System.out.println("result1.size(): "+result1.size());
			ArrayList<String> r = estatement.getGoSave_fail_trans_Ref_from_coreaccount_list();
			if(r==null||r.size()==0) {
				estatement.setGoSave_fail_trans_Ref_from_coreaccount_list(result);
			}else {
				r.addAll(result);
				estatement.setGoSave_fail_trans_Ref_from_coreaccount_list(r);
			}
		}
		return estatement;
	}
	
	public Estatement check_usd_gosave_duplicate_interest(Estatement e, ArrayList<GoSave> gosave_trans_list) {
		
		ArrayList<String> result = goSave2_Utils.check_gosave_trans_list_duplicate(gosave_trans_list);

		//这一部分代码好像后面没有用到
		if(result.size()>0) {
			//System.out.println(e.getCustomer_id()+": "+result.toString());
			ArrayList<String> r= e.getGoSave_fail_trans_Ref_from_coreaccount_list();
			if(r==null||r.size()==0) {
				e.setGoSave_Amount_list_in_coreaccount_trans_history(result);
			}else if(r.size()>0){
				if(result.size()>0) {
					r.addAll(result);
					e.setGoSave_Amount_list_in_coreaccount_trans_history(r);
				}
			}
		}
		return e;
	}
	
	public static Estatement set_GoSave_fail_usd_trans_Ref_for_amount_single(Estatement e, ArrayList<GoSave> gosave_trans_list) {
		ArrayList<String> gosave_trans_ref_list_for_fail_amount_single = GoSave2_Utils.check_GoSave_fail_trans_Ref_for_amount_single(gosave_trans_list);
		
		if(gosave_trans_ref_list_for_fail_amount_single.size()>0) {
			
			ArrayList<String> r= e.getGoSave_fail_trans_Ref_for_amount_single();
			if(r==null||r.size()==0) {
				e.setGoSave_fail_trans_Ref_for_amount_single(gosave_trans_ref_list_for_fail_amount_single);
			}else if(r.size()>0){
				if(gosave_trans_ref_list_for_fail_amount_single.size()>0) {
					r.addAll(gosave_trans_ref_list_for_fail_amount_single);
					e.setGoSave_fail_trans_Ref_for_amount_single(r);
				}
			}
		}
		return e;
	}

//	public static ArrayList<String> CompareTransactionAndGosaveList(ArrayList<Transactions> transactions,ArrayList<GoSave> gosave_trans_list) {
//		ArrayList<String> result = new ArrayList<>();
//		for(int i =0;i<transactions.size();i++) {
//			if(transactions.get(i).isIs_goSave_trans()) {
//				String amout = transactions.get(i).getAmount();
//				String date = transactions.get(i).getDate();
//				String ref = transactions.get(i).getRef();
//				boolean this_trans_can_be_found =false;
//				for(int j=0;j<gosave_trans_list.size();j++) {
//					GoSave g = gosave_trans_list.get(j);
//					if(g.getGosave_trans_Date().equals(date)) {
//						String gosave_amount= g.getAmount();
//						if(Common.compare_absolute_amount(amout,gosave_amount)) {
//							this_trans_can_be_found =true;
//						}
//					}
//				}
//				if(!this_trans_can_be_found) {
//					if(!(ref.contains("NONE"))) {
//						System.out.println("根据核心账户中gosave记录是否存在于gosave模块中");
//						result.add(ref);
//					}
//				}
//			}
//		}
//		return result;
//	}
//	
//	public static ArrayList<String> CompareTransactionAndGosaveListAccordingGosave(ArrayList<Transactions> transactions,ArrayList<GoSave> gosave_trans_list) {
//		int size = transactions.size();
//		ArrayList<String> result = new ArrayList<>();
//		for(int i=0;i<gosave_trans_list.size();i++) {
//			GoSave gosave = gosave_trans_list.get(i);
//			//String Gosave_ID = gosave_trans_list.get(i).getID();
//			String amout = gosave_trans_list.get(i).getAmount();
//			String date = gosave_trans_list.get(i).getGosave_trans_Date();
//			boolean is_this_gosave_trans_can_be_found_in_coreaccount =false;
//			for(int j =0;j<transactions.size();j++) {
//				if(transactions.get(j).isIs_goSave_trans()) {
//					String core_amout = transactions.get(j).getAmount();
//					String core_date = transactions.get(j).getDate();
//					if(core_date.equals(date)) {
//						if(Common.compare_absolute_amount(amout,core_amout)) {
//							is_this_gosave_trans_can_be_found_in_coreaccount =true;
//						}
//					}
//				}
//			}
//			if(!is_this_gosave_trans_can_be_found_in_coreaccount) {
//				//System.out.println(gosave.getGosave_trans_Date());
//				if(!result.contains(gosave.getGoSave_Trans_Ref())) {
//					//result.add(gosave.getGoSave_Trans_Ref());
//					if(gosave.getRef()!=null && gosave.getRef().length() >3) {
//						result.add(gosave.getRef());
//						System.out.println("根据gosave模块的记录查询是否存在于核心账户记录中,fail item "+gosave.getRef());
//					}else {
//						result.add(gosave.getGoSave_Trans_Ref());
//						System.out.println("根据gosave模块的记录查询是否存在于核心账户记录中,fail item "+gosave.getGoSave_Trans_Ref());
//					}
//				}
//			}
//		}
//		return result;
//	}
}
