package com.utils;

public class Global {
	public static final String[] month_list= {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	
	//金额太小导致NoOfUnit为TBC
	public static final double	NO_OF_LIMIT_AMOUNT= 0.1;
	
	//OK
	public static final String	ACCOUNT_CLOSED_STRING = "取消賬戶";
	public static final String	ACCOUNT_CLOSED_STRING_SIMPLIFIED_CHINESE= "取消帐户";
	public static final String	ACCOUNT_CLOSED_STRING_SIMPLIFIED_CHINESE2= "取消账户";
	//OK
	public static final String	CORE_ACCOUNT_STRING = "核心賬戶";
	public static final String	CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE= "核心帐户";
	public static final String	CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE2= "核心账户";
	
	//OK
	public static final String	MONTHLY_STATEMENT_STRING = "月結單";
	public static final String	MONTHLY_STATEMENT_STRING_SIMPLIFIED_CHINESE= "月结单";
	//OK
	public static final String	YOUR_BANK_STATEMENT_STRING = "你的銀行月結單  Your Bank Statement";
	public static final String	YOUR_BANK_STATEMENT_STRING_SIMPLIFIED_CHINESE= "你的银行月结单  Your Bank Statement";
	//OK
	public static final String	DATE_OF_ISSUE_STRING ="列印日期 Date of Issue";
	public static final String	DATE_OF_ISSUE_STRING_SIMPLIFIED_CHINESE="列印日期 Date of Issue";
	//OK
	public static final String	ACCOUNT_NUMBER_STRING ="賬戶號碼";
	public static final String	ACCOUNT_NUMBER_STRING_SIMPLIFIED_CHINESE="帐户号码";
	public static final String	ACCOUNT_NUMBER_STRING_SIMPLIFIED_CHINESE2="账户号码";
	//OK
	public static final String	ACCOUNT_NUMBER_FULL_STRING ="賬戶號碼 Account Number:";
	public static final String	ACCOUNT_NUMBER_FULL_STRING_SIMPLIFIED_CHINESE="帐户号码 Account Number:";
	public static final String	ACCOUNT_NUMBER_FULL_STRING_SIMPLIFIED_CHINESE2="账户号码 Account Number:";
	//OK
	public static final String	BRANCH_CODE_STRING = "分行編號 Branch Code";
	public static final String	BRANCH_CODE_STRING_SIMPLIFIED_CHINESE = "分行编号 Branch Code";
	//OK
	public static final String	BANK_CODE_STRING = "銀行代碼 Bank Code";
	public static final String	BANK_CODE_STRING_SIMPLIFIED_CHINESE = "银行代码 Bank Code";
	
	public static final String	 HK_DOLLAR_STRING = "港元 HKD";
	public static final String	 HK_DOLLAR_STRING_SIMPLIFIED_CHINESE = "港元 HKD";
	public static final String	 US_DOLLAR_STRING = "美元 USD";
	public static final String	 US_DOLLAR_STRING_SIMPLIFIED_CHINESE = "美元 USD";
	//OK
	public static final String	 GOSAVE_TITLE_STRING = "GoSave";
	public static final String	 GOSAVE_TITLE_STRING_SIMPLIFIED_CHINESE = "定存";
	
	//OK
	public static final String	 GOSAVE20_TITLE_STRING = "GoSave 2.0";
	public static final String	 GOSAVE20_TITLE_STRING_SIMPLIFIED_CHINESE = "定存 2.0";
	//OK
	public static final String   GOSAVE_START_STRING ="「GoSave」定期存款 “GoSave” Time Deposit";
	public static final String   GOSAVE_START_STRING_SIMPLIFIED_CHINESE="「定存」定期存款 “GoSave” Time Deposit";
	//OK
	//public static final String   GOSAVE2_START_STRING ="「GoSave 2.0」定期存款 “GoSave 2.0” Time Deposit";
	public static final String   GOSAVE2_START_STRING ="定期存款 - 港元 GoSave 2.0 Time Deposit - HKD GoSave 2.0";
	//public static final String   GOSAVE2_START_STRING_SIMPLIFIED_CHINESE="「定存 2.0」定期存款 “GoSave 2.0” Time Deposit";
	public static final String   GOSAVE2_START_STRING_SIMPLIFIED_CHINESE="定期存款 - 港元 GoSave 2.0 Time Deposit - HKD GoSave 2.0";
	//OK
	public static final String   GOSAVE2_TRANSACTION_HISTORY_START_STRING ="GoSave 2.0 - 交易紀錄 Transaction History";
	public static final String   GOSAVE2_TRANSACTION_HISTORY_START_STRING_SIMPLIFIED_CHINESE="GoSave 2.0 - 交易纪录 Transaction History";
	
	//简体繁体待确认
	public static final String  LOANS_STRING ="GoFlexi";
	public static final String  LOANS_STRING_SIMPLIFIED_CHINESE="GoFlexi";
	//简体繁体待确认
	public static final String   LOANS_CHINESE_STRING ="Welab Bank 私人貸款";
	public static final String   LOANS_CHINESE_STRING_SIMPLIFIED_CHINESE ="Welab Bank 私人贷款";
	//简体繁体待确认
	public static final String   PERSONAL_LOAN_STRING ="私人貸款";
	public static final String   PERSONAL_LOAN_STRING_SIMPLIFIED_CHINESE="私人贷款";	
	
	//只涉及到type的使用
	public static final String	MONTHLY_REPAYMENT_STRING = "每月供款";
	public static final String	MONTHLY_REPAYMENT_STRING_SIMPLIFIED_CHINESE= "每月供款";
	
	//只涉及到type的使用
	public static final String	LOAN_DRAWDOWN_STRING = "提取貸款";
	public static final String	LOAN_DRAWDOWN_STRING_SIMPLIFIED_CHINESE= "提取贷款";
	
	//OK
	public static final String   IMPORTANT_NOTES ="重要提示";
	public static final String   IMPORTANT_NOTES_SIMPLIFIED_CHINESE="重要提示";
	
	public static final String   FX_RATE_AS_OF ="Fx Rate as of";

	//OK
	public static final String	 CURRENTLY_HELD_STRING = "目前持有 Currently Held";
	public static final String	 CURRENTLY_HELD_STRING_SIMPLIFIED_CHINESE= "目前持有 Currently Held";
	//OK
	public static final String   TRANSACTION_HISTORY_START_STRING ="交易紀錄 Transaction History";
//	public static final String   TRANSACTION_HISTORY_START_STRING ="交易紀錄 Transaction History";
	public static final String   TRANSACTION_HISTORY_START_STRING_SIMPLIFIED_CHINESE="交易纪录 Transaction History";
	public static final String   TRANSACTION_HISTORY_START_STRING_SIMPLIFIED_CHINESE2="交易记录 Transaction History";
	//OK
	public static final String   GOSAVE_TRANSACTION_HISTORY_START_STRING ="- 交易紀錄 Transaction History";
	public static final String   GOSAVE_TRANSACTION_HISTORY_START_STRING_SIMPLIFIED_CHINESE="- 交易纪录 Transaction History";

	//OK
	public static final String   TOTAL_AMOUNT_STRING ="結存總額";
	public static final String   TOTAL_AMOUNT_STRING_SIMPLIFIED_CHINESE="结存总额";


	//OK
	public static final String	BALANCE_FROM_PREVIOUS_STATEMENT_STRING = "承上結餘";
	public static final String	BALANCE_FROM_PREVIOUS_STATEMENT_STRING_SIMPLIFIED_CHINESE = "承上结余";
	
	//OK
	public static final String	CLOSING_BALANCE_STRING = "戶口結餘";
	public static final String	CLOSING_BALANCE_STRING_SIMPLIFIED_CHINESE = "帐户结余";
	
	//OK
	public static final String  GOSAVE_INTEREST_STRING ="GoSave利息";
	public static final String  GOSAVE_INTEREST_STRING_SIMPLIFIED_CHINESE="GoSave利息";
	
	//OK
	public static final String	GOSAVE_TIME_DEPOSIT_STRING ="GoSave Time Deposit";
	public static final String	GOSAVE_TIME_DEPOSIT_STRING_SIMPLIFIED_CHINESE="GoSave Time Deposit";
	
	//OK   用于gosave utils
	public static final String	GOSAVE_PRINCIPAL_TRANSFERRED_TO_CORE_ACCOUNT_STRING = "本金轉至核心賬戶";
	public static final String	GOSAVE_PRINCIPAL_TRANSFERRED_TO_CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE= "本金转至核心账户";
	//OK
	public static final String	GOSAVE_INTEREST_TRANSFER_TO_CORE_ACCOUNT_STRING = "利息轉至核心賬戶";
	public static final String	GOSAVE_INTEREST_TRANSFER_TO_CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE= "利息转至核心账户";
	//OK 用于core accout utils
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING = "GoSave 轉至核心賬戶";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING2 = "GoSave 轉至 HKD 核心賬戶";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING3 = "HKD 定存转至 HKD 核心账户";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING4 = "USD 定存转至 USD 核心账户";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING5 = "USD GoSave 轉至 USD 核心賬戶";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING6 = "定存";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING7 = "GoSave";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING8 = "利息";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE= "定存转至核心账户";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_SC= "核心账户";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_TC= "核心賬戶";
	public static final String	GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_EN= "core account";
	
	//OK
	public static final String	DEPOSIT_TO_GOSAVE_STRING = "存入GoSave";
	public static final String	DEPOSIT_TO_GOSAVE_HKD_STRING= "存入 HKD GoSave";
	public static final String	DEPOSIT_TO_GOSAVE_HKD_STRING2= "存入 HKD 定存";
	public static final String	DEPOSIT_TO_GOSAVE_USD_STRING= "存入 USD GoSave";
	public static final String	DEPOSIT_TO_GOSAVE_STRING_SIMPLIFIED_CHINESE= "存入定存";

	//OK
	public static final String	DEPOSIT_INTEREST_STRING = "存款利息";
	public static final String	DEPOSIT_INTEREST_STRING_SIMPLIFIED_CHINESE= "存款利息";
	
	//OK
	public static final String	RECEIVE_MONEY_STRING = "收款";
	public static final String	RECEIVE_MONEY_STRING_SIMPLIFIED_CHINESE= "收款";
	
	//OK
	public static final String	SEND_MONEY_STRING = "付款";
	public static final String	SEND_MONEY_STRING_SIMPLIFIED_CHINESE= "付款";
	
	//OK
	public static final String	DEBIT_CARD_SPENDING_STRING = "Debit Card 消費";
	public static final String	DEBIT_CARD_SPENDING_STRING_SIMPLIFIED_CHINESE= "借记卡消费";
	
	//OK
	public static final String	DEBIT_CARD_STRING = "Debit Card";
	public static final String	DEBIT_CARD_STRING_SIMPLIFIED_CHINESE= "借记卡";
	
	//只涉及到type的使用
	public static final String	CASH_REBATE_STRING = "現金回贈";
	public static final String	CASH_REBATE_STRING_SIMPLIFIED_CHINESE= "现金返现"; //现金回馈
	
	//只涉及到type的使用
	public static final String	ADD_MONEY_STRING = "存入款項";
	public static final String	ADD_MONEY_STRING_SIMPLIFIED_CHINESE= "存入款项";
	
	//只涉及到type的使用
	public static final String	JETCO_ATM_WITHDRAWAL_STRING = "銀通櫃員機提款";
	public static final String	JETCO_ATM_WITHDRAWAL_STRING_SIMPLIFIED_CHINESE= "银通柜员机提款";
	
	//只涉及到type的使用
	public static final String	JETCO_ATM_REFUND_STRING = "銀通櫃員機退款";
	public static final String	JETCO_ATM_REFUND_STRING_SIMPLIFIED_CHINESE= "银通柜员机退款";
	
	//只涉及到type的使用
	public static final String	JETCO_ATM_STRING = "銀通櫃員機";
	public static final String	JETCO_ATM_STRING_SIMPLIFIED_CHINESE= "银通柜员机";
	
	//OK
	public static final String	OTHER_STRING = "其他";
	public static final String	OTHER_STRING_SIMPLIFIED_CHINESE= "其他";
	
	//只涉及到type的使用
	public static final String	OVERSEAS_ATM_WITHDRAWAL_STRING = "Cirrus/海外櫃員機提款";
	public static final String	OVERSEAS_ATM_WITHDRAWAL_STRING_SIMPLIFIED_CHINESE= "Cirrus/海外柜员机提款";
	
	//只涉及到type的使用
	public static final String	OVERSEAS_ATM_REFUND_STRING = "Cirrus/海外櫃員機退款";
	public static final String	OVERSEAS_ATM_REFUND_STRING_SIMPLIFIED_CHINESE= "Cirrus/海外柜员机退款";
	
	//只涉及到type的使用
	public static final String	OVERSEAS_ATM_STRING = "海外櫃員機";
	public static final String	OVERSEAS_ATM_STRING_SIMPLIFIED_CHINESE= "海外柜员机";
	
	//只涉及到type的使用
	public static final String	DEBIT_ARRANGEMENT_STRING = "Debit Arrangement";
	public static final String	DEBIT_ARRANGEMENT_STRING_SIMPLIFIED_CHINESE= "Debit Arrangement";

	//OK
	public static final String	FOREIGN_CURRENCY_SAVING_ACCOUNT_STRING ="外幣賬戶 - 美元 Foreign Currency Account";
	public static final String	FOREIGN_CURRENCY_SAVING_ACCOUNT_STRING_SIMPLIFIED_CHINESE="外币帐户 - 美元 Foreign Currency Account";
	//OK
	public static final String	PENDING_TRANSACTIONS_STIRNG ="處理中的交易 Pending Transactions";
	public static final String	PENDING_TRANSACTIONS_STIRNG_SIMPLIFIED_CHINESE="处理中的交易 Pending Transactions";
	//OK
	public static final String	FOREIGN_CURRENCY_EXCHANGE_STRING ="外幣兌換";
	public static final String	FOREIGN_CURRENCY_EXCHANGE_STRING_SIMPLIFIED_CHINESE="外币兑换";
	
	//OK
	public static final String	USD_STRING ="美元 USD";
	public static final String	USD_STRING_SIMPLIFIED_CHINESE="美元 USD";
	//OK
	public static final String	INVESTMENT_ACCOUNT_STRING ="投資賬戶 Investment Account";
	public static final String	INVESTMENT_ACCOUNT_STRING_SIMPLIFIED_CHINESE="投资账户 Investment Account";
	//OK
	public static final String	FIREIGN_CURRENCY_ACCOUNT="外幣賬戶 Foreign Currency Account";
	public static final String	FIREIGN_CURRENCY_ACCOUNT_SIMPLIFIED_CHINESE="外币帐户 Foreign Currency Account";
	
	//简体繁体待确认
//	public static final String	CORE_ACCOUNT_USD="核心賬戶 Core Account (1020905190)  - 美元 USD";
//	public static final String	CORE_ACCOUNT_USD_SIMPLIFIED_CHINESE="核心賬戶 Core Account (1020905190)  - 美元 USD";
	public static final String	CORE_ACCOUNT_USD="核心賬戶 Core Account ({})  - 美元 USD";
	public static final String	CORE_ACCOUNT_USD_SIMPLIFIED_CHINESE="核心賬戶 Core Account ({})  - 美元 USD";
	//简体繁体待确认
	public static final String	GOSAVE2_USD="定期存款 - 美元 GoSave 2.0 Time Deposit - USD GoSave 2.0";
	public static final String	GOSAVE2_USD_SIMPLIFIED_CHINESE="定期存款 - 美元 GoSave 2.0 Time Deposit - USD GoSave 2.0";
	
	
	
	
	
	// For Wealth PDF
	
	//OK
	public static final String	ACCOUNT_TOTAL ="賬戶總額 Account Total";
	//public static final String	ACCOUNT_TOTAL_SIMPLIFIED_CHINESE="Account Total";
	public static final String	ACCOUNT_TOTAL_SIMPLIFIED_CHINESE="账户总额 Account Total";
	//OK
	public static final String	ACCOUNT_DETAILS ="賬戶詳情 Account Details";
	public static final String	ACCOUNT_DETAILS_SIMPLIFIED_CHINESE="账户详情 Account Details";

	//OK
	public static final String	INVESTMENT_ACCOUNT_MONTHLY_STATEMENT="投資賬戶月結單";
	public static final String	INVESTMENT_ACCOUNT_MONTHLY_STATEMENT_SIMPLIFIED_CHINESE="投资账户月结单";
	//OK
	public static final String	INVESTMENT_ACCOUNT_NUMBER="投資賬戶號碼";
	public static final String	INVESTMENT_ACCOUNT_NUMBER_SIMPLIFIED_CHINESE="投资账户号码";
	//OK
	public static final String	STATEMENT_PERIOD ="結單期 Statement Period:";
	public static final String	STATEMENT_PERIOD_SIMPLIFIED_CHINESE="结单期 Statement Period:";
	//OK
	public static final String	REMARKS2 ="本結單所載之基金單位資產淨值及巿值之價格僅供參考";
	public static final String	REMARKS2_SIMPLIFIED_CHINESE="本结单所载之基金单位资产净值及巿值之价格仅供参考";
	//OK
	public static final String	EMAIL_ADDRESS ="wecare@welab.bank";
	public static final String	EMAIL_ADDRESS_SIMPLIFIED_CHINESE="wecare@welab.bank";
	
	//OK
	public static final String	REMARKS ="備註 Remarks";
	public static final String	REMARKS_SIMPLIFIED_CHINESE="备注 Remarks";
	
//	public static final String	GOAL_BASED_PORTFOLIO ="組合性基金交易 GoalBasedPortfolio";
//	public static final String	GOAL_BASED_PORTFOLIO_SIMPLIFIED_CHINESE="組合性基金交易 GoalBasedPortfolio";
	
//	public static final String	INDIVIDUAL_FUND_POSITIONS ="個別基金總持倉 Total Individual Fund Positions";
//	public static final String	INDIVIDUAL_FUND_POSITIONS_SIMPLIFIED_CHINESE="個別基金總持倉 Total Individual Fund Positions";
	
	//OK
	public static final String	INDIVIDUAL_FUND_POSITIONS2 ="個別基金總持倉";
	public static final String	INDIVIDUAL_FUND_POSITIONS2_SIMPLIFIED_CHINESE="基金持仓明细";
	
//	public static final String	TBC ="待確認之交易明細信息會顯示為 \"TBC\" (即待確認)";
//	public static final String	TBC_SIMPLIFIED_CHINESE="待確認之交易明細信息會顯示為 \"TBC\" (即待確認)";
	
	//简体繁体待确认
	public static final String	FUND_PLATFORM_FEE_SUMMARY ="基金平台費 Fund Platform Fee Summary";
	public static final String	FUND_PLATFORM_FEE_SUMMARY_SIMPLIFIED_CHINESE="基金平台费 Fund Platform Fee Summary";
	//OK
	public static final String	TRANSACTION_HISTORY_AND_POSITIONS ="Detailed Transaction History and Positions";
	public static final String	TRANSACTION_HISTORY_AND_POSITIONS_SIMPLIFIED_CHINESE="Detailed Transaction History and Positions";
	//OK
	public static final String	INVESTMENT_TRANSACTION ="Investment Transactions";
	public static final String	INVESTMENT_TRANSACTION_SIMPLIFIED_CHINESE="Investment Transactions";
	//简体繁体待确认
	public static final String	NO_OF_UNIT ="交易單位No. of Unit:";
	public static final String	NO_OF_UNIT_SIMPLIFIED_CHINESE="交易单位No. of Unit:";
	//简体繁体待确认
	public static final String	NAV_PER_UNIT ="交易單位資產淨值NAV per Unit:";
	public static final String	NAV_PER_UNIT_SIMPLIFIED_CHINESE="交易单位资产净值NAV per Unit:";
	//简体繁体待确认
	public static final String	SUBSCRIPTION_REGULAR ="基金認購";
	public static final String	SUBSCRIPTION_REGULAR_SIMPLIFIED_CHINESE="基金认购";
	//简体繁体待确认
	public static final String	REDEMPTION_ONE_OFF="基金贖回";
	public static final String	REDEMPTION_ONE_OFF_SIMPLIFIED_CHINESE="基金赎回";
	//OK
	public static final String	FEATURED_FUNDS="自選基金";
	public static final String	FEATURED_FUNDS_SIMPLIFIED_CHINESE="自选基金";//自选基金 Featured Funds
	//OK
	public static final String	FEATURED_FUNDS_SERVICES="詳細交易紀錄及持倉 - 自選基金";
	public static final String	FEATURED_FUNDS_SERVICES_SIMPLIFIED_CHINESE="交易记录与持仓详情 - 自选基金";
	//OK
	public static final String	CASH_DIVIDEND_SUMMARY="現金派息分派紀錄 Cash Dividend Summary";
	public static final String	CASH_DIVIDEND_SUMMARY_SIMPLIFIED_CHINESE="现金派息分派记录 Cash Dividend Summary";
}
