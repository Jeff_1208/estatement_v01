package com.utils;


//eStatement_001	eStatement with no transaction nor GoSave
//eStatement_002	eStatement with FPS transactons
//eStatement_003	eStatement with Deposit interest
//eStatement_004	eStatement with WeLab Bank internal transactons
//eStatement_005	eStatement with non-WeLab Bank transactons
//eStatement_006	eStatement with transactons via WeLab Bank Mastercard
//eStatement_007	eStatement with transactons via ATM (local currency)
//eStatement_008	eStatement with transactons via ATM (foreign currency)
//eStatement_009	eStatement with Cash Rebate
//eStatement_010	eStatement with eDDA transactions
//eStatement_011	eStatement with invaild transaction
//eStatement_012	eStatement with all types of transactions
//eStatement_013	No missing records in PDF format eStatement 
//eStatement_014	eStatement with GoSave joined pot but not started
//eStatement_015	eStatement with GoSave started within reporting month
//eStatement_016	eStatement with active GoSave (without early withdrawal)
//eStatement_017	eStatement with active GoSave (with early withdrawal)
//eStatement_018	eStatement with GoSave matured (without early withdrawal)
//eStatement_019	eStatement with GoSave matured (with early withdrawal)
//eStatement_020	eStatement with no active loan
public class TestCase {

} 
