8000049583
你的银行月结单  Your Bank Statement (1 Mar 2024 - 31 Mar 2024)
列印日期 Date of Issue: 12 Apr 2024
帐户号码 Account Number: 1020649607
银行代码 Bank Code / 分行编号 Branch Code: 390 / 750
CHEN, PEIWEN
你的帐户总览 截至2024年03月31日 (港元等值)
Your Financial Status As at 31 Mar 2024 (HKD Equivalent)
核心帐户 Core Account 47,640,715.01
外币帐户 Foreign Currency Account
美元 USD 263,708.15
投资帐户 Investment Account 0.00
结存总额 Net Position 47,904,423.16
交易纪录 Transaction History
核心帐户 Core Account (1020649607)
交易日期 种类 进支详情 金额 (港元)
Transaction Date Type Transaction Description Amount (HKD)
01 Mar 2024 承上结余 Balance From Previous Statement 49,818,022.00
存款利息 Deposit interest
1 Mar 2024 122.59
Deposit interest Ref: AAACT24060NDBZ2N8L
外币兑换
USD/ HKD @ 7.84562742, debit date: 05 Mar 2024
5 Mar 2024 Foreign currency exchange -100.00
Ref: FX0000194863
外币兑换
USD/ HKD @ 7.84562742, debit date: 05 Mar 2024
5 Mar 2024 Foreign currency exchange -100,000.00
Ref: FX0000194864
基金申购费 Debit date: 06 Mar 2024
5 Mar 2024 -1,500.00
Fund subscription fee Ref: SE0000075629
外币兑换
USD/ HKD @ 7.84562742, debit date: 05 Mar 2024
5 Mar 2024 Foreign currency exchange -5,000.00
Ref: FX0000194866
基金申购 Debit date: 13 Mar 2024
11 Mar 2024 -1,005.00
Fund subscription Ref: SE0000075663
基金申购 Debit date: 13 Mar 2024
11 Mar 2024 -1,001.00
Fund subscription Ref: SE0000075664
外币兑换
USD/ HKD @ 7.84128529, debit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange -538.59
Ref: FX0000194892
外币兑换
USD/ HKD @ 7.84128529, debit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange -200.00
Ref: FX0000194893
外币兑换
USD/ HKD @ 7.79757010, credit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange 14.97
Ref: FX0000194894
外币兑换
USD/ HKD @ 7.84128529, debit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange -300.00
Ref: FX0000194895
基金申购 Debit date: 13 Mar 2024
12 Mar 2024 -230.00
Fund subscription Ref: SE0000075668
外币兑换
USD/ HKD @ 7.84128529, debit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange -610.00
Ref: FX0000194896
外币兑换
USD/ HKD @ 7.84128529, debit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange -300,000.00
Ref: FX0000194897
Page 1 of 7
你的银行月结单  Your Bank Statement (1 Mar 2024 - 31 Mar 2024)
列印日期 Date of Issue: 12 Apr 2024
帐户号码 Account Number: 1020649607
银行代码 Bank Code / 分行编号 Branch Code: 390 / 750
交易日期 种类 进支详情 金额 (港元)
Transaction Date Type Transaction Description Amount (HKD)
基金申购费 Debit date: 13 Mar 2024
12 Mar 2024 -4,500.00
Fund subscription fee Ref: SE0000075671
外币兑换
USD/ HKD @ 7.84128529, debit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange -300,000.00
Ref: FX0000194898
基金申购费 Debit date: 13 Mar 2024
12 Mar 2024 -4,500.00
Fund subscription fee Ref: SE0000075672
基金申购费 Debit date: 13 Mar 2024
12 Mar 2024 -510.00
Fund subscription fee Ref: SE0000075673
外币兑换
USD/ HKD @ 7.84128529, debit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange -34,000.00
Ref: FX0000194899
外币兑换
USD/ HKD @ 7.83968081, debit date: 22 Mar 2024
22 Mar 2024 Foreign currency exchange -100.00
Ref: FX0000197614
外币兑换
USD/ HKD @ 7.83989140, debit date: 25 Mar 2024
22 Mar 2024 Foreign currency exchange -2,900.00
Ref: FX0000197615
基金申購費 Debit date: 26 Mar 2024
22 Mar 2024 -43.50
Fund subscription fee Ref: SE0000075715
外币兑换
USD/ HKD @ 7.84249868, debit date: 25 Mar 2024
25 Mar 2024 Foreign currency exchange -61,121.47
Ref: FX0000198969
外币兑换
USD/ HKD @ 7.79872855, credit date: 25 Mar 2024
25 Mar 2024 Foreign currency exchange 269.99
Ref: FX0000198970
基金申购 Debit date: 27 Mar 2024
25 Mar 2024 -501.00
Fund subscription Ref: SE0000075717
基金申购 Debit date: 27 Mar 2024
25 Mar 2024 -200.00
Fund subscription Ref: SE0000075718
基金申购费 Debit date: 27 Mar 2024
25 Mar 2024 -2,250.00
Fund subscription fee Ref: SE0000075719
外幣兌換
USD/ HKD @ 7.84249868, debit date: 26 Mar 2024
25 Mar 2024 Foreign currency exchange -150,000.00
Ref: FX0000198971
外币兑换
USD/ HKD @ 7.84249868, debit date: 27 Mar 2024
27 Mar 2024 Foreign currency exchange -101.00
Ref: FX0000198974
基金申购 Debit date: 28 Mar 2024
27 Mar 2024 -501.00
Fund subscription Ref: SE0000075724
外币兑换
USD/ HKD @ 7.84249868, debit date: 27 Mar 2024
27 Mar 2024 Foreign currency exchange -1,000,000.00
Ref: FX0000200437
基金申购费 Debit date: 28 Mar 2024
27 Mar 2024 -15,000.00
Fund subscription fee Ref: SE0000075727
基金申购费 Debit date: 28 Mar 2024
27 Mar 2024 -562.50
Fund subscription fee Ref: SE0000075728
外币兑换
USD/ HKD @ 7.84249868, debit date: 27 Mar 2024
27 Mar 2024 Foreign currency exchange -37,500.00
Ref: FX0000200438
外币兑换
USD/ HKD @ 7.84249868, debit date: 28 Mar 2024
28 Mar 2024 Foreign currency exchange -150,000.00
Ref: FX0000200442
Page 2 of 7
你的银行月结单  Your Bank Statement (1 Mar 2024 - 31 Mar 2024)
列印日期 Date of Issue: 12 Apr 2024
帐户号码 Account Number: 1020649607
银行代码 Bank Code / 分行编号 Branch Code: 390 / 750
交易日期 种类 进支详情 金额 (港元)
Transaction Date Type Transaction Description Amount (HKD)
外币兑换
USD/ HKD @ 7.84249868, debit date: 28 Mar 2024
28 Mar 2024 Foreign currency exchange -500.00
Ref: FX0000200443
外币兑换
USD/ HKD @ 7.79872855, credit date: 28 Mar 2024
28 Mar 2024 Foreign currency exchange 60.52
Ref: FX0000200444
外币兑换
USD/ HKD @ 7.84249868, debit date: 28 Mar 2024
28 Mar 2024 Foreign currency exchange -2,500.00
Ref: FX0000200445
31 Mar 2024 帐户结余 Closing Balance 47,640,715.01
处理中的交易 Pending Transactions
以下交易之结算并非于此月结单当月进行，结算日期请见进支详情。当交易已结算，交易将于结算当月之月结单中的交易纪录中显示。
The transaction(s) below is/are not settled in the current statement month, please refer to the transaction details for the settlement date.  After the transaction is settled, it will be
shown in the transaction record of the monthly statement of the settlement month.
交易日期 种类 进支详情 金額 (港元)
Transaction Date Type Transaction Description Amount (HKD)
基金申购 Debit date: 02 Apr 2024
28 Mar 2024 -503.00
Fund subscription Ref: SE0000075736
基金申购费 Debit date: 02 Apr 2024
28 Mar 2024 -37.50
Fund subscription fee Ref: SE0000075735
基金申购费 Debit date: 02 Apr 2024
28 Mar 2024 -2,250.00
Fund subscription fee Ref: SE0000075734
外币帐户 - 美元 Foreign Currency Account - USD (1020649615)
交易日期 种类 进支详情 金额 (美元)
Transaction Date Type Transaction Description Amount (USD)
01 Mar 2024 承上结余 Balance From Previous Statement 6,289.16
基金申购 Debit date: 01 Mar 2024
29 Feb 2024 -12.74
Fund subscription Ref: SE0000075625
外币兑换 USD/ HKD @ 7.84562742, credit date: 05 Mar 2024
5 Mar 2024 Foreign currency exchange 12.75
Ref: FX0000194863
基金申购 Debit date: 06 Mar 2024
5 Mar 2024 -12.75
Fund subscription Ref: SE0000075628
外币兑换 USD/ HKD @ 7.84562742, credit date: 05 Mar 2024
5 Mar 2024 Foreign currency exchange 12,745.95
Ref: FX0000194864
基金申购 Debit date: 06 Mar 2024
5 Mar 2024 -12,745.95
Fund subscription Ref: SE0000075629
外币兑换 USD/ HKD @ 7.84562742, credit date: 05 Mar 2024
5 Mar 2024 Foreign currency exchange 637.30
Ref: FX0000194866
基金申购 Debit date: 13 Mar 2024
11 Mar 2024 -106.00
Fund subscription Ref: SE0000075665
外币兑换 USD/ HKD @ 7.84128529, credit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange 68.69
Ref: FX0000194892
Page 3 of 7
你的银行月结单  Your Bank Statement (1 Mar 2024 - 31 Mar 2024)
列印日期 Date of Issue: 12 Apr 2024
帐户号码 Account Number: 1020649607
银行代码 Bank Code / 分行编号 Branch Code: 390 / 750
交易日期 种类 进支详情 金额 (美元)
Transaction Date Type Transaction Description Amount (USD)
外币兑换 USD/ HKD @ 7.84128529, credit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange 25.51
Ref: FX0000194893
外币兑换 USD/ HKD @ 7.79757010, debit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange -1.92
Ref: FX0000194894
基金申购 Debit date: 13 Mar 2024
12 Mar 2024 -38.26
Fund subscription Ref: SE0000075667
外币兑换 USD/ HKD @ 7.84128529, credit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange 38.26
Ref: FX0000194895
基金申购 Debit date: 13 Mar 2024
12 Mar 2024 -59.00
Fund subscription Ref: SE0000075669
外币兑换 USD/ HKD @ 7.84128529, credit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange 77.79
Ref: FX0000194896
基金申购 Debit date: 13 Mar 2024
12 Mar 2024 -77.79
Fund subscription Ref: SE0000075670
基金申购 Debit date: 13 Mar 2024
12 Mar 2024 -38,259.03
Fund subscription Ref: SE0000075671
外币兑换 USD/ HKD @ 7.84128529, credit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange 38,259.03
Ref: FX0000194897
基金申购 Debit date: 13 Mar 2024
12 Mar 2024 -38,259.03
Fund subscription Ref: SE0000075672
外币兑换 USD/ HKD @ 7.84128529, credit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange 38,259.03
Ref: FX0000194898
基金申购 Debit date: 13 Mar 2024
12 Mar 2024 -4,336.02
Fund subscription Ref: SE0000075673
外币兑换 USD/ HKD @ 7.84128529, credit date: 12 Mar 2024
12 Mar 2024 Foreign currency exchange 4,336.02
Ref: FX0000194899
外币兑换 USD/ HKD @ 7.83968081, credit date: 22 Mar 2024
22 Mar 2024 Foreign currency exchange 12.76
Ref: FX0000197614
基金申購 Debit date: 26 Mar 2024
22 Mar 2024 -12.76
Fund subscription Ref: SE0000075714
外币兑换 USD/ HKD @ 7.83989140, credit date: 25 Mar 2024
22 Mar 2024 Foreign currency exchange 369.90
Ref: FX0000197615
基金申購 Debit date: 26 Mar 2024
22 Mar 2024 -369.90
Fund subscription Ref: SE0000075715
外币兑换 USD/ HKD @ 7.84249868, credit date: 25 Mar 2024
25 Mar 2024 Foreign currency exchange 7,793.62
Ref: FX0000198969
外币兑换 USD/ HKD @ 7.79872855, debit date: 25 Mar 2024
25 Mar 2024 Foreign currency exchange -34.62
Ref: FX0000198970
外幣兌換 USD/ HKD @ 7.84249868, credit date: 26 Mar 2024
25 Mar 2024 Foreign currency exchange 19,126.56
Ref: FX0000198971
基金申购 Debit date: 27 Mar 2024
25 Mar 2024 -19,126.56
Fund subscription Ref: SE0000075719
Page 4 of 7
你的银行月结单  Your Bank Statement (1 Mar 2024 - 31 Mar 2024)
列印日期 Date of Issue: 12 Apr 2024
帐户号码 Account Number: 1020649607
银行代码 Bank Code / 分行编号 Branch Code: 390 / 750
交易日期 种类 进支详情 金额 (美元)
Transaction Date Type Transaction Description Amount (USD)
外币兑换 USD/ HKD @ 7.84249868, credit date: 27 Mar 2024
27 Mar 2024 Foreign currency exchange 12.88
Ref: FX0000198974
基金申购 Debit date: 28 Mar 2024
27 Mar 2024 -12.88
Fund subscription Ref: SE0000075722
基金申购 Debit date: 28 Mar 2024
27 Mar 2024 -205.00
Fund subscription Ref: SE0000075725
基金申购 Debit date: 28 Mar 2024
27 Mar 2024 -201.00
Fund subscription Ref: SE0000075726
基金申购 Debit date: 28 Mar 2024
27 Mar 2024 -127,510.38
Fund subscription Ref: SE0000075727
外币兑换 USD/ HKD @ 7.84249868, credit date: 27 Mar 2024
27 Mar 2024 Foreign currency exchange 127,510.38
Ref: FX0000200437
基金申购 Debit date: 28 Mar 2024
27 Mar 2024 -4,781.64
Fund subscription Ref: SE0000075728
外币兑换 USD/ HKD @ 7.84249868, credit date: 27 Mar 2024
27 Mar 2024 Foreign currency exchange 4,781.64
Ref: FX0000200438
外币兑换 USD/ HKD @ 7.84249868, credit date: 28 Mar 2024
28 Mar 2024 Foreign currency exchange 19,126.56
Ref: FX0000200442
外币兑换 USD/ HKD @ 7.84249868, credit date: 28 Mar 2024
28 Mar 2024 Foreign currency exchange 63.76
Ref: FX0000200443
外币兑换 USD/ HKD @ 7.79872855, debit date: 28 Mar 2024
28 Mar 2024 Foreign currency exchange -7.76
Ref: FX0000200444
外币兑换 USD/ HKD @ 7.84249868, credit date: 28 Mar 2024
28 Mar 2024 Foreign currency exchange 318.78
Ref: FX0000200445
31 Mar 2024 帐户结余 Closing Balance 33,695.34
处理中的交易 Pending Transactions
以下交易之结算并非于此月结单当月进行，结算日期请见进支详情。当交易已结算，交易将于结算当月之月结单中的交易纪录中显示。
The transaction(s) below is/are not settled in the current statement month, please refer to the transaction details for the settlement date.  After the transaction is settled, it will be
shown in the transaction record of the monthly statement of the settlement month.
交易日期 种类 进支详情 金額 (美元)
Transaction Date Type Transaction Description Amount (USD)
基金申购 Debit date: 02 Apr 2024
28 Mar 2024 -318.78
Fund subscription Ref: SE0000075735
基金申购 Debit date: 02 Apr 2024
28 Mar 2024 -19,126.56
Fund subscription Ref: SE0000075734
「定存」定期存款 “GoSave” Time Deposit
目前持有 Currently Held
Page 5 of 7
你的银行月结单  Your Bank Statement (1 Mar 2024 - 31 Mar 2024)
列印日期 Date of Issue: 12 Apr 2024
帐户号码 Account Number: 1020649607
银行代码 Bank Code / 分行编号 Branch Code: 390 / 750
由 至 存款编号 利息 (年利率) 起存金额 (港元) 结余 (港元)
From To Deposit Number Interest Rate (p.a.) Opening Deposit Amount (HKD) Balance (HKD)
GoSave  - 交易记录 Transaction History
交易日期 种类 交易详情 金额 (港元)
Transaction Date Type Transaction Description Amount (HKD)
「定存 2.0」定期存款 “GoSave 2.0” Time Deposit
目前持有 Currently Held
由 至 参考编号 利息 (年利率) 存款期 结余 (港元)
From To Reference Number Interest Rate (p.a.) Tenor Balance (HKD)
GoSave 2.0 - 交易纪录 Transaction History
交易日期 种类 进支详情 金額 (港元)
Transaction Date Type Transaction Description Amount (HKD)
Page 6 of 7
你的银行月结单  Your Bank Statement (1 Mar 2024 - 31 Mar 2024)
列印日期 Date of Issue: 12 Apr 2024
帐户号码 Account Number: 1020649607
银行代码 Bank Code / 分行编号 Branch Code: 390 / 750
重要提示 IMPORTANT NOTES
? 客户 （「你」）可在登入汇立银行APP 后按「我的帐户」>「协助」>「存款利率」，以浏览汇立银行有限公司 （「我们」） 最新的
存款利率。你亦可在「我的帐户」>「关于我们」>「一般服务收费」了解我们的收费。
Customers ("You" or "Your") can browse through WeLab Bank Limited ("We" or "Our") latest interest rate at "My account" > "Support" >
"Deposit interest rate" on our WeLab Bank app. You can also browse our fees and charges at "My account" > "About us" > "General
service charges" on our WeLab Bank app.
? 定期存款部份所显示的金额并不包含累计利息。
The balance shown in the GoSave Time Deposit section excludes accrued interest.
? 你可查阅及下载长达7年的电子月结单，亦可打印或下载你的电子月结单以记录所需信息。如你下载电子月结单到任何电脑或电子设
备，请你确保该电脑及电子设备保安的安全，并防止其他人士查阅任何机密资料，包括电子月结单。
You may view and download eStatement(s) of up to 7 years in our app, and you can print or download a copy of your eStatement(s) for
record purposes. If you download an eStatement to any computer or electronic device, please ensure that the device is secured to
prevent others from accessing any confidential information, including the eStatement.
? 我们提醒你请注意冒充我们职员的欺诈来电、语音讯息、电邮、短讯或其他伪冒方式，请不要向可疑第三者提供任何敏感个人资料。
如你怀疑曾向可疑人士披露个人资料，请致电我们的24小时客户服务热线 （852） 3898 6988 核实来电者身份及立即向警方报案。
We remind you to be alert to fraudulent phone calls, voice messages, emails, SMS or communications in other formats imitating our
staff. Please be reminded to protect your personal information and not provide any sensitive information to suspicious callers. If you
have disclosed your personal details, please contact our 24-hour Customer Service hotline at (852) 3898 6988 to verify the caller’s
identity, and report to the Police immediately.
? 如有任何因使用借记卡而引致的错漏，必须要于月结单列印日后60日内通知我们。如有其他错漏（如转帐或定存定期存款的交易），
亦须于月结单列印日后90日内通知我们。请参阅我们的「戶口条款」了解详情。
All errors or discrepancies on the use of the Debit Card should be reported to the Bank within 60 calendar days from the eStatement
date. For any other errors or discrepancies (such as transfer or GoSave time deposit transaction), you must report to the Bank within 90
calendar days from the eStatement date. Please refer to our Account Terms for details.
? 如你对帐单有任何问题，你可在登入汇立银行APP，进入「我的帐户」后按「协助」，致电客户服务热线 （852） 3898 6988 或发送
电邮至wecare@welab.bank 联络我们。
If you have any queries concerning your statement, you can contact us by login the WeLab Bank app and tap "My account" then tap
"Support". You can also call our Customer Service hotline at (852) 3898 6988 or email us at wecare@welab.bank.
? 结存总额为核心帐户之结余及 (如有) 定存结余、外币帐户之港元等值结余及投资帐户之港元等值结余之总和，仅供参考。
Net Position is the sum of the Core Account balance, and (if any) GoSave balance, the Hong Kong Dollar equivalent value of the Foreign
Currency Account balance and the Hong Kong Dollar equivalent value of the Investment Account balance, which is for reference only.
? 如你持有外币帐户及/或投资帐户，本结单所载之港元等值(包括但不限于外币帐户及投资帐户结存之港元等值) 是以我们于本结单期末
之参考外币汇率计算并仅供参考。
If you have a Foreign Currency Account and/or an Investment Account, the Hong Kong Dollar equivalent values shown in this statement
(including but not limited to the balances of the Foreign Currency Account and the Investment Account) are calculated based on our
reference exchange rate at the end of the statement period and are for reference only.
? 如你持有投资帐户，本结单只显示投资帐户于本结单期末的结余金额，该金额以我们于本结单期末之的相关基金参考单位价格计算，
你于本结单期内的基金持仓、基金交易及/或你持有的基金之公司行动之详情将于你的投资结单内显示，你须将该投资结单与此结单一
并参阅。
If you have an Investment Account, this statement will only show the closing balance of the Investment Account as at the end of the
statement period, which is calculated based on our reference unit price of relevant Funds at the end of the statement period.  Details of
your holding and/or transactions of Funds and/or the corporation actions of your Fund holdings within the statement period will be
shown in the relevant Investment Statement, which should be read in conjunction with this statement.
# # 上述交易已在月结单当月前进行，惟未曾结算， 该笔金额现已结算。
The above transaction took place prior to the current statement month and was previously unsettled. The transaction has now been
settled.
Page 7 of 7

Executing python script file now.
