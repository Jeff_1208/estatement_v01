package com.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.entity.Estatement;
import com.entity.GoSave;
import com.entity.Transactions;

public class Common {
	public static final String[] month_list= {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	public static final String fail_refs_fileName= "/Result_fail_Refs.xlsx";

	public static String get_month_number(String M) {
		for(int i=0;i<month_list.length;i++) {
			if(month_list[i].equals(M)) {
				int index = i+1;
				if(index>=10) {
					return index+"";
				}else {
					return "0"+index;
				}
			}
		}
		return "NONE";
	}
	
	public static int getMonthIndex(String month) {
		for(int i=0;i<month_list.length;i++) {
			if(month_list[i].equals(month)) {
				int index = i+1;
				return index;
			}
		}
		return 0;
	}
	
	public static boolean isDividend(String line) {
		if(line.contains("每月派息")||line.contains("每季派息)")||line.contains("派息)")||line.contains("(每月")) {
			return true;
		}
		return false;
	}
	
	public static boolean checkIsDollarFund(String line) {
		if(line.contains("(美元)")) return true;
		if(line.contains("(美元對沖)")) return true;
		if(line.contains("(美元对冲)")) return true;
		if(line.contains("(美元")) return true;
		if(line.contains("USD")) return true;
		return false;
	}

	public static int get_month_index(String M) {
		for(int i=0;i<month_list.length;i++) {
			if(month_list[i].equals(M)) {
				int index = i+1;
				return index;
			}
		}
		return 0;
	}

	public static String get_today_date_string() {
		//2021-07-27
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(System.currentTimeMillis());
		//System.out.println(formatter.format(date));
		return formatter.format(date).trim();
	}

	public static String get_settlement_date_string() {
		//2021-07-27
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(System.currentTimeMillis());
		//System.out.println(formatter.format(date));
		String s = formatter.format(date);
		s= s.substring(0,s.length()-2);
		s = s+"01";
		return s;
	}


	public static String get_settlement_date_string(int Y,String M) {
		//2021-07-01
		String m = get_month_number(M);
		int days= Common.get_this_month_days(Y, M);
		return Y+"-"+m.trim()+"-"+days;
	}

	public static String get_date_string(String Y,String M, String D) {
		String m = get_month_number(M);
		return Y.trim()+"-"+m.trim()+"-"+D.trim();
	}


	public static int  get_days_diffs(String date_star, String date_end,Estatement estatement,Transactions tran) {
		/***
		int i=0;
		DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
		try {
			//Date star = dft.parse("2021-02-03");//开始时间
			//Date endDay=dft.parse("2021-07-02");//结束时间
			Date star = dft.parse(date_star);//开始时间
			Date endDay=dft.parse(date_end);//结束时间
			Date nextDay=star;
			while(nextDay.before(endDay)){//当明天不在结束时间之前是终止循环
				Calendar cld = Calendar.getInstance();
				cld.setTime(star);
				cld.add(Calendar.DATE, 1);
				star = cld.getTime();
				//获得下一天日期字符串
				nextDay = star; 
				i++;
			}
			//System.out.println("相差天数为："+i);

		} catch (Exception e) {
			//System.out.println(estatement.getCustomer_id()+": 日期解析异常" +tran.getRef());
			e.printStackTrace();
		}
		return i;
		 ***/
		int settlement_date_index=get_index_of_date_from_ref(tran.getRef().trim());
		int spending_date_year = str_to_int(tran.getYear());
		int settlement_date_year = estatement.getYEAR();
		int year_diff = settlement_date_year-spending_date_year;
		int spending_date = getDaysInYear(str_to_int(tran.getYear()), get_month_index(tran.getMonth().trim()), str_to_int(tran.getDay_string()));
		int v = settlement_date_index-spending_date+year_diff*365;
		return v;
	}

	public static int get_index_of_date_from_ref(String ref) {
		String date_str = ref.substring(4,7);
		int a = Integer.parseInt(date_str);
		return a;
	}

	public static int str_to_int(String s) {

		return Integer.parseInt(s);
	}
	public static int getDaysInYear(int year, int month, int day) {
		/*平年二月28天*/
		int DAYS_28 = 28;
		/*闰年二月29天*/
		int DAYS_29 = 29;
		/*除了31天的月份其他均为30天*/
		int DAYS_30 = 30;
		/*1、3、5、7、8、10、12月份31天*/
		int DAYS_31 = 31;

		int totalDays = 0;

		switch (month) {
		// 12 月份加的是11月份的天数，依次类推
		case 12:
			totalDays += DAYS_30;
		case 11:
			totalDays += DAYS_31;
		case 10:
			totalDays += DAYS_30;
		case 9:
			totalDays += DAYS_31;
		case 8:
			totalDays += DAYS_31;
		case 7:
			totalDays += DAYS_30;
		case 6:
			totalDays += DAYS_31;
		case 5:
			totalDays += DAYS_30;
		case 4:
			totalDays += DAYS_31;
		case 3:
			// 判断是否是闰年
			if (((year / 4 == 0) && (year / 100 != 0)) || (year / 400 == 0)) {
				totalDays += DAYS_29;
			} else {
				totalDays += DAYS_28;
			}
		case 2:
			totalDays += DAYS_31;
		case 1: // 如果是1月份就加上输入的天数
			totalDays += day;
		}
		return totalDays;

	}

	public static int get_year_days(int year) {
		if(year%400==0) {
			return 366;
		}else {
			if(year%4==0&&year%100==0) {
				return 365;
			}else if(year%4==0&&year%100!=0) {
				return 366;
			}else {
				return 365;
			}
		}
	}
	public static int get_this_month_days(int year,String Month) {
		String[] month_list_1= {"Jan","Mar","May","Jul","Aug","Oct","Dec"};
		String[] month_list_2= {"Apr","Jun","Sep","Nov"};
		if(is_one_of_shuzu_item_in_line(Month, month_list_1)) {
			return 31;
		}else if(is_one_of_shuzu_item_in_line(Month, month_list_2)) {
			return 30;
		}else if(Month.contains("Feb")) {
			if(year%400==0) {
				return 29;
			}else {
				if(year%4==0&&year%100==0) {
					return 28;
				}else if(year%4==0&&year%100!=0) {
					return 29;
				}else {
					return 28;
				}
			}
		}else {
			return -1;
		}
	}

	public static float get_float_round(float float_value) {
		return (float)(Math.round(float_value*100))/100;
	}

	// 判断一个字符串是否含有数字
	public static boolean is_have_digit(String s) {
		boolean flag = false;
		//		Pattern p1 = Pattern.compile("[0-9]*");  //适用于纯数字判断
		Pattern p = Pattern.compile(".*\\d+.*");
		Matcher m = p.matcher(s);
		if(m.matches()) {
			flag=true;
		}
		return flag;
	}


	public static Transactions get_Date_String_from_line(String line,Estatement e,Transactions tran) {
		//		String y1 = e.getYEAR()+"";
		//		String y2 = e.getLast_months_YEAR()+"";
		//		String value="";
		//		if(line.contains(y1)) {
		//			value=line.substring(0, line.indexOf(y1)+y1.length());
		//		}else {
		//			value=line.substring(0, line.indexOf(y2)+y2.length());
		//		}
		//		value =value.trim();
		//		tran.setDate(value);

		tran.setDate(tran.getDay_string()+" "+tran.getMonth()+" "+tran.getYear());
		return tran;
	}

	public static GoSave get_gosave_Date_from_line(String line,Estatement e,GoSave goSave) {
		String y1 = e.getYEAR()+"";
		String y2 = e.getLast_months_YEAR()+"";
		String value="";
		if(line.contains(y1)) {
			value=line.substring(0, line.indexOf(y1)+y1.length());
		}else {
			value=line.substring(0, line.indexOf(y2)+y2.length());
		}

		value =value.trim();
		String year= get_value_string_from_line(value, -1);
		String Month= get_value_string_from_line(value, -2);
		goSave.setGosave_trans_Date(value);
		//		goSave.setDate(value);
		goSave.setYear(year);
		goSave.setMonth(Month);

		String ref = value+" GoSave ID: "+goSave.getID();
		goSave.setGoSave_Trans_Ref(ref);

		return goSave;
	}

	public static double get_double_round(double float_value) {
		return (double)(Math.round(float_value*100))/100;
	}

	public static double compare_interest(Estatement e) {
		double i = e.getInterest() -e.getInterest_capture();
		i = get_double_round(Math.abs(i));
		return i;
	}

	public static boolean compare_absolute_amount(String amount1,String amount2) {
		boolean r = false;
		String s1 = remove_for_fushu(amount1);
		String s2 = remove_for_fushu(amount2);
		if(compare_amount_string_to_double(s1, s2)) {
			r=true;
		}
		return r;
	}

	public static String remove_for_fushu(String amount) {
		if(amount.contains("-")) {
			int index = amount.indexOf("-");
			if(index==0){
				return amount.substring(1);
			}else {
				return amount;
			}
		}else {
			return amount;
		}
	}

	public static ArrayList<String> Compate_list(Estatement estatement,ArrayList<GoSave> gosave_trans_list) {
		ArrayList<String> result = new ArrayList<>();
		for(int i =0;i<estatement.getTransations_history().size();i++) {
			if(estatement.getTransations_history().get(i).isIs_goSave_trans()) {
				String amout = estatement.getTransations_history().get(i).getAmount();
				String date = estatement.getTransations_history().get(i).getDate();
				String ref = estatement.getTransations_history().get(i).getRef();
				boolean this_trans_can_be_found =false;
				for(int j=0;j<gosave_trans_list.size();j++) {
					GoSave g = gosave_trans_list.get(j);
					if(g.getGosave_trans_Date().equals(date)) {
						String gosave_amount= g.getAmount();
						if(compare_absolute_amount(amout,gosave_amount)) {
							this_trans_can_be_found =true;
						}
					}
				}
				if(!this_trans_can_be_found) {
					if(!(ref.contains("NONE"))) {
						System.out.println("根据核心账户中gosave记录是否存在于gosave模块中");
						result.add(ref);
					}
				}
			}
		}
		return result;
	}


	public static ArrayList<String> Compate_list_according_gosave_tran(Estatement estatement,ArrayList<GoSave> gosave_trans_list) {
		int size = estatement.getTransations_history().size();
		/***
		//System.out.println("estatement.getTransations_history().size(): "+estatement.getTransations_history().size());
		//System.out.println("gosave_trans_list.size(): "+gosave_trans_list.size());
		for (int i = 0; i < gosave_trans_list.size(); i++) {
			GoSave g = gosave_trans_list.get(i);
			System.out.println(g.toString());
		}
		
		for (int i = 0; i <size; i++) {
			Transactions t = estatement.getTransations_history().get(i);
			System.out.println(t.toString());
			if(t.isIs_goSave_trans()) {
			}
		}
		***/
		System.out.println("estatement.getTransations_history().size(): " +size);
		
		ArrayList<String> result = new ArrayList<>();
		for(int i=0;i<gosave_trans_list.size();i++) {
			GoSave gosave = gosave_trans_list.get(i);
			//String Gosave_ID = gosave_trans_list.get(i).getID();
			String amout = gosave_trans_list.get(i).getAmount();
			String date = gosave_trans_list.get(i).getGosave_trans_Date();
			String type = gosave_trans_list.get(i).getType();
			//System.out.println("type: "+gosave.getType());
			//System.out.println("ref: "+gosave.getRef());
			//System.out.println("date: "+date);
			//System.out.println("amout: "+amout);
			//if(type.contains("定存2.0利息")) continue;
			if(type.contains("利息")) continue;
			boolean is_this_gosave_trans_can_be_found_in_coreaccount =false;
			for(int j =0;j<estatement.getTransations_history().size();j++) {
				Transactions t = estatement.getTransations_history().get(j);
				//System.out.println(t.getRef()+ " is goSave : " + t.isIs_goSave_trans());
				if(t.isIs_goSave_trans()) {
					String core_amout = estatement.getTransations_history().get(j).getAmount();
					String core_date = estatement.getTransations_history().get(j).getDate();
					//System.out.println("core_date: "+core_date);
					//System.out.println("core_amout: "+core_amout);
					if(core_date.equals(date)) {
						if(compare_absolute_amount(amout,core_amout)) {
							is_this_gosave_trans_can_be_found_in_coreaccount =true;
						}
					}
				}
			}
			if(!is_this_gosave_trans_can_be_found_in_coreaccount) {
				//System.out.println(gosave.getGosave_trans_Date());
				if(!result.contains(gosave.getGoSave_Trans_Ref())) {
					//result.add(gosave.getGoSave_Trans_Ref());
					if(gosave.getRef()!=null && gosave.getRef().length() >3) {
						result.add(gosave.getRef());
						System.out.println("根据gosave模块的记录查询是否存在于核心账户记录中,fail item "+gosave.getRef());
					}else {
						result.add(gosave.getGoSave_Trans_Ref());
						System.out.println("根据gosave模块的记录查询是否存在于核心账户记录中,输出GoSave_Trans_Ref fail item "+gosave.getGoSave_Trans_Ref());
					}
				}
			}
		}
		return result;
	}

	

	public static ArrayList<String> CompareTransactionAndGosaveList(ArrayList<Transactions> transactions,ArrayList<GoSave> gosave_trans_list) {
		ArrayList<String> result = new ArrayList<>();
		for(int i =0;i<transactions.size();i++) {
			if(transactions.get(i).isIs_goSave_trans()) {
				String amout = transactions.get(i).getAmount();
				String date = transactions.get(i).getDate();
				String ref = transactions.get(i).getRef();
				boolean this_trans_can_be_found =false;
				for(int j=0;j<gosave_trans_list.size();j++) {
					GoSave g = gosave_trans_list.get(j);
					if(g.getGosave_trans_Date().equals(date)) {
						String gosave_amount= g.getAmount();
						if(Common.compare_absolute_amount(amout,gosave_amount)) {
							this_trans_can_be_found =true;
						}
					}
				}
				if(!this_trans_can_be_found) {
					if(!(ref.contains("NONE"))) {
						System.out.println("根据核心账户中gosave记录是否存在于gosave模块中");
						result.add(ref);
					}
				}
			}
		}
		return result;
	}
	
	public static ArrayList<String> CompareTransactionAndGosaveListAccordingGosave(ArrayList<Transactions> transactions,ArrayList<GoSave> gosave_trans_list) {
		int size = transactions.size();
		ArrayList<String> result = new ArrayList<>();
		for(int i=0;i<gosave_trans_list.size();i++) {
			GoSave gosave = gosave_trans_list.get(i);
			//String Gosave_ID = gosave_trans_list.get(i).getID();
			String amout = gosave_trans_list.get(i).getAmount();
			String date = gosave_trans_list.get(i).getGosave_trans_Date();
			boolean is_this_gosave_trans_can_be_found_in_coreaccount =false;
			for(int j =0;j<transactions.size();j++) {
				if(transactions.get(j).isIs_goSave_trans()) {
					String core_amout = transactions.get(j).getAmount();
					String core_date = transactions.get(j).getDate();
					if(core_date.equals(date)) {
						if(Common.compare_absolute_amount(amout,core_amout)) {
							is_this_gosave_trans_can_be_found_in_coreaccount =true;
						}
					}
				}
			}
			if(!is_this_gosave_trans_can_be_found_in_coreaccount) {
				//System.out.println(gosave.getGosave_trans_Date());
				if(!result.contains(gosave.getGoSave_Trans_Ref())) {
					//result.add(gosave.getGoSave_Trans_Ref());
					if(gosave.getRef()!=null && gosave.getRef().length() >3) {
						result.add(gosave.getRef());
						System.out.println("根据gosave模块的记录查询是否存在于核心账户记录中,fail item "+gosave.getRef());
					}else {
						result.add(gosave.getGoSave_Trans_Ref());
						System.out.println("根据gosave模块的记录查询是否存在于核心账户记录中,fail item "+gosave.getGoSave_Trans_Ref());
					}
				}
			}
		}
		return result;
	}
	public static String get_last_month(String Month) {
		int index =0;
		String  m = "";
		if("Jan".equals(Month)) {
			m = "Dec";
		}else {
			for(int i=0;i<month_list.length;i++) {
				if(Month.equals(month_list[i])) {
					index=i;
					m=month_list[index-1];
					break;
				}
			}
		}
		return m;
	}

	public static String get_next_month(String Month) {
		int index =0;
		String  m = "";
		if("Dec".equals(Month)) {
			m = "Jan";
		}else {
			for(int i=0;i<month_list.length;i++) {
				if(Month.equals(month_list[i])) {
					index=i;
					m=month_list[index+1];
					break;
				}
			}
		}
		return m;
	}

	public static String get_time_string() {
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		Date date = new Date(System.currentTimeMillis());
		//System.out.println(formatter.format(date));
		return formatter.format(date);
	}

	public static String get_date_string() {
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy_MM_dd");
		Date date = new Date(System.currentTimeMillis());
		//System.out.println(formatter.format(date));
		return formatter.format(date);
	}

	public static double get_double_value(String line,int index) {
		String v = get_value_string_from_line(line, index);
		if(!Common.is_valued_number(v)) return 0.0;
		return string_to_double(v);
	}
	
	public static boolean the_line_is_have_a_double_string(String line) {
		//if(!line.contains(".")) return false;
		line =line.replace("\n", "");
		String[] ss= line.split(" ");
		for (int i = 0; i < ss.length; i++) {
			boolean flag = is_double_string(ss[i].trim());
			// boolean flag = isValidDouble(ss[i].trim());  //这种方法会导致其他错误
			if(flag) return true;
		}
		
		return false;
	}
	
	public static boolean is_double_string(String data) {
		if(data==null||data.trim().length()<1) return false;
		String re= "(-)?[0-9,]+[.]{0,1}[0-9]*[dD]{0,1}";
		Pattern NUMBER_PATTERN = Pattern.compile(re);
		return NUMBER_PATTERN.matcher(data).matches();
	}
	
    public static boolean isValidDouble(String str) {
        // 正则表达式，匹配必须包含小数点的double类型数值
        // 匹配由数字开头，后面跟着零个或一个点和至少一个数字的情况
    	str = str.replace(",", ""); //去除金额中的逗号
        String pattern = "^\\d+\\.\\d+$";
        return str.matches(pattern);
    }

	public static int get_days_value_from_line(String line) {
		String[] ss=line.split(" ");
		String value = ss[0];
		value = string_move_special(value);
		return Integer.parseInt(value);
	}
	public static String get_days_String_value_from_line(String line) {
		String[] ss=line.split(" ");
		String value = ss[0];
		value = string_move_special(value);
		return value;
	}
	public static String get_month_value_from_line(String line) {
		String[] ss=line.split(" ");
		String value = ss[1];
		return string_move_special(value);
	}
	public static String get_year_value_from_line(String line) {
		String[] ss=line.split(" ");
		String value = ss[2];
		return string_move_special(value);
	}
	public static String get_value_string_from_line(String line,int index) {
		String[] ss=line.split(" ");
		String value = ss[ss.length+index];
		value = string_move_special(value);
		return value.trim();
	}

	//	public static boolean compare_amount_string_to_float(String amount1,String amount2) {
	//		boolean re = false;
	//		float a1 = string_to_float(amount1);
	//		float a2 = string_to_float(amount2);
	//		if(a1==a2) {
	//			re =true;
	//		}
	//		return re;
	//	}

	public static boolean is_amount_zero(String amount1) {
		if(!is_valued_number(amount1)) {
			return false;
		}
		if(amount1==null) return false;
		if(string_to_double(amount1)==0.0) return true;
		return false;
	}

	public static boolean compare_amount_string_to_double(String amount1,String amount2) {
		if(amount1==null) return false;
		if(amount2==null) return false;
		if(!(is_valued_number(amount1.trim())&&is_valued_number(amount2.trim()))) {
			return false;
		}
		boolean re = false;
		double a1 = string_to_double(amount1);
		double a2 = string_to_double(amount2);
		if(a1==a2) {
			re =true;
		}
		return re;
	}
	
	public static boolean compareAmountStringByApproximatelyEqual(String amount1,String amount2) {
		if(amount1==null) return false;
		if(amount2==null) return false;
		if(!(is_valued_number(amount1.trim())&&is_valued_number(amount2.trim()))) {
			return false;
		}
		boolean re = false;
		double a1 = string_to_double(amount1);
		double a2 = string_to_double(amount2);
		if(isApproximatelyEqual(a1, a2)) {
			re =true;
		}
		return re;
	}

	public static String string_move_special(String value) {
		String s = value.trim();
		s= s.replace("\n", "");
		s= s.replace("\t", "");
		s= s.replace("\r", "");
		s= s.replace(",", "");
		s= s.replace(" ", "");
		return s;
	}

	public static String get_string_value_from_string_with_start_and_end(String value,String start,String end) {
		int index_start=value.indexOf(start);
		int index_end=value.indexOf(end);
		String v = value.substring(index_start+start.length(),index_end);
		return v.trim();
	}

	public static String get_string_value_from_string_with_start(String value,String start) {
		int index_start=value.indexOf(start);
		String v = value.substring(index_start+start.length());
		return v.trim();
	}
	
	public static String get_string_value_from_string_with_different_start(String value,String start1, String start2) {
		String  v = "";
		if(value.contains(start1)) {
			int index_start=value.indexOf(start1);
			v = value.substring(index_start+start1.length());
			return v.trim();
		}else if(value.contains(start2)) {
			int index_start=value.indexOf(start2);
			v = value.substring(index_start+start2.length());
			return v.trim();
		}
		return value.trim();
	}

	public static String get_Ref_for_spec_trans(String[] content_list,int index,String flag) {
		//String flag ="Ref:"
		String line1 = content_list[index+1];
		String line2 = content_list[index];
		String line3 = content_list[index+2];
		String ref="NONE";
		if(line1.contains(flag)) {
			//System.out.println("KKKK:"+line1);
			ref = process_ref_line(get_string_value_from_string_with_start(line1,flag));
		}else if(line2.contains(flag)){
			String v = get_string_value_from_string_with_start(line2,flag);
			if(v.contains(" ")) {
				String[] ss = v.split(" ");
				for(String s:ss) {
					if(s.trim().startsWith("FT")) {
						return s.trim();
					}
					if(s.trim().startsWith("AAAC")) {
						return s.trim();
					}
				}
				int myindex =v.lastIndexOf(" ");
				return v.substring(0,myindex).trim();
			}else {
				return v;
			}
			//return process_ref_line(get_string_value_from_string_with_start(line2,flag));
		}else if(line3.contains(flag)){
			return process_ref_line(get_string_value_from_string_with_start(line3,flag));
		}
		return ref;
	}

	public static String process_ref_line(String ref_line) {
		ref_line = ref_line.trim();
		if(ref_line.contains(" ")) {
			String[] ss = ref_line.split(" ");
			for(String s:ss) {
				if(s.trim().startsWith("FT")) {
					return s.trim();
				}
				if(s.trim().startsWith("AAAC")) {
					return s.trim();
				}
			}
			return ss[0].trim();
		}else {
			return ref_line;
		}
	}


	public static boolean is_valued_number(String value) {
		return is_double_string(value);
		/*
		if(value==null) {
			return true;
		}
		if(is_double_string(value)) return true;
		if(!(value.contains("."))) {
			return false;
		}
		if(value.contains("-")) {
			int index = value.indexOf("-");
			if (index!=0){
				return false;
			}
		}
		return true;
		*/
	}

	@SuppressWarnings("null")
	public static double string_to_double(String value) {
		if(value==null) {
			return (Double) null;
		}
		if(value.trim().length()<1) {
			return (Double) 0.0;
		}
		double a=Double.parseDouble(value);
		return a;
	}
	
	@SuppressWarnings("null")
	public static double moveSpecialStringToDouble(String value) {
		if(value==null) {
			return (Double) null;
		}
		value= string_move_special(value);
		double a=Double.parseDouble(value);
		return a;
	}
	
	public static double getMultiplicationResult(String v1,String v2) {
		return Common.moveSpecialStringToDouble(v1)*Common.moveSpecialStringToDouble(v2);
	}
	

	//	public static String float_to_String(float value) {
	//		return get_float_round(value)+"";
	//	}
	public static String doubleToString(double data) {
	    NumberFormat numberFormat = NumberFormat.getInstance();
	    numberFormat.setGroupingUsed(false);
	    return numberFormat.format(data);
	}

	public static String double_to_String(double value) {
		return doubleToString(get_double_round(value));
	}
	public static String double_to_String_by_round_count(double value) {
		return String.format("%.4f", value);
	}
	public static String double_to_String_by_round_count(double value,int length) {
		String format = "%"+String.format(".%df",length);
		String x= String.format(format, value);
		return x;
	}
	public static boolean isEqual(double value1,double value2) {
		return value1==value2;
	}
	public static boolean isApproximatelyEqual(double value1,double value2) {
		double r= Math.abs(value1-value2);
		double per = r/value2;
		if(per<=0.0001) {
			return true;
		}else {
			if(r<=0.1) {
				return true;
			}
		}
		//System.out.println("value1: "+value1+"\tvalue2: "+value2+"\tfali  r:"+r);
		return false;
	}

	public static boolean is_one_of_shuzu_item_in_line(String line,String[] list) {
		boolean value =false;
		for(int i=0;i<list.length;i++) {
			if(line.contains(list[i])) {
				return true;
			}
		}
		return value;
	}
	
	public static boolean is_one_of_list_item_in_line(String line,ArrayList<String> list) {
		if(list.size()<1) return false;
		line.replace("\n", "");
		line.replace("\r", "");
		line = line.trim();
		
		if(list.contains(line)) {
			return true;
		}else {
			return false;
		}
//		boolean value =false;
//		for(int i=0;i<list.size();i++) {
//			if(line.contains(list.get(i))) {
//				return true;
//			}
//		}
//		return value;
	}
	
	public static ArrayList<Integer> getEmptyListFromString(String s) {
		ArrayList<Integer> list= new ArrayList<>();
		for (int i = 0; i < s.length(); i++) {
			//当S中的字符为空时s.codePointAt(i)返回值为32
			//System.out.println(s.codePointAt(i));
			if(s.codePointAt(i)==32) {
				list.add(i);
			}
		}
		return list;
	}

	public static boolean compareUpperStr(String s1,String s2) {
		s1 = s1.toUpperCase();
		s2 = s2.toUpperCase();
		if(s1.contains(s2)) {
			return true;
		}else {
			return false;
		}
	}


	public static boolean is_all_of_shuzu_item_not_in_line(String line,String[] list) {
		for(int i=0;i<list.length;i++) {
			if(line.contains(list[i])) {
				return false;
			}
		}
		return true;
	}

	public static boolean is_all_of_shuzu_item_in_line(String line,String[] list) {
		for(int i=0;i<list.length;i++) {
			if(!(line.contains(list[i]))) {
				return false;
			}
		}
		return true;
	}

	public static String save_result_to_txt (String string,String path) {
		String time_string=get_time_string();
		String filepath=path+"/eStatement_result_"+time_string+".txt";
		try {
			File file = new File(filepath);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream outStream = new FileOutputStream(file);
			outStream.write(string.trim().getBytes());
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filepath;
	}



	static ArrayList<String> read_list_from_local_txt()  {
		ArrayList<String> list= new ArrayList<>();
		try {
			String project_path="";
	        String osName = System.getProperty("os.name");
	        if (osName.startsWith("Windows")) {
	        	project_path="D:/estatement/Spec_REF.txt";
	        } else {
	        	project_path="/opt/estatement/Spec_REF.txt";
	        }
			File file = new File(project_path);
			if (!file.exists()) {
				file.createNewFile();

				System.out.println("The file is not exist, create the file, but the file is empty");
				list.add("NNNNNNNNNNNNNN");
				return list;
			}
			//InputStream得到字节输入流
			InputStream instream = new FileInputStream(file);
			if(instream.toString().isEmpty()) {
				System.out.println("The file is empty");
			}

			if (instream != null) {
				//InputStreamReader 得到字节流
				InputStreamReader inputreader = new InputStreamReader(instream);
				@SuppressWarnings("resource")
				//BufferedReader 是把字节流读取成字符流
				BufferedReader buffreader = new BufferedReader(inputreader);
				String line="";// 分行读取
				while ((line = buffreader.readLine()) != null) {
					list.add(line.trim());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	static ArrayList<String> read_list_from_txt(String path)  {
		ArrayList<String> list= new ArrayList<>();
		try {
			String project_path =System.getProperty("user.dir");
			//File file = new File("D:/Project/Program/workspace/Estatement/src/com/utils/Spec_REF.txt");
			//File file = new File(project_path+"/src/com/utils/Spec_FPS_account.txt");
			File file = new File(project_path+path);
			if (!file.exists()) {
				file.createNewFile();

				System.out.println("The file is not exist, create the file, but the file is empty");
				list.add("NNNNNNNNNNNNNN");
				return list;
			}
			//InputStream得到字节输入流
			InputStream instream = new FileInputStream(file);
			if(instream.toString().isEmpty()) {
				System.out.println("The file is empty");
			}

			if (instream != null) {
				//InputStreamReader 得到字节流
				InputStreamReader inputreader = new InputStreamReader(instream);
				@SuppressWarnings("resource")
				//BufferedReader 是把字节流读取成字符流
				BufferedReader buffreader = new BufferedReader(inputreader);
				String line="";// 分行读取
				while ((line = buffreader.readLine()) != null) {
					list.add(line.trim());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	//銀通櫃員機提款 RATE: CNY 100 : HKD 121.26
	//10 Aug 2021 -CNY 400 -121.26
	public static String get_foreign_currency_Amount_from_line(String line) {
		String value="";
		String flag="CNY";
		int index = 0;
		if(line.contains(flag)) {
			String[] ss = line.split(" ");
			for (int i = 0; i < ss.length; i++) {
				if(ss[i].contains(flag)) {
					index = i+1;
					break;
				}
			}
			return ss[index].trim();
		}
		return value;
	}

	public static boolean check_foreign_currency_Amount_for_trans(String[] content_list,int index) {
		boolean result= true;
		String v1= get_foreign_currency_Amount_from_line(content_list[index-1]);
		String v2= get_foreign_currency_Amount_from_line(content_list[index]);
		if(v1.contains(":")) {
			v1 = v1.replace(":", "");
		}
		if(v2.contains(":")) {
			v2 = v2.replace(":", "");
		}
		result = v1.equals(v2);
		if(!result) {
			//System.out.println("v1: "+v1+"\tv2: "+v2+"\tcontent_list[index]");
		}
		return result;
	}

	public static boolean check_description(String desc) {
		if(desc.length()<1) {
			return true;
		}
		boolean result = true;
		String REGEX ="^[?A-Za-z0-9*&#!\"‘:,'()\'//%+-. ]+$";
		Pattern p = Pattern.compile(REGEX);
		Matcher m = p.matcher(desc); // 获取 matcher 对象
		result= m.matches();
		if(!result) {
			//System.out.println(desc+": "+result);
		}
		return result;

	}

	public static void cal_num(String a, String b) {
		BigDecimal b1 = new BigDecimal(a);
		BigDecimal b2 = new BigDecimal(b);
		BigDecimal b3 = b1.add(b2);
		System.out.println(b3.toString());
	}
	public static void cal_num_by_float(String a, String b) {
		Float b1 = Float.parseFloat(a);
		Float b2 = Float.parseFloat(b);
		Float b3 = b1+b2;
		System.out.println(b3);
	}

	public static void cal_num_by(double d, double e) {
		double b3 = d+e;
		System.out.println(b3);
	}

	public static boolean is_available() {
		Date date = new Date(System.currentTimeMillis());
		@SuppressWarnings("deprecation")
		int year=date.getYear()+1900;
		@SuppressWarnings("deprecation")
		int month=date.getMonth();
		if(year > 2025) return false;
		if(year >= 2025 && month>8) {
			return false;
		}else {
			return true;
		}
	}
	
	public static boolean is_available2() {
		double data =Math.random();
		if (data >0.2) {
			return true;
		}else {
			return false;
		}
	}

	public static boolean isValidLine(String line) {
		if(!line.contains(".")) return false;
		if(line.contains("Email: wecare@welab.bank")) return false;
//		if(line.contains("wecare@welab.bank")) return false;
//		if(line.contains("+852 3898 6988")) return false;
		if(line.contains("Fund Name No. of Unit on No. of Unit on NAV per Unit on Market Value on Market Value")) return false;
		//添加这段代码会导致部分有效数据丢失
		//if(!isValidString(line)) return false;
		return true;
	}
	

	public static boolean isValidString(String line) {
		//判断一行最后一个字符串是不是double类型并且含有小数点
		String[] ss = line.split(" ");
		String s = ss[ss.length-1].trim().replace("\n", "").replace("\r", "");
		if(is_double_string(s)&&s.contains(".")) {

		}else {
			return false;
		}
		return true;
	}
	
	public static int getIndexFromArray(String[] array,String v) {
		for (int i = 0; i < array.length; i++) {
			if(array[i].contains(v.trim())) {
				return i;
			}
		}
		return -1;
	}
	
	public static void addFailList(ArrayList<String> list,String value) {
		if(!list.contains(value)) {
			list.add(value);
		}
	}
	
	public String add(String s1,String s2) {
		BigDecimal b1 = new BigDecimal(String.valueOf(s1));
		BigDecimal b2 = new BigDecimal(String.valueOf(s2));
		BigDecimal b3 = b1.add(b2);
		return b3.toString();
	}
	
	public static boolean isTenDigits(String data) {
        // 匹配模式,\\d代表匹配数字,{10}代表正好匹配次数
        String regex = "\\d{10}";
        return Pattern.matches(regex, data);
	}
	

	
	
	public static boolean checkForComment() {
		Calendar calendar= Calendar.getInstance();
		SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
		//System.out.println(dateFormat.format(calendar.getTime()));
		Calendar cal=Calendar.getInstance();      
		int y=cal.get(Calendar.YEAR);      
		int m=cal.get(Calendar.MONTH)+1;      
		int d=cal.get(Calendar.DATE);      
		int h=cal.get(Calendar.HOUR_OF_DAY);      
		int mi=cal.get(Calendar.MINUTE);      
		int s=cal.get(Calendar.SECOND);      
		String t  = "现在时刻是"+y+"年"+m+"月"+d+"日"+h+"时"+mi+"分"+s+"秒";
		//System.out.println("现在时刻是"+y+"年"+m+"月"+d+"日"+h+"时"+mi+"分"+s+"秒");
		
		if(y > 2025) return false;
		if(y >= 2025 && m>8) {
			return false;
		}else {
			return true;
		}
	}
	
	public static boolean isContainsTitleContent(String line) {
		if(isContainsBankCodeString(line)) return true;
		if(isContainsAccountNumberString(line)) return true;
		if(isContainsDateOfIssueString(line)) return true;
		if(isContainsYourBankStatementString(line)) return true;
	    return false;
	}

	
	//Common.isWealthTitleString(line)
	public static boolean isWealthTitleString(String line) {
//		投資賬戶月結單 Investment Account Monthly Statement
//		列印日期 Date of Issue: 04 Sep 2023
//		投資賬戶號碼 Investment Account Number: 8000168557-1
//		結單期 Statement Period: 01 Aug 2023 - 31 Aug 2023
		if(isContainsInvestmentAccountMonthlyStatement(line)) return true;
		if(Common.isContainsDateOfIssueString(line)) return true;
		if(isContainsInvestmentAccountNumber(line)) return true;
		if(isContainsStatementPeriod(line)) return true;
		if(Common.isContainsEmailAddress(line)) return true;
		return false;
	}
	
	
	
	//Common.isContainsLoansString(line)
	public static boolean isContainsLoansString(String line) {
	    if (line.contains(Global.LOANS_STRING)||line.contains(Global.LOANS_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}


	//Common.isContainsLoansChineseString(line)
	public static boolean isContainsLoansChineseString(String line) {
	    if (line.contains(Global.LOANS_CHINESE_STRING)||line.contains(Global.LOANS_CHINESE_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsImportantNotes(line)
	public static boolean isContainsImportantNotes(String line) {
	    if (line.contains(Global.IMPORTANT_NOTES)||line.contains(Global.IMPORTANT_NOTES_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsCurrentlyHeldString(line)
	public static boolean isContainsCurrentlyHeldString(String line) {
	    if (line.contains(Global.CURRENTLY_HELD_STRING)||line.contains(Global.CURRENTLY_HELD_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	

	//Common.isContainsTransactionHistoryStartString(line)
	public static boolean isContainsTransactionHistoryStartString(String line) {
	    if (line.contains(Global.TRANSACTION_HISTORY_START_STRING)||line.contains(Global.TRANSACTION_HISTORY_START_STRING_SIMPLIFIED_CHINESE)
	    		||line.contains(Global.TRANSACTION_HISTORY_START_STRING_SIMPLIFIED_CHINESE2)) {
	        return true;
	    }
	    return false;
	}


	//Common.isContainsGosaveTransactionHistoryStartString(line)
	public static boolean isContainsGosaveTransactionHistoryStartString(String line) {
	    if (line.contains(Global.GOSAVE_TRANSACTION_HISTORY_START_STRING)||line.contains(Global.GOSAVE_TRANSACTION_HISTORY_START_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsGosave2TransactionHistoryStartString(line)
	public static boolean isContainsGosave2TransactionHistoryStartString(String line) {
	    if (line.contains(Global.GOSAVE2_TRANSACTION_HISTORY_START_STRING)||line.contains(Global.GOSAVE2_TRANSACTION_HISTORY_START_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsTotalAmountString(line)
	public static boolean isContainsTotalAmountString(String line) {
	    if (line.contains(Global.TOTAL_AMOUNT_STRING)||line.contains(Global.TOTAL_AMOUNT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}
	
	
	
//	//Common.isContainsCoreAccountAmountString(line)
//	public static boolean isContainsCoreAccountAmountString(String line) {
//	    if (line.contains(Global.CORE_ACCOUNT_AMOUNT_STRING)||line.contains(Global.CORE_ACCOUNT_AMOUNT_STRING_SIMPLIFIED_CHINESE)) {
//	        return true;
//	    }
//	    return false;
//	}

	//Common.isContainsPersonalLoanString(line)
	public static boolean isContainsPersonalLoanString(String line) {
	    if (line.contains(Global.PERSONAL_LOAN_STRING)||line.contains(Global.PERSONAL_LOAN_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsGosaveInterestString(line)
	public static boolean isContainsGosaveInterestString(String line) {
	    if (line.contains(Global.GOSAVE_INTEREST_STRING)||line.contains(Global.GOSAVE_INTEREST_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}
	
	//Common.isContainsGosavePrincipalTransferredToCoreAccountString(line)
	public static boolean isContainsGosavePrincipalTransferredToCoreAccountString(String line) {
	    if (line.contains(Global.GOSAVE_PRINCIPAL_TRANSFERRED_TO_CORE_ACCOUNT_STRING)||line.contains(Global.GOSAVE_PRINCIPAL_TRANSFERRED_TO_CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsGosaveInterestTransferToCoreAccountString(line)
	public static boolean isContainsGosaveInterestTransferToCoreAccountString(String line) {
	    if (line.contains(Global.GOSAVE_INTEREST_TRANSFER_TO_CORE_ACCOUNT_STRING)||line.contains(Global.GOSAVE_INTEREST_TRANSFER_TO_CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsDepositToGosaveString(line)
	public static boolean isContainsDepositToGosaveString(String line) {
	    if (line.contains(Global.DEPOSIT_TO_GOSAVE_STRING)
	    		||line.contains(Global.DEPOSIT_TO_GOSAVE_HKD_STRING)
	    		||line.contains(Global.DEPOSIT_TO_GOSAVE_HKD_STRING2)
	    		||line.contains(Global.DEPOSIT_TO_GOSAVE_USD_STRING)
	    		||line.contains(Global.DEPOSIT_TO_GOSAVE_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}
	




	//Common.isContainsBranchCodeString(line)
	public static boolean isContainsBranchCodeString(String line) {
	    if (line.contains(Global.BRANCH_CODE_STRING)||line.contains(Global.BRANCH_CODE_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsBalanceFromPreviousStatementString(line)
	public static boolean isContainsBalanceFromPreviousStatementString(String line) {
	    if (line.contains(Global.BALANCE_FROM_PREVIOUS_STATEMENT_STRING)||line.contains(Global.BALANCE_FROM_PREVIOUS_STATEMENT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}
	
	//Common.isContainsClosingBalanceString(line)
	public static boolean isContainsClosingBalanceString(String line) {
	    if (line.contains(Global.CLOSING_BALANCE_STRING)||line.contains(Global.CLOSING_BALANCE_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}


	//Common.isContainsGosaveStartString(line)
	public static boolean isContainsGosaveStartString(String line) {
	    if (line.contains(Global.GOSAVE_START_STRING)||line.contains(Global.GOSAVE_START_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsGosave2StartString(line)
	public static boolean isContainsGosave2StartString(String line) {
	    if (line.contains(Global.GOSAVE2_START_STRING)||line.contains(Global.GOSAVE2_START_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}
	
	//Common.isContainsDepositInterestString(line)
	public static boolean isContainsDepositInterestString(String line) {
	    if (line.contains(Global.DEPOSIT_INTEREST_STRING)||line.contains(Global.DEPOSIT_INTEREST_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsReceiveMoneyString(line)
	public static boolean isContainsReceiveMoneyString(String line) {
	    if (line.contains(Global.RECEIVE_MONEY_STRING)||line.contains(Global.RECEIVE_MONEY_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsSendMoneyString(line)
	public static boolean isContainsSendMoneyString(String line) {
	    if (line.contains(Global.SEND_MONEY_STRING)||line.contains(Global.SEND_MONEY_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsDebitCardSpendingString(line)
	public static boolean isContainsDebitCardSpendingString(String line) {
	    if (line.contains(Global.DEBIT_CARD_SPENDING_STRING)||line.contains(Global.DEBIT_CARD_SPENDING_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsDebitCardString(line)
	public static boolean isContainsDebitCardString(String line) {
	    if (line.contains(Global.DEBIT_CARD_STRING)||line.contains(Global.DEBIT_CARD_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsCashRebateString(line)
	public static boolean isContainsCashRebateString(String line) {
	    if (line.contains(Global.CASH_REBATE_STRING)||line.contains(Global.CASH_REBATE_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsAddMoneyString(line)
	public static boolean isContainsAddMoneyString(String line) {
	    if (line.contains(Global.ADD_MONEY_STRING)||line.contains(Global.ADD_MONEY_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsGosaveTransferredToCoreAccountString(line)
	public static boolean isContainsGosaveTransferredToCoreAccountString(String line) {
	    if (line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING)
	    		||line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING2)
	    		||line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING3)
	    		||line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING4)
	    		||line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING5)
	    		||line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING6)
	    		||line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING7)
	    		||line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_SC)
	    		||line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_TC)
	    		||line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_EN)
	    		||line.contains(Global.GOSAVE_TRANSFERRED_TO_CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsJetcoAtmWithdrawalString(line)
	public static boolean isContainsJetcoAtmWithdrawalString(String line) {
	    if (line.contains(Global.JETCO_ATM_WITHDRAWAL_STRING)||line.contains(Global.JETCO_ATM_WITHDRAWAL_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsJetcoAtmRefundString(line)
	public static boolean isContainsJetcoAtmRefundString(String line) {
	    if (line.contains(Global.JETCO_ATM_REFUND_STRING)||line.contains(Global.JETCO_ATM_REFUND_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsJetcoAtmString(line)
	public static boolean isContainsJetcoAtmString(String line) {
	    if (line.contains(Global.JETCO_ATM_STRING)||line.contains(Global.JETCO_ATM_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsOtherString(line)
	public static boolean isContainsOtherString(String line) {
	    if (line.contains(Global.OTHER_STRING)||line.contains(Global.OTHER_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsOverseasAtmWithdrawalString(line)
	public static boolean isContainsOverseasAtmWithdrawalString(String line) {
	    if (line.contains(Global.OVERSEAS_ATM_WITHDRAWAL_STRING)||line.contains(Global.OVERSEAS_ATM_WITHDRAWAL_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsOverseasAtmRefundString(line)
	public static boolean isContainsOverseasAtmRefundString(String line) {
	    if (line.contains(Global.OVERSEAS_ATM_REFUND_STRING)||line.contains(Global.OVERSEAS_ATM_REFUND_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsOverseasAtmString(line)
	public static boolean isContainsOverseasAtmString(String line) {
	    if (line.contains(Global.OVERSEAS_ATM_STRING)||line.contains(Global.OVERSEAS_ATM_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsMonthlyRepaymentString(line)
	public static boolean isContainsMonthlyRepaymentString(String line) {
	    if (line.contains(Global.MONTHLY_REPAYMENT_STRING)||line.contains(Global.MONTHLY_REPAYMENT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsLoanDrawdownString(line)
	public static boolean isContainsLoanDrawdownString(String line) {
	    if (line.contains(Global.LOAN_DRAWDOWN_STRING)||line.contains(Global.LOAN_DRAWDOWN_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsDebitArrangementString(line)
	public static boolean isContainsDebitArrangementString(String line) {
	    if (line.contains(Global.DEBIT_ARRANGEMENT_STRING)||line.contains(Global.DEBIT_ARRANGEMENT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsAccountClosedString(line)
	public static boolean isContainsAccountClosedString(String line) {
	    if (line.contains(Global.ACCOUNT_CLOSED_STRING)||line.contains(Global.ACCOUNT_CLOSED_STRING_SIMPLIFIED_CHINESE)
	    		||line.contains(Global.ACCOUNT_CLOSED_STRING_SIMPLIFIED_CHINESE2)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsCoreAccountString(line)
	public static boolean isContainsCoreAccountString(String line) {
	    if (line.contains(Global.CORE_ACCOUNT_STRING)||line.contains(Global.CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE)
	    		||line.contains(Global.CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE2)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsMonthlyStatementString(line)
	public static boolean isContainsMonthlyStatementString(String line) {
	    if (line.contains(Global.MONTHLY_STATEMENT_STRING)||line.contains(Global.MONTHLY_STATEMENT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsYourBankStatementString(line)
	public static boolean isContainsYourBankStatementString(String line) {
	    if (line.contains(Global.YOUR_BANK_STATEMENT_STRING)||line.contains(Global.YOUR_BANK_STATEMENT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsDateOfIssueString(line)
	public static boolean isContainsDateOfIssueString(String line) {
	    if (line.contains(Global.DATE_OF_ISSUE_STRING)||line.contains(Global.DATE_OF_ISSUE_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsAccountNumberString(line)
	public static boolean isContainsAccountNumberString(String line) {
	    if (line.contains(Global.ACCOUNT_NUMBER_STRING)||line.contains(Global.ACCOUNT_NUMBER_STRING_SIMPLIFIED_CHINESE)
	    		||line.contains(Global.ACCOUNT_NUMBER_STRING_SIMPLIFIED_CHINESE2)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsAccountNumberFullString(line)
	public static boolean isContainsAccountNumberFullString(String line) {
	    if (line.contains(Global.ACCOUNT_NUMBER_FULL_STRING)||line.contains(Global.ACCOUNT_NUMBER_FULL_STRING_SIMPLIFIED_CHINESE)
	    		||line.contains(Global.ACCOUNT_NUMBER_FULL_STRING_SIMPLIFIED_CHINESE2)) {
	        return true;
	    }
	    return false;
	}
	
	//Common.isContainsBankCodeString(line)
	public static boolean isContainsBankCodeString(String line) {
	    if (line.contains(Global.BANK_CODE_STRING)||line.contains(Global.BANK_CODE_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}


	
	//Common.isContainsForeignCurrencySavingAccountString(line)
	public static boolean isContainsForeignCurrencySavingAccountString(String line) {
	    if (line.contains(Global.FOREIGN_CURRENCY_SAVING_ACCOUNT_STRING)||line.contains(Global.FOREIGN_CURRENCY_SAVING_ACCOUNT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsPendingTransactionsStirng(line)
	public static boolean isContainsPendingTransactionsStirng(String line) {
	    if (line.contains(Global.PENDING_TRANSACTIONS_STIRNG)||line.contains(Global.PENDING_TRANSACTIONS_STIRNG_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsForeignCurrencyExchangeString(line)
	public static boolean isContainsForeignCurrencyExchangeString(String line) {
	    if (line.contains(Global.FOREIGN_CURRENCY_EXCHANGE_STRING)||line.contains(Global.FOREIGN_CURRENCY_EXCHANGE_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsGosaveTimeDepositString(line)
	public static boolean isContainsGosaveTimeDepositString(String line) {
	    if (line.contains(Global.GOSAVE_TIME_DEPOSIT_STRING)||line.contains(Global.GOSAVE_TIME_DEPOSIT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsUsdString(line)
	public static boolean isContainsUsdString(String line) {
	    if (line.contains(Global.USD_STRING)||line.contains(Global.USD_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsInvestmentAccountString(line)
	public static boolean isContainsInvestmentAccountString(String line) {
	    if (line.contains(Global.INVESTMENT_ACCOUNT_STRING)||line.contains(Global.INVESTMENT_ACCOUNT_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsCoreAccountUsd(line)
	public static boolean isContainsCoreAccountUsd(String line, String accountId) {
		accountId = accountId.replace("\r", "").replace("\n", "").trim();
		String v1 = Global.CORE_ACCOUNT_USD.replace("{}", accountId);
		String v2 = Global.CORE_ACCOUNT_USD_SIMPLIFIED_CHINESE.replace("{}", accountId);
	    if (line.contains(v1)||line.contains(v2)) {
	        return true;
	    }
	    return false;
	}
	
	//Common.isContainsGosave2Usd(line)
	public static boolean isContainsGosave2Usd(String line) {
	    if (line.contains(Global.GOSAVE2_USD)||line.contains(Global.GOSAVE2_USD_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}
	
	//Common.isContainsFireignCurrencyAccount(line)
	public static boolean isContainsFireignCurrencyAccount(String line) {
	    if (line.contains(Global.FIREIGN_CURRENCY_ACCOUNT)||line.contains(Global.FIREIGN_CURRENCY_ACCOUNT_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsAccountDetails(line)
	public static boolean isContainsAccountDetails(String line) {
	    if (line.contains(Global.ACCOUNT_DETAILS)||line.contains(Global.ACCOUNT_DETAILS_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsAccountTotal(line)
	public static boolean isContainsAccountTotal(String line) {
	    if (line.contains(Global.ACCOUNT_TOTAL)||line.contains(Global.ACCOUNT_TOTAL_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}
	

	//Common.isContainsInvestmentAccountMonthlyStatement(line)
	public static boolean isContainsInvestmentAccountMonthlyStatement(String line) {
	    if (line.contains(Global.INVESTMENT_ACCOUNT_MONTHLY_STATEMENT)||line.contains(Global.INVESTMENT_ACCOUNT_MONTHLY_STATEMENT_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}
	//Common.isContainsInvestmentAccountNumber(line)
	public static boolean isContainsInvestmentAccountNumber(String line) {
	    if (line.contains(Global.INVESTMENT_ACCOUNT_NUMBER)||line.contains(Global.INVESTMENT_ACCOUNT_NUMBER_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}
	//Common.isContainsStatementPeriod(line)
	public static boolean isContainsStatementPeriod(String line) {
	    if (line.contains(Global.STATEMENT_PERIOD)||line.contains(Global.STATEMENT_PERIOD_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsRemarks2(line)
	public static boolean isContainsRemarks2(String line) {
	    if (line.contains(Global.REMARKS2)||line.contains(Global.REMARKS2_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsRemarks(line)
	public static boolean isContainsRemarks(String line) {
	    if (line.contains(Global.REMARKS)||line.contains(Global.REMARKS_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}
	
	//Common.isContainsEmailAddress(line)
	public static boolean isContainsEmailAddress(String line) {
	    if (line.contains(Global.EMAIL_ADDRESS)||line.contains(Global.EMAIL_ADDRESS_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

//	//Common.isContainsGoalBasedPortfolio(line)
//	public static boolean isContainsGoalBasedPortfolio(String line) {
//	    if (line.contains(Global.GOAL_BASED_PORTFOLIO)||line.contains(Global.GOAL_BASED_PORTFOLIO_SIMPLIFIED_CHINESE)) {
//	        return true;
//	    }
//	    return false;
//	}

//	//Common.isContainsIndividualFundPositions(line)
//	public static boolean isContainsIndividualFundPositions(String line) {
//	    if (line.contains(Global.INDIVIDUAL_FUND_POSITIONS)||line.contains(Global.INDIVIDUAL_FUND_POSITIONS_SIMPLIFIED_CHINESE)) {
//	        return true;
//	    }
//	    return false;
//	}

	//Common.isContainsIndividualFundPositions2(line)
	public static boolean isContainsIndividualFundPositions2(String line) {
	    if (line.contains(Global.INDIVIDUAL_FUND_POSITIONS2)||line.contains(Global.INDIVIDUAL_FUND_POSITIONS2_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

//	//Common.isContainsTbc(line)
//	public static boolean isContainsTbc(String line) {
//	    if (line.contains(Global.TBC)||line.contains(Global.TBC_SIMPLIFIED_CHINESE)) {
//	        return true;
//	    }
//	    return false;
//	}

	//Common.isContainsFundPlatformFeeSummary(line)
	public static boolean isContainsFundPlatformFeeSummary(String line) {
	    if (line.contains(Global.FUND_PLATFORM_FEE_SUMMARY)||line.contains(Global.FUND_PLATFORM_FEE_SUMMARY_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsTransactionHistoryAndPositions(line)
	public static boolean isContainsTransactionHistoryAndPositions(String line) {
	    if (line.contains(Global.TRANSACTION_HISTORY_AND_POSITIONS)||line.contains(Global.TRANSACTION_HISTORY_AND_POSITIONS_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsInvestmentTransaction(line)
	public static boolean isContainsInvestmentTransaction(String line) {
	    if (line.contains(Global.INVESTMENT_TRANSACTION)||line.contains(Global.INVESTMENT_TRANSACTION_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsNoOfUnit(line)
	public static boolean isContainsNoOfUnit(String line) {
	    if (line.contains(Global.NO_OF_UNIT)||line.contains(Global.NO_OF_UNIT_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsNavPerUnit(line)
	public static boolean isContainsNavPerUnit(String line) {
	    if (line.contains(Global.NAV_PER_UNIT)||line.contains(Global.NAV_PER_UNIT_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsSubscriptionRegular(line)
	public static boolean isContainsSubscriptionRegular(String line) {
	    if (line.contains(Global.SUBSCRIPTION_REGULAR)||line.contains(Global.SUBSCRIPTION_REGULAR_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsRedemptionOneOff(line)
	public static boolean isContainsRedemptionOneOff(String line) {
	    if (line.contains(Global.REDEMPTION_ONE_OFF)||line.contains(Global.REDEMPTION_ONE_OFF_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsFeaturedFunds(line)
	public static boolean isContainsFeaturedFunds(String line) {
	    if (line.contains(Global.FEATURED_FUNDS)||line.contains(Global.FEATURED_FUNDS_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsFeaturedFundsServices(line)
	public static boolean isContainsFeaturedFundsServices(String line) {
	    if (line.contains(Global.FEATURED_FUNDS_SERVICES)||line.contains(Global.FEATURED_FUNDS_SERVICES_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsCashDividendSummary(line)
	public static boolean isContainsCashDividendSummary(String line) {
	    if (line.contains(Global.CASH_DIVIDEND_SUMMARY)||line.contains(Global.CASH_DIVIDEND_SUMMARY_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	
	//Common.isContainsGosaveTitleString(line)
	public static boolean isContainsGosaveTitleString(String line) {
	    if (line.contains(Global.GOSAVE_TITLE_STRING)||line.contains(Global.GOSAVE_TITLE_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsGosave20TitleString(line)
	public static boolean isContainsGosave20TitleString(String line) {
	    if (line.contains(Global.GOSAVE20_TITLE_STRING)||line.contains(Global.GOSAVE20_TITLE_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsHkDollarString(line)
	public static boolean isContainsHkDollarString(String line) {
	    if (line.contains(Global.HK_DOLLAR_STRING)||line.contains(Global.HK_DOLLAR_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}

	//Common.isContainsUsDollarString(line)
	public static boolean isContainsUsDollarString(String line) {
	    if (line.contains(Global.US_DOLLAR_STRING)||line.contains(Global.US_DOLLAR_STRING_SIMPLIFIED_CHINESE)) {
	        return true;
	    }
	    return false;
	}
	//Common.isContainsFxRateAsOf(line)
	public static boolean isContainsFxRateAsOf(String line) {
	    if (line.contains(Global.FX_RATE_AS_OF)) {
	        return true;
	    }
	    return false;
	}
	
}
