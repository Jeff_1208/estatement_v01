package com.utils;

import com.entity.Estatement;

public class Foreign_Currency_Utils {
	private String[] want_find_shuzu = {"202","."};
	private String[] donot_want_find_shuzu = {Global.FOREIGN_CURRENCY_EXCHANGE_STRING,Global.FOREIGN_CURRENCY_EXCHANGE_STRING_SIMPLIFIED_CHINESE,
			Global.BALANCE_FROM_PREVIOUS_STATEMENT_STRING,Global.BALANCE_FROM_PREVIOUS_STATEMENT_STRING_SIMPLIFIED_CHINESE,
			Global.CLOSING_BALANCE_STRING,Global.CLOSING_BALANCE_STRING_SIMPLIFIED_CHINESE};

	public  Estatement get_foreign_currency_transactions(String content,Estatement estatement) {
		boolean foreign_currency_flag =false;
		boolean is_have_foreign_currency =false;
		String foreign_currency_total_amount_capture = "";

		String[] content_list = content.split("\n");
		double foreign_currency_total_calculate=0.0;
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			//外幣存款賬戶 - 美元 Foreign Currency Saving Account - USD (1002446376)
			//外币帐户 - 美元 Foreign Currency Account - USD (1020649615)
			if(Common.isContainsForeignCurrencySavingAccountString(line)) {
				foreign_currency_flag =true;
			}
			if(Common.isContainsCurrentlyHeldString(line)||Common.isContainsImportantNotes(line)) {
				break;
			}
			if(!foreign_currency_flag) {
				continue;
			}

			//01 Dec 2021 承上結餘 Balance From Previous Statement 199069.92
			if(Common.isContainsBalanceFromPreviousStatementString(line)) {
				foreign_currency_total_calculate=Common.get_double_value(line, -1);
			}
			
			//31 Mar 2024 帐户结余 Closing Balance 33,695.34
			if(Common.isContainsClosingBalanceString(line)) {
				String v = Common.get_string_value_from_string_with_start(line, "Closing Balance");
				foreign_currency_total_amount_capture = Common.string_move_special(v);
				//System.out.println("Closing Balance: "+v);
				break;
			}

//			基金申購 Debit date: 02 Dec 2021
//			1 Dec 2021 -1534.51
//			Fund subscription Ref: FT21335XR1X8
//
//			基金申購費 Debit date: 09 Dec 2021
//			8 Dec 2021 -12.79
//			Fund subscription fee Ref: FT21342QLBB2
//
//			外幣兌換  @ , credit date: 09 Dec 2021
//			9 Dec 2021 Foreign currency 12.79
//			Ref: FT21343JWGTT
//			conversion
			if(isAmountLine(line)) {
				is_have_foreign_currency =true;
				estatement.setIs_have_foreign_currency_tran(true);
				foreign_currency_total_calculate +=Common.get_double_value(line, -1);
				//System.out.println(line);
			}

		}
		
		if(!is_have_foreign_currency) {
			estatement.setIs_foreign_total_amount_pass(true);
			estatement.setForeign_currency_total_capture("0.0");
			estatement.setForeign_currency_total_calculate("0.0");
			return estatement;
		}
		
		//System.out.println("foreign_currency_total_calculate:"+foreign_currency_total_calculate);
		//System.out.println("foreign_currency_total_amount_capture:"+foreign_currency_total_amount_capture);
		estatement.setForeign_currency_total_capture(foreign_currency_total_amount_capture);
		estatement.setForeign_currency_total_calculate(foreign_currency_total_calculate+"");
		if(Common.isApproximatelyEqual(foreign_currency_total_calculate,  Common.moveSpecialStringToDouble(foreign_currency_total_amount_capture) )) {
			//System.out.println("foreign_currency_total_calculate PASS");
			estatement.setIs_foreign_total_amount_pass(true);
		}
		
		String usdAccountAmount = estatement.getUsdAccountAmount();
		double rate = Common.moveSpecialStringToDouble(usdAccountAmount) / foreign_currency_total_calculate;
		//System.out.println("dollor rate: " + rate);
		if(rate > 9 || rate < 6) {
			estatement.setIs_foreign_total_amount_pass(false);
		}
		return estatement;
	}


	public boolean isAmountLine(String line) {
		if(Common.the_line_is_have_a_double_string(line)&&Common.is_all_of_shuzu_item_in_line(line,want_find_shuzu)&&Common.is_all_of_shuzu_item_not_in_line(line,donot_want_find_shuzu)) {
			return true;
		}else {
			return false;
		}

	}
	
}
