package com.utils;

import java.util.ArrayList;

import com.entity.Estatement;
import com.entity.Transactions;

public class CoreAccountUsdUtils {
	public Estatement getCoreAccountUsdTransactions(String content,Estatement estatement) {
		
		boolean printflag = false;
		boolean coreAccounUsdFlag =false;
		double coreaccountUsdTotalCalculate=0;
		String[] coreaccount_want_find_shuzu_1 = {estatement.getMonth(),String.valueOf(estatement.getYEAR()),"."};
		Core_Account_Utils core_Account_Utils = new Core_Account_Utils();

		ArrayList<Transactions> usdTransList= new ArrayList<>();
		String[] content_list = content.split("\n");
		int trans_id=0;
		String accountId = estatement.getAccount_id().trim();
		for(int i =0;i<content_list.length;i++) {
			
			String line =content_list[i];
			
			if(Common.isContainsDebitCardSpendingString(line)) { continue;}//这一行没有金额，所有不抓取
			if(Common.isContainsTitleContent(line)) {continue;}
			if(Common.isContainsCurrentlyHeldString(line)) {break;}
			if(Common.isContainsCoreAccountUsd(line,accountId)) {coreAccounUsdFlag =true;}

			if(coreAccounUsdFlag&&Common.isContainsClosingBalanceString(line)) {
				String v = Common.get_string_value_from_string_with_start(line, "Closing Balance");
				if(v.trim().equals("-")) {
					estatement.setCoreaccount_total_usd_capture("0.0");
				}else {
					estatement.setCoreaccount_total_usd_capture(Common.string_move_special(v));
				}
				break;
			}

			//1 Jan 2021 承上結餘 Balance From Previous Statement -
			if(Common.isContainsBalanceFromPreviousStatementString(line)&&coreAccounUsdFlag) {
				String v = Common.get_string_value_from_string_with_start(line, "Previous Statement");
				estatement.setUsd_last_month_amount(Common.string_move_special(v));
				if(!(Common.string_move_special(v).equals("-"))) {
					coreaccountUsdTotalCalculate += Common.string_to_double(Common.string_move_special(v));
				}
				coreAccounUsdFlag =true;
			}

			if(!coreAccounUsdFlag) {continue;}

			if(Common.is_all_of_shuzu_item_in_line(line,coreaccount_want_find_shuzu_1)
					&& Common.is_all_of_shuzu_item_not_in_line(line,Core_Account_Utils.donot_want_find_shuzu)
					&&Common.the_line_is_have_a_double_string(line)){
				//System.out.println(line);
				//检查当月的记录
				trans_id+=1;
				if(printflag) {
					System.out.println(line);
					System.out.println(Common.get_double_value(line, -1));
				}
				usdTransList=core_Account_Utils.get_Transactions(content_list, i,usdTransList,estatement,trans_id);
				coreaccountUsdTotalCalculate +=Common.get_double_value(line, -1);
			}
		}
		
		estatement.setUsd_transations_history(usdTransList);
		
		double v  = Common.string_to_double(estatement.getCoreaccount_total_hkd_calculate())
				+ Common.string_to_double(estatement.getCoreAccountUsdTitle());
		estatement.setCoreaccount_total_hkd_and_usd_calculate(Common.double_to_String(v));
		estatement.setCoreaccount_total_usd_calculate(Common.double_to_String(coreaccountUsdTotalCalculate));
		Common.double_to_String_by_round_count(coreaccountUsdTotalCalculate, 2);
		//System.out.println(coreaccountUsdTotalCalculate);
		//System.out.println(estatement.getCoreAccountUsdTitle());
		//System.out.println(estatement.getCoreaccount_total_usd_calculate());
		//System.out.println(estatement.getCoreaccount_total_usd_capture());
		
		
		//美元乘以汇率和首页对比
		double usdToHKD =Common.string_to_double(estatement.getCoreaccount_total_usd_capture()) * Common.string_to_double(estatement.getFxRate());
		//System.out.println("usdToHKD: "+usdToHKD);
		//System.out.println("estatement.getCoreAccountUsdTitle(): "+estatement.getCoreAccountUsdTitle());
		
		String usdToHKDString =  Common.double_to_String_by_round_count(usdToHKD,2);
		if(Common.compare_amount_string_to_double(estatement.getCoreAccountUsdTitle(), usdToHKDString)) {
			//System.out.println("对比美元转换成港币正确！");
		}else {
			System.out.println("对比美元转换成港币错误！");
		}

		
		//对比计算结果是否相等
		if(estatement.isIs_account_close()) {
			estatement.setCore_account_usd_result(true);	
		}else if(estatement.getCoreaccount_total_usd_capture()==null) {
			estatement.setCore_account_usd_result(false);	
		}else if(Common.compare_amount_string_to_double(estatement.getCoreaccount_total_usd_calculate(), estatement.getCoreaccount_total_usd_capture())) {
				estatement.setCore_account_usd_result(true);
		}else if(estatement.getCoreaccount_total_usd_capture().contains("-")) {
			if(Common.string_to_double(estatement.getCoreaccount_total_usd_calculate()) ==0.0) {
				estatement.setCore_account_usd_result(true);
			}else {
				estatement.setCore_account_usd_result(false);	
			}
		}else{
			estatement.setCore_account_usd_result(false);	
		}
		return estatement;
	}
}
