package com.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.entity.Estatement;
import com.entity.GoSave;

public class GoSave2_Utils {

	public  Estatement get_go_save_exsit(String content,Estatement estatement) {
		//System.out.println("Check Gosave2!!!!!!!!!!!!!!!!!!!!!");
		boolean go_save_flag =false;
		boolean is_have_go_save =false;
		double go_save_total_calculate=0;
		HashMap<String, String> current_gosave_exist= new HashMap<>();
		//25 Oct 2022 10 Nov 2022 GS16654 0% p.a. 300,000.00
		String[] want_find_shuzu = {"p.a.","%"};
		
		ArrayList<GoSave> gosave2CurrentHeldList = new ArrayList<>();
		int gosave_index = 0;
		String[] content_list = content.split("\n");
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsGosave2StartString(line)) {
				go_save_flag =true;
				continue;
			}
			if(go_save_flag&&Common.isContainsTransactionHistoryStartString(line)) {break;}
			if(Common.isContainsLoansString(line)&&go_save_flag&&Common.isContainsGosave2TransactionHistoryStartString(line)) {break;}
			if(go_save_flag&&Common.isContainsImportantNotes(line)) {break;}
			if(go_save_flag&&Common.isContainsGosave2Usd(line)) {break;}
			if(!go_save_flag) {continue;}
			if(Common.isContainsTitleContent(line)) {continue;}

			if(Common.is_all_of_shuzu_item_in_line(line,want_find_shuzu) ){
				is_have_go_save=true;
				double amount = Common.get_double_value(line, -1);
				String gosave_id = Common.get_value_string_from_line(line, -4);
				//System.out.println("gosave_id: "+gosave_id);
				//System.out.println(line);
				go_save_total_calculate +=Common.get_double_value(line, -1);
				current_gosave_exist.put(gosave_id, Common.double_to_String(amount));
				gosave_index+=1;
				GoSave gosave = setExistGosaveList(content_list, i, line, gosave_index);
				gosave2CurrentHeldList.add(gosave);
			}
		}
		
		
		estatement.setGosave2_hkd_size(gosave_index);

		if(is_have_go_save) {
			estatement.setIs_have_gosave2_hkd_record(is_have_go_save);
			ArrayList<String> failReferenceNumberList =checkExistGosaveHeldIsDuplicate(gosave2CurrentHeldList); 
			estatement.setGoSave2_Exist_Held_Duplicate_Reference_Number(failReferenceNumberList);
		}else {
			estatement.setIs_have_gosave2_hkd_record(false);
			estatement.setGosave2_hkd_total_calculate("0");
			estatement.setGo_save2_result(true);
			return estatement;
		}
		HashMap<String, String> map=sort_map(current_gosave_exist);
		estatement.setCurrent_gosave_exist(map);

		go_save_total_calculate = Common.get_double_round(go_save_total_calculate);
		estatement.setGosave2_hkd_total_calculate(Common.doubleToString(go_save_total_calculate));
		
		System.out.println(" e.getGosave2_hkd_total_calculate(): "+ estatement.getGosave2_hkd_total_calculate());
		System.out.println("estatement.getGosave2_total_calculate(): "+estatement.getGosave2_hkd_total_calculate());
		System.out.println("estatement.getGosave2_total_capture_title(): "+estatement.getGosave2_total_capture_title());
		
		//print_current_gosave(map);//输出现有GoSave金额
		if(Common.compare_amount_string_to_double(estatement.getGosave2_hkd_total_calculate(), estatement.getGosave2_hkd_capture_title())){
			estatement.setGo_save2_result(true);
		}else {
			System.out.println("GoSave 2.0港币 金额异常");
			estatement.setGo_save2_result(false);
		}
		return estatement;
	}
	
	public static ArrayList<String> checkExistGosaveHeldIsDuplicate(ArrayList<GoSave> gosave2CurrentHeldList) {
		
		ArrayList<String> failReferenceNumberList = new ArrayList<>();
		int size = gosave2CurrentHeldList.size();
		//System.out.println("size: "+size);
		for (int i = 0; i < size; i++) {
			GoSave sample = gosave2CurrentHeldList.get(i);
			int sample_gosave_index = sample.getIndex();
			for (int j = 0; j < size; j++) {
				
				GoSave gosave = gosave2CurrentHeldList.get(j);
				int index = gosave.getIndex();
				if(sample_gosave_index ==index) {
					//System.out.println("sample_gosave_index:" +sample_gosave_index +"index:" +index);
					continue;
				}else {
					
					if(compareGosave2ExistHeld(sample, gosave)) {
						String referenceNumber = gosave.getReferenceNumber();
						if(!failReferenceNumberList.contains(referenceNumber)) {
							//System.out.println(referenceNumber);
							failReferenceNumberList.add(referenceNumber);
						}
					}
				}
			}
		}
		return failReferenceNumberList;
	}
	
	public static boolean compareGosave2ExistHeld(GoSave g1, GoSave g2) {
		if(!g1.getAmount().equals(g2.getAmount())) return false;
		if(!g1.getFrom().equals(g2.getFrom())) return false;
		if(!g1.getTo().equals(g2.getTo())) return false;
		if(!g1.getReferenceNumber().equals(g2.getReferenceNumber())) return false;
		if(!g1.getRate().equals(g2.getRate())) return false;
		if(!g1.getTenor().equals(g2.getTenor())) return false;
		return true;
	}
	

	public static GoSave setExistGosaveList(String[] content_list, int line_number, String line,int gosave_index) {

		//20 Jun 2024 20 Jan 2025 GS143415 4% p.a. 2,300.00
		//7 Months
		GoSave gosave  = new GoSave();
		String gosave_amount = Common.get_value_string_from_line(line, -1);
		String ref = Common.get_value_string_from_line(line, -4);
		String rate = Common.get_value_string_from_line(line, -3);
		String from = Common.get_value_string_from_line(line, -10) +"_"+Common.get_value_string_from_line(line, -9) +"_"+ Common.get_value_string_from_line(line, -8);
		String to = Common.get_value_string_from_line(line, -7) +"_"+Common.get_value_string_from_line(line, -6) +"_"+ Common.get_value_string_from_line(line, -5);
		String tenor = getGosaveTenor(content_list, line_number);
		gosave.setReferenceNumber(ref);
		gosave.setAmount(gosave_amount);
		gosave.setFrom(from);
		gosave.setTo(to);
		gosave.setRate(rate);
		gosave.setTenor(tenor);
		gosave.setIndex(gosave_index);
//		System.out.println(line);
//		System.out.println("gosave_amount: "+gosave_amount);
//		System.out.println("ref: "+ref);
//		System.out.println("from: "+from);
//		System.out.println("to: "+to);
//		System.out.println("rate: "+rate);
//		System.out.println("tenor: "+tenor);
//		System.out.println(gosave.toString());
		return gosave;
	}
	
	public  static String getGosaveTenor(String[] content_list, int line_number) {
		String line  = content_list[line_number+1];
		line = line.replace("\r", "");
		line = line.replace("\n", "");
		return line.trim();
	}
	

	public  Estatement get_go_save_transactions_history(String content,Estatement estatement) {
		boolean go_save_trans_flag =false;
		boolean go_save_start_flag =false;
		boolean is_have_go_save2_transactions =false;
		ArrayList<GoSave>  gosave2_trans_list = new ArrayList<>();
 		int count=0;
		//String[] want_find_shuzu_2 = {estatement.getMonth(),String.valueOf(estatement.getYEAR()),"."};
		
		String[] content_list = content.split("\n");
		
		for(int i =0;i<content_list.length;i++) {
			String line =content_list[i];
			if(Common.isContainsGosave2StartString(line)) {
				go_save_start_flag =true;
				continue;
			}
			if(Common.isContainsGosave2Usd(line)) {break;}
			if(Common.isContainsTransactionHistoryStartString(line)&&go_save_start_flag) {go_save_trans_flag=true;}
			if(Common.isContainsLoansString(line)||Common.isContainsLoansChineseString(line)||Common.isContainsImportantNotes(line)) {break;}
			if(!go_save_trans_flag) {continue;}
			//if(Common.isContainsGosave20TitleString(line)) {continue;}
			if(Common.isContainsTitleContent(line)) {continue;}
			

			//25 Oct 2022 Principal transferred to core -21.00
			//25 Oct 2022 22.00
			//20,000.00 21 Jul 2021 20 Jul 2023 AA21202ZKT06 -8,752.29
			//18 Mar 2024 Principal transferred to core -6.00   REF 在下一行
			if(Common.is_one_of_shuzu_item_in_line(line, Global.month_list)&&Common.the_line_is_have_a_double_string(line)){
				//System.out.println(line);
				count+=1;
				is_have_go_save2_transactions=true;
				gosave2_trans_list=set_one_gosave_trans(gosave2_trans_list,line,-1,-3,estatement,content_list,i,count);
			}
		}
		
		//gosave_trans_list=generate_combine_gosave_trans(gosave_trans_list);//组合记录

		if(is_have_go_save2_transactions) {
			estatement.setGoSave2_trans_history(gosave2_trans_list);
			estatement.setIs_have_gosave2_trans(is_have_go_save2_transactions);
			//System.out.println(estatement.getCustomer_id()+" estatement.getGoSave_trans_history().size():  "+estatement.getGoSave_trans_history().size());
		}else {
			estatement.setGoSave_fail_trans_Ref_for_amount_single(new ArrayList<>());
			estatement.setIs_have_gosave2_trans(false);
			return estatement;
		}

		//检查核心账户中关于Gosave的记录，是否和Gosave的转账记录匹配
		//estatement=Set_Gosave_fail_amout_list(estatement, gosave2_trans_list);
		estatement=Set_Gosave_fail_amout_list_with_gosave1_gosave2(estatement);

		//检查gosave交易记录是否有利息重复
		estatement=check_gosave_duplicate_interest(estatement, gosave2_trans_list);

		//检查Gosave转账记录金额符号是否正确
		estatement=set_GoSave_fail_trans_Ref_for_amount_single(estatement, gosave2_trans_list);

		//System.out.println("fail:"+estatement.getGoSave_fail_trans_Ref_from_coreaccount_list());
		return estatement;
	}
	
	public ArrayList<String> check_gosave_trans_list_duplicate(ArrayList<GoSave> gosave_trans_list) {
		ArrayList<String> result = new ArrayList<>();

		for(int i =0;i<gosave_trans_list.size();i++) {
			//System.out.println(gosave_trans_list.get(i).toString());
		}
		for(int i =0;i<gosave_trans_list.size();i++) {
			boolean is_dulicate_interest=false;
			GoSave g1 = gosave_trans_list.get(i);
			for(int j=0;j<gosave_trans_list.size();j++) {
				GoSave g2 = gosave_trans_list.get(j);
				if((g1.getID().equals(g2.getID()))&&(!(g1.getGosave_trans_Date().equals(g2.getGosave_trans_Date())))) {//记录日期不一样
					if(Common.compare_absolute_amount(g1.getAmount(),g2.getAmount())) {
						if(Common.isContainsGosaveInterestString(g1.getType())&&Common.isContainsGosaveInterestString(g2.getType())) {	
							is_dulicate_interest =true;
							//暂时没有输出到报告中
							//System.out.println(e.getCustomer_id()+" duplicate interest: "+g1.getGoSave_Trans_Ref());
							System.out.println(" duplicate interest1: "+g1.getRef());
						}
					}
				}
				if(g1.getGosave_trans_account_number()!=g2.getGosave_trans_account_number()) {
					if((g1.getID().equals(g2.getID()))&&((g1.getGosave_trans_Date().equals(g2.getGosave_trans_Date())))) {//记录日期一样,金额是一样的，不是比较绝对值
						if(g1.getAmount().equals(g2.getAmount())) {
							if(g1.getType().equals(g2.getType())) {
								is_dulicate_interest =true;
								//暂时没有输出到报告中
								//System.out.println(e.getCustomer_id()+" duplicate interest: "+g1.getGoSave_Trans_Ref());
								System.out.println(" duplicate interest2: "+g1.getRef());
							}
						}
					}
				}
			}
			if(is_dulicate_interest) {
				if(!result.contains(g1.getGoSave_Trans_Ref())) {
					result.add(g1.getGoSave_Trans_Ref());
				}
			}
		}

		return result;
	}

	public Estatement check_gosave_duplicate_interest(Estatement e, ArrayList<GoSave> gosave_trans_list) {
		
		ArrayList<String> result = check_gosave_trans_list_duplicate(gosave_trans_list);

		if(result.size()>0) {
			//System.out.println(e.getCustomer_id()+": "+result.toString());
			ArrayList<String> r= e.getGoSave_fail_trans_Ref_from_coreaccount_list();
			if(r==null||r.size()==0) {
				e.setGoSave_Amount_list_in_coreaccount_trans_history(result);
			}else if(r.size()>0){
				r.addAll(result);
				e.setGoSave_Amount_list_in_coreaccount_trans_history(r);
			}
		}
		return e;
	}

	
	public static ArrayList<String> check_GoSave_fail_trans_Ref_for_amount_single(ArrayList<GoSave> gosave_trans_list) {
		ArrayList<String> gosave_trans_ref_list_for_fail_amount_single = new ArrayList<>();
		for(int i =0;i<gosave_trans_list.size();i++) {

			/**
			if(!Common.is_have_digit(gosave_trans_list.get(i).getID())) {
				//暂时没有输出到报告中
				System.out.println(e.getCustomer_id()+" "+gosave_trans_list.get(i).getGosave_trans_Date()+" gosave transaction record description error!" );
			}
			**/

			if(!gosave_trans_list.get(i).isIs_amount_have_right_single()) {
				gosave_trans_ref_list_for_fail_amount_single.add(gosave_trans_list.get(i).getGoSave_Trans_Ref());
			}
		}
		return gosave_trans_ref_list_for_fail_amount_single;
	}
	public static Estatement set_GoSave_fail_trans_Ref_for_amount_single(Estatement e, ArrayList<GoSave> gosave_trans_list) {
		ArrayList<String> gosave_trans_ref_list_for_fail_amount_single = check_GoSave_fail_trans_Ref_for_amount_single(gosave_trans_list);
		if(gosave_trans_ref_list_for_fail_amount_single.size()>0) {
			e.setGoSave_fail_trans_Ref_for_amount_single(gosave_trans_ref_list_for_fail_amount_single);
		}else {
		}
		return e;
	}

	public static Estatement Set_Gosave_fail_amout_list(Estatement estatement, ArrayList<GoSave> gosave_trans_list) {
		ArrayList<String> gosave_trans_amount_list = new ArrayList<>();
		for(int i =0;i<gosave_trans_list.size();i++) {
			gosave_trans_amount_list.add(gosave_trans_list.get(i).getAmount());
		}
		//ArrayList<String> coreaccount_gosave_trans = estatement.getGoSave_Amount_list_in_coreaccount_trans_history();
		ArrayList<String> result =Common.Compate_list(estatement, gosave_trans_list);
		ArrayList<String> result1 =Common.Compate_list_according_gosave_tran(estatement, gosave_trans_list);
		result.addAll(result1);
		if(result.size()>0) {
			estatement.setGoSave_fail_trans_Ref_from_coreaccount_list(result);
		}
		return estatement;
	}
	
	public static Estatement Set_Gosave_fail_amout_list_with_gosave1_gosave2(Estatement estatement) {
		ArrayList<GoSave> gosave_trans_all = new ArrayList<GoSave>();
		if(!(estatement.getGoSave_trans_history()==null)) {
			if( estatement.getGoSave_trans_history().size()>0) {
			gosave_trans_all= estatement.getGoSave_trans_history();
			}
		}
		if(!(estatement.getGoSave2_trans_history()==null)) {
			if( estatement.getGoSave2_trans_history().size()>0) {
				gosave_trans_all.addAll(estatement.getGoSave2_trans_history());
			}
		}
		if( gosave_trans_all.size()==0) {
			return estatement;
		}
		//System.out.println("gosave_trans_all.size(): "+gosave_trans_all.size());
		ArrayList<String> result =Common.Compate_list(estatement, gosave_trans_all);
		ArrayList<String> result1 =Common.Compate_list_according_gosave_tran(estatement, gosave_trans_all);
		result.addAll(result1);
		if(result.size()>0) {
			//System.out.println("result1.size(): "+result1.size());
			estatement.setGoSave_fail_trans_Ref_from_coreaccount_list(result);
		}
		return estatement;
	}


	//	GoSave利息	本金轉至核心賬戶
	public static GoSave get_gosave_type(GoSave  g,String[] content_list,int line_index,Estatement e,String gosave_amount) {
		//这两种情况type只有一行，金额都为负数
		//25 Jan 2021 INTEREST REPAYMENT GoSave 435 Time Deposit -301.83
		//25 Jan 2021 Debit Arrangement GoSave 435 Time Deposit -99,999.00

		boolean is_right_amount_single=true;

		String[] checK_gosave_amount_need_single= {"INTEREST REPAYMENT","Debit Arrangement",
				Global.GOSAVE_PRINCIPAL_TRANSFERRED_TO_CORE_ACCOUNT_STRING,Global.GOSAVE_PRINCIPAL_TRANSFERRED_TO_CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE,
				Global.GOSAVE_INTEREST_TRANSFER_TO_CORE_ACCOUNT_STRING,Global.GOSAVE_INTEREST_TRANSFER_TO_CORE_ACCOUNT_STRING_SIMPLIFIED_CHINESE};
		String[] checK_gosave_amount_not_need_single= {Global.GOSAVE_INTEREST_STRING,Global.GOSAVE_INTEREST_STRING_SIMPLIFIED_CHINESE,
				Global.DEPOSIT_TO_GOSAVE_STRING,Global.DEPOSIT_TO_GOSAVE_STRING_SIMPLIFIED_CHINESE};
		String line =content_list[line_index-1];
		String amount_line =content_list[line_index];
		String type = "";
		if(amount_line.contains("INTEREST REPAYMENT")){
			type = "INTEREST REPAYMENT";
		}else if(amount_line.contains("Debit Arrangement")){
			type = "Debit Arrangement";
		}else if(Common.isContainsGosaveInterestString(line)){
			type = Global.GOSAVE_INTEREST_STRING;
		}else if(Common.isContainsGosavePrincipalTransferredToCoreAccountString(line)) {
			type = Global.GOSAVE_PRINCIPAL_TRANSFERRED_TO_CORE_ACCOUNT_STRING;
		}else if(Common.isContainsGosaveInterestTransferToCoreAccountString(line)){
			type = Global.GOSAVE_INTEREST_TRANSFER_TO_CORE_ACCOUNT_STRING;
		}else if(Common.isContainsDepositToGosaveString(line)){
			type = Global.DEPOSIT_TO_GOSAVE_STRING;
		}else{
			type = line;
		}
		g.setType(type);

		if(Common.is_one_of_shuzu_item_in_line(type, checK_gosave_amount_need_single)) {
			if(!gosave_amount.contains("-")) {
				is_right_amount_single=false;
			}
		}
		if(Common.is_one_of_shuzu_item_in_line(type, checK_gosave_amount_not_need_single)) {
			if(gosave_amount.contains("-")) {
				is_right_amount_single=false;
			}
		}
		g.setIs_amount_have_right_single(is_right_amount_single);
		return g;
	}

	public static ArrayList<GoSave>  set_one_gosave_trans(ArrayList<GoSave> gosave_trans_list, String line,int amount_index,
			int id_index,Estatement e,String[] content_list,int line_index,int gosave_trans_account_number) {
		String gosave_amount = Common.get_value_string_from_line(line, amount_index);
		String gosave_id = Common.get_value_string_from_line(line, id_index);
		GoSave gosave = new GoSave();
		gosave.setGosave_trans_account_number(gosave_trans_account_number);
		gosave.setID(gosave_id);
		gosave.setAmount(gosave_amount);
		gosave = getGosaveTransactionRef(gosave, content_list[line_index+1]);
		gosave = get_gosave_type(gosave, content_list, line_index,e,gosave_amount);
		gosave=Common.get_gosave_Date_from_line(line, e, gosave);
		gosave_trans_list.add(gosave);
		//System.out.println(gosave.toString());
		return gosave_trans_list;
	}
	
	public static GoSave getGosaveTransactionRef(GoSave gosave, String line) {
		//Mar 2024 Interest transferred to core -0.01
		//Ref: AAACT240782F1HRHMT
		//18 Mar 2024 Principal transferred to core -6.00
		//Ref: AAACT240782F1HRHV8
		//17 Mar 2024 -0.03
		//Others Ref: AAACT2407751KB6N78
		if(line.contains("Ref:")) {
			String line1 = line.split("Ref:")[1];
			String ref = line1.trim().split(" ")[0].trim();
			gosave.setRef(ref);
		}
		return gosave;
	}

	public static HashMap<String, String> sort_map(HashMap<String, String> map) {
		HashMap<String, String> new_map= new HashMap<String, String>();
		Set<String> set =map.keySet();
		ArrayList<String> arrayList =new ArrayList<String>(set);
		Collections.sort(arrayList);//从小到大的排序
		for (int len = arrayList.size(),i = len-1; i >= 0; i--) {
			new_map.put(arrayList.get(i),map.get(arrayList.get(i)));
			//System.out.println("key键---值: "+arrayList.get(i)+","+map.get(arrayList.get(i)));
		}
		return new_map;
	}

	@SuppressWarnings({ "rawtypes"})
	public void print_current_gosave(HashMap<String, String> current_gosave_exist) {
		Iterator iter = current_gosave_exist.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			Object key = entry.getKey();
			Object val = entry.getValue();
			System.out.println("GoSave " + key+" current amount: "+val);
		}
	}
	
	public String get_GoSave_id(String line) {
		String id =line.split(" ")[1].trim();
		return id.replace("-", "");
	}

}
