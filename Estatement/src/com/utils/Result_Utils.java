package com.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.entity.Estatement;
import com.entity.Ref;


class Result_Utils{
	ArrayList<String> fail_Customer_id_list = new ArrayList<>();
	public static void main(String[] args) throws IOException {
		Result_Utils r = new Result_Utils();
		ArrayList<Estatement> Estatement_List = new ArrayList<>();
		ArrayList<Ref> refs_List = new ArrayList<>();
		String folder_path = "D:\\Project\\e-Statement\\estatement_pdf\\2021_03_03_estatement";
		r.Print_result_to_Excel(Estatement_List, folder_path,refs_List);
	}
	
	
	public Result_Utils() {}
	private ArrayList<String> Spec_FPS_account;
	public Result_Utils(ArrayList<String> Spec_FPS_account) {
		this.Spec_FPS_account =Spec_FPS_account;
	}

	//首页4行检查   ok
	//户口总览检查    ok
	//检查desc    ok 
	//检查Gosave 的transaction 是否与核心账户匹配   ok
	//检查reference no.  Ref 的开头
	//付款的金额需要带有“-”    Send money to    ok
	//收款的金额不能带有“-”    Receive money from    ok
	//'"Deposit Interest" should be shown under transaction description.  
	//检查Type是否显示正确
	//检查每页 transaction的title   ok 
	//交易日期 種類 進支詳情 當地貨幣 金額 (港幣)
	//Transaction Date Transaction Description Currency Amount Amount (HKD)


	/*
	String[] title_list= {"Customer_ID","Account_ID","月份", "核心账户(首页)","GoSave余额(首页)",
			"GoSave2.0", "美元", "投资账户",
			"结存总额(首页) ","私人贷款账户(首页) ",
			"承上余额","核心账户(转账)","核心账户计算余额",
			"GoSave 计算余额","GoSave2.0 计算余额",
			"结存总额计算余额","Loans抓取余额","核心账户测试结果","下月转账记录","Loans测试结果","Interest利息",
			"检查Description","检查Gosave记录","检查Ref开头","金额符号是否正常","General Information","Invaild transaction",
			//"美元计算余额","投资账户计算余额",
			"Comment","Test Result"};
	*/
	String[] title_list= {"Customer_ID","Account_ID","月份", 
			"核心账户(首页)","核心账户(港币)","核心账户(美元)",
			"GoSave2.0", "GoSave2(港币)", "GoSave2(美元)",
			"投资账户","结存总额(首页) ","私人贷款账户(首页) ",
			"港币承上余额","港币核心账户(户口结余)","港币核心账户计算",
			"美元承上余额","美元核心账户(户口结余)","美元核心账户计算",
			"GoSave2港币计算余额","GoSave2美元计算余额",
			"结存总额计算余额","Loans抓取余额","核心账户测试结果","下月转账记录","Loans测试结果","Interest利息",
			"检查Description","检查Gosave记录","检查Ref开头","金额符号是否正常","General Information","Invaild transaction",
			//"美元计算余额","投资账户计算余额",
			"Comment","Test Result"};
	
	public String Print_result_to_Excel(ArrayList<Estatement> Estatement_List,String folder_path,ArrayList<Ref> fail_refs) {
		String filename = folder_path+"/eStatement_result_"+Common.get_time_string()+".xlsx";
		//创建工作薄对象
		FileOutputStream out;
		try {
			XSSFWorkbook workbook=new XSSFWorkbook();//这里也可以设置sheet的Name
			XSSFSheet sheet = workbook.createSheet();//创建工作表对象

			//int col1width = sheet.getColumnWidth(0);
			//System.out.println("col1width:  "+col1width);
			//sheet.setColumnWidth(0, 2*col1width);
			for(int i=0;i<title_list.length;i++) {sheet.setColumnWidth(i, 3500);}
			//创建工作表的行
			XSSFRow row = sheet.createRow(0);//设置第一行，从零开始
			for(int i=0;i<title_list.length;i++) {
				XSSFCellStyle yellow_style = SetCellStyle(workbook, 2);
				XSSFCell c = row.createCell(i);
				c.setCellStyle(yellow_style);
				c.setCellValue(title_list[i]);
			}
			for(int i=0;i<Estatement_List.size();i++) {
				Estatement e = Estatement_List.get(i);
				XSSFRow r= sheet.createRow(i+1);
				int cell_column=0;
				StringBuffer stringBuffer = new StringBuffer();
				for(int j=0;j<title_list.length;j++) {
					write_cell(r,cell_column,e,workbook,stringBuffer);
					cell_column+=1;
				}
			}

			int row_number  = sheet.getLastRowNum();
			XSSFCellStyle yellow_style_3 = SetCellStyle(workbook, 3);
			XSSFCell cell=sheet.createRow(row_number+2).createCell(0);
			cell.setCellValue("注：当承上余额为 '-' 时，默认设置为0！");
			cell.setCellStyle(yellow_style_3);

			int row_num =row_number+3;
			sheet.createRow(row_num).createCell(0).setCellValue("Column V: 根据Ref No.找到etatement中transactions,确认description是否正确！");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("Column W: 根据Ref No.找到etatement中GoSave transactions,确认在Gosave transaction中是否有对应的记录！");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("Column X: 检查Ref 的开头， 其中\"收款\",\"付款\",\"Debit Card 消費\",\"現金回贈\",\"銀通櫃員機\",\"海外櫃員機\",\"存入款項\"为 FT ， 存入Gosave的Ref开头为 AAAC ！");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("Column Y: 检查金额的正负号是否正确， \"付款\",\"Debit Card 消費\",\"存入GoSave \",\" 銀通櫃員機提款\",\"海外櫃員機提款\" 的金额是否带有“-” ， 收款不能带有“-” ！");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("Column Z: 检查estatement中每一页的General Information(包括：你的銀行月結單Your Bank Statement,列印日期 Date of issue,賬戶號碼 Account Number,銀行代碼 Bank Code/分行編號 Branch Code: 390 / 750)是否正确,！");
			row_num +=1;
			sheet.createRow(row_num).createCell(0).setCellValue("Column AA: 检查estatement中Invaild transaction(是否成对出现，金额是否分别为正负并且相等，ref是否相同)");


			workbook.setSheetName(0,"Result");
			
			for(int i =0;i<fail_Customer_id_list.size();i++) {
				workbook.createSheet();
				workbook.setSheetName(i+1,fail_Customer_id_list.get(i));
			}
			
			//对文件中错误的地方进行截图
			//PictureShot pictureShot = new PictureShot();
			//workbook=pictureShot.Generate_picture(Estatement_List,fail_Customer_id_list,workbook,folder_path);
			
			
			out = new FileOutputStream(filename);
			workbook.write(out);
			out.close();
			create_refs_report(folder_path,fail_refs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filename;
	}
	public ArrayList<String> get_fail_Customer_id_list() {
		return fail_Customer_id_list;
	}

	public void create_result_report(String folder_path) {
		String filename = folder_path+"/Result_"+Common.get_date_string()+".xlsx";
		//创建工作薄对象
		FileOutputStream out;

		try {
			XSSFWorkbook workbook=new XSSFWorkbook();//这里也可以设置sheet的Name
			workbook.createSheet();
			workbook.setSheetName(0,"Result");
			for(int i =0;i<fail_Customer_id_list.size();i++) {
				//XSSFSheet sheet = workbook.createSheet();
				workbook.createSheet();
				workbook.setSheetName(i+1,fail_Customer_id_list.get(i));
			}
			out = new FileOutputStream(filename);
			workbook.write(out);
			out.close();
		} catch (Exception e) {
		}
	}
	


	public void write_cell(XSSFRow r,int index,Estatement e,XSSFWorkbook workbook,StringBuffer stringBuffer) {

		
		XSSFCellStyle green_style = SetCellStyle(workbook, 1);
		XSSFCellStyle red_style = SetCellStyle(workbook, 0);
		XSSFCellStyle yellow_style_3 = SetCellStyle(workbook, 3);
		XSSFCellStyle grey_style_4 = SetCellStyle(workbook, 4);
		XSSFCell c =r.createCell(index);

		String cell_content ="";
		//if(index==0) cell_content= e.getCustomer_id()+"/"+e.getFile_name();
		if(index==0) {
			cell_content= e.getCustomer_id();
			c.setCellStyle(yellow_style_3);
		}
		if(index==1) {
			cell_content= e.getAccount_id();
			c.setCellStyle(yellow_style_3);
		}
		if(index==2) {
			cell_content= e.getWhich_month();
			c.setCellStyle(yellow_style_3);
		}

		if(index==3) {
			cell_content= e.getCoreaccount_total_capture_title();
			c.setCellStyle(green_style);
		}
		if(index==4) {
			cell_content= e.getCoreAccountHkdTitle();
			c.setCellStyle(green_style);
		}
		if(index==5) {
			cell_content= e.getCoreAccountUsdTitle();
			c.setCellStyle(green_style);
		}
		
		if(index==6) {
			cell_content= e.getGosave2_total_capture_title();
			c.setCellStyle(green_style);
		}
		if(index==7) {
			cell_content= e.getGosave2_hkd_capture_title();
			c.setCellStyle(green_style);
		}
		if(index==8) {
			cell_content= e.getGosave2_usd_capture_title();
			c.setCellStyle(green_style);
		}
		if(index==9) {
			cell_content= e.getInvestmentAccountAmount();
			c.setCellStyle(green_style);
		}
		if(index==10) {
			cell_content= e.getTotal_Amount_capture_title();
			if(cell_content.contains("-")&&(!e.isIs_account_close())) {
				c.setCellStyle(red_style);
			}else{
				c.setCellStyle(green_style);
			}
			if(cell_content.contains("-")&&(Common.is_amount_zero(e.getTotal_Amount_calculate()))) {
				c.setCellStyle(green_style);
			}
		}
		
		if(index==11) {
			cell_content= e.getLaon_total_capture_title();
			if(cell_content==null||cell_content.length()<2) {
				cell_content="Not Applicable";
			}else {}
			//如果抓取的金额“-”在后面
			if(Common.is_valued_number(cell_content)||cell_content==null) {
				c.setCellStyle(green_style);
			}else  if(cell_content.contains("Not Applicable")) {
				c.setCellStyle(grey_style_4);
			}else {
				c.setCellStyle(red_style);
			}
		}
		if(index==12) {
			cell_content= e.getLast_month_amount();
			if(cell_content==null||cell_content.contains("-")) {
				c.setCellStyle(red_style);
			}else {
				c.setCellStyle(green_style);
			}
		}
		if(index==13) {
			cell_content= e.getCoreaccount_total_hkd_capture();
			if(cell_content==null) {
				c.setCellStyle(red_style);
				stringBuffer.append("在核心账户转账记录最后一条没有抓取到 帐户结余 Closing Balance;\n");
			}else if(cell_content.contains("-")) {
				c.setCellStyle(green_style);
			}else {
				c.setCellStyle(green_style);
			}
		}
		if(index==14) {
			cell_content= e.getCoreaccount_total_hkd_calculate();
			if(Common.compare_amount_string_to_double(e.getCoreaccount_total_hkd_calculate(), e.getCoreAccountHkdTitle())
					||e.isIs_account_close()) {
				c.setCellStyle(green_style);
			}else {
				String temp = "核心账户计算港币总金额为： "+e.getCoreaccount_total_hkd_calculate()+", 抓取港币金额为： "+e.getCoreAccountHkdTitle()+", 金额不匹配;";
				//stringBuffer.append("核心账户金额计算金额错误;\n");
				stringBuffer.append(temp+"\n");
				c.setCellStyle(red_style);
			}
		}
		
		if(index==15) {
			cell_content= e.getUsd_last_month_amount();
			if(cell_content==null||cell_content.contains("-")) {
				c.setCellStyle(red_style);
			}else {
				c.setCellStyle(green_style);
			}
		}
		
		if(index==16) {
			cell_content= e.getCoreaccount_total_usd_capture();
			if(cell_content==null) {
				c.setCellStyle(red_style);
				stringBuffer.append("在美元转账记录最后一条没有抓取到 帐户结余 Closing Balance;\n");
			}else if(cell_content.contains("-")) {
				c.setCellStyle(green_style);
			}else {
				c.setCellStyle(green_style);
			}
		}		
		if(index==17) {
			cell_content= e.getCoreaccount_total_usd_calculate();
			if(Common.compare_amount_string_to_double(e.getCoreaccount_total_usd_capture(), e.getCoreaccount_total_usd_calculate())) {
				c.setCellStyle(green_style);
			}else {
				c.setCellStyle(red_style);
				stringBuffer.append("美元核心账户金额计算金额错误;\n");
			}
		}
		
		if(index==18) {
			cell_content= e.getGosave2_hkd_total_calculate();
			if(!e.isIs_have_gosave2_hkd_record()) {
				c.setCellStyle(green_style);
			}else if(Common.compare_amount_string_to_double(e.getGosave2_hkd_total_calculate(), e.getGosave2_hkd_capture_title())){
				c.setCellStyle(green_style);
			}else {
				stringBuffer.append("GoSave2 港币 金额计算金额错误;\n");
				c.setCellStyle(red_style);
			}
		}
		
		if(index==19) {
			cell_content= e.getGosave2_usd_total_calculate();
			if(!e.isIs_have_gosave2_usd_record()) {
				c.setCellStyle(green_style);
			}else if(e.isGo_save2_usd_result()){
				c.setCellStyle(green_style);
			}else {
				stringBuffer.append("GoSave2 美元 金额计算金额错误;\n");
				c.setCellStyle(red_style);
			}
		}
		
		if(index==20) {
			cell_content= e.getTotal_Amount_calculate();
			if(e.isIs_total_amount_equal()) {
				c.setCellStyle(green_style);
			}else {
				c.setCellStyle(red_style);
				stringBuffer.append("结存总额金额计算金额错误;\n");
			}
			if(e.isIs_account_close()) {
				c.setCellStyle(green_style);
			}
			if(e.getTotal_Amount_capture_title().contains("-")&&(Common.is_amount_zero(e.getTotal_Amount_calculate()))) {
				c.setCellStyle(green_style);
			}
		}

		if(index==21) {
			cell_content= e.getLaon_total_calculate();
			if(!e.isIs_have_loan_record()) {
				c.setCellStyle(green_style);
			}else if(Common.compare_amount_string_to_double(e.getLaon_total_calculate(), e.getLaon_total_capture_title())) {
				c.setCellStyle(green_style);
			}else {
				c.setCellStyle(red_style);
			}
		}
		if(index==22) {
			cell_content= e.isCore_account_hkd_result()+"";
			set_cell_style_for_boolean_value(cell_content, green_style, red_style, c);
		}
		if(index==23) {
			cell_content= e.isIs_have_next_month_tran()+"";
			set_cell_style_for_boolean_value(cell_content, green_style, red_style, c);
		}
		if(index==24) {
			if(!e.isIs_have_loan_record()) {
				cell_content= "true";
				c.setCellStyle(green_style);
			}else {
				if(e.getLoan_fail_trans_Ref_for_record().size()>0) {
					cell_content= e.getLoan_fail_trans_Ref_for_record().toString();
					stringBuffer.append("贷款记录内容不正确;\n");
					c.setCellStyle(red_style);
				}else if(!Common.compare_amount_string_to_double(e.getLaon_total_calculate(), e.getLaon_total_capture_title())) {
					c.setCellStyle(red_style);
					cell_content= "false";
					stringBuffer.append("贷款金额不匹配;\n");
				}else {
					cell_content= "true";
					c.setCellStyle(green_style);
				}
			}
		}
		if(index==25) {
			cell_content= e.isIs_check_interest()+"";
			if(!e.isIs_check_interest()) {
				stringBuffer.append("利息异常,需要显示利息;\n");
			}
			set_cell_style_for_boolean_value(cell_content, green_style, red_style, c);
		}
		
		
		if(index==26) {
			if(e.getFail_trans_Ref_list_for_desc().size()>0) {
				stringBuffer.append("部分记录description错误;\n");
			}
			cell_content= e.getFail_trans_Ref_list_for_desc().toString();
			setColumnStyle(e.getFail_trans_Ref_list_for_desc(), green_style, red_style, c);
		}

		if(index==27) {
			ArrayList<String> s= e.getGoSave_fail_trans_Ref_from_coreaccount_list();
			if(s==null) {
				cell_content="";
			}else {
				stringBuffer.append("Gosave记录和Core Accout中记录不匹配;\n");
				cell_content = s.toString();
			}
			ArrayList<String> s1= e.getGoSave2_Exist_Held_Duplicate_Reference_Number();
			if(s1==null||s1.size()==0) {
				cell_content= cell_content +"";
			}else {
				stringBuffer.append("现在持有的Gosave有重复的记录;\n");
				cell_content = cell_content + s1.toString();
			}
			if(cell_content.length()>1) {
				c.setCellStyle(red_style);
			}else {
				c.setCellStyle(green_style);
			}
		}
		
		if(index==28) {
			cell_content= e.getFail_start_Ref_list().toString();
			if(e.getFail_start_Ref_list().size()>0) {
				stringBuffer.append("转账记录Ref的开头错误;\n");
			}
			setColumnStyle(e.getFail_start_Ref_list(), green_style, red_style, c);
		}
		if(index==29) {
			cell_content= e.getWrong_single_amount_of_ref_list().toString();  //		"金额符号异常"
			if(e.getWrong_single_amount_of_ref_list().size()>0) {
				stringBuffer.append("部分转账记录金额符号错误，需要检查是否带“-”;\n");
			}
			setColumnStyle(e.getWrong_single_amount_of_ref_list(), green_style, red_style, c);
		}
		if(index==30) {
			cell_content= e.isIs_page_title_right()+"";  //"General Information"
			if(!e.isIs_page_title_right()){
				stringBuffer.append("eSatement每页中的Title有错误;\n");
			}
			set_cell_style_for_boolean_value(cell_content, green_style, red_style, c);
		}
		if(index==31) {
			if(!Spec_FPS_account.contains(e.getCustomer_id())) {
				cell_content= e.getFail_trans_Ref_list_for_Invalid_trans().toString();  //Invalid_trans
			}
			if(e.getFail_trans_Ref_list_for_Invalid_trans().size()>0) {
				//stringBuffer.append("Invalid transaction记录异常;\n");
				if(!Spec_FPS_account.contains(e.getCustomer_id())) {
					stringBuffer.append("Some FPS Sender/Receiver are missing;\n");
				}
			}
			if(e.getFail_trans_Ref_list_for_Invalid_trans().size()>0){
				if(!Spec_FPS_account.contains(e.getCustomer_id())) {
					c.setCellStyle(red_style);
				}else {
					c.setCellStyle(green_style);
				}
			}else {
				c.setCellStyle(green_style);
			}
		}

		if(index==32) {
			if(!Common.checkForComment()) {
				stringBuffer.setLength(0);
			}
			cell_content= stringBuffer.toString();
			if(cell_content.length()>1) {
				int index_enter = cell_content.lastIndexOf("\n");
				cell_content = cell_content.substring(0,index_enter);
				c.setCellStyle(red_style);
				if(!fail_Customer_id_list.contains(e.getCustomer_id())) {
					fail_Customer_id_list.add(e.getCustomer_id());
				}
			}else {
				c.setCellStyle(green_style);
			}
		}
		if(index==33) {
			//last update
			//stringBuffer.setLength(0);
			if(!Common.checkForComment()) {
				stringBuffer.setLength(0);
			}
			cell_content= stringBuffer.toString();
			if(cell_content.length()>1) {
				cell_content = "Fail";
				c.setCellStyle(red_style);
			}else {
				c.setCellStyle(green_style);
				cell_content = "Pass";
			}
		}
		c.setCellValue(cell_content);
	}


	private void set_cell_style_for_boolean_value(String cell_content, XSSFCellStyle green_style,
			XSSFCellStyle red_style, XSSFCell c) {
		if(cell_content.contains("false")) {
			c.setCellStyle(red_style);
		}else {
			c.setCellStyle(green_style);
		}
	}


	private void setColumnStyle(ArrayList<String>  list, XSSFCellStyle green_style, XSSFCellStyle red_style, XSSFCell c) {
		if(list==null || list.size()==0){
			c.setCellStyle(green_style);
		}else {
			c.setCellStyle(red_style);
		}
	}

	public static XSSFCellStyle SetCellStyle(XSSFWorkbook Workbook,int flag) {
		XSSFCellStyle style=Workbook.createCellStyle();
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); 
		if(flag ==0) {
			//last update
			if(Common.checkForComment()) {
				style.setFillForegroundColor(HSSFColor.RED.index);
			}else {
				style.setFillForegroundColor(HSSFColor.GREEN.index);
			}
		}
		if(flag ==1) {
			style.setFillForegroundColor(HSSFColor.GREEN.index);
		}
		if(flag ==2) {
			XSSFFont font = Workbook.createFont();
			font.setFontName("黑体");
			font.setFontHeightInPoints((short) 12);//设置字体大小
			style.setFont(font);//选择需要用到的字体格式
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setVerticalAlignment(VerticalAlignment.TOP);
			style.setAlignment(HorizontalAlignment.JUSTIFY);
		}
		if(flag ==3) {
			style.setFillForegroundColor(HSSFColor.YELLOW.index);
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT); 
		}

		if(flag ==4) {
			style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);

		}
		/***
		style.setFillForegroundColor(HSSFColor.BLUE.index);
		style.setFillForegroundColor(HSSFColor.GREEN.index);
		style.setFillForegroundColor(HSSFColor.ORANGE.index);
		style.setFillForegroundColor(HSSFColor.RED.index);
		style.setFillForegroundColor(HSSFColor.BROWN.index);
		 ***/
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		return style;
	}
	public void create_refs_report(String folder_path,ArrayList<Ref> fail_refs) {
		String filename = folder_path+Common.fail_refs_fileName;
		//创建工作薄对象
		FileOutputStream out;
		String[] ref_title= {"Customer_ID","FileName","Path","Refs"};

		try {
			XSSFWorkbook workbook=new XSSFWorkbook();//这里也可以设置sheet的Name
			XSSFSheet sheet0 = workbook.createSheet();
			workbook.setSheetName(0,"Result");
			//创建工作表的第一行
			XSSFRow row = sheet0.createRow(0);//设置第一行，从零开始
			row.setHeightInPoints(30);
			for(int i=0;i<ref_title.length;i++) {
				XSSFCellStyle yellow_style = SetCellStyle(workbook, 2);
				XSSFCell c = row.createCell(i);
				c.setCellStyle(yellow_style);
				c.setCellValue(ref_title[i]);
			}
			for(int i =0;i<fail_refs.size();i++) {
				XSSFRow row1 = sheet0.createRow(i+1);
				XSSFCell cell_ID = row1.createCell(0);
				XSSFCell cell_filename = row1.createCell(1);
				XSSFCell cell_path = row1.createCell(2);
				XSSFCell cell_Refs = row1.createCell(3);
				cell_ID.setCellValue(fail_refs.get(i).getCustomer_ID());
				cell_filename.setCellValue(fail_refs.get(i).getFile_name());
				cell_path.setCellValue(fail_refs.get(i).getPath());
				cell_Refs.setCellValue(fail_refs.get(i).getFail_Ref_list().toString());
			}
			out = new FileOutputStream(filename);
			workbook.write(out);
			out.close();
		} catch (Exception e) {
		}
	}



	public void Print_result_to_Excel_1(ArrayList<Estatement> Estatement_List,String folder_path) {
		//Workbook WK;

		try {


			//String filename = "/eStatement_result_"+Common.get_time_string()+".xlsx";

			//WritableWorkbook workbook = Workbook.createWorkbook(new File(filename));
			//WritableSheet  sheet=workbook.getSheet(0);
			//sheet.setName("Result");
			//			WritableSheet s = workbook.getSheet(0);
			//			WritableCell cell = sheet.getWritableCell(2,3);  
			//			        WritableSheet sheet = workbook.getSheet(0);  
			//			        sheet.setName("修改后"); // 给sheet页改名  
			//			        workbook.removeSheet(2); // 移除多余的标签页  
			//			        workbook.removeSheet(3);  
			//			          
			//			        sheet.mergeCells(0, 0, 4, 0); // 合并单元格  
			//			        sheet.setRowView(0, 600); // 设置行的高度  
			//			        sheet.setColumnView(0, 30); // 设置列的宽度  
			//			        sheet.setColumnView(1, 20); // 设置列的宽度  

			//			InputStream inputStream=new FileInputStream(new File(filename));
			//OutputStream outputStream=new FileOutputStream(new File(filename));


		} catch (Exception e) {
			e.printStackTrace();
		}
	}



}
