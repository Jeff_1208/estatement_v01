
8000000028
你的銀行月結單  Your Bank Statement (1 Jul 2023 - 31 Jul 2023)
列印日期 Date of Issue: 2 Aug 2023
賬戶號碼 Account Number: 1000000192
銀行代碼 Bank Code / 分行編號 Branch Code: 390 / 750
LAW, Wing Mei Nicole
你的戶口總覽 截至2023年07月31日 (港元等值)
Your Financial Status As at 31 Jul 2023 (HKD Equivalent)
核心賬戶 Core Account 1,709.75
外幣賬戶 Foreign Currency Account
美元 USD 0.00
投資賬戶 Investment Account 8,487.47
結存總額 Net Position 10,197.22
交易紀錄 Transaction History
核心賬戶 Core Account (1000000192)
交易日期 種類 進支詳情 金額 (港元)
Transaction Date Type Transaction Description Amount (HKD)
01 Jul 2023 承上結餘 Balance From Previous Statement 2,765.83
存款利息 Deposit interest
1 Jul 2023 0.02
Deposit interest Ref: AAACT2318117HC1ND2
付款 Send money to LAW, W*** M** N*****
3 Jul 2023 -100.00
Send money Ref: FT23184QBW0Q
付款 Send money to OOH L* L* N****
3 Jul 2023 -572.00
Send money Ref: FT23184L870N
付款 Send money to HO K** V******** A** F****
6 Jul 2023 -301.00
Send money Ref: FT23187T3PRV
付款 Send money to CHAN H** L** J*****
7 Jul 2023 -45.00
Send money Ref: FT23188HNGZ3
賬單/商戶繳付 Pay to DIVIT LTD
10 Jul 2023 -695.00
Bill/merchant payment Ref: FT2319183CV5
外幣兌換
USD/ HKD @ 7.85606923, debit date: 10 Jul 2023
10 Jul 2023 Foreign currency exchange -340.00
Ref: FX0000009387
基金申購費 Debit date: 11 Jul 2023
10 Jul 2023 -5.10
Fund subscription fee Ref: SE0000013523
收款 Receive money from LAW Wing Mei Nicole
10 Jul 2023 2,000.00
Receive money Ref: FT23191K1JVM
Debit Card 消費 DELIVEROO PLUS HK HKG Online 5814
19 Jul 2023 -98.00
Debit Card spending Ref: FT23200NPG3F
收款 Receive money from LAW, Wing Mei Nicole
19 Jul 2023 2,000.00
Receive money Ref: FT23200HVR1K
付款 Send money to LAW, W*** M** N*****
19 Jul 2023 -2,500.00
Send money Ref: FT23200YC616
付款 Send money to SHAM S*** P* B******* C***
20 Jul 2023 -200.00
Send money Ref: FT2320119QW9
付款 Send money to SHAM S*** P* B******* C***
23 Jul 2023 -200.00
Send money Ref: FT232043CVK0
Page 1 of 3
你的銀行月結單  Your Bank Statement (1 Jul 2023 - 31 Jul 2023)
列印日期 Date of Issue: 2 Aug 2023
賬戶號碼 Account Number: 1000000192
銀行代碼 Bank Code / 分行編號 Branch Code: 390 / 750
交易日期 種類 進支詳情 金額 (港元)
Transaction Date Type Transaction Description Amount (HKD)
31 Jul 2023 戶口結餘 Closing Balance 1,709.75
外幣賬戶 - 美元 Foreign Currency Account - USD (1004019793)
交易日期 種類 進支詳情 金額 (美元)
Transaction Date Type Transaction Description Amount (USD)
01 Jul 2023 承上結餘 Balance From Previous Statement 0.00
外幣兌換 USD/ HKD @ 7.85606923, credit date: 10 Jul 2023
10 Jul 2023 Foreign currency exchange 43.28
Ref: FX0000009387
基金申購 Debit date: 11 Jul 2023
10 Jul 2023 -43.28
Fund subscription Ref: SE0000013523
31 Jul 2023 戶口結餘 Closing Balance 0.00
「GoSave」定期存款 “GoSave” Time Deposit
目前持有 Currently Held
由 至 存款編號 利息 (年利率) 起存金額 (港幣) 結餘 (港元)
From To Deposit Number Interest Rate (p.a.) Opening Deposit Amount (HKD) Balance (HKD)
GoSave  - 交易紀錄 Transaction History
交易日期 種類 交易詳情 金額 (港元)
Transaction Date Type Transaction Description Amount (HKD)
「GoSave 2.0」定期存款 “GoSave 2.0” Time Deposit
目前持有 Currently Held
由 至 參考編號 利息 (年利率) 存款期 結餘 (港元)
From To Reference Number Interest Rate (p.a.) Tenor Balance (HKD)
GoSave 2.0 - 交易紀錄 Transaction History
交易日期 種類 交易詳情 金額 (港元)
Transaction Date Type Transaction Description Amount (HKD)
Page 2 of 3
你的銀行月結單  Your Bank Statement (1 Jul 2023 - 31 Jul 2023)
列印日期 Date of Issue: 2 Aug 2023
賬戶號碼 Account Number: 1000000192
銀行代碼 Bank Code / 分行編號 Branch Code: 390 / 750
重要提示 IMPORTANT NOTES
? 客戶 （「你」）可在登入WeLab Bank app 後按「我的賬戶」>「協助」>「存款利率」，以瀏覽匯立銀行有限公司 （「我們」） 最
新的存款利率。你亦可在「我的賬戶」>「關於我們」>「一般服務收費」了解我們的收費。
Customers ("You" or "Your") can browse through WeLab Bank Limited ("We" or "Our") latest interest rate at "My account" > "Support" >
"Deposit interest rate" on our WeLab Bank app. You can also browse our fees and charges at "My account" > "About us" > "General
service charges" on our WeLab Bank app.
? GoSave 定期存款部份所顯示的金額並不包含累計利息。
The balance shown in the GoSave Time Deposit section excludes accrued interest.
? 你可查閱及下載長達7年的電子月結單，亦可打印或下載你的電子月結單以記錄所需信息。如你下載電子月結單到任何電腦或電子設
備，請你確保該電腦及電子設備保安的安全，並防止其他人士查閱任何機密資料，包括電子月結單。
You may view and download eStatement(s) of up to 7 years in our app, and you can print or download a copy of your eStatement(s) for
record purposes. If you download an eStatement to any computer or electronic device, please ensure that the device is secured to
prevent others from accessing any confidential information, including the eStatement.
? 我們提醒你請注意冒充我們職員的欺詐來電、語音訊息、電郵、短訊或其他偽冒方式，請不要向可疑第三者提供任何敏感個人資料。
如你懷疑曾向可疑人士披露個人資料，請致電我們的24小時客戶服務熱線 （852） 3898 6988 核實來電者身份及立即向警方報案。
We remind you to be alert to fraudulent phone calls, voice messages, emails, SMS or communications in other formats imitating our
staff. Please be reminded to protect your personal information and not provide any sensitive information to suspicious callers. If you
have disclosed your personal details, please contact our 24-hour Customer Service hotline at (852) 3898 6988 to verify the caller’s
identity, and report to the Police immediately.
? 如有任何因使用Debit Card 而引致的錯漏，必須要於月結單列印日後60日內通知我們。如有其他錯漏（如轉賬或GoSave定期存款的
交易），亦須於月結單列印日後90日内通知我們。請參閱我們的「賬戶條款」了解詳情。
All errors or discrepancies on the use of the Debit Card should be reported to the Bank within 60 calendar days from the eStatement
date. For any other errors or discrepancies (such as transfer or GoSave time deposit transaction), you must report to the Bank within 90
calendar days from the eStatement date. Please refer to our Account Terms for details.
? 如你對賬單有任何問題，你可在登入WeLab Bank app，進入「我的賬戶」後按「協助」，致電客戶服務熱線 （852） 3898 6988 或
發送電郵至wecare@welab.bank 聯絡我們。
If you have any queries concerning your statement, you can contact us by login the WeLab Bank app and tap "My account" then tap
"Support". You can also call our Customer Service hotline at (852) 3898 6988 or email us at wecare@welab.bank.
? 結存總額為核心賬戶之結餘及 (如有) GoSave結餘、外幣賬戶之港元等值結餘及投資賬戶之港元等值結餘之總和，僅供參考。
Net Position is the sum of the Core Account balance, and (if any) GoSave balance, the Hong Kong Dollar equivalent value of the Foreign
Currency Account balance and the Hong Kong Dollar equivalent value of the Investment Account balance, which is for reference only.
? 如你持有外幣賬戶及/或投資賬戶，本結單所載之港元等值 (包括但不限於外幣賬戶及投資賬戶結存之港元等值) 是以我們於本結單期
末之參考外幣匯率計算並僅供參考。
If you have a Foreign Currency Account and/or an Investment Account, the Hong Kong Dollar equivalent values shown in this statement
(including but not limited to the balances of the Foreign Currency Account and the Investment Account) are calculated based on our
reference exchange rate at the end of the statement period and are for reference only.
? 如你持有投資賬戶，本結單只顯示投資賬戶於本結單期末的結餘金額，該金額以我們於本結單期末之的相關基金參考單位價格計算，
你於本結單期內的基金持倉、基金交易及/或你持有的基金之公司行動之詳情將於你的投資結單內顯示，你須將該投資結單與此結單一
併參閱。
If you have an Investment Account, this statement will only show the closing balance of the Investment Account as at the end of the
statement period, which is calculated based on our reference unit price of relevant Funds at the end of the statement period.  Details of
your holding and/or transactions of Funds and/or the corporation actions of your Fund holdings within the statement period will be
shown in the relevant Investment Statement, which should be read in conjunction with this statement.
# 上述交易已在月結單當月前進行，惟未曾結算， 該筆金額現已結算。
The above transaction took place prior to the current statement month and was previously unsettled. The transaction has now been
settled.
Page 3 of 3

