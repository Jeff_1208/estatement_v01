package com.entity;

public class CashDividend {
	private String fundName;
	private String dividendPaidDATE;
	private String dividendCurrency;
	private String dividendAmount;
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	public String getDividendPaidDATE() {
		return dividendPaidDATE;
	}
	public void setDividendPaidDATE(String dividendPaidDATE) {
		this.dividendPaidDATE = dividendPaidDATE;
	}
	public String getDividendCurrency() {
		return dividendCurrency;
	}
	public void setDividendCurrency(String dividendCurrency) {
		this.dividendCurrency = dividendCurrency;
	}
	public String getDividendAmount() {
		return dividendAmount;
	}
	public void setDividendAmount(String dividendAmount) {
		this.dividendAmount = dividendAmount;
	}
	@Override
	public String toString() {
		return "CashDividend [fundName=" + fundName + ", dividendPaidDATE=" + dividendPaidDATE + ", dividendCurrency="
				+ dividendCurrency + ", dividendAmount=" + dividendAmount + "]";
	}

}


