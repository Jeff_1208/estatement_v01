package com.entity;

public class AccountDetails {
	private String goalName;
	private String startValueDollar;  
	private String endValueDollar;  
	private String endValueHKD;
	private boolean isDollarFund=true;
	public String getGoalName() {
		return goalName;
	}
	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}
	public String getStartValueDollar() {
		return startValueDollar;
	}
	public void setStartValueDollar(String startValueDollar) {
		this.startValueDollar = startValueDollar;
	}
	public String getEndValueDollar() {
		return endValueDollar;
	}
	public void setEndValueDollar(String endValueDollar) {
		this.endValueDollar = endValueDollar;
	}
	public String getEndValueHKD() {
		return endValueHKD;
	}
	public void setEndValueHKD(String endValueHKD) {
		this.endValueHKD = endValueHKD;
	}
	@Override
	public String toString() {
		return "AccountDetails [goalName=" + goalName + ", startValueDollar=" + startValueDollar + ", endValueDollar="
				+ endValueDollar + ", endValueHKD=" + endValueHKD + "]";
	}
	public boolean isDollarFund() {
		return isDollarFund;
	}
	public void setDollarFund(boolean isDollarFund) {
		this.isDollarFund = isDollarFund;
	} 
	
}
