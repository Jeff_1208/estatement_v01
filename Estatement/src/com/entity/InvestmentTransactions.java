package com.entity;


public class InvestmentTransactions {
	private String transactionDate;
	private String settlementDate;
	private String transactionType;
	private String transactionID;
	private String transactionDescription;
	private String transactionAmount;
	private String Subscription;
	private String NoOfUnit;
	private String NavOfUnit;
	private boolean isDividend=false;
	private boolean checkTBC=true;
	private boolean checkType=true;
	private boolean checkTransactionID=true;
	private boolean is_HDK_transation=false;
	
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public String getTransactionDescription() {
		return transactionDescription;
	}
	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}
	public String getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public String getSubscription() {
		return Subscription;
	}
	public void setSubscription(String subscription) {
		Subscription = subscription;
	}
	public String getNoOfUnit() {
		return NoOfUnit;
	}
	public void setNoOfUnit(String noOfUnit) {
		NoOfUnit = noOfUnit;
	}
	public String getNavOfUnit() {
		return NavOfUnit;
	}
	public void setNavOfUnit(String navOfUnit) {
		NavOfUnit = navOfUnit;
	}
	public boolean isCheckTBC() {
		return checkTBC;
	}
	public void setCheckTBC(boolean checkTBC) {
		this.checkTBC = checkTBC;
	}
	public boolean isCheckType() {
		return checkType;
	}
	public void setCheckType(boolean checkType) {
		this.checkType = checkType;
	}
	public boolean isCheckTransactionID() {
		return checkTransactionID;
	}
	public void setCheckTransactionID(boolean checkTransactionID) {
		this.checkTransactionID = checkTransactionID;
	}
	@Override
	public String toString() {
		return "InvestmentTransactions [transactionDate=" + transactionDate + ", settlementDate=" + settlementDate
				+ ", transactionType=" + transactionType + ", transactionID=" + transactionID
				+ ", transactionDescription=" + transactionDescription + ", transactionAmount=" + transactionAmount
				+ ", Subscription=" + Subscription + ", NoOfUnit=" + NoOfUnit + ", NavOfUnit=" + NavOfUnit
				+ ", checkTBC=" + checkTBC + ", checkType=" + checkType + ", checkTransactionID=" + checkTransactionID
				+ "]";
	}
	public boolean isDividend() {
		return isDividend;
	}
	public void setDividend(boolean isDividend) {
		this.isDividend = isDividend;
	}
	public boolean isIs_HDK_transation() {
		return is_HDK_transation;
	}
	public void setIs_HDK_transation(boolean is_HDK_transation) {
		this.is_HDK_transation = is_HDK_transation;
	}

}
