package com.entity;

public class IndividualFundPositions {
	private String fundName;
	private String startUnitNumber;
	private String endUnitNumber;
	private String navPerUnit;
	private String marketValueDollar;
	private String marketValueHKD;
	private boolean isDividend=false;
	private boolean isDollarFund;
	
	public String getFundName() {
		return fundName;
	}
	public void setFundName(String fundName) {
		this.fundName = fundName;
	}
	public String getStartUnitNumber() {
		return startUnitNumber;
	}
	public void setStartUnitNumber(String startUnitNumber) {
		this.startUnitNumber = startUnitNumber;
	}
	public String getEndUnitNumber() {
		return endUnitNumber;
	}
	public void setEndUnitNumber(String endUnitNumber) {
		this.endUnitNumber = endUnitNumber;
	}
	public String getNavPerUnit() {
		return navPerUnit;
	}
	public void setNavPerUnit(String navPerUnit) {
		this.navPerUnit = navPerUnit;
	}
	public String getMarketValueDollar() {
		return marketValueDollar;
	}
	public void setMarketValueDollar(String marketValueDollar) {
		this.marketValueDollar = marketValueDollar;
	}
	public String getMarketValueHKD() {
		return marketValueHKD;
	}
	public void setMarketValueHKD(String marketValueHKD) {
		this.marketValueHKD = marketValueHKD;
	}
	public boolean isDividend() {
		return isDividend;
	}
	public void setDividend(boolean isDividend) {
		this.isDividend = isDividend;
	}
	@Override
	public String toString() {
		return "IndividualFundPositions [fundName=" + fundName + ", startUnitNumber=" + startUnitNumber
				+ ", endUnitNumber=" + endUnitNumber + ", navPerUnit=" + navPerUnit + ", marketValueDollar="
				+ marketValueDollar + ", marketValueHKD=" + marketValueHKD + "]";
	}
	public boolean isDollarFund() {
		return isDollarFund;
	}
	public void setDollarFund(boolean isDollarFund) {
		this.isDollarFund = isDollarFund;
	}
	
}
