package com.entity;

import java.util.ArrayList;
import java.util.HashMap;

public class Wealth {

	private double FxRate;
	private String content;
	private String CustomerID;
	private String thisMonth;
	private String thisYear;
	
	private ArrayList<AccountDetails> AccountDetailsList = new ArrayList<>();
	private ArrayList<IndividualFundPositions> IndividualFundPositionsList= new ArrayList<>();
	private ArrayList<TransactionHistoryAndPositions> TransactionHistoryAndPositionsList= new ArrayList<>();
	private ArrayList<InvestmentTransactions> InvestmentTransactionsList= new ArrayList<>();
	private ArrayList<CashDividend> CashDividendList= new ArrayList<>();
	private ArrayList<TransactionHistoryAndPositions> FeaturedFundsServicesInventmentHoldings= new ArrayList<>(); 
	private ArrayList<InvestmentTransactions> FeaturedFundsServicesInventmentTransactions= new ArrayList<>(); 
	private ArrayList<String> FeaturedFundsServicesFailList= new ArrayList<>();

	private String TotalCashDividend="0.0";
	private String accountTotalDollarCalc;
	private String accountTotalDollarCapture;
	private String accountTotalHKDCalc;
	private String accountTotalHKDCapture;
	private boolean isAccountDetailsDollarEqualHKD=false;
	private boolean isAccountDetailsTotalDollar=false;
	private boolean isAccountDetailsTotalHKD=false;
	private boolean isAccountDetailsEachItemPass=true;
	private boolean isTotalHKDShow=true;
	private boolean isTotalUSDShow=true;
	
	private String IndividualFundPositionsTotalDollarCalc;
	private String IndividualFundPositionsHKDCalc;
	private boolean isIndividualFundPositionsEachItemPass=true;
	private boolean isIndividualFundPositionsEachItemOfNAVPass=true;
	private boolean isIndividualFundPositionsTotalHKDPass=false;
	private boolean isIndividualFundPositionsTotalDollarPass=false;
	private boolean isIndividualFundPositionsDollarEqualHKDPass=false;

	private String TransactionHistoryAndPositionsTotalDollarCalc;
	private String TransactionHistoryAndPositionsHKDCalc;
	private boolean isTransactionHistoryAndPositionsEachItemPass=true;
	private boolean isTransactionHistoryAndPositionsEachItemOfNAVPass=true;
	private boolean isTransactionHistoryAndPositionsTotalHKDPass=false;
	private boolean isTransactionHistoryAndPositionsTotalDollarPass=false;
	private boolean isTransactionHistoryAndPositionsDollarEqualHKDPass=false;
	
	private boolean isFeaturedFundsServicesInventmentHoldingsEachItemPass=true;
	private boolean isFeaturedFundsServicesInventmentHoldingsEachItemOfNAVPass=true;
	private boolean isFeaturedFundsServicesInventmentHoldingsTotalDollarPass=false;
	private boolean isFeaturedFundsServicesInventmentHoldingsTotalHKDPass=false;
	private boolean isFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass=false;
	
	private boolean isFeaturedFundsServicesInventmentTransactionsEachItemPass=true;
	private boolean isFeaturedFundsServicesInventmentTransactionsEachItemOfNAVPass=true;
	private boolean isFeaturedFundsServicesInventmentTransactionsTotalHKDPass=false;
	private boolean isFeaturedFundsServicesInventmentTransactionsTotalDollarPass=false;
	private boolean isFeaturedFundsServicesInventmentTransactionsDollarEqualHKDPass=false;
	
	private ArrayList<String> checkSettlementDateList= new ArrayList<>();
	private ArrayList<String> checkTransactionTypeList= new ArrayList<>();
	private ArrayList<String> checkTransactionIdList= new ArrayList<>();
	private HashMap<String, String> investmentTransactionsMap;
	private HashMap<String, String> featureFundsServiceInvestmentTransactionsMap;
	
	private ArrayList<String> AccountDetailsFailList= new ArrayList<>();
	private ArrayList<String> IndividualFundPositionsFailList= new ArrayList<>();
	private ArrayList<String> TransactionHistoryAndPositionsFailList= new ArrayList<>();
	private ArrayList<String> InvestmentTransactionsFailList= new ArrayList<>();
	private ArrayList<String> InvestmentNameList= new ArrayList<>();
	
	private boolean isHaveSelfFund=false;
	private String FeaturedFundsStartValueDollar="0.0";
	private String FeaturedFundsEndValueDollar="0.0";
	private String FeaturedFundsEndValueHKD="0.0";
	
	
	private String HKDFeaturedFundsStartValue="0.0";
	private String HKDFeaturedFundsEndValue="0.0";
	private boolean isHKDFeaturedFundsTotalHKDPass=true;
	private boolean isAccountTotalFormat=true;
	
	public double getFxRate() {
		return FxRate;
	}
	public void setFxRate(double fxRate) {
		FxRate = fxRate;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	public ArrayList<AccountDetails> getAccountDetailsList() {
		return AccountDetailsList;
	}
	public void setAccountDetailsList(ArrayList<AccountDetails> accountDetailsList) {
		AccountDetailsList = accountDetailsList;
	}
	public ArrayList<IndividualFundPositions> getIndividualFundPositionsList() {
		return IndividualFundPositionsList;
	}
	public void setIndividualFundPositionsList(ArrayList<IndividualFundPositions> individualFundPositionsList) {
		IndividualFundPositionsList = individualFundPositionsList;
	}
	public ArrayList<TransactionHistoryAndPositions> getTransactionHistoryAndPositionsList() {
		return TransactionHistoryAndPositionsList;
	}
	public void setTransactionHistoryAndPositionsList(
			ArrayList<TransactionHistoryAndPositions> transactionHistoryAndPositionsList) {
		TransactionHistoryAndPositionsList = transactionHistoryAndPositionsList;
	}
	public ArrayList<InvestmentTransactions> getInvestmentTransactionsList() {
		return InvestmentTransactionsList;
	}
	public void setInvestmentTransactionsList(ArrayList<InvestmentTransactions> investmentTransactionsList) {
		InvestmentTransactionsList = investmentTransactionsList;
	}
	public String getAccountTotalDollarCalc() {
		return accountTotalDollarCalc;
	}
	public void setAccountTotalDollarCalc(String accountTotalDollarCalc) {
		this.accountTotalDollarCalc = accountTotalDollarCalc;
	}
	public String getAccountTotalDollarCapture() {
		return accountTotalDollarCapture;
	}
	public void setAccountTotalDollarCapture(String accountTotalDollarCapture) {
		this.accountTotalDollarCapture = accountTotalDollarCapture;
	}
	public String getAccountTotalHKDCalc() {
		return accountTotalHKDCalc;
	}
	public void setAccountTotalHKDCalc(String accountTotalHKDCalc) {
		this.accountTotalHKDCalc = accountTotalHKDCalc;
	}
	public String getAccountTotalHKDCapture() {
		return accountTotalHKDCapture;
	}
	public void setAccountTotalHKDCapture(String accountTotalHKDCapture) {
		this.accountTotalHKDCapture = accountTotalHKDCapture;
	}
	public boolean isAccountDetailsDollarEqualHKD() {
		return isAccountDetailsDollarEqualHKD;
	}
	public void setAccountDetailsDollarEqualHKD(boolean isAccountDetailsDollarEqualHKD) {
		this.isAccountDetailsDollarEqualHKD = isAccountDetailsDollarEqualHKD;
	}
	public boolean isAccountDetailsTotalDollar() {
		return isAccountDetailsTotalDollar;
	}
	public void setAccountDetailsTotalDollar(boolean isAccountDetailsTotalDollar) {
		this.isAccountDetailsTotalDollar = isAccountDetailsTotalDollar;
	}
	public boolean isAccountDetailsTotalHKD() {
		return isAccountDetailsTotalHKD;
	}
	public void setAccountDetailsTotalHKD(boolean isAccountDetailsTotalHKD) {
		this.isAccountDetailsTotalHKD = isAccountDetailsTotalHKD;
	}
	public boolean isAccountDetailsEachItemPass() {
		return isAccountDetailsEachItemPass;
	}
	public void setAccountDetailsEachItemPass(boolean isAccountDetailsEachItemPass) {
		this.isAccountDetailsEachItemPass = isAccountDetailsEachItemPass;
	}
	public String getIndividualFundPositionsTotalDollarCalc() {
		return IndividualFundPositionsTotalDollarCalc;
	}
	public void setIndividualFundPositionsTotalDollarCalc(String individualFundPositionsTotalDollarCalc) {
		IndividualFundPositionsTotalDollarCalc = individualFundPositionsTotalDollarCalc;
	}
	public String getIndividualFundPositionsHKDCalc() {
		return IndividualFundPositionsHKDCalc;
	}
	public void setIndividualFundPositionsHKDCalc(String individualFundPositionsHKDCalc) {
		IndividualFundPositionsHKDCalc = individualFundPositionsHKDCalc;
	}
	public boolean isIndividualFundPositionsEachItemPass() {
		return isIndividualFundPositionsEachItemPass;
	}
	public void setIndividualFundPositionsEachItemPass(boolean isIndividualFundPositionsEachItemPass) {
		this.isIndividualFundPositionsEachItemPass = isIndividualFundPositionsEachItemPass;
	}
	public boolean isIndividualFundPositionsEachItemOfNAVPass() {
		return isIndividualFundPositionsEachItemOfNAVPass;
	}
	public void setIndividualFundPositionsEachItemOfNAVPass(boolean isIndividualFundPositionsEachItemOfNAVPass) {
		this.isIndividualFundPositionsEachItemOfNAVPass = isIndividualFundPositionsEachItemOfNAVPass;
	}
	public boolean isIndividualFundPositionsTotalHKDPass() {
		return isIndividualFundPositionsTotalHKDPass;
	}
	public void setIndividualFundPositionsTotalHKDPass(boolean isIndividualFundPositionsTotalHKDPass) {
		this.isIndividualFundPositionsTotalHKDPass = isIndividualFundPositionsTotalHKDPass;
	}
	public boolean isIndividualFundPositionsTotalDollarPass() {
		return isIndividualFundPositionsTotalDollarPass;
	}
	public void setIndividualFundPositionsTotalDollarPass(boolean isIndividualFundPositionsTotalDollarPass) {
		this.isIndividualFundPositionsTotalDollarPass = isIndividualFundPositionsTotalDollarPass;
	}
	public boolean isIndividualFundPositionsDollarEqualHKDPass() {
		return isIndividualFundPositionsDollarEqualHKDPass;
	}
	public void setIndividualFundPositionsDollarEqualHKDPass(boolean isIndividualFundPositionsDollarEqualHKDPass) {
		this.isIndividualFundPositionsDollarEqualHKDPass = isIndividualFundPositionsDollarEqualHKDPass;
	}
	public String getTransactionHistoryAndPositionsTotalDollarCalc() {
		return TransactionHistoryAndPositionsTotalDollarCalc;
	}
	public void setTransactionHistoryAndPositionsTotalDollarCalc(String transactionHistoryAndPositionsTotalDollarCalc) {
		TransactionHistoryAndPositionsTotalDollarCalc = transactionHistoryAndPositionsTotalDollarCalc;
	}
	public String getTransactionHistoryAndPositionsHKDCalc() {
		return TransactionHistoryAndPositionsHKDCalc;
	}
	public void setTransactionHistoryAndPositionsHKDCalc(String transactionHistoryAndPositionsHKDCalc) {
		TransactionHistoryAndPositionsHKDCalc = transactionHistoryAndPositionsHKDCalc;
	}
	public boolean isTransactionHistoryAndPositionsEachItemPass() {
		return isTransactionHistoryAndPositionsEachItemPass;
	}
	public void setTransactionHistoryAndPositionsEachItemPass(boolean isTransactionHistoryAndPositionsEachItemPass) {
		this.isTransactionHistoryAndPositionsEachItemPass = isTransactionHistoryAndPositionsEachItemPass;
	}
	public boolean isTransactionHistoryAndPositionsEachItemOfNAVPass() {
		return isTransactionHistoryAndPositionsEachItemOfNAVPass;
	}
	public void setTransactionHistoryAndPositionsEachItemOfNAVPass(
			boolean isTransactionHistoryAndPositionsEachItemOfNAVPass) {
		this.isTransactionHistoryAndPositionsEachItemOfNAVPass = isTransactionHistoryAndPositionsEachItemOfNAVPass;
	}
	public boolean isTransactionHistoryAndPositionsTotalHKDPass() {
		return isTransactionHistoryAndPositionsTotalHKDPass;
	}
	public void setTransactionHistoryAndPositionsTotalHKDPass(boolean isTransactionHistoryAndPositionsTotalHKDPass) {
		this.isTransactionHistoryAndPositionsTotalHKDPass = isTransactionHistoryAndPositionsTotalHKDPass;
	}
	public boolean isTransactionHistoryAndPositionsTotalDollarPass() {
		return isTransactionHistoryAndPositionsTotalDollarPass;
	}
	public void setTransactionHistoryAndPositionsTotalDollarPass(boolean isTransactionHistoryAndPositionsTotalDollarPass) {
		this.isTransactionHistoryAndPositionsTotalDollarPass = isTransactionHistoryAndPositionsTotalDollarPass;
	}
	public boolean isTransactionHistoryAndPositionsDollarEqualHKDPass() {
		return isTransactionHistoryAndPositionsDollarEqualHKDPass;
	}
	public void setTransactionHistoryAndPositionsDollarEqualHKDPass(
			boolean isTransactionHistoryAndPositionsDollarEqualHKDPass) {
		this.isTransactionHistoryAndPositionsDollarEqualHKDPass = isTransactionHistoryAndPositionsDollarEqualHKDPass;
	}
	public ArrayList<String> getCheckSettlementDateList() {
		return checkSettlementDateList;
	}
	public void setCheckSettlementDateList(ArrayList<String> checkSettlementDateList) {
		this.checkSettlementDateList = checkSettlementDateList;
	}
	public ArrayList<String> getCheckTransactionTypeList() {
		return checkTransactionTypeList;
	}
	public void setCheckTransactionTypeList(ArrayList<String> checkTransactionTypeList) {
		this.checkTransactionTypeList = checkTransactionTypeList;
	}
	public ArrayList<String> getCheckTransactionIdList() {
		return checkTransactionIdList;
	}
	public void setCheckTransactionIdList(ArrayList<String> checkTransactionIdList) {
		this.checkTransactionIdList = checkTransactionIdList;
	}
	public ArrayList<String> getAccountDetailsFailList() {
		return AccountDetailsFailList;
	}
	public void setAccountDetailsFailList(ArrayList<String> accountDetailsFailList) {
		AccountDetailsFailList = accountDetailsFailList;
	}
	public ArrayList<String> getIndividualFundPositionsFailList() {
		return IndividualFundPositionsFailList;
	}
	public void setIndividualFundPositionsFailList(ArrayList<String> individualFundPositionsFailList) {
		IndividualFundPositionsFailList = individualFundPositionsFailList;
	}
	public ArrayList<String> getTransactionHistoryAndPositionsFailList() {
		return TransactionHistoryAndPositionsFailList;
	}
	public void setTransactionHistoryAndPositionsFailList(ArrayList<String> transactionHistoryAndPositionsFailList) {
		TransactionHistoryAndPositionsFailList = transactionHistoryAndPositionsFailList;
	}
	public ArrayList<String> getInvestmentTransactionsFailList() {
		return InvestmentTransactionsFailList;
	}
	public void setInvestmentTransactionsFailList(ArrayList<String> investmentTransactionsFailList) {
		InvestmentTransactionsFailList = investmentTransactionsFailList;
	}
	public HashMap<String, String> getInvestmentTransactionsMap() {
		return investmentTransactionsMap;
	}
	public void setInvestmentTransactionsMap(HashMap<String, String> investmentTransactionsMap) {
		this.investmentTransactionsMap = investmentTransactionsMap;
	}
	public String getThisMonth() {
		return thisMonth;
	}
	public void setThisMonth(String thisMonth) {
		this.thisMonth = thisMonth;
	}
	public String getThisYear() {
		return thisYear;
	}
	public void setThisYear(String thisYear) {
		this.thisYear = thisYear;
	}
	public ArrayList<String> getInvestmentNameList() {
		return InvestmentNameList;
	}
	public void setInvestmentNameList(ArrayList<String> investmentNameList) {
		InvestmentNameList = investmentNameList;
	}
	public boolean isTotalHKDShow() {
		return isTotalHKDShow;
	}
	public void setTotalHKDShow(boolean isTotalHKDShow) {
		this.isTotalHKDShow = isTotalHKDShow;
	}
	public boolean isTotalUSDShow() {
		return isTotalUSDShow;
	}
	public void setTotalUSDShow(boolean isTotalUSDShow) {
		this.isTotalUSDShow = isTotalUSDShow;
	}
	public ArrayList<CashDividend> getCashDividendList() {
		return CashDividendList;
	}
	public void setCashDividendList(ArrayList<CashDividend> cashDividendList) {
		CashDividendList = cashDividendList;
	}
	public String getFeaturedFundsStartValueDollar() {
		return FeaturedFundsStartValueDollar;
	}
	public void setFeaturedFundsStartValueDollar(String featuredFundsStartValueDollar) {
		FeaturedFundsStartValueDollar = featuredFundsStartValueDollar;
	}
	public String getFeaturedFundsEndValueDollar() {
		return FeaturedFundsEndValueDollar;
	}
	public void setFeaturedFundsEndValueDollar(String featuredFundsEndValueDollar) {
		FeaturedFundsEndValueDollar = featuredFundsEndValueDollar;
	}
	public String getFeaturedFundsEndValueHKD() {
		return FeaturedFundsEndValueHKD;
	}
	public void setFeaturedFundsEndValueHKD(String featuredFundsEndValueHKD) {
		FeaturedFundsEndValueHKD = featuredFundsEndValueHKD;
	}
	public ArrayList<TransactionHistoryAndPositions> getFeaturedFundsServicesInventmentHoldings() {
		return FeaturedFundsServicesInventmentHoldings;
	}
	public void setFeaturedFundsServicesInventmentHoldings(
			ArrayList<TransactionHistoryAndPositions> featuredFundsServicesInventmentHoldings) {
		FeaturedFundsServicesInventmentHoldings = featuredFundsServicesInventmentHoldings;
	}
	public ArrayList<InvestmentTransactions> getFeaturedFundsServicesInventmentTransactions() {
		return FeaturedFundsServicesInventmentTransactions;
	}
	public void setFeaturedFundsServicesInventmentTransactions(
			ArrayList<InvestmentTransactions> featuredFundsServicesInventmentTransactions) {
		FeaturedFundsServicesInventmentTransactions = featuredFundsServicesInventmentTransactions;
	}
	public ArrayList<String> getFeaturedFundsServicesFailList() {
		return FeaturedFundsServicesFailList;
	}
	public void setFeaturedFundsServicesFailList(ArrayList<String> featuredFundsServicesFailList) {
		FeaturedFundsServicesFailList = featuredFundsServicesFailList;
	}
	public boolean isFeaturedFundsServicesInventmentHoldingsTotalDollarPass() {
		return isFeaturedFundsServicesInventmentHoldingsTotalDollarPass;
	}
	public void setFeaturedFundsServicesInventmentHoldingsTotalDollarPass(
			boolean isFeaturedFundsServicesInventmentHoldingsTotalDollarPass) {
		this.isFeaturedFundsServicesInventmentHoldingsTotalDollarPass = isFeaturedFundsServicesInventmentHoldingsTotalDollarPass;
	}
	public boolean isFeaturedFundsServicesInventmentHoldingsTotalHKDPass() {
		return isFeaturedFundsServicesInventmentHoldingsTotalHKDPass;
	}
	public void setFeaturedFundsServicesInventmentHoldingsTotalHKDPass(
			boolean isFeaturedFundsServicesInventmentHoldingsTotalHKDPass) {
		this.isFeaturedFundsServicesInventmentHoldingsTotalHKDPass = isFeaturedFundsServicesInventmentHoldingsTotalHKDPass;
	}
	public boolean isFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass() {
		return isFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass;
	}
	public void setFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass(
			boolean isFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass) {
		this.isFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass = isFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass;
	}
	public boolean isFeaturedFundsServicesInventmentHoldingsEachItemPass() {
		return isFeaturedFundsServicesInventmentHoldingsEachItemPass;
	}
	public void setFeaturedFundsServicesInventmentHoldingsEachItemPass(
			boolean isFeaturedFundsServicesInventmentHoldingsEachItemPass) {
		this.isFeaturedFundsServicesInventmentHoldingsEachItemPass = isFeaturedFundsServicesInventmentHoldingsEachItemPass;
	}
	public boolean isFeaturedFundsServicesInventmentHoldingsEachItemOfNAVPass() {
		return isFeaturedFundsServicesInventmentHoldingsEachItemOfNAVPass;
	}
	public void setFeaturedFundsServicesInventmentHoldingsEachItemOfNAVPass(
			boolean isFeaturedFundsServicesInventmentHoldingsEachItemOfNAVPass) {
		this.isFeaturedFundsServicesInventmentHoldingsEachItemOfNAVPass = isFeaturedFundsServicesInventmentHoldingsEachItemOfNAVPass;
	}
	public boolean isFeaturedFundsServicesInventmentTransactionsEachItemPass() {
		return isFeaturedFundsServicesInventmentTransactionsEachItemPass;
	}
	public void setFeaturedFundsServicesInventmentTransactionsEachItemPass(
			boolean isFeaturedFundsServicesInventmentTransactionsEachItemPass) {
		this.isFeaturedFundsServicesInventmentTransactionsEachItemPass = isFeaturedFundsServicesInventmentTransactionsEachItemPass;
	}
	public boolean isFeaturedFundsServicesInventmentTransactionsEachItemOfNAVPass() {
		return isFeaturedFundsServicesInventmentTransactionsEachItemOfNAVPass;
	}
	public void setFeaturedFundsServicesInventmentTransactionsEachItemOfNAVPass(
			boolean isFeaturedFundsServicesInventmentTransactionsEachItemOfNAVPass) {
		this.isFeaturedFundsServicesInventmentTransactionsEachItemOfNAVPass = isFeaturedFundsServicesInventmentTransactionsEachItemOfNAVPass;
	}
	public boolean isFeaturedFundsServicesInventmentTransactionsTotalHKDPass() {
		return isFeaturedFundsServicesInventmentTransactionsTotalHKDPass;
	}
	public void setFeaturedFundsServicesInventmentTransactionsTotalHKDPass(
			boolean isFeaturedFundsServicesInventmentTransactionsTotalHKDPass) {
		this.isFeaturedFundsServicesInventmentTransactionsTotalHKDPass = isFeaturedFundsServicesInventmentTransactionsTotalHKDPass;
	}
	public boolean isFeaturedFundsServicesInventmentTransactionsTotalDollarPass() {
		return isFeaturedFundsServicesInventmentTransactionsTotalDollarPass;
	}
	public void setFeaturedFundsServicesInventmentTransactionsTotalDollarPass(
			boolean isFeaturedFundsServicesInventmentTransactionsTotalDollarPass) {
		this.isFeaturedFundsServicesInventmentTransactionsTotalDollarPass = isFeaturedFundsServicesInventmentTransactionsTotalDollarPass;
	}
	public boolean isFeaturedFundsServicesInventmentTransactionsDollarEqualHKDPass() {
		return isFeaturedFundsServicesInventmentTransactionsDollarEqualHKDPass;
	}
	public void setFeaturedFundsServicesInventmentTransactionsDollarEqualHKDPass(
			boolean isFeaturedFundsServicesInventmentTransactionsDollarEqualHKDPass) {
		this.isFeaturedFundsServicesInventmentTransactionsDollarEqualHKDPass = isFeaturedFundsServicesInventmentTransactionsDollarEqualHKDPass;
	}
	
	public HashMap<String, String> getFeatureFundsServiceInvestmentTransactionsMap() {
		return featureFundsServiceInvestmentTransactionsMap;
	}
	public void setFeatureFundsServiceInvestmentTransactionsMap(
			HashMap<String, String> featureFundsServiceInvestmentTransactionsMap) {
		this.featureFundsServiceInvestmentTransactionsMap = featureFundsServiceInvestmentTransactionsMap;
	}
	
	public String getTotalCashDividend() {
		return TotalCashDividend;
	}
	public void setTotalCashDividend(String totalCashDividend) {
		TotalCashDividend = totalCashDividend;
	}
	@Override
	public String toString() {
		return "Wealth [FxRate=" + FxRate + ", content=" + content + ", CustomerID=" + CustomerID + ", thisMonth="
				+ thisMonth + ", thisYear=" + thisYear + ", AccountDetailsList=" + AccountDetailsList
				+ ", IndividualFundPositionsList=" + IndividualFundPositionsList
				+ ", TransactionHistoryAndPositionsList=" + TransactionHistoryAndPositionsList
				+ ", InvestmentTransactionsList=" + InvestmentTransactionsList + ", CashDividendList="
				+ CashDividendList + ", FeaturedFundsServicesInventmentHoldings="
				+ FeaturedFundsServicesInventmentHoldings + ", FeaturedFundsServicesInventmentTransactions="
				+ FeaturedFundsServicesInventmentTransactions + ", FeaturedFundsServicesFailList="
				+ FeaturedFundsServicesFailList + ", accountTotalDollarCalc=" + accountTotalDollarCalc
				+ ", accountTotalDollarCapture=" + accountTotalDollarCapture + ", accountTotalHKDCalc="
				+ accountTotalHKDCalc + ", accountTotalHKDCapture=" + accountTotalHKDCapture
				+ ", isAccountDetailsDollarEqualHKD=" + isAccountDetailsDollarEqualHKD
				+ ", isAccountDetailsTotalDollar=" + isAccountDetailsTotalDollar + ", isAccountDetailsTotalHKD="
				+ isAccountDetailsTotalHKD + ", isAccountDetailsEachItemPass=" + isAccountDetailsEachItemPass
				+ ", isTotalHKDShow=" + isTotalHKDShow + ", isTotalUSDShow=" + isTotalUSDShow
				+ ", IndividualFundPositionsTotalDollarCalc=" + IndividualFundPositionsTotalDollarCalc
				+ ", IndividualFundPositionsHKDCalc=" + IndividualFundPositionsHKDCalc
				+ ", isIndividualFundPositionsEachItemPass=" + isIndividualFundPositionsEachItemPass
				+ ", isIndividualFundPositionsEachItemOfNAVPass=" + isIndividualFundPositionsEachItemOfNAVPass
				+ ", isIndividualFundPositionsTotalHKDPass=" + isIndividualFundPositionsTotalHKDPass
				+ ", isIndividualFundPositionsTotalDollarPass=" + isIndividualFundPositionsTotalDollarPass
				+ ", isIndividualFundPositionsDollarEqualHKDPass=" + isIndividualFundPositionsDollarEqualHKDPass
				+ ", TransactionHistoryAndPositionsTotalDollarCalc=" + TransactionHistoryAndPositionsTotalDollarCalc
				+ ", TransactionHistoryAndPositionsHKDCalc=" + TransactionHistoryAndPositionsHKDCalc
				+ ", isTransactionHistoryAndPositionsEachItemPass=" + isTransactionHistoryAndPositionsEachItemPass
				+ ", isTransactionHistoryAndPositionsEachItemOfNAVPass="
				+ isTransactionHistoryAndPositionsEachItemOfNAVPass + ", isTransactionHistoryAndPositionsTotalHKDPass="
				+ isTransactionHistoryAndPositionsTotalHKDPass + ", isTransactionHistoryAndPositionsTotalDollarPass="
				+ isTransactionHistoryAndPositionsTotalDollarPass
				+ ", isTransactionHistoryAndPositionsDollarEqualHKDPass="
				+ isTransactionHistoryAndPositionsDollarEqualHKDPass
				+ ", isFeaturedFundsServicesInventmentHoldingsEachItemPass="
				+ isFeaturedFundsServicesInventmentHoldingsEachItemPass
				+ ", isFeaturedFundsServicesInventmentHoldingsEachItemOfNAVPass="
				+ isFeaturedFundsServicesInventmentHoldingsEachItemOfNAVPass
				+ ", isFeaturedFundsServicesInventmentHoldingsTotalDollarPass="
				+ isFeaturedFundsServicesInventmentHoldingsTotalDollarPass
				+ ", isFeaturedFundsServicesInventmentHoldingsTotalHKDPass="
				+ isFeaturedFundsServicesInventmentHoldingsTotalHKDPass
				+ ", isFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass="
				+ isFeaturedFundsServicesInventmentHoldingsDollarEqualHKDPass
				+ ", isFeaturedFundsServicesInventmentTransactionsEachItemPass="
				+ isFeaturedFundsServicesInventmentTransactionsEachItemPass
				+ ", isFeaturedFundsServicesInventmentTransactionsEachItemOfNAVPass="
				+ isFeaturedFundsServicesInventmentTransactionsEachItemOfNAVPass
				+ ", isFeaturedFundsServicesInventmentTransactionsTotalHKDPass="
				+ isFeaturedFundsServicesInventmentTransactionsTotalHKDPass
				+ ", isFeaturedFundsServicesInventmentTransactionsTotalDollarPass="
				+ isFeaturedFundsServicesInventmentTransactionsTotalDollarPass
				+ ", isFeaturedFundsServicesInventmentTransactionsDollarEqualHKDPass="
				+ isFeaturedFundsServicesInventmentTransactionsDollarEqualHKDPass + ", checkSettlementDateList="
				+ checkSettlementDateList + ", checkTransactionTypeList=" + checkTransactionTypeList
				+ ", checkTransactionIdList=" + checkTransactionIdList + ", investmentTransactionsMap="
				+ investmentTransactionsMap + ", AccountDetailsFailList=" + AccountDetailsFailList
				+ ", IndividualFundPositionsFailList=" + IndividualFundPositionsFailList
				+ ", TransactionHistoryAndPositionsFailList=" + TransactionHistoryAndPositionsFailList
				+ ", InvestmentTransactionsFailList=" + InvestmentTransactionsFailList + ", InvestmentNameList="
				+ InvestmentNameList ;
	}
	public String getHKDFeaturedFundsStartValue() {
		return HKDFeaturedFundsStartValue;
	}
	public void setHKDFeaturedFundsStartValue(String hKDFeaturedFundsStartValue) {
		HKDFeaturedFundsStartValue = hKDFeaturedFundsStartValue;
	}
	public String getHKDFeaturedFundsEndValue() {
		return HKDFeaturedFundsEndValue;
	}
	public void setHKDFeaturedFundsEndValue(String hKDFeaturedFundsEndValue) {
		HKDFeaturedFundsEndValue = hKDFeaturedFundsEndValue;
	}
	public boolean isHKDFeaturedFundsTotalHKDPass() {
		return isHKDFeaturedFundsTotalHKDPass;
	}
	public void setHKDFeaturedFundsTotalHKDPass(boolean isHKDFeaturedFundsTotalHKDPass) {
		this.isHKDFeaturedFundsTotalHKDPass = isHKDFeaturedFundsTotalHKDPass;
	}
	public boolean isHaveSelfFund() {
		return isHaveSelfFund;
	}
	public void setHaveSelfFund(boolean isHaveSelfFund) {
		this.isHaveSelfFund = isHaveSelfFund;
	}
	public boolean isAccountTotalFormat() {
		return isAccountTotalFormat;
	}
	public void setAccountTotalFormat(boolean isAccountTotalFormat) {
		this.isAccountTotalFormat = isAccountTotalFormat;
	}
	
	
}
