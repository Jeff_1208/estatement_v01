package com.main;

import com.utils.Main_utils;

public class ExcuteThread extends Thread{

	private String Folder_path;
	private String unusual_FC_message;

	public ExcuteThread(String Folder_path) {
		this.Folder_path=Folder_path;
	}
	@Override
	public void run() {
		Main_utils main_utils = new Main_utils();
		try {
			main_utils.run_all_estatement(Folder_path);
			String result_file_path = main_utils.get_result_file_path();
			System.out.println("");
			unusual_FC_message="Please find the eStatements  test Result from:\n"+result_file_path;
		} catch (Exception e2) {
			unusual_FC_message="Something is Wrong, the checking is not Finished:\n"+" "+e2;
		}
	}

	public String getResult() {
		return unusual_FC_message;
	}
	
}
