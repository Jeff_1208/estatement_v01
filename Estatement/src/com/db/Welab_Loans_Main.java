package com.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DBUtils;


public class Welab_Loans_Main {
	
	public static void main(String[] args) throws SQLException {
//		runSQLServer();
//		runPaymentPostgreSQL();
//		runPostgreSQL();
		getOtp();
	}
	public static void getOtp() throws SQLException {
		DBUtils dbUtils = new DBUtils();
		Connection c = dbUtils.GetOtpConnect();
		Statement stmt = c.createStatement();
		String sql="select send_to, code from otp order by update_time desc limit 10";
		ResultSet resultSet=stmt.executeQuery(sql);
		int count =0;
		while(resultSet.next())
		{
			String value  = resultSet.getString(1).toString();
			System.out.println(value);
			count++;
		}
		System.out.println(count);
		System.out.println("check finish");
		dbUtils.Close_Connection(c, stmt);
	}
	
	public static void runPaymentPostgreSQL() throws SQLException {
		DBUtils dbUtils = new DBUtils();
		Connection c = dbUtils.GetPaymentConnect();
		Statement stmt = c.createStatement();
		String sql="select id,public_token from card_info where bank_account_number = '1000432009' order by id desc";
		ResultSet resultSet=stmt.executeQuery(sql);
		int count =0;
		while(resultSet.next())
		{
			String value  = resultSet.getString(1).toString();
			System.out.println(value);
			count++;
		}
		System.out.println(count);
		System.out.println("check finish");
		dbUtils.Close_Connection(c, stmt);
	}
	public static void runPostgreSQL() throws SQLException {
		DBUtils dbUtils = new DBUtils();
		Connection c = dbUtils.GetpostgreConnect();
		Statement stmt = c.createStatement();
		String sql="select customer_id from public.customer_info where customer_id='8000000022'";
		ResultSet resultSet=stmt.executeQuery(sql);
		int count =0;
		while(resultSet.next())
		{
			String value  = resultSet.getString(1).toString();
			System.out.println(value);
			count++;
		}
		System.out.println(count);
		System.out.println("check finish");
		dbUtils.Close_Connection(c, stmt);
	}
	
	public  static void runSQLServer() throws SQLException {
		DBUtils dbUtils = new DBUtils();
		Connection c = dbUtils.GetConnect();
		Statement stmt = c.createStatement();
		String sql="select * from BS.AA_ACCOUNT_DETAILS  where LEAD_CO_MNE='BNK'";
		ResultSet resultSet=stmt.executeQuery(sql);
		int count =0;
		while(resultSet.next())
		{
			String value  = resultSet.getString(2).toString();
			System.out.println(value);
			count++;
		}
		System.out.println(count);
		System.out.println("check finish");
		dbUtils.Close_Connection(c, stmt);
	}

}
