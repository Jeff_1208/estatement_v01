package com.db;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;


public class DBUtils {
	//sqlserver: 8.135.86.106 1433  sa / vb2020.mssql
	//InsightImport  insightlanding  insightWarehouse  VbSupplmentDB
	public static final String user="sa";
	public static final String password="vb2020.mssql";
	public static final String sqlServerDriverName="com.microsoft.sqlserver.jdbc.SQLServerDriver";
	public static final String URL="jdbc:sqlserver://8.135.86.106:1433;DatabaseName=insightlanding";

	
	//postgre 8.135.86.106 5432  postgres/vb@pg.2020
	//airflow alfresco documents-srv e-statement-srv postgres  sms-srv  staging   teans-data-srv
	public static final String postgreurl = "jdbc:postgresql://8.135.86.106:5432/e-statement-srv";
	public static final String postgreDriverName="org.postgresql.Driver";
	public static final String postgreUser="postgres";
	public static final String postgrePassword="vb@pg.2020";

	
	public Connection GetConnect(){
		Connection conn = null;
		try {
			Class.forName(sqlServerDriverName);
			conn=DriverManager.getConnection(URL,user,password);
			if (conn!=null) {
				System.out.println("connect the sqlServer DB pass");
			}
			//stmt = BLDdbConn.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("connect the sqlServer DB fail");
		}
		return conn;
	}
	
	//public static final String Paymentpostgreurl = "jdbc:postgresql://sta-payment-db-01.c3cnebukromj.ap-east-1.rds.amazonaws.com:5432/card-sys";
	public static final String otpUrl = "jdbc:postgresql://sta-system-db-01.c3cnebukromj.ap-east-1.rds.amazonaws.com:5432/otp-srv";
	public static final String otpUser="readonly";
	public static final String otpPassword="secret!0708";
	public Connection GetOtpConnect(){
		Connection conn = null;
		try {
			Class.forName(postgreDriverName);
			conn=DriverManager.getConnection(otpUrl,otpUser,otpPassword);
			if (conn!=null) {
				System.out.println("connect the Otp DB pass");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("connect the Otp postgre url DB fail");
		}
		return conn;
	}
	

	//public static final String Paymentpostgreurl = "jdbc:postgresql://sta-payment-db-01.c3cnebukromj.ap-east-1.rds.amazonaws.com:5432/card-sys";
	public static final String Paymentpostgreurl = "jdbc:postgresql://sta-payment-db-01:5432/card-sys";
	public static final String PaymentpostgreUser="postgres";
	public static final String PaymentpostgrePassword="vb@pg.2020";
	public Connection GetPaymentConnect(){
		Connection conn = null;
		try {
			Class.forName(postgreDriverName);
			conn=DriverManager.getConnection(Paymentpostgreurl,PaymentpostgreUser,PaymentpostgrePassword);
			if (conn!=null) {
				System.out.println("connect the Payment DB pass");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("connect the Payment postgre url DB fail");
		}
		return conn;
	}
	
		
	public Connection GetpostgreConnect(){
		Connection conn = null;
		try {
			Class.forName(postgreDriverName);
			conn=DriverManager.getConnection(postgreurl,postgreUser,postgrePassword);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("connect the postgre DB fail");
		}
		return conn;
	}
	public void Close_Connection(Connection conn,Statement stmt) {
		try {
			if(stmt !=null) {
				stmt.close();
			}
			if(conn !=null) {
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
