package study;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class readCase {


	public static void main(String[] args) throws Exception {
		long start = System.currentTimeMillis();
		
		readFeatureFiles();
		
		System.out.println("coast time: "+( System.currentTimeMillis()-start));
	}


	public static void readFeatureFiles() throws Exception {
		//String path="D:\\Project\\Program\\IDEAWorkspace\\auto\\automation-test\\src\\main\\java\\com\\welab\\automation\\projects\\channel\\features\\ios";
		String path="D:\\Project\\Program\\IDEAWorkspace\\auto\\automation-test\\src\\main\\java\\com\\welab\\automation\\projects\\wealth\\features\\mobile\\android";
		//String path="D:\\Project\\Program\\IDEAWorkspace\\auto\\automation-test\\src\\main\\java\\com\\welab\\automation\\projects\\wealth\\features\\mobile\\ios";
		List<String> caseNames =new ArrayList<String>();
		File f = new File(path);
		File[] files = f.listFiles();

        for (int i = 0; i < files.length; i++) {
            System.out.println(files[i].getName());
            String absPath = files[i].getAbsolutePath();
            List<String> lines = Files.readAllLines(Paths.get(absPath));

            for (int j = 0; j < lines.size(); j++) {
                String line = lines.get(j);
                if(line.contains("Scenario")) {
                    if(line.trim().startsWith("#")) continue;
                    if(line.trim().contains("set account")) continue;
                    System.out.println(line);
                    String caseName = line.substring(line.indexOf(":")+1).trim();
                    caseName = caseName.split(" ")[0].trim();
                    caseNames.add(caseName);
                }
            }
        }
        for (int i = 0; i < caseNames.size(); i++) {
            //System.out.println(caseNames.get(i));
        }
        System.out.println("caseNames.size(): "+caseNames.size());
	}
}
