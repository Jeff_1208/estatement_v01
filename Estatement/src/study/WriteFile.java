package study;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class WriteFile {

	public static void main(String[] args) {
		try {
			//写入文件
			FileWriter file = new FileWriter("d:/c.txt",true);//true表示追加数据
			String content ="hello world \r\nthis is my name";
			file.write(content);
			file.close();
			
			
			//java读取文件
			File f = new File("d:/c.txt");
			FileReader bytes  = new FileReader(f);
			BufferedReader chars = new BufferedReader(bytes); //字节类型转换成字符形式
			String row ="";
			while((row=chars.readLine())!=null) {
				System.out.println(row);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
