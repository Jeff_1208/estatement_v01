package study;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;

public class CompareTwoFile {
	 private static final Logger log = LoggerFactory.getLogger(CompareTwoFile.class);
	 
	 
	 public static void main(String[] args) {
		String path1="D:\\Project\\Automation\\ZyphrcScale\\faillogs2.txt";
		String path2="D:\\Project\\Automation\\ZyphrcScale\\faillogs3.txt";
		try {
			compareTwoFile(path1, path2);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void compareTwoFile(String oldFile, String newFile) throws IOException {
	    List<String> list1 =  Files.readAllLines(Paths.get(oldFile));
	    List<String> list2 =  Files.readAllLines(Paths.get(newFile));
	    
	    //log.trace("比较{}和{}两个文件，以 {} 为主", oldFile, newFile, newFile);
	    List<String> finalList = list2.stream().filter(line -> 
	    	list1.stream().filter(line2 -> line2.equals(line)).count() == 0
	    ).collect(Collectors.toList());
	    if (finalList.size() == 0) {
	    	System.out.println();
	        log.trace("两个文件无差别?");
	    }else{
	        log.trace("以下为差异的地方?");
	        log.error("差异的地?");
	        finalList.forEach(one -> System.out.println(one));
	    }
	}
}
